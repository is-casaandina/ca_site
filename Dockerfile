### STAGE 1: Build ###

# We label our stage as 'builder'
FROM node:10-alpine as builder

ARG env

COPY package.json package-lock.json ./
COPY /external_library ./external_library

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm ci && mkdir /ng-app && mv ./node_modules ./ng-app

## Choosing a WORKDIR
WORKDIR /ng-app

## Copy everything from host to /ng-app in the container
COPY . .

## Build the angular app in production mode and store the artifacts in dist folder
RUN npm run generate:env --env=$env

RUN npm run copy:bundles --upload --env=$env

RUN node --max_old_space_size=5048 &&  \
    npm run build:caadmin:$env

RUN node --max_old_space_size=5048 &&  \
    npm run build:casite:$env

RUN node --max_old_space_size=5048 && \
    npm run build:server:$env

RUN node --max_old_space_size=5048 && \
    npm run compile:server

RUN rm -R modules
RUN rm -R apps
RUN rm -R core
RUN rm -R patternlab
RUN rm -R patternlab-site
RUN rm -R styles-scss
