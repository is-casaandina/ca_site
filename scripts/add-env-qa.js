const angular = require('../angular.json');
const modules_key = Object.keys(angular.projects);
const fs = require('fs').promises;

modules_key
  .filter(key => !key.startsWith('ca'))
  .forEach(key => {
    const structure = angular.projects[key];
    const replaceFile = `modules/${key}/src/environments/environment.ts`;
    const withFile = `modules/${key}/src/environments/environment.qa.ts`;
    const configurations = structure.architect && structure.architect.build && structure.architect.build.configurations;

    if (configurations) {
      configurations.qa = configurations.qa || {};
      configurations.qa.fileReplacements = configurations.qa.fileReplacements || [];
      const exists = !!configurations.qa.fileReplacements.find(fileReplacement => fileReplacement.replace === replaceFile)
      if (!exists) {
        configurations.qa.fileReplacements.push({ replace: replaceFile, with: withFile });
      }
    }

  });

(async () => {
  await fs.writeFile('./angular.json', JSON.stringify(angular, null, 2), 'utf8');
})();

