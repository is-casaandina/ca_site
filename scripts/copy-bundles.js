const fs = require('fs-extra');
const { readFile, writeFileSync } = require('fs');
const handlebars = require('handlebars');
const upload = process.env.npm_config_upload || false;
const minify = require('@node-minify/core');
const uglifyjs = require('@node-minify/uglify-js');

const env = process.env.npm_config_env || 'dev';
if (!(env === 'dev' || env === 'qa' || env === 'prod')) {
  throw 'Variable de entorno inválida. Correctas => dev | qa | prod';
}

console.log('Copy UMD bundles ...');

const rootAssets = 'apps/casite/src';
const outputDir = 'assets/bundles';
const outputDirWithServer = env === 'dev' ? 'assets/bundles' : 'web-components/bundles';

const bundles = require('./config/bundles.json');
const config = require('./config/config.json');
const s3 = require('./s3');

bundles.forEach(bundle => {
  const output = `${rootAssets}/${outputDir}/${bundle.filename}`;

  if (!bundle.nominify) {
    doMinify(bundle.input, output)
      .then(() => console.log('\x1b[36m%s\x1b[34m%s\x1b[0m', 'COPY SUCCESS => ', output))
      .catch(err => console.error(err));
  } else {
    fs.copySync(bundle.input, output);
    console.log('\x1b[36m%s\x1b[34m%s\x1b[0m', 'COPY SUCCESS => ', output);
  }

  if (upload) {
    const key = `${config.s3.directoryBundles[env]}/${bundle.filename}`;
    s3.uploadFile(bundle.input, key, env).then(location =>
      console.log('\x1b[36m%s\x1b[34m%s\x1b[0m', 'UPLOAD SUCCESS => ', location)
    );
  }
});

async function doMinify(input, output) {
  return await minify({ compressor: uglifyjs, input, output });
}

readFile(config.templates.site.index.input, 'utf-8', function(error, source) {
  handlebars.registerHelper('toJSON', obj => {
    return JSON.stringify(obj, null, 2);
  });

  const template = handlebars.compile(source);
  const html = template({ outputDir: outputDirWithServer, bundles });
  writeFileSync(config.templates.site.index.output, html);
});

readFile(config.templates.admin.index.input, 'utf-8', function(error, source) {
  handlebars.registerHelper('toJSON', obj => {
    return JSON.stringify(obj, null, 2);
  });

  const template = handlebars.compile(source);
  const outputDirCms = `${config.s3.directory.public[env]}${config.s3.directoryBundles[env]}`;
  const html = template({ outputDirCms, bundles });
  writeFileSync(config.templates.admin.index.output, html);
});
