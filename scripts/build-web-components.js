const exec = require('child_process').exec;
const fs = require('fs');
const comp = process.env.npm_config_comp;
const indexInit = Number(process.env.npm_config_index || 0);

const configS3 = require('./config/config.json').s3;
const s3 = require('./s3');

const env = process.env.npm_config_env || 'dev';
if (!(env === 'dev' || env === 'qa' || env === 'prod')) {
  throw 'Variable de entorno inválida. Correctas => dev | qa | prod';
}

const directory = configS3.directoryWebcomponents[env];

const commandLine = (compName) => {
  const ngBuild = (env === 'prod')
                    ? `ng build --project ${compName} --prod`:
                  (env === 'dev')
                    ? `ng build --project ${compName}`
                    : `ng build --project ${compName} -c ${env}`

  const ngOptimization = `--optimization=true --sourceMap=false --extractCss=true --namedChunks=false --aot=true --extractLicenses=true --vendorChunk=false --buildOptimizer=true`;
  const ngExtra = ` --extraWebpackConfig ../../webpack.externals.js --output-hashing=none --single-bundle=true`;

  return `${ngBuild} ${ngOptimization} ${ngExtra}`;
}

const execute = (commandLine, compName, compNumber) => {
  const origin = `./dist/${compName}/main.js`;
  const key = `${directory}/${compName}.js`;

  console.log(`-------------------- ${compNumber} - ${compName} --------------------`);
  console.log(`\x1b[36m%s\x1b[47m\x1b[0m`, `BUILDING...`);
  return new Promise((resolve, reject) => {
    exec(commandLine, { maxBuffer: 1024 * 500 }, (error, stdout) => {
      if (error) {
        reject(error);
        return;
      }
      resolve(stdout.trim());
    });
  })
    .then(() => console.log('\x1b[32m%s\x1b[47m\x1b[0m', `SUCCESSFUL BUILD  `))
    .then(() => console.log(`\x1b[36m%s\x1b[47m\x1b[0m`, `UPLOADING... `))
    .then(() => s3.uploadFile(origin, key, env))
    .then(location => console.log('\x1b[32m%s\x1b[47m\x1b[0m', `SUCCESSFUL UPLOAD => `, location, `\n`))
    .catch(error => console.log('\x1b[31m%s\x1b[0m', `${error} \n`));
};

const buildWC = async (comp) => {
  if (comp === undefined || comp === 'all') {
    function getDirectories(path) {
      return fs.readdirSync(path).filter(function (file) {
        return fs.statSync(path + '/' + file).isDirectory();
      });
    }

    const components = getDirectories('./modules')

    for (let index = indexInit; index < components.length; index++) {
      await execute(commandLine(components[index]), components[index], `${index + 1}/${components.length}`);
    }
  } else {
    await execute(commandLine(comp), comp, '1/1')
  }
}

buildWC(comp);
