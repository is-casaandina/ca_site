// requiring path and fs modules
const path = require('path');
const fs = require('fs');
const start = Number(process.env.npm_config_start || 0);
const end = Number(process.env.npm_config_end || 0);

// joining path of directory
const directoryPath = path.join(__dirname, '../tmp/images');
const s3 = require('./s3');

const env = process.env.npm_config_env || 'prod';
if (!(env === 'dev' || env === 'qa' || env === 'prod')) {
  throw 'Variable de entorno inválida. Correctas => dev | qa | prod';
}

const files = fs.readdirSync(directoryPath);

const upload = async () => {
  const max = end || files.length;
  console.log(max, start, env);
  for (let index = start; index < max; index++) {
    const file = files[index];
    const origin = path.join(__dirname, '../tmp/images', file);
    const key = `images/${file}`;

    console.log(`-------------------- ${index}/${max} --------------------`);
    console.log(`FILE => ${file}`);
    console.log(`\x1b[36m%s\x1b[47m\x1b[0m`, `UPLOADING... `);

    const location = await s3.uploadFile(origin, key, env);
    console.log('\x1b[32m%s\x1b[47m\x1b[0m', `SUCCESSFUL UPLOAD => `, location, `\n`);
  }
};

upload();
