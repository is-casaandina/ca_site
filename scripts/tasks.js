const tslintHtmlReport = require('tslint-html-report');
const configSite = {
  tslint: 'apps/casite/tslint.json', // path to tslint.json
  srcFiles: "apps/casite/src/**/*.ts",
  outDir: 'reports/casite', // output folder to write the report to
  html: 'tslint-report.html', // name of the html report generated
  breakOnError: false, // Should it throw an error in tslint errors are found
  typeCheck: true, // enable type checking. requires tsconfig.json
  tsconfig: 'apps/casite/tsconfig.app.json' // path to tsconfig.json
}

const configAdmin = {
  tslint: 'apps/caadmin/tslint.json', // path to tslint.json
  srcFiles: "apps/caadmin/src/**/*.ts",
  outDir: 'reports/caadmin', // output folder to write the report to
  html: 'tslint-report.html', // name of the html report generated
  breakOnError: false, // Should it throw an error in tslint errors are found
  typeCheck: true, // enable type checking. requires tsconfig.json
  tsconfig: 'apps/caadmin/tsconfig.app.json' // path to tsconfig.json
}

tslintHtmlReport(configSite);
tslintHtmlReport(configAdmin);

