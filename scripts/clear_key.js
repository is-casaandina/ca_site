const config = require('./config/config.json');
const fs = require('fs-extra');

config.s3.accessKeyId = '';
config.s3.secretAccessKey = '';
config.s3.region = '';

(async () => {
  await fs.writeFile('./scripts/config/config.json', JSON.stringify(config, null, 2), 'utf8');
})();
