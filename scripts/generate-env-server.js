const fs = require('fs');

const env = process.env.npm_config_env || 'dev';
if (!(env === 'dev' || env === 'qa' || env === 'prod')) {
  throw 'Variable de entorno inválida. Correctas => dev | qa | prod';
}

fs.writeFile(".env", `ENV=${env}`, function(err) {
  if(err) {
      return console.log(err);
  }

  console.log("The file was saved!");
});
