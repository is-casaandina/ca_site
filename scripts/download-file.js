const https = require('https');

module.exports = {

  /*
  ** This method downloads the file
  ** from the URL specified in the
  ** parameters
  */
  download_file: function (url) {
    return new Promise((resolve, reject) => {
      var data = [];

      https.get(url, function (response) {
        response.on('data', function (chunk) {
          data.push(chunk);
        })
        response.on('end', function () {
          data = Buffer.concat(data);
          resolve(data)
        })
      })
    })
  }
}
