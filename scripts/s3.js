var AWS = require('aws-sdk');
const fs = require('fs');

const config = require('./config/config.json').s3;
const s3 = new AWS.S3({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey
});

const uploadFile = (origin, Key, env) => {

  return new Promise((resolve, reject) => {
    fs.readFile(origin, (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      const params = {
        Bucket: config.bucketName[env], // pass your bucket name
        Key: Key, // file will be saved as testBucket/contacts.csv
        ACL: 'public-read',
        Body: data
      };

      s3.upload(params, function (s3Err, data) {
        if (s3Err) {
          reject(s3Err);
          return;
        }
        resolve(data.Location);
      });
    });
  });
};

module.exports = {
  uploadFile: uploadFile
}
