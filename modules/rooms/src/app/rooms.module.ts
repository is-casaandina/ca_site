import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { GalleryModalModule, NavbarModule, SectionHeaderModule } from '@ca/library';
import { PhotoSliderModule } from '../components/photo-slider/photo-slider.module';
import { ServiceModule } from '../components/service/service.module';
import { RoomsServices } from '../providers/rooms.service';
import { RoomsComponent } from './rooms.component';

@NgModule({
  declarations: [
    RoomsComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    NavbarModule,
    BrowserAnimationsModule,
    PipesModule,
    ServiceModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    ModalModule,
    GalleryModalModule,
    PhotoSliderModule
  ],
  providers: [
    RoomsServices
  ],
  entryComponents: [
    RoomsComponent
  ]
})
export class RoomsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-rooms'; // Nombre del selector que leera la aplicacion principal
      const element = createCustomElement(RoomsComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
