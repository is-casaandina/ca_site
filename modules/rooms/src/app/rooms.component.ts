import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { NAV_HEADER } from '@ca-core/shared/settings/constants/general.constant';
import { IBenefit, ITab } from '@ca/library';
import { IDescriptor, IDescriptorResponse, IService, IServiceRequest } from 'core/typings';
import { RoomsServices } from '../providers/rooms.service';

@Component({
  selector: 'ca-root',
  templateUrl: './rooms.component.html',
  animations: [
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0.5
        })
      ),
      transition('* => void', animate('0s ease-out')),
      transition('void => *', animate('.2s ease-in'))
    ])
  ]
})
export class RoomsComponent extends ComponentBase implements OnInit {
  @Input() title: string;

  private _rooms: Array<any>;
  @Input()
  set rooms(rooms: Array<any>) {
    this._rooms = rooms;
  }
  get rooms(): Array<any> {
    return (this._rooms && typeof this._rooms === 'string' && JSON.parse(this._rooms as any)) || (this._rooms || []);
  }

  tabs: Array<ITab>;
  activeTab = 0;
  descriptorInfo: IDescriptor;
  features: Array<IBenefit>;
  NAV_HEADER = NAV_HEADER;
  roomActive: any;
  activeSlideIndex = 0;
  slideHeight: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private roomsServices: RoomsServices,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.rooms = this.convertPropsToCamelCase();
      this.tabs = this.getTabs();
      this.getAllServices();
      this.getDescriptor();
    }
  }

  getAllServices(): void {
    const items: Array<string> = this.rooms.reduce((oldVal, newVal) => {
      const itemsPart = newVal.services.map((item: any) => item.service);

      return oldVal.concat(itemsPart);
    }, []);

    this.getServices(items);
  }

  private getServices(items: Array<string>): void {
    const params: IServiceRequest = {
      language: this.pageInfo.language,
      items
    };

    this.roomsServices.getServices(params).subscribe((services: Array<IService>) => {
      this.features = services.map(service => {
        return {
          icon: service.icon,
          title: service.text,
          value: service.value
        };
      });
      this.rooms = this.updateRooms();
    });
  }

  private updateRooms(): any {
    return this.rooms.map(room => {
      const features = room.services.map((item: any) => {
        const feat = this.features.find(feature => feature.value === item.service);

        return feat;
      });

      return {
        ...room,
        features
      };
    });
  }

  private getTabs(): Array<ITab> {
    return this.rooms.map(room => {
      return {
        tab: room.title
      };
    });
  }

  private convertPropsToCamelCase(): Array<any> {
    return this.rooms.map((room: any) => (camelcaseKeys(room)));
  }

  onChangeTab(activeTab: number): void {
    this.ngZone.run(() => {
      this.roomActive = {};
      this.activeTab = activeTab;
      this.roomActive = this.rooms[this.activeTab];
      this.activeSlideIndex = 0;
    });
  }

  getDescriptor(): void {
    this.roomsServices.getDescriptor().subscribe((response: Array<IDescriptorResponse>) => {
      this.ngZone.run(() => {
        this.descriptorInfo = response.find(descriptor => descriptor.id === this.pageInfo.descriptorId);
      });
    });
  }
}
