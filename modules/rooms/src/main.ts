import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { RoomsModule } from './app/rooms.module';

platformBrowserDynamic()
  .bootstrapModule(RoomsModule)
  .catch(err => console.error(err));
