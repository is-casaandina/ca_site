import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse, IService, IServiceRequest } from 'core/typings';
import { Observable } from 'rxjs';
import { RoomEndPoint } from './endpoints';

@Injectable()
export class RoomsServices {

  constructor(
    private apiService: ApiService
  ) { }

  getServices(queryParams: IServiceRequest): Observable<Array<IService>> {
    return this.apiService.get(RoomEndPoint.services, { preloader: false, params: queryParams});
  }

  getDescriptor(): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(RoomEndPoint.descriptors, { preloader: false });
  }
}
