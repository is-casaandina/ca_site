import { environment } from '../environments/environment';

export class RoomEndPoint {
  public static services = `${environment.API_URL}site/services`;
  public static descriptors = `${environment.API_URL}api/descriptors`;
}
