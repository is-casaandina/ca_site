import { Component, Input } from '@angular/core';
import { FEATURE_TYPES } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor } from 'core/typings';
import { IService } from './service';

@Component({
  selector: 'ca-service',
  templateUrl: './service.component.html'
})
export class ServiceComponent {
  @Input() data: IService;
  @Input() descriptorInfo: IDescriptor;
  FEATURE_TYPES = FEATURE_TYPES;
}
