export interface IService {
  icon?: string;
  title?: string;
  subtitle?: string;
  value?: string;
}
