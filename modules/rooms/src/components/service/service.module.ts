import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceComponent } from './service.component';


@NgModule({
    imports: [ CommonModule, FormsModule ],
    declarations: [ ServiceComponent ],
    exports: [ ServiceComponent ]
})
export class ServiceModule {

}
