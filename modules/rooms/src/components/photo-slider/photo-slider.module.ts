import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { GalleryModalModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { PhotoSliderComponent } from './photo-slider.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ElemBackgroundurlModule,
      GalleryModalModule,
      ModalModule,
      SpinnerModule,
      NotificationModule,
      HttpClientModule
    ],
    declarations: [ PhotoSliderComponent ],
    exports: [ PhotoSliderComponent ]
})
export class PhotoSliderModule {

}
