import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterContentInit, Component, Input } from '@angular/core';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { FORM_TYPE, IMAGE_SIZE } from '@ca-core/shared/settings/constants/general.constant';
import { GalleryModalComponent } from '@ca/library';
import { interval, Observable } from 'rxjs';

@Component({
  selector: 'ca-photo-slider',
  templateUrl: './photo-slider.component.html',
  animations: [
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0.5
        })
      ),
      transition('* => void', animate('0s ease-out')),
      transition('void => *', animate('1.5s ease-in'))
    ])
  ]
})
export class PhotoSliderComponent implements AfterContentInit {
  @Input() roomActive: any;
  private _activeSlideRoom: number;
  @Input()
  set activeSlideRoom(activeSlideRoom: number) {
    this.swapActiveSlide(0);
    this._activeSlideRoom = activeSlideRoom;
  }
  get activeSlideRoom(): number {
    return this._activeSlideRoom;
  }
  timer: Observable<any>;
  activeSlideIndex = 0;

  constructor(private modalService: ModalService) {}

  swapActiveSlide(index): void {
    this.activeSlideIndex = index;
  }

  openGallery(): void {
    const modalConfig = {
      classDialog: 'carousel',
      size: IMAGE_SIZE.md
    };

    const slides = this.roomActive.photos.map(photo => {
      return {
        type: FORM_TYPE.image,
        backgroundImage: photo.basicPhoto,
        zoomImage: photo.zoomPhoto && photo.zoomPhoto.length ? photo.zoomPhoto : null
      };
    });
    const modalRef = this.modalService.open(GalleryModalComponent, modalConfig);

    modalRef.setPayload({
      type: 'gallery',
      slides,
      activeSlide: this.activeSlideIndex
    });
    modalRef.result.then(() => {}).catch(err => {});
  }

  ngAfterContentInit(): void {
    this.timer = interval(4000);
    this.startAnimation();
  }

  startAnimation(): void {
    this.timer.subscribe(() => {
      let newIndex = 0;
      if (this.activeSlideIndex + 1 < this.roomActive.photos.length) {
        newIndex = this.activeSlideIndex + 1;
      }

      this.swapActiveSlide(newIndex);
    });
  }
}
