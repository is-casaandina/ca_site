import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BadgeDescriptionModule } from './app/badge-description.module';

platformBrowserDynamic()
  .bootstrapModule(BadgeDescriptionModule)
  .catch(err => console.error(err));
