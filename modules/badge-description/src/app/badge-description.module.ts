import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { BadgeDescriptionService } from '../providers/badge-description.service';
import { BadgeDescriptionComponent } from './badge-description.component';

@NgModule({
  declarations: [
    BadgeDescriptionComponent
  ],
  imports: [
    BrowserModule,
    PipesModule,
    SpinnerModule,
    HttpClientModule,
    NotificationModule
  ],
  providers: [
    BadgeDescriptionService
  ],
  entryComponents: [
    BadgeDescriptionComponent
  ]
})

export class BadgeDescriptionModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
    const name = 'ca-badge-description';

    const element = createCustomElement(BadgeDescriptionComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
