import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IDescriptor } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { BadgeDescriptionService } from '../providers/badge-description.service';

@Component({
  selector: 'ca-badge-description',
  templateUrl: './badge-description.component.html'
})
export class BadgeDescriptionComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() descriptor: string;
  @Input() description: string;

  descriptorInfo: IDescriptor;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private badgeDescriptionService: BadgeDescriptionService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getDescriptors();
    }
  }

  getDescriptors(): void {
    this.badgeDescriptionService
      .getDescriptors(this.descriptor)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IDescriptor) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response;
        });
      });
  }
}
