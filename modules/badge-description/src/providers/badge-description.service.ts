import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { BadgeDescriptionEndpoint } from './endpoints';

@Injectable()
export class BadgeDescriptionService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptors(descriptorId: string): Observable<IDescriptorResponse> {
    return this.apiService.get(BadgeDescriptionEndpoint.descriptors, { preloader: false, params: {descriptorId} });
  }

}
