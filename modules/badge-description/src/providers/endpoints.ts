import { environment } from '../environments/environment';

export class BadgeDescriptionEndpoint {
  public static descriptors = `${environment.API_URL}site/descriptors/{descriptorId}`;
}
