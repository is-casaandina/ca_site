import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderBannerModule } from './app/slider-banner.module';

platformBrowserDynamic()
  .bootstrapModule(SliderBannerModule)
  .catch(err => console.error(err));
