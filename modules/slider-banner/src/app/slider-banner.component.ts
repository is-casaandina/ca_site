import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, NgZone, OnInit } from '@angular/core';
import { AngularUtil, StorageService, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { hexToRgb } from '@ca-core/shared/helpers/util/color';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { IMAGE_SIZE, STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { BANNER_COLOR, DESCRIPTOR_TYPE_ID, FORM_TYPE } from 'core/shared/src/settings/constants/general.constant';
import { IBanner, IDescriptorResponse, ILogoInfo, IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { SliderBannerService } from '../providers/slider-banner.service';

@Component({
  selector: 'ca-slider-banner-standalone',
  templateUrl: './slider-banner.component.html',
  animations: [
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0.5
        })
      ),
      transition('* => void', animate('0s ease-out')),
      transition('void => *', animate('1s ease-in'))
    ])
  ]
})
export class SliderBannerComponent extends UnsubscribeOnDestroy implements OnInit {
  @Input() sliders: any;
  @Input() descriptor: string;
  @Input() bannerSize: string;

  private _isAnimation: boolean;
  @Input()
  set isAnimation(animation: boolean) {
    this._isAnimation = coerceBooleanProp(animation);
  }
  get isAnimation(): boolean {
    return coerceBooleanProp(this._isAnimation);
  }

  activeDescriptor = 0;
  slidersNew: Array<IBanner> = [];
  hexToRgb = hexToRgb;
  isCollapsed = true;
  pageInfo: IPageInfo;
  DESCRIPTOR_TYPE_ID = DESCRIPTOR_TYPE_ID;
  FORM_TYPE = FORM_TYPE;
  BANNER_COLOR = BANNER_COLOR;
  IMAGE_SIZE = IMAGE_SIZE;
  hoverButtonState = false;
  configurations: any;
  logoInfo: ILogoInfo;

  constructor(
    private sliderBannerServices: SliderBannerService,
    private storageService: StorageService,
    private ngZone: NgZone
  ) {
    super();
  }

  ngOnInit(): void {
    this.sliders = AngularUtil.toJson(this.sliders, []);
    this.bannerSize = this.bannerSize ? this.bannerSize.toLocaleLowerCase() : IMAGE_SIZE.md;
    this.slidersNew = this.sliders.map(slide => {
      const item = camelcaseKeys(slide);
      if (item.typeDescriptor === DESCRIPTOR_TYPE_ID.go) {
        delete item.descriptor;
      }

      return item;
    });
    this.pageInfo = AngularUtil.toJson(this.storageService.getItem(STORAGE_KEY.pageInfo));
    this.configurations = this.storageService.getItemObject(STORAGE_KEY.configurations);
    if (this.configurations) {
      this.logoInfo = { url: this.configurations.logo, alt: this.configurations.company };
    }
    const descriptorIds: Array<any> = this.slidersNew.map((slide: any) => {
      return slide.descriptor;
    });

    this.getDescriptors();
    this.getPagesByDescriptors(descriptorIds, this.pageInfo.language);
  }

  getDescriptors(): void {
    this.sliderBannerServices
      .getDescriptors()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.updateBanners(response);
        });
      });
  }

  private addPagesToBanner(pagesDescriptor): any {
    return this.slidersNew.map(slide => {
      const pagesDesc = pagesDescriptor.find((page: any) => page.descriptorId === slide.descriptor);
      const pages = pagesDesc ? pagesDesc.pages : [];

      return {
        ...slide,
        pages
      };
    });
  }

  private getPagesByDescriptors(ids: Array<string>, lang: string): void {
    this.sliderBannerServices
      .getPagesByDescriptors(ids, lang)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.slidersNew = this.addPagesToBanner(response);
        });
      });
  }

  private updateBanners(descriptors: Array<IDescriptorResponse>): void {
    this.slidersNew = this.slidersNew.map((slider: IBanner) => {
      const descriptorInfo = descriptors.find((descriptor: IDescriptorResponse) => descriptor.id === slider.descriptor);
      const isGoto = slider.typeDescriptor === DESCRIPTOR_TYPE_ID.go;
      const colorTitle = slider.colorTitle === 'Dorado' ? '#d69c4f' : '';
      const colorRgba = descriptorInfo && !isGoto ? hexToRgb(descriptorInfo.color, '.85') : '';
      const colorRgbaLow = descriptorInfo && !isGoto ? hexToRgb(descriptorInfo.color, '.5') : '';
      const colorFrame = descriptorInfo && !isGoto ? descriptorInfo.color : BANNER_COLOR.deft;

      return {
        ...slider,
        colorTitle,
        colorRgba,
        colorFrame,
        colorRgbaLow,
        descriptorInfo
      };
    });
  }

  onChangeSlide(activeSlide): void {
    this.ngZone.run(() => {
      this.isCollapsed = true;
      this.activeDescriptor = activeSlide;
    });
  }

  isHovereDButton(stateNow: boolean): void {
    this.hoverButtonState = stateNow;
  }
}
