import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { BannerCaptionModule, LogoModule, SliderButtonModule, SliderModule } from '@ca/library';
import { ElemBackgroundurlModule, MymeTypeModule, UrlLinkModule } from '@ca/library/directives';
import { VideoAutoplayModule } from '@ca/library/directives/video-autoplay/video-autoplay.module';
import { SliderBannerService } from '../providers/slider-banner.service';
import { SliderBannerComponent } from './slider-banner.component';

@NgModule({
  declarations: [SliderBannerComponent],
  imports: [
    BannerCaptionModule,
    BrowserAnimationsModule,
    BrowserModule,
    CollapseModule,
    ElemBackgroundurlModule,
    HttpClientModule,
    LogoModule,
    MymeTypeModule,
    NotificationModule,
    SliderButtonModule,
    SliderModule,
    SpinnerModule,
    UrlLinkModule,
    VideoAutoplayModule
  ],
  providers: [SliderBannerService],
  exports: [SliderBannerComponent],
  entryComponents: [SliderBannerComponent]
})
export class SliderBannerModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-slider-banner';
    const element = createCustomElement(SliderBannerComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
