import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { SliderBannerEndpoint } from './endpoints';

@Injectable()
export class SliderBannerService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptors(showSpin = false): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(SliderBannerEndpoint.descriptors, { preloader: showSpin });
  }

  getPagesByDescriptors(descriptorIds: Array<string>, language: string): Observable<any> {
    return this.apiService.get(SliderBannerEndpoint.pagesDescriptor, { preloader: false, params: {descriptors: descriptorIds, language} });
  }

}
