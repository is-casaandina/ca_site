import { environment } from '../environments/environment';

export class SliderBannerEndpoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static pagesDescriptor = `${environment.API_URL}site/pages/descriptors/ids`;
}
