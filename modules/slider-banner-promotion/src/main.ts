import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { SliderBannerPromotionsModule } from './app/slider-banner-promotion.module';

platformBrowserDynamic()
  .bootstrapModule(SliderBannerPromotionsModule)
  .catch(err => console.error(err));
