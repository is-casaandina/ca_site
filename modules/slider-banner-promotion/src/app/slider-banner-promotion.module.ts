import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CommonModule } from '@angular/common';
import { createCustomElement } from '@angular/elements';
import { SliderBannerModule } from 'modules/slider-banner/src/app/slider-banner.module';
import { SliderBannerPromotionComponent } from './slider-banner-promotion.component';

@NgModule({
  declarations: [
    SliderBannerPromotionComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    SliderBannerModule
  ],
  entryComponents: [SliderBannerPromotionComponent]
})
export class SliderBannerPromotionsModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-slider-banner-promotion';
    const element = createCustomElement(SliderBannerPromotionComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
