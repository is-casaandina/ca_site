import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { IPageInfo } from 'core/typings';

@Component({
  selector: 'ca-slider-banner-promotion',
  template: `
  <ca-slider-banner-standalone
    [isAnimation]="isAnimation"
    [sliders]="currentSlide"
    [bannerSize]="bannerSize">
  </ca-slider-banner-standalone>
  `
})
export class SliderBannerPromotionComponent extends ComponentBase implements OnInit {
  @Input() bannerSize: string;
  @Input() isAnimation: boolean;
  @Input() slidersBefore: any;
  @Input() slidersDuring: any;
  @Input() slidersAfter: any;

  currentSlide: any;
  pageInfo: IPageInfo;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    protected ngZone: NgZone
  ) {
    super(
      elem,
      storageService
    );
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.setCurrent();
    }
  }

  private setCurrent(): void {
    if (!DateUtil.isExpired(DateUtil.setTimezoneDate(this.pageInfo.beginDate))) {
      this.currentSlide = this.slidersBefore;
    } else if (
      this.pageInfo.firstExpiration &&
      !DateUtil.isExpired(DateUtil.setTimezoneDate(this.pageInfo.firstExpiration))) {
      this.currentSlide = this.slidersDuring;
    } else if (
      this.pageInfo.secondExpiration &&
      !DateUtil.isExpired(DateUtil.setTimezoneDate(this.pageInfo.secondExpiration))) {
      this.currentSlide = this.slidersAfter;
    } else if (!this.pageInfo.firstExpiration) {
      this.currentSlide = this.slidersDuring;
    }

    if (!this.currentSlide) {
      this.currentSlide = this.slidersAfter || this.slidersDuring || this.slidersBefore;
    }
  }

}
