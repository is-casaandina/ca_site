import { environment } from '../environments/environment';

export class OurLivingRoomEndpoint {
  public static destinations = `${environment.API_URL}site/living-rooms/destinations`;
  public static hotels = `${environment.API_URL}site/living-rooms/hotels?destinations=`;
  public static range = `${environment.API_URL}site/living-rooms/range`;
  public static filters = `${environment.API_URL}site/living-rooms/filters`;
  public static armedTypes = `${environment.API_URL}site/parameters/armed-types`;
  public static foodOptions = `${environment.API_URL}site/parameters/food-options`;
  public static audiovisualEquipments = `${environment.API_URL}site/parameters/audiovisual-equipments`;
  public static saveQuote = `${environment.API_URL}api/quotes`;
}
