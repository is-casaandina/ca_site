import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IOutLivinRoomFilter, IOutLivinRoomList, IOutLivinRoomRange } from '../app/our-living-room';
import { OurLivingRoomEndpoint } from './endpoints';

@Injectable()
export class OurLivingRoomServices {
  private _destinations: string;
  private _hotels: Array<IOutLivinRoomList>;
  private _roomsRequest: IOutLivinRoomFilter;
  private _rooms: any;

  constructor(private apiService: ApiService) {}

  getDestinations(): IObservableArray<IOutLivinRoomList> {
    return this.apiService.get<IObservableArray<IOutLivinRoomList>>(
      OurLivingRoomEndpoint.destinations, { preloader: false });
  }
  getHotels(params: { destinations: string; }): IObservableArray<IOutLivinRoomList> {

    if (params.destinations === this._destinations) { return of(this._hotels); }

    return this.apiService.get<IObservableArray<IOutLivinRoomList>>(
      `${OurLivingRoomEndpoint.hotels}${params.destinations}`, { preloader: false })
      .pipe(map((preResponse: any) => {
        this._hotels = preResponse;
        this._destinations = params.destinations;

        return preResponse;
      }));
  }
  getRange(): Observable<IOutLivinRoomRange> {
    return this.apiService.get(
      OurLivingRoomEndpoint.range, { preloader: false });
  }
  getRooms(filters: IOutLivinRoomFilter):  Observable<any> {

    if (JSON.stringify(filters) === JSON.stringify(this._roomsRequest)) {
      this._rooms.byService = false;

      return of(this._rooms);
    }

    return this.apiService.get<Observable<any>>(
      OurLivingRoomEndpoint.filters, { preloader: false, params: filters })
      .pipe(map((preResponse: any) => {
        this._rooms = preResponse;
        this._roomsRequest = filters;
        preResponse.byService = true;

        return preResponse;
      }));
  }
  getArmedTypes(lang: string):  Observable<any> {
    return this.apiService.get(
      `${OurLivingRoomEndpoint.armedTypes}/${lang}`, { preloader: false });
  }
  getFoodOptions(lang: string):  Observable<any> {
    return this.apiService.get(
      `${OurLivingRoomEndpoint.foodOptions}/${lang}`, { preloader: false });
  }
  getAudiovisualEquipments(lang: string):  Observable<any> {
    return this.apiService.get(
      `${OurLivingRoomEndpoint.audiovisualEquipments}/${lang}`, { preloader: false });
  }
  saveQuote(request):  Observable<any> {
    return this.apiService.post(
      OurLivingRoomEndpoint.saveQuote, request, { preloader: false });
  }
}
