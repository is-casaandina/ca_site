import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { OurLivingRoomModule } from './app/our-living-room.module';

platformBrowserDynamic()
.bootstrapModule(OurLivingRoomModule)
  .catch(err => console.error(err));
