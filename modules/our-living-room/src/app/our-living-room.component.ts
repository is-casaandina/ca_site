import { Component, ElementRef, Input, NgZone, OnInit, ViewEncapsulation } from '@angular/core';
import { LOADING_TEXT } from '@ca-admin/settings/constants/general.constant';
import { GENERAL_TEXT } from '@ca-admin/settings/constants/general.lang';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { joinSelected } from '@ca-core/shared/helpers/util/join';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { FormQuotationComponent } from '@ca/library';
import { Options } from 'ng5-slider';
import { takeUntil } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { OurLivingRoomServices } from '../providers/our-living-room.services';
import { IOutLivinRoomFilter, IOutLivinRoomList } from './our-living-room';

@Component({
  selector: 'ca-our-living-room',
  templateUrl: './our-living-room.component.html',
  styleUrls: ['./our-living-room.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OurLivingRoomComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  destinations: Array<IOutLivinRoomList> = [];
  hotels: Array<IOutLivinRoomList> = [];
  minRange = 1;
  maxRange = 1;
  options: Options;
  totalElements: number;
  pageCurrent = 1;
  tablesData: any;
  formLabels = {};
  loading = false;

  GENERAL_TEXT = GENERAL_TEXT;
  LOADING_TEXT = LOADING_TEXT;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private ourLinvingServices: OurLivingRoomServices,
    private modalService: ModalService,
    private ngZone: NgZone,
    private newsLetterServices: NewsLetterServices
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getFormLabels();
      this.getDestinations();
      this.getRange();
      this.getHotelsByDestin({ destinations: '' });
    }
  }
  setLabel(name): string {
    return Object.keys(this.formLabels).length ? this.formLabels[name] : '';
  }
  selectedLength(type: string): void {
    const filter = this[type].filter(item => item.selected);

    return filter ? filter.length : 0;
  }
  getHotels(): void {
    setTimeout(() => {
      this.getHotelsByDestin({
        destinations: joinSelected(this.destinations)
      });
      this.pageCurrent = 1;
      this.getRooms();
    });
  }
  changePage(page): void {
    const body = {
      destination: joinSelected(this.destinations),
      capacity: `${this.minRange},${this.maxRange}`,
      hotel: joinSelected(this.hotels),
      page,
      size: 10
    };
    this.pageCurrent = page;
    this.getRooms(body);
  }
  getRooms(body?: IOutLivinRoomFilter): void {
    setTimeout(() => {
      if (!body) {
        body = {
          destination: joinSelected(this.destinations),
          capacity: `${this.minRange},${this.maxRange}`,
          hotel: joinSelected(this.hotels),
          page: 1,
          size: 10
        };
      }
      this.ourLinvingServices
        .getRooms(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => {
          this.ngZone.run(() => {
            if (response.byService) {
              this.loading = true;
              setTimeout(() => {
                this.loading = false;
                this.tablesData = response.content;
                this.totalElements = response.totalElements;
              }, 1000);
            }
          });
        });
    });
  }
  clearFilters(): void {
    const body = {
      destination: '',
      capacity: `${this.minRange},${this.maxRange}`,
      hotel: '',
      page: 1,
      size: 10
    };
    this.pageCurrent = 1;
    this.getDestinations();
    this.getHotelsByDestin({ destinations: '' });
    this.getRooms(body);
  }
  openForm(): void {
    const modalRef = this.modalService.open(FormQuotationComponent, {
      classDialog: 'site promo',
      size: 'lg'
    });
    const hotels: Array<any> = JSON.parse(JSON.stringify(this.hotels));
    const filterHotels: Array<any> = JSON.parse(JSON.stringify(hotels.filter(hotel => hotel.selected)));

    filterHotels.map(hotel => {
      hotel.selected = false;

      return hotel;
    });

    modalRef.setPayload({
      minRange: this.minRange,
      maxRange: this.maxRange,
      hotels: (filterHotels.length && filterHotels) || hotels,
      options: this.options,
      recaptchaKey: environment.CAPTCHA_KEY
    });
    modalRef.result
      .then(response => {
        this.minRange = response.minRange;
        this.maxRange = response.maxRange;
      })
      .catch(err => {});
  }
  private getDestinations(): void {
    this.ourLinvingServices
      .getDestinations()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.destinations = response;
          this.destinations.forEach(item => (item.selected = false));
        });
      });
  }
  private getHotelsByDestin(destinations?: { destinations: string }): void {
    this.ourLinvingServices
      .getHotels(destinations)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.hotels = response;
          this.hotels.forEach(item => (item.selected = false));
        });
      });
  }
  private getRange(): void {
    this.ourLinvingServices
      .getRange()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.minRange = response.theaterMin + 1;
          this.maxRange = response.theaterMax;
          this.options = {
            floor: 1,
            ceil: response.theaterMax
          };
        });
        this.getRooms();
      });
  }
  private getFormLabels(): void {
    this.newsLetterServices
      .getFormLabels({
        code: `ca-our-living-room`,
        language: this.language
      })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          response.forEach(item => {
            this.formLabels[item.name] = item.text;
            if (item.details) {
              item.details.forEach(value => {
                this.formLabels[value.name] = value.text;
              });
            }
          });
        });
      });
  }
}
