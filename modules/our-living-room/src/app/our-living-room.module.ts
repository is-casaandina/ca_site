import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ApiModule } from '@ca-core/shared/helpers/api';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaPaginatorModule } from '@ca-core/ui/lib/components';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { FormQuotationModule } from '@ca/library';
import { HoverCollapseModule } from '@ca/library/directives';
import { SideArrowsModule } from '@ca/library/side-arrows/side-arrows.module';
import { Ng5SliderModule } from 'ng5-slider';
import { OurLivingRoomServices } from '../providers/our-living-room.services';
import { OurLivingRoomComponent } from './our-living-room.component';

@NgModule({
  declarations: [
    OurLivingRoomComponent
  ],
  imports: [
    BrowserModule,
    ApiModule,
    FormsModule,
    Ng5SliderModule,
    CaPaginatorModule,
    ModalModule,
    FormQuotationModule,
    HoverCollapseModule,
    SideArrowsModule
  ],
  providers: [
    OurLivingRoomServices,
    NewsLetterServices
  ],
  entryComponents: [OurLivingRoomComponent]
})
export class OurLivingRoomModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-our-living-room';
    const element = createCustomElement(OurLivingRoomComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
