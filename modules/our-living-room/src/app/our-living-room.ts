export interface IOutLivinRoomList {
    text: string;
    value: string;
    selected?: boolean;
}
export interface IOutLivinRoomRange {
    theaterMax: number;
    theaterMin: number;
}
export interface IOutLivinRoomFilter {
    destination: string;
    capacity: string;
    hotel: string;
    page: number;
    size: number;
}
