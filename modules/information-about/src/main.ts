import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { InformationAboutModule } from './app/information-about.module';

platformBrowserDynamic()
  .bootstrapModule(InformationAboutModule)
  .catch(err => console.error(err));
