import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { AWARDS, FORM_TYPE } from '@ca-core/shared/settings/constants/general.constant';
import { Ilink } from '@core/typings';

@Component({
  selector: 'ca-information-about',
  templateUrl: './information-about.component.html'
})
export class InformationAboutComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() description: string;
  @Input() textLink: string;
  @Input() urlLink: Ilink;
  @Input() titleDownload: string;
  @Input() badges: any;
  @Input() downloadImage: any;
  @Input() item: any;
  FORM_TYPE = FORM_TYPE;
  AWARDS = AWARDS;

  constructor(protected elem: ElementRef<HTMLElement>, protected storageService: StorageService) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    this.badges = AngularUtil.toJson(this.badges, []);
    this.urlLink = AngularUtil.toJson(this.urlLink, {});
    this.downloadImage = camelcaseKeys(AngularUtil.toJson(this.downloadImage, []));
    this.item = camelcaseKeys(AngularUtil.toJson(this.item, []));
  }
}
