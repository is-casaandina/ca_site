import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';

import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CarouselControlModule, CarouselControlService, SectionHeaderModule } from '@ca/library';
import { ImgSrcsetModule, MymeTypeModule, UrlLinkModule } from '@ca/library/directives';
import { InformationAboutComponent } from './information-about.component';

@NgModule({
  declarations: [
    InformationAboutComponent
  ],
  imports: [
    BrowserModule,
    PipesModule,
    SectionHeaderModule,
    ImgSrcsetModule,
    SpinnerModule,
    HttpClientModule,
    CarouselControlModule,
    NotificationModule,
    MymeTypeModule,
    UrlLinkModule
  ],
  providers: [
    CarouselControlService
  ],
  entryComponents: [InformationAboutComponent]
})
export class InformationAboutModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-information-about';
      const element = createCustomElement(InformationAboutComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
