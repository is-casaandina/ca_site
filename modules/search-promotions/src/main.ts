import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SearchPromotionsModule } from './app/search-promotions.module';

platformBrowserDynamic()
.bootstrapModule(SearchPromotionsModule)
  .catch(err => console.error(err));
