import { environment } from '../environments/environment';

export class SearchPromotionsEndPoints {
  public static promotions = `${environment.API_URL}site/pages/promotions/featured`;
  public static listPromotions = `${environment.API_URL}site/pages/promotions/details/filter`;
  public static parameter = `${environment.API_URL}site/parameters/roiback-hotel/{language}`;
  public static listDestination = `${environment.API_URL}site/promotions/filter/destinations`;
  public static listHotel = `${environment.API_URL}site/promotions/filter/hotels`;
}
