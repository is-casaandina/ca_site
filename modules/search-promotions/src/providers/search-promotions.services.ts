import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservablePagination } from '@ca-core/shared/helpers/models/general.model';
import { IServiceRequest } from '@core/typings';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IPromotionsRequest, ISearchPromotions } from '../app/search-promotions';
import { SearchPromotionsEndPoints } from './search-promotions.endpoints';

@Injectable()
export class SeachPromotionsServices {
  private _promosRequest: IPromotionsRequest;
  private _promos: any;

  constructor(private apiService: ApiService) {}

  getPromotions(params: IPromotionsRequest): ICaObservablePagination<ISearchPromotions> {
    if (JSON.stringify(params) === JSON.stringify(this._promosRequest)) {
      this._promos.byService = false;

      return of(this._promos);
    }

    return this.apiService
      .get<Observable<any>>(SearchPromotionsEndPoints.promotions, { preloader: false, params })
      .pipe(
        map((preResponse: any) => {
          this._promos = preResponse;
          this._promosRequest = params;
          preResponse.byService = true;

          return preResponse;
        })
      );
  }

  getListPromos(params: IServiceRequest): Observable<any> {
    return this.apiService.get(SearchPromotionsEndPoints.listPromotions, { preloader: false, params });
  }

  getParameter(language: string): Observable<any> {
    return this.apiService.get(SearchPromotionsEndPoints.parameter, { params: { language } });
  }

  getDestinations(hotels: string, language: string): Observable<any> {
    return this.apiService.get(SearchPromotionsEndPoints.listDestination, { params: { language, items: hotels } });
  }

  getHotels(destinations: string, language: string): Observable<any> {
    return this.apiService.get(SearchPromotionsEndPoints.listHotel, { params: { language, items: destinations } });
  }
}
