import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ApiModule } from '@ca-core/shared/helpers/api';
import { SectionHeaderModule } from '@ca/library';
import { HoverCollapseModule } from '@ca/library/directives';
import { HotelRestPromotionServices } from 'modules/hotel-rest-promotion/src/providers/hotel-rest-promotion.service';
import { OurLivingRoomServices } from 'modules/our-living-room/src/providers/our-living-room.services';
import { CardPromotionModule } from 'modules/slider-promotions/src/components/card/card-promotion.module';
import { SeachPromotionsServices } from '../providers/search-promotions.services';
import { SearchPromotionsComponent } from './search-promotions.component';

@NgModule({
  declarations: [
    SearchPromotionsComponent
  ],
  imports: [
    BrowserModule,
    HoverCollapseModule,
    CardPromotionModule,
    ApiModule,
    FormsModule,
    SectionHeaderModule
  ],
  providers: [
    OurLivingRoomServices,
    SeachPromotionsServices,
    HotelRestPromotionServices
  ],
  entryComponents: [SearchPromotionsComponent]
})
export class SearchPromotionsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-search-promotions';
    const element = createCustomElement(SearchPromotionsComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
