import { IBackgroundImage } from '@core/typings';

export interface IPromotionsRequest {
  items: string;
  hotels: string;
  destinations: string;
  descriptors: string;
  language: string;
  page: number;
  size: number;
  currency: string;
}

export interface ISearchPromotions {
  id: string;
  modifiedDate: string;
  genericType: string;
  language: string;
  path: string;
  roiback?: string;
  info: ISearchPromotionInfo;
  item: ISearchPromotionItem;
  byService: boolean;
}

export interface ISearchPromotionInfo {
  price: number;
  discount: number;
  title: string;
  imagePromotions: Array<IBackgroundImage>;
  isRestriccion: boolean;
  imageRestriction: Array<IBackgroundImage>;
  bodyRestriction: string;
  textButtonRestriction: string;
  footerRestriction: string;
  urlReservation: string;

  imageMain?: Array<IBackgroundImage>;
  infoPromotion?: {
    bodyRestrictionFirst: string;
    footerRestrictionFirst: string;
    imageRestrictionFirst: Array<IBackgroundImage>;
  };
}

export interface ISearchPromotionItem {
  descriptor: string;
  descriptorColor: string;
  descriptorTitle: string;
  hotel: string;
  titleHotel: string;
  price: number;
  discount: number;
  days: number;
  nights: number;
  isRestriction: boolean;
  restriction: string;
  bodyRestriction: string;
  footerRestriction: string;
  backgroundImage: Array<IBackgroundImage>;
  urlReservation: string;
  imageRestriction: Array<IBackgroundImage>;

  classNative?: string;
  urlPromotion?: string;
  descriptorInfo?: {
    color: string;
    title: string;
  };
}
