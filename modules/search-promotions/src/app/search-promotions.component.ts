import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { LOADING_TEXT } from '@ca-admin/settings/constants/general.constant';
import { GENERAL_TEXT } from '@ca-admin/settings/constants/general.lang';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { joinSelected } from '@ca-core/shared/helpers/util/join';
import { getStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import { CURRENCY_KEYS } from '@ca-core/shared/settings/constants/general.constant';
import { HotelRestPromotionServices } from 'modules/hotel-rest-promotion/src/providers/hotel-rest-promotion.service';
import { IOutLivinRoomList } from 'modules/our-living-room/src/app/our-living-room';
import { ICardPromotion } from 'modules/slider-promotions/src/components/card/card-promotion';
import { takeUntil } from 'rxjs/operators';
import { SeachPromotionsServices } from '../providers/search-promotions.services';
import { IPromotionsRequest } from './search-promotions';

@Component({
  selector: 'ca-search-promotions',
  templateUrl: './search-promotions.component.html',
  styleUrls: ['./search-promotions.component.scss']
})
export class SearchPromotionsComponent extends ComponentBase implements OnInit {
  @Input() title: string;

  destinations: Array<IOutLivinRoomList> = [];
  hotels: Array<IOutLivinRoomList> = [];
  listPromos: Array<IOutLivinRoomList> = [];
  promotions: Array<ICardPromotion>;
  page = 1;
  totalElements = 1;
  exchange: number;
  currency = getStorageCurrency();
  bookingUrl: string;

  GENERAL_TEXT = GENERAL_TEXT;
  LOADING_TEXT = LOADING_TEXT;

  loading = false;

  private _boxClass = ['lg', 'md', 'xs', 'xs', 'xs', 'xs', 'md', 'md', 'xs', 'xs'];

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private searchPromotionsServices: SeachPromotionsServices,
    private hotelRestServices: HotelRestPromotionServices,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getExchange();
      this.getListPromos();
      this.getDestinations();
      this.getBookingUrlService();
    }
  }

  selectedLength(type: string): void {
    const filter = this[type].filter(item => item.selected);

    return filter ? filter.length : 0;
  }

  getHotels(): void {
    this.searchPromotionsServices
      .getHotels(joinSelected(this.destinations, true), this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.hotels = response;
          this.hotels.forEach(item => (item.selected = false));
          this.getPromotions();
        });
      });
  }

  private getBookingUrlService(): void {
    this.searchPromotionsServices
      .getParameter(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(indicator => {
        this.ngZone.run(() => {
          this.bookingUrl = (indicator && indicator.url) || '';
        });
      });
  }

  morePromotions(): void {
    this.page = this.page + 1;
    this.getPromotions(this.page, true);
  }

  getPromotions(page = 1, add = false): void {
    this.searchPromotionsServices
      .getPromotions({
        items: joinSelected(this.listPromos, true),
        hotels: joinSelected(this.hotels, true),
        destinations: joinSelected(this.destinations, true),
        descriptors: '',
        language: this.language,
        page,
        size: 10,
        currency: this.currency
      } as IPromotionsRequest)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          if (response.byService) {
            this.loading = true;
            setTimeout(() => (this.loading = false), 800);
            const domain = JSON.parse(sessionStorage.getItem('CONFIGURATIONS')).domain;
            const promos = response.content.map((value, index) => {
              const item = value.item;
              const info = value.info;
              item.descriptorInfo = {
                color: item.descriptorColor,
                title: item.descriptorTitle
              };
              info.infoPromotion = {
                bodyRestrictionFirst: info.bodyRestriction,
                footerRestrictionFirst: info.footerRestriction,
                imageRestrictionFirst: info.imageRestriction
              };

              delete info.price;
              delete info.discount;

              info.imageMain = info.imageMain || (item as any).imageMain || info.imageRestriction;
              item.classNative = this._boxClass[index];
              item.urlPromotion = `${domain}${value.path}`;

              return { ...item, ...info, roiback: value.roiback } as any;
            });
            this.totalElements = response.totalElements;

            if (!add) {
              this.promotions = promos;

              return;
            }
            promos.forEach(item => this.promotions.push(item));
          }
        });
      });
  }

  private getListPromos(): void {
    this.searchPromotionsServices
      .getListPromos({
        language: this.language
      })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.listPromos = response;
          this.listPromos.forEach(item => (item.selected = false));
        });
      });
  }
  private getDestinations(): void {
    this.searchPromotionsServices
      .getDestinations(joinSelected(this.listPromos), this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.destinations = response;
          this.destinations.forEach(item => (item.selected = false));
          this.getHotels();
        });
      });
  }

  private getExchange(): void {
    this.hotelRestServices
      .getExchangeRate({
        codeIsoOrigin: CURRENCY_KEYS.USD,
        codeIsoTarget: this.currency === CURRENCY_KEYS.USD ? CURRENCY_KEYS.PEN : this.currency,
        amount: 1
      })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.ngZone.run(() => (this.exchange = (response as any).value)));
  }

  dataByMouseEvent(events: string): any {
    const tmp = events.split('|');
    tmp.forEach(event => setTimeout(() => this[event]()));
  }

  clearFilters(): void {
    this.listPromos = [];
    this.hotels = [];
    this.destinations = [];
    this.getPromotions();
    this.getListPromos();
    this.getDestinations();
  }
}
