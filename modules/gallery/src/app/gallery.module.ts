import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CarouselControlModule, CarouselControlService, GalleryModalModule, SectionHeaderModule } from '@ca/library';
import { ImgSrcsetModule } from '@ca/library/directives';
import { GalleryComponent } from './gallery.component';

@NgModule({
  declarations: [
    GalleryComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    GalleryModalModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule,
    ModalModule,
    CarouselControlModule,
    ImgSrcsetModule
  ],
  providers: [
    CarouselControlService
  ],
  entryComponents: [
    GalleryComponent
  ]
})
export class GalleryModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-gallery';
      const element = createCustomElement(GalleryComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
