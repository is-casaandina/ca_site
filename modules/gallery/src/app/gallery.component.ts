import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, ElementRef, Inject, Input, NgZone, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { groupCarousel } from '@ca-core/shared/helpers/util/group-carousel';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { FORM_TYPE, IMAGE_SIZE } from '@ca-core/shared/settings/constants/general.constant';
import { CarouselControlComponent, CarouselControlService, GalleryModalComponent } from '@ca/library';
import { IMoveInfo } from '@core/typings';
import { AngularUtil, StorageService } from 'core/shared/src/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ca-gallery',
  templateUrl: './gallery.component.html'
})
export class GalleryComponent extends ComponentBase implements OnInit, AfterViewInit {

  @ViewChild('controls') carouselControls: CarouselControlComponent;

  @Input() title: string;
  @Input() sliders: any;

  activeSlide = 0;
  height: string;
  modalStatus = false;
  itemGroups = [];
  slidesAll = [];
  FORM_TYPE = FORM_TYPE;
  elementWidthInPx = '';
  elementWidthSum = 0;
  elementWidth = 0;
  transitionNormal = 'transform 1500ms ease 0s';
  transitionFast = 'transform 500ms ease 0s';
  transitionTime = 'transform 1500ms ease 0s';

  active = 0;

  groupSizeOne: Array<Array<any>> = [];
  groupSizeTwo: Array<Array<any>> = [];
  groupSizeThree: Array<Array<any>> = [];

  width: number;

  constructor(
    protected elem: ElementRef,
    protected storageService: StorageService,
    private carouselControlService: CarouselControlService,
    private modalService: ModalService,
    private renderer2: Renderer2,
    private resizeService: ResizeService,
    private ngZone: NgZone,
    @Inject(DOCUMENT) private document: Document
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.sliders = AngularUtil.toJson(this.sliders);

      this.slidesAll = this.sliders.reduce((oldVal: any, newVal: any) => {
        const items = newVal.items.map((item: any) => {
          return camelcaseKeys(item);
        });

        return oldVal.concat(items);
      }, []);

      this.groupSlidesNormal();

      this.resizeService.resizeEvent
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(width => {
        this.ngZone.run(() => {
          this.width = width;
          this.height = this.getCarouselHeight();
        });
      });

      if (window.innerWidth <= 991) {
        setTimeout(() => this.initSwiper());
      }
      this.slidesAll = this.groupSlidesModal();
      this.setSectionColor();
    }

  }

  groupSlidesNormal(): void {
    this.sliders.forEach((group: any) => {
      group.items.forEach(item => {
        const backgroundImg = item['background-image'];
        if (backgroundImg && backgroundImg.length) {
          this.itemGroups.push(camelcaseKeys(item));
        }
      });
    });

    this.groupSizeOne = groupCarousel(1, this.itemGroups);
    this.groupSizeTwo = groupCarousel(2, this.itemGroups);
    this.groupSizeThree = groupCarousel(3, this.itemGroups);
  }

  groupSlidesModal(): Array<any> {
    return this.sliders.reduce((oldVal, newVal) => {
      const items = newVal.items.map(item => {
        const backgroundImg = item['background-image'];
        if (backgroundImg && backgroundImg.length) {
          return camelcaseKeys(item);
        }
      })
      .filter(item => item !== undefined);

      return oldVal.concat(items);
    }, []);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.carouselControlService.moveDirection
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe((moveInfo: IMoveInfo) => {
          this.ngZone.run(() => {
            if (!this.modalService.isActiveModal()) {
              if (moveInfo.direction === 'L') {
                this.leftMove();
              } else if (moveInfo.direction === 'R') {
                this.rightMove();
              } else {
                this.superLeftMove();
              }
              this.activeSlide = moveInfo.active;
            }

          });
        });
      this.ngZone.run(() => {
        this.height = this.getCarouselHeight();
        this.getWidth();
      });
    }, 800);
  }

  private getWidth(): void {
    this.elementWidth = this.elem.nativeElement.querySelector('.carousel-group-item')
      ? this.elem.nativeElement.querySelector('.carousel-group-item').offsetWidth
      : 0;
    this.elementWidthInPx = `translateX(0px)`;
  }

  private getCarouselHeight(): string {
    const elements = this.elem.nativeElement.querySelectorAll('.carousel-item');
    const maxHeight: number = [].reduce.call(elements, (oldVal: any, elem: any) => {
      if (oldVal < elem.offsetHeight) {
        oldVal = elem.offsetHeight;
      }

      return oldVal;
    }, 0);

    return `${maxHeight}px`;
  }

  private getActiveSlideModal(slideIndex: number, carouselActive: number, groupSize: number): number {
    return slideIndex + (carouselActive * groupSize);
  }

  rightMove(): void {
    this.transitionTime = this.transitionFast;
    this.elementWidthSum = this.elementWidthSum - this.elementWidth;
    this.elementWidthInPx = `translateX(${this.elementWidthSum}px)`;
  }

  leftMove(): void {
    this.transitionTime = this.transitionFast;
    this.elementWidthSum = this.elementWidth + this.elementWidthSum;
    this.elementWidthInPx = `translateX(${this.elementWidthSum}px)`;
  }

  superLeftMove(): void {
    this.transitionTime = this.transitionFast;
    this.elementWidthSum = 0;
    this.elementWidthInPx = `translateX(-${this.elementWidthSum}px)`;
  }

  openGallery(slideIndex = 0, groupSize): void {
    this.activeSlide = this.activeSlide ? this.activeSlide : 0;
    const modalConfig = {
      classDialog: 'carousel',
      size: IMAGE_SIZE.lg
    };
    this.ngZone.run(() => {
      const modalRef = this.modalService.open(GalleryModalComponent, modalConfig);
      this.modalStatus = this.modalService.isActiveModal();
      modalRef.setPayload({
        type: 'gallery',
        slides: this.slidesAll,
        activeSlide: this.getActiveSlideModal(slideIndex, this.activeSlide, groupSize)
      });
      modalRef.result
        .then(() => {
        })
        .catch(err => {
          this.modalStatus = false;
        });
    });
  }

  setSectionColor(): void {
    const content = this.document.querySelector('.g-carousel-content');
    if (content) {
      const sectionElem: HTMLElement = (content as any).closest('.g-section');
      const sectionElemColor = sectionElem.style.backgroundColor;
      const carouselAfterElem = this.document.querySelector('.g-carousel-content-after');
      const carouselBeforeElem = this.document.querySelector('.g-carousel-content-before');
      this.renderer2.setStyle(carouselAfterElem, 'background-color', sectionElemColor);
      this.renderer2.setStyle(carouselBeforeElem, 'background-color', sectionElemColor);
    }
  }
  private initSwiper(): void {
    const carouselContent: HTMLElement = this.elem.nativeElement.querySelector('.g-carousel-content');
    carouselContent.addEventListener('touchstart', event => {
      const timerSucriber = this.carouselControls.timerSubscription;
      if (timerSucriber) { timerSucriber.unsubscribe(); }

      const dFlex: HTMLElement = this.elem.nativeElement.querySelector('.d-flex');
      const elementWidth = this.elementWidth = this.elem.nativeElement.querySelector('.carousel-group-item')
        ? this.elem.nativeElement.querySelector('.carousel-group-item').offsetWidth
        : 0;
      const initTransform = dFlex.style.transform;
      const totalWidth = this.elem.nativeElement.querySelector('.g-carousel-content--wrap').offsetWidth - elementWidth;
      const currentTransformValue = +initTransform
        .split('(')[1]
        .replace('px)', '');
      let moving;

      const onTouchEnd = eventOut => {

        if (moving) {
          const byInitPoint = moving.calcMovingWithInitPoint;
          if (byInitPoint < -100 && moving.value > (totalWidth * -1)) {
            this.carouselControls.next();
          } else if (byInitPoint > 100 && moving.value <= 0) {
            this.carouselControls.prev();
          } else {
            dFlex.style.transform = initTransform;
          }
        }
        event.target.removeEventListener('touchmove', onTouchMove);
        event.target.removeEventListener('touchend', onTouchEnd);
      };
      const onTouchMove = eventMove => {
        moving = (() => {
          const calcMovingWithInitPoint = eventMove.touches[0].clientX - event.changedTouches[0].clientX;

          return {
            value: currentTransformValue + calcMovingWithInitPoint,
            calcMovingWithInitPoint
          };
        })();

        if (moving.value <= 0 && moving.value > (totalWidth * -1)) {
          dFlex.style.transform = `translateX(${moving.value}px)`;
        }
      };
      event.target.addEventListener('touchmove', onTouchMove);
      event.target.addEventListener('touchend', onTouchEnd);
    });
  }
}
