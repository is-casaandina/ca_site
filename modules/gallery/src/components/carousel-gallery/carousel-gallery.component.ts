import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Inject,
  Output,
  Renderer2
} from '@angular/core';
import { CarouselControlService } from 'core/lib/src';

@Component({
  selector: 'ca-carousel',
  templateUrl: './carousel-gallery.component.html'
})
export class CarouselGalleryComponent implements AfterViewInit {
  @Input() sliders?: Array<any>;
  @Input() height: string;
  @Input() animation: boolean;
  @Output() watchActiveVal = new EventEmitter<number>();
  @Input() controlColor?: string;
  elementWidthInPx = '';
  elementWidthSum = 0;
  elementWidth = 0;

  constructor(
    private carouselControlService: CarouselControlService,
    private elem: ElementRef,
    private renderer2: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) {

  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.carouselControlService.moveDirection
      .subscribe((moveInfo: any) => {
        moveInfo.direction === 'L' ? this.leftMove() : this.rightMove();
      });
      this.getWidth();
    }, 0);
  }

  getWidth(): void {
    this.elementWidth = this.elem.nativeElement.querySelector('.carousel-group-item')
      ? this.elem.nativeElement.querySelector('.carousel-group-item').offsetWidth
      : 0;
    this.elementWidthInPx = `translateX(0px)`;
  }

  rightMove(): void {
    this.elementWidthSum = this.elementWidthSum - this.elementWidth;
    this.elementWidthInPx = `translateX(${this.elementWidthSum}px)`;
  }

  leftMove(): void {
    this.elementWidthSum = this.elementWidth + this.elementWidthSum;
    this.elementWidthInPx = `translateX(${this.elementWidthSum}px)`;
  }

  setSectionColor(): void {
    const sectionElem = this.document.querySelector('.g-carousel-content')
      .closest('.g-section') as HTMLElement;
    const sectionElemColor = sectionElem ? sectionElem.style.backgroundColor : '';
    const carouselAfterElem = this.document.querySelector('.g-carousel-content-after');
    const carouselBeforeElem = this.document.querySelector('.g-carousel-content-before');

    this.renderer2.setStyle(carouselAfterElem, 'background-color', sectionElemColor);
    this.renderer2.setStyle(carouselBeforeElem, 'background-color', sectionElemColor);
  }
}
