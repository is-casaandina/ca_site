import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CarouselControlModule } from 'core/lib/src';
import { CarouselGalleryComponent } from './carousel-gallery.component';

@NgModule({
    imports: [ CommonModule, CarouselControlModule ],
    declarations: [ CarouselGalleryComponent],
    exports: [ CarouselGalleryComponent]
})
export class CarouselGalleryModule {
}
