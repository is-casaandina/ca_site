import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { GalleryModule } from './app/gallery.module';

platformBrowserDynamic()
  .bootstrapModule(GalleryModule)
  .catch(err => console.error(err));
