import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BasicTagsModule } from './app/basic-tags.module';

platformBrowserDynamic()
  .bootstrapModule(BasicTagsModule)
  .catch(err => console.error(err));
