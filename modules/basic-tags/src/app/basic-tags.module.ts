import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BasicTagsComponent } from './basic-tags.component';

@NgModule({
  declarations: [
    BasicTagsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [BasicTagsComponent]
})
export class BasicTagsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-basic-tags';
    const element = createCustomElement(BasicTagsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
