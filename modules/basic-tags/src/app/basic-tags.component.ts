import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';

@Component({
  selector: 'ca-basic-tags',
  templateUrl: './basic-tags.component.html'
})
export class BasicTagsComponent extends ComponentBase implements OnInit {
  @Input() title: string;

  private _tags: Array<any>;
  @Input()
  set tags(tags: Array<any>) {
    this._tags = tags;
  }
  get tags(): Array<any> {
    return this._tags && typeof this._tags === 'string' && JSON.parse(this._tags as any) || (this._tags || []);
  }

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
  }
}
