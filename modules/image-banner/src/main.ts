import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ImageBannerModule } from './app/image-banner.module';

platformBrowserDynamic()
  .bootstrapModule(ImageBannerModule)
  .catch(err => console.error(err));
