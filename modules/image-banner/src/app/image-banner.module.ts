import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { LogoModule, SliderModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { ImageBannerCaptionModule } from '../components/image-banner-caption/image-banner-caption.module';
import { ImageBannerComponent } from './image-banner.component';

@NgModule({
  declarations: [
    ImageBannerComponent
  ],
  imports: [
    BrowserModule,
    ImageBannerCaptionModule,
    LogoModule,
    SliderModule,
    ElemBackgroundurlModule
  ],
  providers: [],
  entryComponents: [ImageBannerComponent]
})
export class ImageBannerModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
    const name = 'ca-image-banner';
    const element = createCustomElement(ImageBannerComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
