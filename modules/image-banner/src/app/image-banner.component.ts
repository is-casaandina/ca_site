import { Component, Input, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { FORM_TYPE, IMAGE_SIZE, STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IBanner, ILogoInfo } from 'core/typings';

@Component({
  selector: 'ca-image-banner',
  templateUrl: './image-banner.component.html'
})
export class ImageBannerComponent implements OnInit {
  @Input() textButton: string;
  @Input() description: string;
  @Input() title: string;
  @Input() backgroundImage: any;
  @Input() urlButton: string;

  IMAGE_SIZE = IMAGE_SIZE;
  FORM_TYPE = FORM_TYPE;
  bannerSize: string;
  sliders: Array<IBanner> = [];
  logoInfo: ILogoInfo;

  constructor(private storageService: StorageService) { }

  ngOnInit(): void {
    this.backgroundImage = AngularUtil.fromJson(this.backgroundImage);
    this.urlButton = AngularUtil.toJson(this.urlButton);
    const newSlides: Array<IBanner> = [];

    newSlides.push({
      backgroundImage: this.backgroundImage,
      title: this.title,
      subtitle: this.description,
      isButton: false,
      type: FORM_TYPE.image,
      descriptor: '',
      textButton: '',
      typeDescriptor: ''
    });

    this.sliders = [...newSlides];
    this.setLogo();
  }

  private setLogo(): void {
    const configurations = this.storageService.getItemObject<any>(STORAGE_KEY.configurations);
    if (configurations) {
      this.logoInfo = { url: configurations.logo, alt: configurations.company };
    }
  }
}
