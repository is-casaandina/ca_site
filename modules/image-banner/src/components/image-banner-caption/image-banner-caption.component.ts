import { Component, Input } from '@angular/core';
import { COLORS } from '@ca-core/shared/settings/constants/general.constant';

@Component({
  selector: 'ca-image-banner-caption',
  templateUrl: './image-banner-caption.component.html'
})
export class ImageBannerCaptionComponent {

  @Input() title: string;
  @Input() subtitle: string;
  @Input() type: boolean;
  @Input() urlButton: any;
  @Input() textButton: any;
  private _slider: any;
  @Input()
  set slider(slider: any) {
    this._slider = slider;
  }
  get slider(): any {
    return this._slider;
  }
  COLORS = COLORS;

}
