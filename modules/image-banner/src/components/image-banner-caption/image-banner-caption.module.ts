import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UrlLinkModule } from '@ca/library/directives';
import { ImageBannerCaptionComponent } from './image-banner-caption.component';

@NgModule({
    imports: [
      CommonModule,
      UrlLinkModule
    ],
    declarations: [ ImageBannerCaptionComponent ],
    exports: [ ImageBannerCaptionComponent ]
})
export class ImageBannerCaptionModule {

}
