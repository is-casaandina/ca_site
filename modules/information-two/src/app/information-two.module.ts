import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { EventModule, SectionHeaderModule } from '@ca/library';
import { InformationTwoComponent } from './information-two.component';

@NgModule({
  declarations: [
    InformationTwoComponent
  ],
  imports: [
    BrowserModule,
    EventModule,
    SectionHeaderModule
  ],
  providers: [],
  entryComponents: [
    InformationTwoComponent
  ]
})
export class InformationTwoModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-information-two'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(InformationTwoComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
