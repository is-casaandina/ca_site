import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';

@Component({
  selector: 'ca-information-two',
  templateUrl: './information-two.component.html'
})
export class InformationTwoComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() items: any;
  itemsNew: any;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.items = this.items ? JSON.parse(this.items) : [];
      this.itemsNew = this.items.map((item: any) => (camelcaseKeys(item)));
    }
  }
}
