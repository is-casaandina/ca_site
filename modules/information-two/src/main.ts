import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { InformationTwoModule } from './app/information-two.module';

platformBrowserDynamic()
  .bootstrapModule(InformationTwoModule)
  .catch(err => console.error(err));
