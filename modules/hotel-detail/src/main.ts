import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HotelDetailModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(HotelDetailModule)
  .catch(err => console.error(err));
