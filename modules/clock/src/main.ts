import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ClockModule } from './app/clock.module';

platformBrowserDynamic()
  .bootstrapModule(ClockModule)
  .catch(err => console.error(err));
