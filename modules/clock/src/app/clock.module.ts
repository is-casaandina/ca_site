import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { createCustomElement } from '@angular/elements';
import { ClockComponent } from './clock.component';

@NgModule({
  declarations: [
    ClockComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [ClockComponent]
})
export class ClockModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-clock';
    const element = createCustomElement(ClockComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
