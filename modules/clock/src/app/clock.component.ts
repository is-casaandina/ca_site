import { Component, ElementRef, Input, NgZone, OnDestroy, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { interval, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'underscore';

@Component({
  selector: 'ca-root',
  templateUrl: './clock.component.html'
})
export class ClockComponent extends ComponentBase implements OnDestroy, OnInit {

  @Input() subtitleBefore: string;
  @Input() subtitleDuring: string;
  @Input() subtitleAfter: string;
  @Input() subtitleEnd: string;

  LANG = LANG;

  future: Date;
  config = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  };
  subtitle: string;

  private counter$: Observable<number>;
  private subscription: Subscription;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private ngZone: NgZone
  ) {
    super(
      elem,
      storageService
    );
  }

  ngOnInit(): void {
    this.hide = this.hide || !this.pageInfo.countdown;
    this.onInit();
    if (this.isVisible) {
      if (this.pageInfo.beginDate && this.pageInfo.countdown) {
        this.setCurrent();
      }
    }
  }

  private setCurrent(): void {
    if (!DateUtil.isExpired(DateUtil.setTimezoneDate(this.pageInfo.beginDate))) {
      this.future = DateUtil.setTimezoneDate(this.pageInfo.beginDate);
      this.subtitle = this.subtitleBefore;
    } else if (this.pageInfo.firstExpiration && !DateUtil.isExpired(DateUtil.setTimezoneDate(this.pageInfo.firstExpiration))) {
      this.future = DateUtil.setTimezoneDate(this.pageInfo.firstExpiration);
      this.subtitle = this.subtitleDuring;
    } else if (this.pageInfo.secondExpiration && !DateUtil.isExpired(DateUtil.setTimezoneDate(this.pageInfo.secondExpiration))) {
      this.future = DateUtil.setTimezoneDate(this.pageInfo.secondExpiration);
      this.subtitle = this.subtitleAfter;
    }

    if (this.future) {
      this.initCount();
    } else {
      this.subtitle = this.subtitleEnd ? this.subtitleEnd : (this.pageInfo.secondExpiration ? this.subtitleAfter : this.subtitleDuring);
    }
  }

  initCount(): void {
    this.counter$ = interval(1000)
      .pipe(map(x => this.getTime()));

    this.config = this.dhms(this.getTime());
    this.subscription = this.counter$.subscribe(x => {
      this.ngZone.run(() => {
        const config = this.dhms(x);
        if (_.isEqual(config, this.config) && this.pageInfo.isSite && DateUtil.isExpired(this.future)) {
          this.reload();
          this.subscription.unsubscribe();
        }
        this.config = config;
      });
    });
  }

  private reload(): void {
    if (
      (this.future.getTime() === new Date(this.pageInfo.beginDate).getTime() && this.pageInfo.firstExpiration) ||
      (this.future.getTime() === new Date(this.pageInfo.firstExpiration).getTime() && this.pageInfo.secondExpiration)
    ) {
      location.reload();
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  private unsubscribe(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private getTime(): number {
    return Math.floor((this.future.getTime() - new Date().getTime()) / 1000);
  }

  private dhms(t: number): any {
    const days = Math.floor(t / 86400);
    t -= days * 86400;
    const hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    const minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    const seconds = t % 60;

    return {
      days: days < 0 ? 0 : days,
      hours: days < 0 ? 0 : hours,
      minutes: days < 0 ? 0 : minutes,
      seconds: days < 0 ? 0 : seconds
    };
  }
}

export const LANG = {
  es: {
    days: 'días',
    day: 'día',
    hours: 'hrs',
    hour: 'hr',
    minutes: 'min',
    seconds: 'seg'
  },
  en: {
    days: 'days',
    day: 'day',
    hours: 'hrs',
    hour: 'hrs',
    minutes: 'min',
    seconds: 'sec'
  },
  pt: {
    days: 'días',
    day: 'día',
    hours: 'hrs',
    hour: 'hr',
    minutes: 'min',
    seconds: 'sec'
  }
};
