import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CarouselCaptionModule, SectionHeaderModule } from '@ca/library';
import { ElemBackgroundurlModule, UrlLinkModule } from '@ca/library/directives';
import { RestaurantsService } from '../providers/restaurants.service';
import { VerticalRestaurantsComponent } from './vertical-restaurants.component';

@NgModule({
  declarations: [
    VerticalRestaurantsComponent
  ],
  imports: [
    BrowserModule,
    CarouselCaptionModule,
    SectionHeaderModule,
    SpinnerModule,
    HttpClientModule,
    NotificationModule,
    ElemBackgroundurlModule,
    UrlLinkModule
  ],
  providers: [
    RestaurantsService
  ],
  entryComponents: [
    VerticalRestaurantsComponent
  ]
})
export class VerticalRestaurantsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const name = 'ca-vertical-restaurants';
    const element = createCustomElement(VerticalRestaurantsComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
