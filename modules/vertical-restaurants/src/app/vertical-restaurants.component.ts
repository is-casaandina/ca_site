import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { SLD_RESTAURANT_INPUTS } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor, IDescriptorResponse, IRestaurant } from 'core/typings';
import { RestaurantsService } from '../providers/restaurants.service';

@Component({
  selector: 'ca-vertical-restaurants',
  templateUrl: './vertical-restaurants.component.html'
})
export class VerticalRestaurantsComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() sliders: any;
  descriptors: Array<IDescriptor> = [];
  SLD_RESTAURANT_INPUTS = SLD_RESTAURANT_INPUTS;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private restaurantsService: RestaurantsService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    const slidersTemp = this.sliders ? JSON.parse(this.sliders) : [];
    this.sliders = [];
    if (this.isVisible) {
      this.sliders = slidersTemp.map((slide: any): IRestaurant => {
        return camelcaseKeys(slide);
      });
      this.getDescriptors();
    }
  }

  getDescriptors(): void {
    this.restaurantsService
      .getDescriptors()
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.descriptors = response;
          this.sliders = this.addDescriptorToSlide();
        });
      });
  }

  addDescriptorToSlide(): Array<any> {
    const sliders = this.sliders.map(slider => {
      const descriptorInfo = this.descriptors.find((descriptor: IDescriptor) => descriptor.id === slider.descriptor);

      return {
        ...slider,
        descriptorInfo
      };
    });

    return sliders;
  }
}
