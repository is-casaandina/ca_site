import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { VerticalRestaurantsModule } from './app/vertical-restaurants.module';

platformBrowserDynamic()
  .bootstrapModule(VerticalRestaurantsModule)
  .catch(err => console.error(err));
