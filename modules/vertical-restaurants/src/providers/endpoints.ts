import { environment } from '../environments/environment';

export class RestaurantsEndpoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
}
