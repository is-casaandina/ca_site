import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { RestaurantsEndpoint } from './endpoints';

@Injectable()
export class RestaurantsService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptors(showSpin = false): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(RestaurantsEndpoint.descriptors, { preloader: showSpin });
  }

}
