import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CurrencySymbolPipeModule } from '@ca-core/ui/lib/pipes/currency-symbol.pipe';
import { ElemBackgroundurlModule, ImgSrcsetModule, UrlLinkModule } from '@ca/library/directives';
import { HotelRestPromotionItemComponent } from '../components/hotel-rest-promotion-item/hotel-rest-promotion-item.component';
import { ModalRestrictionComponent } from '../components/modal-restriction/modal-restriction.component';
import { HotelRestPromotionServices } from '../providers/hotel-rest-promotion.service';
import { HotelRestPromotionComponent } from './hotel-rest-promotion.component';

@NgModule({
  declarations: [
    HotelRestPromotionComponent,
    HotelRestPromotionItemComponent,
    ModalRestrictionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ElemBackgroundurlModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    ModalModule,
    ImgSrcsetModule,
    PipesModule,
    UrlLinkModule,
    CurrencySymbolPipeModule
  ],
  providers: [HotelRestPromotionServices],
  entryComponents: [HotelRestPromotionComponent, ModalRestrictionComponent]
})
export class HotelRestPromotionModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-hotel-rest-promotion';
    const element = createCustomElement(HotelRestPromotionComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
