import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { fadeAnimation } from '@ca-core/shared/helpers/animations/fade.animation';
import { itemsTransition, listTransition } from '@ca-core/shared/helpers/animations/list.animation';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { getStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import { CURRENCY_KEYS } from '@ca-core/shared/settings/constants/general.constant';
import { takeUntil } from 'rxjs/operators';
import { HotelRestPromotionServices } from '../providers/hotel-rest-promotion.service';
import { IHotelRestPromotion } from './hotel-rest-promotion.interface';

@Component({
  selector: 'ca-root',
  templateUrl: './hotel-rest-promotion.component.html',
  animations: [listTransition(), itemsTransition(), fadeAnimation()]
})
export class HotelRestPromotionComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() items: Array<IHotelRestPromotion>;

  group: Array<Array<IHotelRestPromotion>> = [];
  currentLength = 0;

  limit = 10;
  isValid: boolean;
  bookingUrl: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private service: HotelRestPromotionServices,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
    this.isValid = this.isValidDate();
  }

  ngOnInit(): void {
    this.hide = this.hide || !this.isValid;
    this.onInit();
    if (this.isVisible) {
      if (this.isValid) {
        this.items = (AngularUtil.toJson(this.items) || [] as Array<IHotelRestPromotion>)
          .filter((item: IHotelRestPromotion) => item.hotel || item.restaurant)
          .map(item => (camelcaseKeys(item)));

        this.getItems();
      }
    }
  }

  private isValidDate(): boolean {
    return DateUtil.betweenDate(
      DateUtil.setTimezoneDate(this.pageInfo.beginDate),
      DateUtil.setTimezoneDate(this.pageInfo.firstExpiration),
      new Date()
    )
      ||
      (
        this.pageInfo.secondExpiration &&
        DateUtil.betweenDate(
          DateUtil.setTimezoneDate(this.pageInfo.firstExpiration),
          DateUtil.setTimezoneDate(this.pageInfo.secondExpiration),
          new Date()
        )
      );
  }

  getItems(): void {
    const list = [];
    const items = this.items.slice(this.currentLength, this.currentLength + this.limit);
    items.forEach(item => list.push(item));

    this.group.push(list);
    this.currentLength += list.length;

    if (items.length) {
      this.getEntity(items);
    }
    this.getExchange();
    this.getBookingUrl();
  }

  private getEntity(items: Array<IHotelRestPromotion>): void {
    this.service.getEntities({
      ids: items.map(item => item.hotel || item.restaurant),
      language: this.language
    })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(entities => {
        this.ngZone.run(() => {
          (entities || []).forEach(entity => {
            items.filter(item => entity.id === item.hotel || entity.id === item.restaurant)
              .forEach(element => element.entity = entity);
          });
        });
      });
  }

  private getExchange(): void {
    this.ngZone.run(() => {
      const currency = getStorageCurrency();
      this.service.getExchangeRate({
        codeIsoOrigin: CURRENCY_KEYS.USD,
        codeIsoTarget: currency === CURRENCY_KEYS.USD ? CURRENCY_KEYS.PEN : currency,
        amount: 1
      })
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => {
          this.ngZone.run(() => {
            this.group.forEach(it => {
              it.forEach(item => {
                item.exchange = response.value;
                item.currency = currency;
              });
            });
          });
        });
    });
  }

  private getBookingUrl(): void {
    this.service.getParameter(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(indicator => {
        this.ngZone.run(() => {
          this.bookingUrl = indicator && indicator.url || '';
        });
      });
  }
}
