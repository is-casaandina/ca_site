import { IBackgroundImage } from 'core/typings';

export interface IHotelRestPromotion extends IHotelRestPromotionRestriction {
  type: string;
  country: string;
  destination: string;
  descriptor: string;
  hotel: string;
  restaurant: string;
  urlReservation: string;
  price: number;
  discount: number;
  days: number;
  nights: number;
  backgroundImage: Array<IBackgroundImage>;
  description: string;
  exchange?: number;
  currency?: string;
  entity?: IEntityResponse;
}

export interface IHotelRestPromotionRestriction {
  isRestriction: true;
  imageRestriction: Array<IBackgroundImage>;
  bodyRestriction: string;
  footerRestriction: string;
}

export interface IEntityRequest {
  ids: Array<string>;
  language: string;
}

export interface IEntityResponse {
  id: string;
  name: string;
  nameDescriptor: string;
  colorDescriptor: string;
  url: string;
  tax: number;
  roiback: string;
  imageMain: IBackgroundImage;
}
