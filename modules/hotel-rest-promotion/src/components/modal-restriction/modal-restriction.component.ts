import { Component, OnInit } from '@angular/core';
import { RESERVE_ACTIONS } from '@ca-admin/settings/constants/general.lang';
import { IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { StorageService } from '@ca-core/shared/helpers/util';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IPageInfo } from 'core/typings';
import { IHotelRestPromotion } from '../../app/hotel-rest-promotion.interface';

@Component({
  selector: 'ca-modal-restriction',
  templateUrl: './modal-restriction.component.html'
})
export class ModalRestrictionComponent implements OnInit, IPayloadModalComponent {

  payload: IHotelRestPromotion;
  pageInfo: IPageInfo;
  RESERVE_ACTIONS = RESERVE_ACTIONS;

  constructor(
    private storageService: StorageService
  ) {
    this.pageInfo = this.storageService.getItemObject<IPageInfo>(STORAGE_KEY.pageInfo) || {} as IPageInfo;
  }

  ngOnInit(): void {
  }

}
