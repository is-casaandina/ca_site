import { Component, Input } from '@angular/core';
import { GENERAL_TEXT, RESERVE_ACTIONS } from '@ca-admin/settings/constants/general.lang';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { BookingUtil, IHotelBooking } from '@ca-core/shared/helpers/util/booking';
import { IPageInfo } from 'core/typings';
import { IHotelRestPromotion } from '../../app/hotel-rest-promotion.interface';
import { ModalRestrictionComponent } from '../modal-restriction/modal-restriction.component';

@Component({
  selector: 'ca-hotel-rest-promotion-item',
  templateUrl: './hotel-rest-promotion-item.component.html',
  styleUrls: ['./hotel-rest-promotion-item.scss']
})
export class HotelRestPromotionItemComponent {

  private _item: IHotelRestPromotion;
  @Input()
  get item(): IHotelRestPromotion {
    return this._item;
  }
  set item(value) {
    this._item = value;
  }

  private _bookingUrl: string;
  @Input()
  get bookingUrl(): string {
    return this._bookingUrl;
  }
  set bookingUrl(bookingUrl: string) {
    this._bookingUrl = bookingUrl;
  }

  @Input() pageInfo: IPageInfo;
  @Input() lang: string;

  RESERVE_ACTIONS = RESERVE_ACTIONS;
  GENERAL_TEXT = GENERAL_TEXT;

  constructor(
    private modalService: ModalService
  ) { }

  openModel(): void {
    const modal = this.modalService.open(ModalRestrictionComponent, {
      classDialog: 'site promo',
      classBody: 'p-0',
      classHeader: 'p-0',
      size: MODAL_BREAKPOINT.lg
    });

    modal.setPayload(this.item);

    modal.result
      .then(() => { })
      .catch(() => { });
  }

  doConvertion(): number {
    return this.item.price * (this.item.entity && this.item.entity.tax || 1) * (this.item.currency !== 'USD' ? this.item.exchange : 1);
  }

  getBookingUrl(): string {
    const booking: IHotelBooking = {
      nights: this.item.nights,
      days: this.item.days,
      roiback: this.item.entity && this.item.entity.roiback,
      urlReservation: this.item.urlReservation
    };

    const url = this.bookingUrl && this.item.hotel && booking.roiback && BookingUtil.getBookingUrl(this.bookingUrl, booking);

    this.item.urlReservation = this.item.urlReservation || url;

    return url;
  }

}
