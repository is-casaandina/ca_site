import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { HotelRestPromotionModule } from './app/hotel-rest-promotion.module';

platformBrowserDynamic()
  .bootstrapModule(HotelRestPromotionModule)
  .catch(err => console.error(err));
