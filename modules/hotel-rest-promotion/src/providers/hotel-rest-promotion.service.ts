import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { IEntityRequest, IEntityResponse } from '../app/hotel-rest-promotion.interface';
import { HotelRestPromotionEndpoint } from './endpoints';

@Injectable()
export class HotelRestPromotionServices {

  constructor(
    private apiService: ApiService
  ) { }

  getEntities(params: IEntityRequest): Observable<Array<IEntityResponse>> {
    return this.apiService.get(HotelRestPromotionEndpoint.descriptors, { params });
  }

  getExchangeRate(params: { codeIsoOrigin: string, codeIsoTarget: string, amount: number }): Observable<{ value: number }> {
    return this.apiService.get(HotelRestPromotionEndpoint.converter, { params });
  }

  getParameter(code: string): Observable<any> {
    return this.apiService.get(HotelRestPromotionEndpoint.parameter, { params: { code } });
  }

}
