import { environment } from '../environments/environment';

export class HotelRestPromotionEndpoint {
  public static descriptors = `${environment.API_URL}site/promotions/detail/ids`;
  public static converter = `${environment.API_URL}api/currencies/currency-converter`;
  public static parameter = `${environment.API_URL}site/parameters/roiback-hotel/{code}`;
}
