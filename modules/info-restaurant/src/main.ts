import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { InfoRestaurantModule } from './app/info-restaurant.module';

platformBrowserDynamic()
  .bootstrapModule(InfoRestaurantModule)
  .catch(err => console.error(err));
