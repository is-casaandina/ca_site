import { environment } from '../environments/environment';

export class InfoRestaurantEndpoint {
  public static descriptors = `${environment.API_URL}site/descriptors/{descriptorId}`;
}
