import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { InfoRestaurantEndpoint } from './endpoints';

@Injectable()
export class InfoRestaurantServices {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptor(descriptorId: string): Observable<IDescriptorResponse> {
    return this.apiService.get(InfoRestaurantEndpoint.descriptors, { preloader: false, params: {descriptorId} });
  }

}
