import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IBackgroundImage, IDescriptor, IDescriptorResponse, Ilink, IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { InfoRestaurantServices } from '../providers/info-restaurant.service';

@Component({
  selector: 'ca-info-restaurant',
  templateUrl: './info-restaurant.component.html'
})
export class InfoRestaurantComponent extends ComponentBase implements OnInit {

  @Input() title: string;
  @Input() subtitle: string;
  @Input() description: string;
  @Input() badge1: string;
  @Input() badge2: string;
  @Input() badge3: string;
  @Input() badge4: string;
  @Input() urlLink: Ilink;
  @Input() textLink: string;
  @Input() backgroundImage: IBackgroundImage;
  pageInfo: IPageInfo;
  descriptorInfo: IDescriptorResponse;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private infoRestaurantServices: InfoRestaurantServices,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.urlLink = AngularUtil.toJson(this.urlLink, null);
      this.backgroundImage = this.backgroundImage ? JSON.parse(this.backgroundImage as string) : [];
      this.pageInfo = JSON.parse(this.storageService.getItem(STORAGE_KEY.pageInfo));
      this.getDescriptor();
    }
  }

  getDescriptor(): void {
    this.infoRestaurantServices
      .getDescriptor(this.pageInfo.descriptorId)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IDescriptor) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response;
        });
      });
  }
}
