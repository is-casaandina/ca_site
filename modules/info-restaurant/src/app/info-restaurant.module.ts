import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { AwardModule } from '@ca/library';
import { UrlLinkModule } from '@ca/library/directives';
import { InfoRestaurantServices } from '../providers/info-restaurant.service';
import { InfoRestaurantComponent } from './info-restaurant.component';

@NgModule({
  declarations: [
    InfoRestaurantComponent
  ],
  imports: [
    BrowserModule,
    AwardModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    PipesModule,
    UrlLinkModule
  ],
  providers: [
    InfoRestaurantServices
  ],
  entryComponents: [InfoRestaurantComponent]
})
export class InfoRestaurantModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-info-restaurant';
      const element = createCustomElement(InfoRestaurantComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
