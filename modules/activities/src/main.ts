import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ActivitiesModule } from './app/activities.module';

platformBrowserDynamic()
  .bootstrapModule(ActivitiesModule)
  .catch(err => console.error(err));
