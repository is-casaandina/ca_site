import { IBackgroundImage, IBackgroundVideo } from 'core/typings';

export  interface IActivitie{
  title?: string;
  subtitle?: string;
  description?: string;
  backgroundVideo?: IBackgroundVideo;
  backgroundImage?: IBackgroundImage;
  type?: string;
}

export interface ITab {
  tab: string;
}
