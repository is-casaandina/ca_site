import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { NavbarModule, SectionHeaderModule } from '@ca/library';
import { ImgSrcsetModule } from '@ca/library/directives';
import { ActivitiesComponent } from './activities.component';

@NgModule({
  declarations: [
    ActivitiesComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    NavbarModule,
    SectionHeaderModule,
    BrowserAnimationsModule,
    PipesModule,
    ImgSrcsetModule,
    SpinnerModule,
    NotificationModule,
    HttpClientModule
  ],
  entryComponents: [
    ActivitiesComponent
  ]
})
export class ActivitiesModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-activities';
    const element = createCustomElement(ActivitiesComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
