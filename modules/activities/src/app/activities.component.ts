import { animate, state, style, transition, trigger } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { FORM_TYPE } from '@ca-core/shared/settings/constants/general.constant';
import { ITab } from '@ca/library';
import { Ilink } from 'core/typings';
import { IActivitie } from './activties';

@Component({
  selector: 'ca-activities',
  templateUrl: './activities.component.html',
  animations: [
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0
        })
      ),
      transition('* => void', animate('0s ease-out')),
      transition('void => *', animate('0.2s 201ms ease-in'))
    ])
  ]
})
export class ActivitiesComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() tabs: Array<ITab>;
  @Input() isButton: boolean;
  @Input() textButton: string;
  @Input() urlButton: Ilink;

  private _items: Array<any>;
  @Input()
  set items(tags: Array<any>) {
    this._items = tags;
  }
  get items(): Array<any> {
    return (this._items && typeof this._items === 'string' && JSON.parse(this._items as any)) || (this._items || []);
  }

  activeTab = 0;
  FORM_TYPE = FORM_TYPE;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    @Inject(DOCUMENT) private document: any,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.urlButton = this.urlButton ? JSON.parse(this.urlButton as any) : {};
      this.items = this.convertPropsToCamelCase();
      this.tabs = this.items.map(
        (item: IActivitie): ITab => {
          return { tab: item.title };
        }
      );
    }
  }

  convertPropsToCamelCase(): Array<IActivitie> {
    return this.items.map((item: any) => {
      return camelcaseKeys(item) as IActivitie;
    });
  }

  goToBlog(): void {
    this.urlButton.isExternal
      ? window.open(this.urlButton.url, '_blank')
      : (this.document.location.href = this.urlButton.url);
  }

  onChangeTab(activeTab: number): void {
    this.ngZone.run(() => {
      this.activeTab = activeTab;
    });
  }
}
