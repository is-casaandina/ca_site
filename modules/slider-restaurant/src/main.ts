import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderRestaurantModule } from './app/slider-restaurant.module';

platformBrowserDynamic()
  .bootstrapModule(SliderRestaurantModule)
  .catch(err => console.error(err));
