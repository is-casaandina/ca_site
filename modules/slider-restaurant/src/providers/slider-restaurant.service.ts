import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { SliderRestaurantEndpoint } from './endpoints';

@Injectable()
export class SliderRestaurantService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptors(): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(SliderRestaurantEndpoint.descriptors);
  }
}
