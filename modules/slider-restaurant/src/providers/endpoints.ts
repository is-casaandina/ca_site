import { environment } from '../environments/environment';

export class SliderRestaurantEndpoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
}
