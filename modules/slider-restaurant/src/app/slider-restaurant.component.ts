import { AfterContentInit, Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { SLD_RESTAURANT_INPUTS } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor, IDescriptorResponse } from 'core/typings';
import { SliderRestaurantService } from '../providers/slider-restaurant.service';
import { ISliderRestaurant } from './slider-restaurant';

@Component({
  selector: 'ca-slider-restaurant',
  templateUrl: './slider-restaurant.component.html'
})
export class SliderRestaurantComponent extends ComponentBase implements OnInit, AfterContentInit {
  @Input() title: string;

  private _sliders: Array<any>;
  @Input()
  set sliders(tags: Array<any>) {
    this._sliders = tags;
  }
  get sliders(): Array<any> {
    return this._sliders && typeof this._sliders === 'string' && JSON.parse(this._sliders as any) || (this._sliders || []);
  }

  height: string;
  restaurants: Array<ISliderRestaurant> = [];
  descriptors: Array<IDescriptor>;
  SLD_RESTAURANT_INPUTS = SLD_RESTAURANT_INPUTS;
  elementWidthPx = '';
  elementWidthSum = 0;
  elementWidth = 0;

  constructor(
    protected elem: ElementRef,
    protected storageService: StorageService,
    private sliderRestaurants: SliderRestaurantService,
    private ngZone: NgZone
    ) {
      super(elem, storageService);
    }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.restaurants = this.sliders.map((slide: any): any => {
        return camelcaseKeys(slide);
      });
      this.getDescriptors();
    }
  }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.height = this.getCarouselHeight();
    }, 100);
  }

  getCarouselHeight(): string {
    const elements = this.element.querySelectorAll('.g-carousel-content-wrapper');
    const maxHeight: number = [].reduce.call(elements, (oldVal: number, elem) => {
        if (oldVal < elem.offsetHeight) {
          oldVal = elem.offsetHeight;
        }

        return oldVal;
      }, 0);

    return `${maxHeight}px`;
  }

  addDescriptorToSlide(): Array<any> {
    const sliders = this.restaurants.map((slider: ISliderRestaurant) => {
      const descriptorInfo = this.descriptors.find((descriptor: IDescriptor) => descriptor.id === slider.descriptor);

      return {
        ...slider,
        descriptorInfo
      };
    });

    return sliders;
  }

  getDescriptors(): void {
    this.sliderRestaurants
      .getDescriptors()
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.descriptors = response;
          this.restaurants = this.addDescriptorToSlide();
          setTimeout(() => {
            this.height = this.getCarouselHeight();
          }, 1000);
        });
      });
  }
}
