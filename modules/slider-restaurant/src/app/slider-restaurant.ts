import { IBackgroundImage, IDescriptor } from '@core/typings';

export interface ISliderRestaurant {
  descriptor?: string;
  backgroundImage?: Array<IBackgroundImage>;
  title?: string;
  descriptionOne?: string;
  descriptionTwo?: string;
  textLink?: string;
  urlLink?: string;
  descriptorInfo?: IDescriptor;
}
