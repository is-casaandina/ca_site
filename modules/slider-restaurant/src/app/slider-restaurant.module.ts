import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import {
  CarouselCaptionModule,
  CarouselControlModule,
  CarouselModule,
  SectionHeaderModule
} from '@ca/library';
import { ElemBackgroundurlModule, UrlLinkModule } from '@ca/library/directives';
import { SliderRestaurantService } from '../providers/slider-restaurant.service';
import { SliderRestaurantComponent } from './slider-restaurant.component';

@NgModule({
  declarations: [SliderRestaurantComponent],
  imports: [
    BrowserModule,
    CommonModule,
    SectionHeaderModule,
    CarouselModule,
    NotificationModule,
    SpinnerModule,
    HttpClientModule,
    CarouselCaptionModule,
    ElemBackgroundurlModule,
    UrlLinkModule,
    CarouselControlModule
  ],
  providers: [ SliderRestaurantService ],
  entryComponents: [SliderRestaurantComponent]
})
export class SliderRestaurantModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-slider-restaurant';
    const element = createCustomElement(SliderRestaurantComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
