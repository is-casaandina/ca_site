import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ParagraphTwoComponent } from './paragraph-two.component';

@NgModule({
  declarations: [
    ParagraphTwoComponent
  ],
  imports: [
    BrowserModule
  ],
  entryComponents: [
    ParagraphTwoComponent
  ]
})
export class ParagraphTwoModule {
  constructor(private injector: Injector) {}

	ngDoBootstrap() {
		const name = 'ca-paragraph-two';
		const element = createCustomElement(ParagraphTwoComponent, { injector: this.injector });
		customElements.define(name, element);
	}
}
