import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';

@Component({
  selector: 'ca-paragraph-two',
  templateUrl: './paragraph-two.component.html'
})
export class ParagraphTwoComponent extends ComponentBase implements OnInit {
  @Input() descriptionLeft: string;
  @Input() descriptionRight: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
  }
}
