import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ParagraphTwoModule } from './app/paragraph-two.module';

platformBrowserDynamic()
.bootstrapModule(ParagraphTwoModule)
  .catch(err => console.error(err));
