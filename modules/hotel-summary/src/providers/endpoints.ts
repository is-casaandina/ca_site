import { environment } from '../environments/environment';

export class HotelSummaryEndPoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static ratings = `${environment.API_URL}api/ratings/{revinate}`;
}
