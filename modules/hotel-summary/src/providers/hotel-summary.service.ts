import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from '@core/typings';
import { Observable } from 'rxjs';
import { HotelSummaryEndPoint } from './endpoints';

@Injectable()
export class HotelSummaryServices {

  constructor(
    private apiService: ApiService
  ) { }

  getOpinionsRate(revinate: string): Observable<any> {
    return this.apiService.get(HotelSummaryEndPoint.ratings, {params: {revinate}});
  }

  getDescriptor(): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(HotelSummaryEndPoint.descriptors, { preloader: false });
  }
}
