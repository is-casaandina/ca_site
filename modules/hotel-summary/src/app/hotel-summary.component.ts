import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { GENERAL_TEXT } from '@ca-admin/settings/constants/general.lang';
import { IDescriptorResponse } from '@ca-admin/statemanagement/models/descriptor.interface';
import { IModalConfig, ModalService } from '@ca-core/shared/helpers/modal';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IMAGE_SIZE } from '@ca-core/shared/settings/constants/general.constant';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { GalleryModalComponent, TranslateModalComponent } from '@ca/library';
import { IDescriptor, ILanguageContact, IOpinionRate } from 'core/typings';
import { HotelSummaryServices } from '../providers/hotel-summary.service';

@Component({
  selector: 'ca-hotel-summary',
  templateUrl: './hotel-summary.component.html'
})
export class HotelSummaryComponent extends ComponentBase implements OnInit {
  @Input() description: string;
  @Input() isUbication: string;
  @Input() isTraslate: string;
  @Input() backgroundImageMap: any;
  @Input() mapUrl: any;
  @Input() descriptionTraslate: any;
  @Input() tables: any;
  @Input() restrictions: any;

  @Input() imageTotalUrl: any;
  private _isImageTotal: boolean;
  @Input()
  set isImageTotal(isImageTotal: boolean) {
    this._isImageTotal = coerceBooleanProp(isImageTotal);
  }
  get isImageTotal(): boolean {
    return coerceBooleanProp(this._isImageTotal);
  }
  private _isValoration: boolean;
  @Input()
  set isValoration(isValoration: boolean) {
    this._isValoration = coerceBooleanProp(isValoration);
  }
  get isValoration(): boolean {
    return coerceBooleanProp(this._isValoration);
  }

  descriptorInfo: IDescriptor;
  opinionsRate: IOpinionRate;
  contactLang: ILanguageContact;
  GENERAL_TEXT = GENERAL_TEXT;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private hotelSummaryServices: HotelSummaryServices,
    private modalService: ModalService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.backgroundImageMap = this.backgroundImageMap ? JSON.parse(this.backgroundImageMap) : [];
      if (this.pageInfo.contact) {
        this.contactLang = this.pageInfo.contact.languages.find(item => item.language === this.language);
      }
      this.tables = this.tables ? JSON.parse(this.tables) : [];

      this.getOpinionsRate();
      this.getDescriptor();
    }
  }

  private getOpinionsRate(): void {
    if (!this.isValoration) {
      this.hotelSummaryServices.getOpinionsRate(this.pageInfo.revinate).subscribe((response: IOpinionRate) => {
        this.ngZone.run(() => {
          this.opinionsRate = response;
        });
      });
    }
  }

  private getDescriptor(): void {
    this.hotelSummaryServices.getDescriptor().subscribe((response: Array<IDescriptorResponse>) => {
      this.ngZone.run(() => {
        this.descriptorInfo = response.find(descriptor => descriptor.id === this.pageInfo.descriptorId);
      });
    });
  }

  openUbication(): void {
    const component = GalleryModalComponent;
    const modalConfig = {
      classDialog: 'multimedia',
      size: IMAGE_SIZE.md
    };

    const payload = {
      type: 'map',
      data: this.backgroundImageMap,
      imageLink: this.mapUrl,
      contact: this.contactLang ? this.contactLang.details : [],
      descriptorInfo: this.descriptorInfo
    };
    this.openModal(modalConfig, payload, component);
  }

  openTranslate(): void {
    const component = TranslateModalComponent;
    const modalConfig = {
      size: IMAGE_SIZE.md
    };

    const payload = {
      type: 'map',
      data: {
        description: this.descriptionTraslate,
        tables: this.tables,
        restrictions: this.restrictions
      }
    };

    this.openModal(modalConfig, payload, component);
  }

  private openModal(modalConfig: IModalConfig, payload: any, component: any): void {
    const modalRef = this.modalService.open(component, modalConfig);
    modalRef.setPayload(payload);
    modalRef.result.then(() => {}).catch(err => {});
  }

  openTour(): void {
    const component = GalleryModalComponent;
    const modalConfig = {
      classDialog: 'multimedia',
      size: IMAGE_SIZE.lg
    };

    const payload = {
      type: 'iframe',
      data: this.imageTotalUrl
    };

    this.openModal(modalConfig, payload, component);
  }
}
