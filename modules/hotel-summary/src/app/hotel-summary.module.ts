import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { GalleryModalModule, RateModule, TranslateModalModule } from '@ca/library';
import { HotelSummaryServices } from '../providers/hotel-summary.service';
import { HotelSummaryComponent } from './hotel-summary.component';

@NgModule({
  declarations: [
    HotelSummaryComponent
  ],
  imports: [
    BrowserModule,
    RateModule,
    NotificationModule,
    SpinnerModule,
    HttpClientModule,
    ModalModule,
    GalleryModalModule,
    TranslateModalModule
  ],
  providers: [
    HotelSummaryServices
  ],
  entryComponents: [
    HotelSummaryComponent
  ]
})
export class HotelSummaryModule {

  constructor(
    private injector: Injector) {}

  ngDoBootstrap(): void  {
    const name = 'ca-hotel-summary';
    const element = createCustomElement(HotelSummaryComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
