import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HotelSummaryModule } from './app/hotel-summary.module';

platformBrowserDynamic()
  .bootstrapModule(HotelSummaryModule)
  .catch(err => console.error(err));
