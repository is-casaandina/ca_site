import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DestinationDescriptionModule } from './app/paragraph.module';

platformBrowserDynamic()
  .bootstrapModule(DestinationDescriptionModule)
  .catch(err => console.error(err));
