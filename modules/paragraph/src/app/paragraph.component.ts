import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';

@Component({
  selector: 'ca-paragraph',
  templateUrl: './paragraph.component.html'
})
export class ParagraphComponent extends ComponentBase implements OnInit {
  @Input() description: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
  }
}
