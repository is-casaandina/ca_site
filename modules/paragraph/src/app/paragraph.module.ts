import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ParagraphComponent } from './paragraph.component';

@NgModule({
  declarations: [
    ParagraphComponent
  ],
  imports: [
    BrowserModule
  ],
  entryComponents: [ParagraphComponent]
})
export class DestinationDescriptionModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-paragraph'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ParagraphComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
