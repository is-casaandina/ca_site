import { environment } from '../environments/environment';

export class BenefitsEndpoint {
  public static configuration = `${environment.API_URL}api/benefits`;
}
