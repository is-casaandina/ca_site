import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { IBenefitsResponse } from '../app/benefits';
import { BenefitsEndpoint } from './endpoints';

@Injectable()
export class BenefitsService {

  constructor(
    private apiService: ApiService
  ) { }

  getBenefits(showSpin = false): Observable<IBenefitsResponse> {
    return this.apiService.get(BenefitsEndpoint.configuration, { preloader: showSpin });
  }

}
