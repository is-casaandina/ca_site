import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BenefitsModule } from './app/benefits.module';

platformBrowserDynamic()
  .bootstrapModule(BenefitsModule)
  .catch(err => console.error(err));
