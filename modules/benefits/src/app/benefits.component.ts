import { Component, ElementRef, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import {
  FEATURE_TYPES,
  LANGUAGE_TYPES_CODE,
  PAGE_TITLES,
  STORAGE_KEY
} from '@ca-core/shared/settings/constants/general.constant';
import { IBenefit } from '@ca/library';
import { Ilink, IPageInfo } from 'core/typings';
import { BenefitsService } from '../providers/benefits.service';
import { IBenefitsResponse } from './benefits';

@Component({
  selector: 'app-root',
  templateUrl: './benefits.component.html'
})
export class BenefitsComponent extends ComponentBase implements OnInit {
  textLink: string;
  urlLink: Ilink;
  benefits: Array<IBenefit> = [];
  lang: string;
  pageInfo: IPageInfo;
  PAGE_TITLES = PAGE_TITLES;
  STORAGE_KEY = STORAGE_KEY;
  LANGS = LANGUAGE_TYPES_CODE;
  FEATURE_TYPES = FEATURE_TYPES;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private benefitsService: BenefitsService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getBenefits();
      this.pageInfo = JSON.parse(
        this.storageService.getItem(STORAGE_KEY.pageInfo)
      );
      this.lang = this.pageInfo.language;
    }
  }

  getBenefits(): void {
    this.benefitsService
      .getBenefits(false)
      .subscribe((response: IBenefitsResponse) => {
        this.ngZone.run(() => {
          this.updateLink(response);
          this.benefits = response.detail
            .filter(det => !det.hide)
            .map(det => {
              const title =
                this.lang === LANGUAGE_TYPES_CODE.es
                  ? det.titleEsp
                  : this.lang === LANGUAGE_TYPES_CODE.en
                    ? det.titleEng
                    : det.titlePor;

              return {
                title,
                icon: det.icon
              };
            });
        });
      });
  }

  private updateLink(data: IBenefitsResponse): void {
    const lang = this.lang === LANGUAGE_TYPES_CODE.pt ? 'pr' : this.lang;
    const propLink = `text${this.capitalize(lang)}`;
    const urlLink = `link${this.capitalize(lang)}`;
    this.textLink = data[propLink];
    this.urlLink = data[urlLink];
  }

  private capitalize(s: string): string {
    return s.charAt(0)
      .toUpperCase() + s.slice(1);
  }
}
