export interface IBenefitsResponse {
  detail: Array<IDetail>;
  id: string;
  linkEn?: IBenefitLink;
  linkEs?: IBenefitLink;
  linkPr?: IBenefitLink;
  textEn?: string;
  textEs?: string;
  textPr?: string;
}

export interface IDetail {
  icon?: string;
  titleEng?: string;
  titleEsp?: string;
  titlePor?: string;
  hide?: boolean;
}

export interface IBenefitLink {
  url: string;
  isExternal: string;
}
