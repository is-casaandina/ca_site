import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { AwardModule, BenefitModule, SectionHeaderModule } from '@ca/library';
import { UrlLinkModule } from '@ca/library/directives';
import { BenefitsService } from '../providers/benefits.service';
import { BenefitsComponent } from './benefits.component';

@NgModule({
  declarations: [
    BenefitsComponent
  ],
  imports: [
    BrowserModule,
    NotificationModule,
    SpinnerModule,
    CommonModule,
    SectionHeaderModule,
    AwardModule,
    HttpClientModule,
    BenefitModule,
    UrlLinkModule
  ],
  providers: [
    BenefitsService
  ],
  entryComponents: [
    BenefitsComponent
  ]
})
export class BenefitsModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-benefits';
    const element = createCustomElement(BenefitsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
