import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BenefitModule } from '@ca/library';
import { DifferentialsComponent } from './differentials.component';

@NgModule({
  declarations: [
    DifferentialsComponent
  ],
  imports: [
    BrowserModule,
    BenefitModule,
    CommonModule
  ],
  entryComponents: [
    DifferentialsComponent
  ]
})
export class DifferentialsModule {
  constructor(private injector: Injector) {}

	ngDoBootstrap() {
		const name = 'ca-differentials';
		const element = createCustomElement(DifferentialsComponent, { injector: this.injector });
		customElements.define(name, element);
	}
}
