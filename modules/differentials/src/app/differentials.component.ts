import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IBenefit } from '@ca/library';

@Component({
  selector: 'ca-differentials',
  templateUrl: './differentials.component.html'
})
export class DifferentialsComponent extends ComponentBase implements OnInit {
  @Input() items: any;
  itemsNew: Array<IBenefit>;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
    this.itemsNew = [];
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.items = this.items ? JSON.parse(this.items) : [];
      this.itemsNew = this.items.map((item: IBenefit) => {
        return {
          subtitle: item.title,
          title: item.subtitle,
          icon: item.icon
        };
      });
    }
  }
}
