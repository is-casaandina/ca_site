import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DifferentialsModule } from './app/differentials.module';

platformBrowserDynamic().bootstrapModule(DifferentialsModule)
  .catch(err => console.error(err));
