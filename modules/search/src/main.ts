import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SearchModule } from './app/search.module';

platformBrowserDynamic()
  .bootstrapModule(SearchModule)
  .catch(err => console.error(err));
