import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CaDatepickerModule, CaInputModule } from '@ca-core/ui/lib/components';
import { SearchComponent } from './search.component';

@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NotificationModule,
    SpinnerModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    CaInputModule,
    CaDatepickerModule
  ],
  entryComponents: [
    SearchComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SearchModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-search';
    const element = createCustomElement(SearchComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
