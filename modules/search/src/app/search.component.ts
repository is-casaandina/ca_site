import { Component, ElementRef, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CATEGORY } from '@ca-admin/settings/constants/general.constant';
import { StorageService } from '@ca-core/shared/helpers/util';
import { SearchBase } from '@ca-core/shared/helpers/util/search-base';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IPageInfo } from 'core/typings';
import { SearchEndpoint } from '../providers/endpoints';
import { SearchService } from '../providers/search.service';

@Component({
  selector: 'ca-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent extends SearchBase {
  CATEGORY = CATEGORY;

  constructor(
    public elem: ElementRef,
    public storageService: StorageService,
    public searchService: SearchService,
    public fb: FormBuilder,
    public ngZone: NgZone
  ) {
    super(
      searchService,
      fb,
      SearchEndpoint.destinations,
      storageService.getItemObject<IPageInfo>(STORAGE_KEY.pageInfo).language,
      elem,
      ngZone,
      storageService
    );
  }

  afterOnInit(): void {
    this.autocomplete.setValidators([Validators.required]);
  }
}
