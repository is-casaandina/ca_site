import { environment } from '../environments/environment';

export class SearchEndpoint {
  public static configuration = `${environment.API_URL}site/configurations`;
  public static destinations = `${environment.API_URL}site/pages/hotels/destinations`;
}
