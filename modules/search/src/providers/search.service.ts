import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IConfigurationResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { SearchEndpoint } from './endpoints';

@Injectable({ providedIn: 'root' })
export class SearchService {

  constructor(
    private apiService: ApiService
  ) { }

  getConfigurations(): Observable<IConfigurationResponse> {
    return this.apiService.get(SearchEndpoint.configuration, { preloader: false });
  }

}
