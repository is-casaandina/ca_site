import { IBackgroundImage, IDescriptor, Ilink } from 'core/typings';

export interface IDestination {
  hotels: Array<IHotelDestination>;
  name: string;
}

export interface IHotelDestination {
  descriptorId: string;
  name: string;
  path: string;
  preName?: string;
  descriptorInfo?: IDescriptor;
  active?: boolean;
}

export interface IOurDestination {
  backgroundImage: Array<IBackgroundImage>;
  title: string;
  description: string;
  textLink: string;
  urlLink: Ilink;
  places: Array<any>;
}

export interface ICircuitsDestionation {
  width: string;
  title: string;
  position: string;
  descriptor: string;
}

export interface ICircuits {
  circuitoNorteMoche?: IOurDestination;
  circuitoLimaIca?: IOurDestination;
  circuitoCusco?: IOurDestination;
  circuitoSur?: IOurDestination;
}

export enum ECircuits {
  circuitoNorteMoche = 'circuitoNorteMoche',
  circuitoLimaIca = 'circuitoLimaIca',
  circuitoCusco = 'circuitoCusco',
  circuitoSur = 'circuitoSur'
}
