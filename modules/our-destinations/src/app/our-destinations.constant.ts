import { ECircuits } from './our-destinations';

export const CIRCUITS_INFO = {
  title: 'LOS MEJORES CIRCUITOS',
  description: 'Elige un circuito y comienza a vivir tu mejor experiencia',
  circuits: [
    {
      key: ECircuits.circuitoNorteMoche,
      class: 's-map-road--nor',
      pathCircuit: 'images/nuestros-destinos/circuito-1.png',
      places: [
        {
          name: 'Zorritos Tumbes',
          class: 's-map-item--tum',
          url: '#'
        },
        {
          name: 'Talara',
          class: 's-map-item--tal',
          url: '#'
        },
        {
          name: 'Piura',
          class: 's-map-item--piu',
          url: '#'
        },
        {
          name: 'Chiclayo',
          class: 's-map-item--cix',
          url: '#'
        },
        {
          name: 'Trujillo',
          class: 's-map-item--tru',
          url: '#'
        }
      ]
    },
    {
      key: ECircuits.circuitoLimaIca,
      class: 's-map-road--lim',
      pathCircuit: 'images/nuestros-destinos/circuito-2.png',
      places: [
        {
          name: 'Lima',
          class: 's-map-item--lim',
          url: '#'
        },
        {
          name: 'Chincha',
          class: 's-map-item--chi',
          url: '#'
        },
        {
          name: 'Nazca',
          class: 's-map-item--naz',
          url: '#'
        }
      ]
    },
    {
      key: ECircuits.circuitoSur,
      class: 's-map-road--sur',
      pathCircuit: 'images/nuestros-destinos/circuito-3.png',
      places: [
        {
          name: 'Colca',
          class: 's-map-item--col',
          url: '#'
        },
        {
          name: 'Arequipa',
          class: 's-map-item--aqp',
          url: '#'
        },
        {
          name: 'Moquegua',
          class: 's-map-item--moq',
          url: '#'
        },
        {
          name: 'Tacna',
          class: 's-map-item--tac',
          url: '#'
        },
        {
          name: 'Puno',
          class: 's-map-item--pun',
          url: '#'
        }
      ]
    },
    {
      key: ECircuits.circuitoCusco,
      class: 's-map-road--cus',
      pathCircuit: 'images/nuestros-destinos/circuito-4.png',
      places: [
        {
          name: 'Aguas Calientes - Machu Piccho',
          class: 's-map-item--mac',
          url: '#'
        },
        {
          name: 'Valle Sagrado',
          class: 's-map-item--val',
          url: '#'
        },
        {
          name: 'Cusco',
          class: 's-map-item--cus',
          url: '#'
        }
      ]
    }
  ]
};

export const OUR_DESTINATIONS_TABS = [
  {
    tab: 'ver hoteles',
    href: '#',
    color: '#d69c4f'
  },
  {
    tab: 'ver mapa',
    href: '#',
    color: '#d69c4f',
    class: 'd-none d-md-block'
  },
  {
    tab: 'ver circuitos',
    href: '#',
    color: '#d69c4f'
  }
];
