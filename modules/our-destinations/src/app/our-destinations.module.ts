import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { NavbarModule } from '@ca/library';
import { CircuitDestinationsModule } from '../components/circuits-destination/circuits-destination.module';
import { DestinationsModule } from '../components/destinations/destinations.module';
import { MapDestinationsModule } from '../components/map-destinations/map-destinations.module';
import { OurDestinationsService } from '../providers/our-destinations.service';
import { OurDestinationsComponent } from './our-destinations.component';

@NgModule({
  declarations: [OurDestinationsComponent],
  imports: [
    BrowserModule,
    CommonModule,
    NavbarModule,
    MapDestinationsModule,
    CircuitDestinationsModule,
    BrowserAnimationsModule,
    SpinnerModule,
    HttpClientModule,
    NotificationModule,
    DestinationsModule
  ],
  providers: [
    OurDestinationsService
  ],
  entryComponents: [OurDestinationsComponent]
})
export class OurDestinationsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-our-destinations';
    const element = createCustomElement(OurDestinationsComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
