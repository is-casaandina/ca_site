import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import {
  DESTINATIONS_MAP,
  TYPE_DESCRIPTORS_ID
} from 'core/shared/src/settings/constants/general.constant';
import { IDescriptor, IDescriptorResponse, IDestinationMap } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { OurDestinationsService } from '../providers/our-destinations.service';
import { ECircuits, ICircuits, IDestination, IHotelDestination, IOurDestination } from './our-destinations';
import { OUR_DESTINATIONS_TABS } from './our-destinations.constant';

@Component({
  selector: 'ca-our-destinations-standalone',
  templateUrl: './our-destinations.component.html'
})
export class OurDestinationsComponent extends ComponentBase implements OnInit {

  @Input() circuitoNorteMoche: Array<IOurDestination>;
  @Input() circuitoLimaIca: Array<IOurDestination>;
  @Input() circuitoCusco: Array<IOurDestination>;
  @Input() circuitoSur: Array<IOurDestination>;

  descriptors: Array<IDescriptor> = [];
  destinationsMap: Array<IDestinationMap> = [];
  destinations: any = [];
  circuits: ICircuits = {};
  activeTab = 0;
  OUR_DESTINATIONS_TABS = OUR_DESTINATIONS_TABS;
  DESTINATIONS_MAP = DESTINATIONS_MAP;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private ourDestinationsServices: OurDestinationsService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.circuitoNorteMoche = camelcaseKeys(AngularUtil.toJson(this.circuitoNorteMoche, []));
      this.circuitoLimaIca = camelcaseKeys(AngularUtil.toJson(this.circuitoLimaIca, []));
      this.circuitoCusco = camelcaseKeys(AngularUtil.toJson(this.circuitoCusco, []));
      this.circuitoSur = camelcaseKeys(AngularUtil.toJson(this.circuitoSur, []));

      this.circuits[ECircuits.circuitoNorteMoche] =
        this.circuitoNorteMoche && this.circuitoNorteMoche.length && this.circuitoNorteMoche[0] || null;
      this.circuits[ECircuits.circuitoLimaIca] = this.circuitoLimaIca && this.circuitoLimaIca.length && this.circuitoLimaIca[0] || null;
      this.circuits[ECircuits.circuitoCusco] = this.circuitoCusco && this.circuitoCusco.length && this.circuitoCusco[0] || null;
      this.circuits[ECircuits.circuitoSur] = this.circuitoSur && this.circuitoSur.length && this.circuitoSur[0] || null;

      this.getHotelsByDestionations();
      this.getMapDestinations();
    }
  }

  private getMapDestinations(): void {
    this.ourDestinationsServices
      .getMapDestinations(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IDestinationMap>) => {
        this.ngZone.run(() => {
          this.destinationsMap = response;
        });
      });
  }

  private getDescriptors(): void {
    this.ourDestinationsServices
      .getDescriptors()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.descriptors = response.filter((des: IDescriptorResponse) => {
            return des.category === TYPE_DESCRIPTORS_ID.hotel;
          });
          this.destinations = this.addDescriptorToDestination();
        });
      });
  }

  private addDescriptorToDestination(): Array<IDestination> {
    return this.destinations.map((destination: IDestination) => {
      destination.hotels = destination.hotels.map((hotel: IHotelDestination) => {
        const descriptorInfo = this.descriptors.find((descriptor: IDescriptor) => descriptor.id === hotel.descriptorId);
        const regx = new RegExp(`\\b(${descriptorInfo.title})\\b`, 'gi');
        hotel.name = hotel.name.
          replace(regx,
            `<strong class="text-capitalize" style="color: ${descriptorInfo.color}">
            ${descriptorInfo.title.toLowerCase()} </strong>`);

        return {
          ...hotel,
          descriptorInfo
        };
      });

      return destination;
    });
  }

  private getHotelsByDestionations(): void {
    this.ourDestinationsServices
      .getHotelsByDestination()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IDestination) => {
        this.ngZone.run(() => {
          this.destinations = this.addStateToDestinations(response);
          this.getDescriptors();
        });
      });
  }

  private addStateToDestinations(destinations): IDestination {
    return destinations.map((destination: IDestination) => {
      const state = true;

      return {
        ...destination,
        state
      };
    });
  }

  onChangeTab(activeTab: number): void {
    this.activeTab = activeTab;
  }
}
