import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { OurDestinationsModule } from './app/our-destinations.module';

platformBrowserDynamic().bootstrapModule(OurDestinationsModule)
  .catch(err => console.error(err));
