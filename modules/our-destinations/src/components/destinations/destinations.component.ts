import { Component, Input } from '@angular/core';

@Component({
  selector: 'ca-destinations',
  templateUrl: './destinations.component.html'
})
export class DestinationsComponent {
  @Input() path: string;
  @Input() destinations: any;

  isCollapsed: boolean;

  constructor() {
    this.isCollapsed = false;
  }

  toggle(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  changeState(ev, destination): void {
    destination.state = !ev;
  }

}
