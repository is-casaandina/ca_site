import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { HotelAcordionModule } from '../hotel-acordion/hotel-acordion.module';
import { DestinationsComponent } from './destinations.component';

@NgModule({
  imports: [
    CommonModule,
    CollapseModule,
    HotelAcordionModule
  ],
  declarations: [DestinationsComponent],
  exports: [DestinationsComponent]
})
export class DestinationsModule {
}
