import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SafeHtmlPipeModule } from '@ca-core/shared/helpers/pipes';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { HotelAcordionComponent } from './hotel-acordion.component';

@NgModule({
  imports: [CommonModule, CollapseModule, SafeHtmlPipeModule],
  declarations: [HotelAcordionComponent],
  exports: [HotelAcordionComponent]
})
export class HotelAcordionModule { }
