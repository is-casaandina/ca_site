
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IDestination } from '../../app/our-destinations';

@Component({
  selector: 'ca-hotel-acordion',
  templateUrl: './hotel-acordion.component.html'
})
export class HotelAcordionComponent {
  @Input() destination: IDestination;
  @Output() updateSt = new EventEmitter<boolean>();

  state = false;

  updateState(): void {
    this.state = !this.state;
    this.updateSt.emit(this.state);
  }
}
