import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RateModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { MapDestinationsComponent } from './map-destinations.component';

@NgModule({
  imports: [CommonModule, FormsModule, RateModule, ElemBackgroundurlModule],
  declarations: [MapDestinationsComponent],
  exports: [MapDestinationsComponent]
})
export class MapDestinationsModule { }
