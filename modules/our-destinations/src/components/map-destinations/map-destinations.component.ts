import { Component, Input, NgZone } from '@angular/core';
import { fadeInOutAnimation } from '@ca-core/shared/helpers/animations/fade.animation';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { DESTINATIONS_MAP } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { CIRCUITS_INFO } from '../../app/our-destinations.constant';
import { environment } from '../../environments/environment';
import { OurDestinationsService } from '../../providers/our-destinations.service';

@Component({
  selector: 'ca-map-destinations',
  templateUrl: './map-destinations.component.html',
  animations: [fadeInOutAnimation(0, 0.5)]
})
export class MapDestinationsComponent extends UnsubscribeOnDestroy {
  @Input() active = false;
  @Input() mapDestinations: any;
  @Input() language: string;
  @Input() descriptors: Array<IDescriptor>;

  isActiveDetail = false;
  destinationInfo: any;
  hotelInfoActive: any;
  rating: any;
  descriptorColorActive: string;

  CIRCUITS_INFO = CIRCUITS_INFO;
  DESTINATIONS_MAP = DESTINATIONS_MAP;
  BUCKET_SYSTEM = environment.BUCKET_SYSTEM;

  constructor(
    private ourDestinationsServices: OurDestinationsService,
    private ngZone: NgZone
  ) {
    super();
  }

  openHotelDesc(destinationId: string, descriptorId: string, descriptorColor: string): void {
    this.descriptorColorActive = descriptorColor;
    this.isActiveDetail = false;
    this.hotelInfoActive = {};
    this.ourDestinationsServices
      .getDestinationInfo(destinationId, descriptorId, this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<any>) => {
        this.ngZone.run(() => {
          this.destinationInfo = response;
          if (response.length) {
            this.setActiveHotel(response[0]);
          }
        });
      });
  }

  setActiveHotel(hotel: any): void {
    this.isActiveDetail = true;
    this.hotelInfoActive = hotel;
    this.getRating();
  }

  getRating(): void {
    this.ourDestinationsServices
      .getRating(this.hotelInfoActive.revinate)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: any) => {
        this.ngZone.run(() => {
          this.rating = response;
        });
      });
  }
}
