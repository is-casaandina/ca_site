import { Component, Input } from '@angular/core';
import { ICircuits } from '../../app/our-destinations';
import { CIRCUITS_INFO } from '../../app/our-destinations.constant';
import { environment } from '../../environments/environment';

@Component({
  selector: 'ca-circuits-destination',
  templateUrl: './circuits-destination.component.html'
})
export class CircuitsDestinationComponent {
  @Input() active = false;
  @Input() circuits: ICircuits;

  circuitActiveIndex: number;
  circuitActive: {
    places: []
  };

  isOpenGeneralDetail: boolean;

  BUCKET_SYSTEM = environment.BUCKET_SYSTEM;
  CIRCUITS_INFO = CIRCUITS_INFO;

  setActive(circuit, index: number): void {
    this.circuitActiveIndex = index;
    circuit.name = this.circuits[circuit.key].title;
    circuit.description = this.circuits[circuit.key].description;
    circuit.backgroundImage = this.circuits[circuit.key].backgroundImage;
    circuit.urlLink = this.circuits[circuit.key].urlLink;
    circuit.textLink = this.circuits[circuit.key].textLink;
    this.circuitActive = circuit;
    this.isOpenGeneralDetail = true;
  }

  onCloseDetail(value): void {
    this.isOpenGeneralDetail = value;
  }

}
