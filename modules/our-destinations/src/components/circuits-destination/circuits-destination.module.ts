import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SectionHeaderModule } from '@ca/library';
import { MapCardDetailModule } from '../map-card-detail/map-card-detail.module';
import { CircuitsDestinationComponent } from './circuits-destination.component';

@NgModule({
  imports: [CommonModule, FormsModule, MapCardDetailModule, SectionHeaderModule],
  declarations: [CircuitsDestinationComponent],
  exports: [CircuitsDestinationComponent]
})
export class CircuitDestinationsModule {}
