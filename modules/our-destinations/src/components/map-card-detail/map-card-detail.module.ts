import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ImgSrcsetModule, UrlLinkModule } from '@ca/library/directives';
import { MapCardDetailComponent } from './map-card-detail.component';

@NgModule({
  imports: [CommonModule, FormsModule, ImgSrcsetModule, UrlLinkModule],
  declarations: [MapCardDetailComponent],
  exports: [MapCardDetailComponent]
})
export class MapCardDetailModule {}
