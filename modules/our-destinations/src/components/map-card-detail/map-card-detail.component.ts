import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ca-map-card-detail',
  templateUrl: './map-card-detail.component.html'
})
export class MapCardDetailComponent {
  @Input() data: any;
  @Input() path?: string;

  @Output() activeCard: EventEmitter<any> = new EventEmitter<boolean>();

  close(): void {
    this.activeCard.emit(false);
  }
}
