import { environment } from '../environments/environment';

export class OurDestinationsEndpoint {
  public static destinations = `${environment.API_URL}site/pages/destinations/hotels`;
  public static mapsDestinations = `${environment.API_URL}site/pages/destinations/map`;
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static destinationInfo = `${environment.API_URL}site/pages/destinations/view/maps`;
  public static ratings = `${environment.API_URL}api/ratings/{revinate}`;
}
