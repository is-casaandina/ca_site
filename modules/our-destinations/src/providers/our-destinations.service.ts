import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { OurDestinationsEndpoint } from './endpoints';

@Injectable()
export class OurDestinationsService {

  constructor(
    private apiService: ApiService
  ) { }

  getHotelsByDestination(): Observable<any> {
    return this.apiService.get(OurDestinationsEndpoint.destinations, { preloader: false});
  }

  getDescriptors(): Observable<any> {
    return this.apiService.get(OurDestinationsEndpoint.descriptors, { preloader: false});
  }

  getMapDestinations(language: string): Observable<any> {
    return this.apiService.get(OurDestinationsEndpoint.mapsDestinations, { preloader: false, params: {language}});
  }

  getDestinationInfo(destinationId: string, descriptorId: string, language: string): Observable<any> {
    return this.apiService.
      get(OurDestinationsEndpoint.destinationInfo, { preloader: false, params: {language, descriptorId, destinationId}});
  }

  getRating(revinate: string): Observable<any> {
    return this.apiService.
      get(OurDestinationsEndpoint.ratings, { preloader: false, params: {revinate}});
  }
}
