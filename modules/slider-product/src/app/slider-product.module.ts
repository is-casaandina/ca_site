import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CarouselCaptionModule, CarouselControlModule, CarouselModule, SectionHeaderModule } from '@ca/library';
import { DragScrollModule } from 'ngx-drag-scroll';
import { SliderProductService } from '../providers/slider-product.service';
import { SliderProductComponent } from './slider-product.component';

@NgModule({
  declarations: [
    SliderProductComponent
  ],
  imports: [
    BrowserModule,
    CarouselControlModule,
    DragScrollModule,
    CarouselCaptionModule,
    SectionHeaderModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    CarouselModule
  ],
  providers: [
    SliderProductService
  ],
  entryComponents: [SliderProductComponent]
})
export class SliderProductModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-slider-product';
    const element = createCustomElement(SliderProductComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
