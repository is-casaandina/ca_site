import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Component, ElementRef, Inject, Input, NgZone, OnInit, Renderer2, ViewChild } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { IDescriptor } from 'core/typings';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { takeUntil } from 'rxjs/operators';
import { SliderProductService } from '../providers/slider-product.service';

@Component({
  selector: 'ca-slider-product',
  templateUrl: './slider-product.component.html'
})
export class SliderProductComponent extends ComponentBase implements OnInit, AfterViewInit {
  @Input() title: string;

  private _sliders: Array<any>;
  @Input()
  set sliders(items: Array<any>) {
    this._sliders = items;
  }
  get sliders(): Array<any> {
    return this._sliders && typeof this._sliders === 'string' && JSON.parse(this._sliders as any) || (this._sliders || []);
  }

  @ViewChild('nav', { read: DragScrollComponent }) ds: DragScrollComponent;

  height = '';
  descriptorInfo: IDescriptor;
  elementWidthPx = '';
  elementWidthSum = 0;
  elementWidth = 0;

  constructor(
    protected elem: ElementRef,
    protected storageService: StorageService,
    private sliderProductServices: SliderProductService,
    private ngZone: NgZone,
    private renderer2: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.sliders = this.sliders.map((slide: any): any => {
        return camelcaseKeys(slide);
      });
      this.getDescriptor();
    }
  }

  ngAfterViewInit(): void {
    if (this.isVisible) {
      setTimeout(() => {
        this.height = this.getCarouselHeight();
      }, 100);
    }
  }

  clicked2(): void {
    this.elementWidthSum = this.elementWidthSum - this.elementWidth;
    this.elementWidthPx = `translateX(${this.elementWidthSum}px)`;
  }

  clicked1(): void {
    this.elementWidthSum = this.elementWidth + this.elementWidthSum;
    this.elementWidthPx = `translateX(${this.elementWidthSum}px)`;
  }

  getCarouselHeight(): string {
    const elements = this.elem.nativeElement.querySelectorAll('.carousel-item');
    const maxHeight: number = [].reduce.call(elements, (oldVal: number, elem) => {
      if (oldVal < elem.offsetHeight) {
        oldVal = elem.offsetHeight;
      }

      return oldVal;
    }, this.height.substr(0, 2));

    return `${maxHeight}px`;
  }

  getDescriptor(): void {
    this.sliderProductServices
      .getDescriptor(this.pageInfo.descriptorId)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IDescriptor) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response;
          this.setSectionColor();
        });
      });
  }

  setSectionColor(): void {
    const sectionElem = this.document.querySelector('#background-color-descriptor');
    const carouselAfterElem = this.elem.nativeElement.querySelector('.g-carousel-content-after');
    const carouselBeforeElem = this.elem.nativeElement.querySelector('.g-carousel-content-before');
    if (sectionElem) {
      this.renderer2.setStyle(sectionElem, 'background-color', this.descriptorInfo.color);
    }
    this.renderer2.setStyle(carouselAfterElem, 'background-color', this.descriptorInfo.color);
    this.renderer2.setStyle(carouselBeforeElem, 'background-color', this.descriptorInfo.color);
  }
}
