import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderProductModule } from './app/slider-product.module';

platformBrowserDynamic()
  .bootstrapModule(SliderProductModule)
  .catch(err => console.error(err));
