import { MAIN_ENV } from '@ca-site/environments/environment';

export const environment = {
  production: true,
  API_URL: MAIN_ENV.API_URL
};
