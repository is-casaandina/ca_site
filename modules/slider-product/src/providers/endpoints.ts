import { environment } from '../environments/environment';

export class SliderProductEndpoint {
  public static descriptors = `${environment.API_URL}site/descriptors/{descriptorId}`;
}
