import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { SliderProductEndpoint } from './endpoints';

@Injectable()
export class SliderProductService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptor(descriptorId: string): Observable<IDescriptorResponse> {
    return this.apiService.get(SliderProductEndpoint.descriptors, { preloader: false, params: {descriptorId} });
  }

}
