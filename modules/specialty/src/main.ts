import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SpecialityModule } from './app/specialty.module';

platformBrowserDynamic()
  .bootstrapModule(SpecialityModule)
  .catch(err => console.error(err));
