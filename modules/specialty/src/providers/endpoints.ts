import { environment } from '../environments/environment';

export class SpecialtyEndpoint {
  public static descriptors = `${environment.API_URL}site/descriptors/{descriptorId}`;
}
