import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { SpecialtyEndpoint } from './endpoints';

@Injectable()
export class SpecialtyService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptor(descriptorId: string): Observable<IDescriptorResponse> {
    return this.apiService.get(SpecialtyEndpoint.descriptors, { preloader: false, params: {descriptorId} });
  }

}
