import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CarouselCaptionModule, SectionHeaderModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { SpecialtyService } from '../providers/specialty.service';
import { SpecialityComponent } from './specialty.component';

@NgModule({
  declarations: [
    SpecialityComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    CarouselCaptionModule,
    PipesModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    ElemBackgroundurlModule
  ],
  providers: [
    SpecialtyService
  ],
  entryComponents: [
    SpecialityComponent
  ]
})
export class SpecialityModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
    const name = 'ca-specialty';
    const element = createCustomElement(SpecialityComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
