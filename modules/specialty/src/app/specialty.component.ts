import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IBackgroundImage, IDescriptor, IDescriptorResponse, IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { SpecialtyService } from '../providers/specialty.service';

@Component({
  selector: 'ca-specialty',
  templateUrl: './specialty.component.html'
})
export class SpecialityComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() description: string;
  private _backgroundImage: Array<IBackgroundImage>;
  @Input()
  set backgroundImage(backgroundImage: Array<IBackgroundImage>) {
    this._backgroundImage = backgroundImage;
  }
  get backgroundImage(): Array<IBackgroundImage> {
    return this._backgroundImage && typeof this._backgroundImage === 'string'
        && JSON.parse(this._backgroundImage as any) || (this._backgroundImage || []);
  }
  pageInfo: IPageInfo;
  descriptorInfo: IDescriptorResponse;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private specialtyService: SpecialtyService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getDescriptor();
    }
  }

  getDescriptor(): void {
    this.specialtyService
      .getDescriptor(this.pageInfo.descriptorId)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IDescriptor) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response;
        });
      });
  }

}
