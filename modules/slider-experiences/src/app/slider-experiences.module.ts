import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CardModule, CarouselControlModule, CarouselControlService, CarouselModule, SectionHeaderModule } from '@ca/library';
import { UrlLinkModule } from '@ca/library/directives';
import { ResizeService } from 'core/shared/src/helpers/util/resize.service';
import { ExperiencesService } from '../providers/experiences.service';
import { SliderExperiencesComponent } from './slider-experiences.component';

@NgModule({
  declarations: [SliderExperiencesComponent],
  imports: [
    BrowserModule,
    CommonModule,
    SectionHeaderModule,
    CardModule,
    CarouselModule,
    NotificationModule,
    CarouselControlModule,
    SpinnerModule,
    HttpClientModule,
    UrlLinkModule
  ],
  providers: [
    ExperiencesService,
    ResizeService,
    CarouselControlService
  ],
  entryComponents: [SliderExperiencesComponent]
})
export class SliderExperiencesModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-slider-experiences';
    const element = createCustomElement(SliderExperiencesComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
