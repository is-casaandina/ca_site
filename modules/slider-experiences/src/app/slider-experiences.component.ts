import { AfterViewInit, Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { groupCarousel } from '@ca-core/shared/helpers/util/group-carousel';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { CARD_TYPE } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor, IDescriptorResponse, IHotel } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { ExperiencesService } from '../providers/experiences.service';
import { IExperience } from './slider-experiences';

@Component({
  selector: 'ca-slider-experiences-standalone',
  templateUrl: './slider-experiences.component.html',
  styleUrls: ['./slider-experiences.component.scss']
})
export class SliderExperiencesComponent extends ComponentBase implements OnInit, AfterViewInit {
  @Input() title: string;
  @Input() sliders: any;
  @Input() textLink: string;
  @Input() urlLink: string;
  slidersNew: Array<IExperience> = [];
  descriptors: Array<IDescriptor> = [];
  slidersGroups: Array<Array<IExperience>> = [];
  height = '';
  CARD_TYPE = CARD_TYPE;
  hotels: Array<IHotel> = [];
  hotelsIds: Array<string> = [];
  elementWidthPx = '';
  elementWidthSum = 0;
  elementWidth = 0;

  groupSizeOne: Array<Array<IExperience>> = [];
  groupSizeTwo: Array<Array<IExperience>> = [];
  groupSizeThree: Array<Array<IExperience>> = [];

  width: number;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private experiencesServices: ExperiencesService,
    private ngZone: NgZone,
    private resizeService: ResizeService
  ) {
    super(
      elem,
      storageService
    );
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.resizeService.resizeEvent
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(width => {
          this.ngZone.run(() => {
            this.width = width;
            this.height = this.getCarouselHeight();
          });
        });
      this.sliders = AngularUtil.toJson(this.sliders, []);
      this.urlLink = AngularUtil.toJson(this.urlLink, {});
      this.initSliders();
    }
  }

  ngAfterViewInit(): void {
    this.getCarouselHeight();
  }

  initSliders(): void {
    let experiences = [];
    this.sliders
      .map(slider => camelcaseKeys(slider.experiences))
      .forEach(slider => experiences = experiences.concat(slider));
    this.hotelsIds = experiences.map(experience => experience.hotel);

    this.groupSizeOne = groupCarousel(1, experiences);
    this.groupSizeTwo = groupCarousel(2, experiences);
    this.groupSizeThree = groupCarousel(3, experiences);

    this.getHotels();
    this.getDescriptors();
    this.height = this.getCarouselHeight();
  }

  getDescriptors(): void {
    this.experiencesServices
      .getDescriptors()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.descriptors = response;
          this.groupSizeOne = this.addDescriptorToSlide(this.groupSizeOne);
          this.groupSizeTwo = this.addDescriptorToSlide(this.groupSizeTwo);
          this.groupSizeThree = this.addDescriptorToSlide(this.groupSizeThree);
        });
      });
  }

  getHotels(): void {
    this.experiencesServices
      .getHotels(this.hotelsIds)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((res: Array<IHotel>) => {
        this.ngZone.run(() => {
          this.hotels = res;
          this.groupSizeOne = this.addHotelToSlide(this.groupSizeOne);
          this.groupSizeTwo = this.addHotelToSlide(this.groupSizeTwo);
          this.groupSizeThree = this.addHotelToSlide(this.groupSizeThree);
        });
      });
  }

  getCarouselHeight(): string {
    const elements = this.elem.nativeElement.querySelectorAll('.g-carousel-content-wrapper');
    const maxHeight: number = [].reduce.call(elements, (oldVal: any, elem: any) => {
      if (oldVal < elem.offsetHeight) {
        oldVal = elem.offsetHeight;
      }

      return oldVal;
    }, this.height.substr(0, 2));

    return `${maxHeight}px`;
  }

  addDescriptorToSlide(sliders: Array<Array<IExperience>>): Array<Array<IExperience>> {
    return sliders.map(sliderGroup => {
      const experiences = sliderGroup.map(slider => {
        const descriptorInfo = this.descriptors.find((descriptor: IDescriptor) => descriptor.id === slider.descriptor);

        return {
          ...slider,
          descriptorInfo
        };
      });

      return experiences;
    });

  }

  addHotelToSlide(sliders: Array<Array<IExperience>>): Array<Array<IExperience>> {
    return sliders.map((sliderGroup: Array<IExperience>) => {
      const experiences = sliderGroup.map(slider => {
        const hotelInfo = this.hotels.find((hotel: IHotel) => hotel.value === slider.hotel) || {} as IHotel;

        return {
          ...slider,
          hotelName: hotelInfo.text
        };
      });

      return experiences;
    });
  }
}
