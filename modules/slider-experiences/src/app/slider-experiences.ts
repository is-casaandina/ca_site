import { IBackgroundProperty } from 'core/typings';

export interface IExperience {
  backgroundImage?: Array<IBackgroundProperty>;
  description?: string;
  descriptor?: string;
  hotel?: string;
  title?: string;
  hotelName?: string;
  urlItem?: string;
}
