import { environment } from '../environments/environment';

export class ExperiencesEndpoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static hotels = `${environment.API_URL}site/pages/title/ids`;
}
