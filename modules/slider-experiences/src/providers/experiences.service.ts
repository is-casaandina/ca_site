import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse, IHotel } from 'core/typings';
import { Observable } from 'rxjs';
import { ExperiencesEndpoint } from './endpoints';

@Injectable()
export class ExperiencesService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptors(showSpin = false): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(ExperiencesEndpoint.descriptors, { preloader: showSpin });
  }

  getHotels(items: Array<string>): Observable<Array<IHotel>> {
    return this.apiService.get(ExperiencesEndpoint.hotels, { preloader: false, params: {items} });
  }
}
