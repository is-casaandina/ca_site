import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderExperiencesModule } from './app/slider-experiences.module';

platformBrowserDynamic()
  .bootstrapModule(SliderExperiencesModule)
  .catch(err => console.error(err));
