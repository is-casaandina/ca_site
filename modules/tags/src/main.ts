import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TagsModule } from './app/tags.module';

platformBrowserDynamic()
  .bootstrapModule(TagsModule)
  .catch(err => console.error(err));
