import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { SectionHeaderModule, TagModule } from '@ca/library';
import { TagsComponent } from './tags.component';
@NgModule({
  declarations: [
    TagsComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    TagModule,
    SectionHeaderModule
  ],
  entryComponents: [
    TagsComponent
  ]
})
export class TagsModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-tags'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(TagsComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
