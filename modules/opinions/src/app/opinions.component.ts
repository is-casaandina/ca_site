import { AfterViewInit, Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { ResizeService, SCREEN_NAMES } from '@ca-core/shared/helpers/util/resize.service';
import { IDescriptor, IDescriptorResponse, IOpinion, IOpinionRate } from 'core/typings';
import { OpinionServices } from '../providers/opinion.service';

@Component({
  selector: 'ca-opinions',
  templateUrl: './opinions.component.html',
  styleUrls: ['./opinions.component.scss']
})
export class OpinionsComponent extends ComponentBase implements OnInit, AfterViewInit {
  @Input() badge1: string;
  @Input() badge2: string;
  @Input() badge3: string;
  @Input() badge4: string;
  @Input() title: string;

  elementWidthPx = '';
  elementWidthSum = 0;
  elementWidth = 0;
  opinionsGroup: Array<IOpinion> = [];
  opinionsRate: IOpinionRate;
  descriptorInfo: IDescriptor;
  height = '';
  updateWidth = false;

  constructor(
    protected elem: ElementRef,
    protected storageService: StorageService,
    private opinionServices: OpinionServices,
    private resizeService: ResizeService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getOpinions();
      this.getOpinionsRate();
      this.getDescriptor();
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.height = this.getCarouselHeight();
    }, 0);
  }

  getOpinions(): void {
    this.opinionServices.getOpinions(this.pageInfo.revinate)
      .subscribe(response => {
        this.ngZone.run(() => {
          this.opinionsGroup = this.groupOpinions(response);
          setTimeout(() => {
            this.height = this.getCarouselHeight();
            this.updateWidth = true;
          }, 100);
        });
      }
      );
  }

  getOpinionsRate(): void {
    this.opinionServices
      .getOpinionsRate(this.pageInfo.revinate)
      .subscribe((response: IOpinionRate) => {
        this.ngZone.run(() => {
          this.opinionsRate = response;
        });
      });
  }

  getCarouselHeight(): string {
    const elements = this.elem.nativeElement.querySelectorAll('.carousel-item');
    const maxHeight: number = [].reduce.call(elements, (oldVal, elem) => {
      if (oldVal < elem.offsetHeight) {
        oldVal = elem.offsetHeight;
      }

      return oldVal;
    }, this.height.substr(0, 2));

    return `${maxHeight}px`;
  }

  getSliderLength(): number {
    const sliderLength: number =
      (this.resizeService.is(SCREEN_NAMES.XS) || this.resizeService.is(SCREEN_NAMES.SM) || this.resizeService.is(SCREEN_NAMES.MD))
        ? 1
        : 4;

    return sliderLength;
  }

  groupOpinions(opinions: Array<IOpinion>): Array<any> {
    const length = this.getSliderLength();
    const opinionsGrouped = opinions.reduce((oldVal: any, newVal: any, index: number, items: Array<any>) => {
      if (index % length === 0) {
        oldVal.push(items.slice(index, index + length));
      }

      return oldVal;
    }, []);

    return opinionsGrouped;
  }

  getDescriptor(): void {
    this.opinionServices
      .getDescriptor()
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response.find(descriptor => descriptor.id === this.pageInfo.descriptorId);
        });
      });
  }
}
