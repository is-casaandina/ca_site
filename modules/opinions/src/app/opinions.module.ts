import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { I18nEsPeModule } from '@ca-core/shared/helpers/i18n-es-pe';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import {
  AwardModule,
  CarouselControlModule,
  CarouselControlService,
  CarouselModule,
  OpinionModule,
  RateModule,
  SectionHeaderModule
} from '@ca/library';
import { OpinionServices } from '../providers/opinion.service';
import { OpinionsComponent } from './opinions.component';

@NgModule({
  declarations: [
    OpinionsComponent
  ],
  imports: [
    BrowserModule,
    NotificationModule,
    SpinnerModule,
    HttpClientModule,
    AwardModule,
    RateModule,
    SectionHeaderModule,
    OpinionModule,
    CarouselControlModule,
    I18nEsPeModule,
    CarouselModule
  ],
  providers: [
    OpinionServices,
    CarouselControlService
  ],
  entryComponents: [
    OpinionsComponent
  ]
})
export class OpinionsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-opinions';
      const element = createCustomElement(OpinionsComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
