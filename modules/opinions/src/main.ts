import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { OpinionsModule } from './app/opinions.module';

platformBrowserDynamic()
  .bootstrapModule(OpinionsModule)
  .catch(err => console.error(err));
