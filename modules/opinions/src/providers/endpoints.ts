import { environment } from '../environments/environment';

export class OpinionEndPoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static ratings = `${environment.API_URL}api/ratings/{revinate}`;
  public static opinions = `${environment.API_URL}api/opinions/{revinate}`;
}
