import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { OpinionEndPoint } from './endpoints';

@Injectable()
export class OpinionServices {

  constructor(
    private apiService: ApiService
  ) { }

  getOpinionsRate(revinate: string): Observable<any> {
    return this.apiService.get(OpinionEndPoint.ratings, {params: {revinate}});
  }

  getDescriptor(): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(OpinionEndPoint.descriptors, { preloader: false });
  }

  getOpinions(revinate: string): Observable<any> {
    return this.apiService.get(OpinionEndPoint.opinions, {params: {revinate}});
  }

}
