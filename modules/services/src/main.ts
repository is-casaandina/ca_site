import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ServicesModule } from './app/services.module';

platformBrowserDynamic()
  .bootstrapModule(ServicesModule)
  .catch(err => console.error(err));
