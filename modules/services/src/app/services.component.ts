import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { IDescriptorResponse } from '@ca-admin/statemanagement/models/descriptor.interface';
import { IMAGE_SIZE } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { GalleryModalComponent, IBenefit } from '@ca/library';
import {
  IDescriptor,
  IPageInfo,
  IService,
  IServiceRequest
} from 'core/typings';
import { ServiceServices } from '../providers/service.service';

@Component({
  selector: 'ca-services',
  templateUrl: './services.component.html'
})
export class ServicesComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() backgroundImage: any;
  @Input() backgroundVideo: any;
  @Input() items: any;
  pageInfo: IPageInfo;
  descriptorInfo: IDescriptor;
  features: Array<IBenefit>;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private serviceServices: ServiceServices,
    private ngZone: NgZone,
    private modalService: ModalService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();

    if (this.isVisible) {
      try {
        this.backgroundImage = this.backgroundImage ? JSON.parse(this.backgroundImage) : [];
        this.backgroundVideo = this.backgroundVideo ? JSON.parse(this.backgroundVideo) : {};
        this.items = this.items ? JSON.parse(this.items) : [];
      } catch (error) {
        console.error(error);
      }

      this.pageInfo = JSON.parse(this.storageService.getItem(STORAGE_KEY.pageInfo));
      this.getServices();
      this.getDescriptor();
    }
  }

  getServices(): void {
    const params: IServiceRequest = {
      language: this.pageInfo.language,
      items: this.items.map((item: any) => item.service)
    };

    this.serviceServices
      .getServices(params)
      .subscribe((services: Array<IService>) => {
        this.ngZone.run(() => {
          this.features = services.map(service => {
            return {
              icon: service.icon,
              title: service.text
            };
          });
        });
      });
  }

  getDescriptor(): void {
    this.serviceServices
      .getDescriptor()
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response.find(
            descriptor => descriptor.id === this.pageInfo.descriptorId
          );
        });
      });
  }

  openGallery(): void {
    if (this.backgroundVideo && this.backgroundVideo.url) {
      const modalConfig = {
        classDialog: 'multimedia',
        size: IMAGE_SIZE.lg
      };

      const modalRef = this.modalService.open(GalleryModalComponent, modalConfig);
      modalRef.setPayload({
        type: 'video',
        data: this.backgroundVideo
      });
      modalRef.result
        .then(() => {
        })
        .catch(err => {

        });
    }
  }
}
