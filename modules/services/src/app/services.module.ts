import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { GalleryModalModule } from '@ca/library';
import { ServiceModule } from '../components/service/service.module';
import { ServiceServices } from '../providers/service.service';
import { ServicesComponent } from './services.component';

@NgModule({
  declarations: [
    ServicesComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NotificationModule,
    SpinnerModule,
    HttpClientModule,
    ServiceModule,
    GalleryModalModule,
    ModalModule
  ],
  providers: [
    ServiceServices
  ],
  entryComponents: [
    ServicesComponent
  ]
})
export class ServicesModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-services'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(ServicesComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
