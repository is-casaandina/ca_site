import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse, IService, IServiceRequest } from 'core/typings';
import { Observable } from 'rxjs';
import { ServiceEndPoint } from './endpoints';

@Injectable()
export class ServiceServices {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptor(): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(ServiceEndPoint.descriptors, { preloader: false});
  }

  getServices(queryParams: IServiceRequest): Observable<Array<IService>> {
    return this.apiService.get(ServiceEndPoint.services, { preloader: false, params: queryParams});
  }
}
