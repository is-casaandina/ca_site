import { environment } from '../environments/environment';

export class ServiceEndPoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static services = `${environment.API_URL}site/services`;
}
