import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderListPromotionsModule } from './app/slider-list-promotions.module';

platformBrowserDynamic()
  .bootstrapModule(SliderListPromotionsModule)
  .catch(err => console.error(err));
