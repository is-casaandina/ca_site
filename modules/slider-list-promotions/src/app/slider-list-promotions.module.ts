import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselControlModule, CarouselModule, SectionHeaderModule } from 'core/lib/src';
import { ElemBackgroundurlModule, PromoDetailModule, UrlLinkModule } from 'core/lib/src/directives';
import { NotificationModule } from 'core/shared/src/helpers/notification';
import { SpinnerModule } from 'core/shared/src/helpers/spinner';
import { SliderListPromotionService } from '../providers/slider-list-promotions.service';
import { SliderListPromotionsComponent } from './slider-list-promotions.component';

@NgModule({
  declarations: [
    SliderListPromotionsComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    CarouselModule,
    CarouselControlModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    UrlLinkModule,
    ElemBackgroundurlModule,
    PromoDetailModule
  ],
  providers: [
    SliderListPromotionService
  ],
  entryComponents: [SliderListPromotionsComponent]
})
export class SliderListPromotionsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-slider-list-promotions';
    const element = createCustomElement(SliderListPromotionsComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
