import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { groupCarousel } from '@ca-core/shared/helpers/util/group-carousel';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { getStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import { AngularUtil, StorageService } from 'core/shared/src/helpers/util';
import { ResizeService } from 'core/shared/src/helpers/util/resize.service';
import { SEE_PROMOTIONS } from 'core/shared/src/settings/constants/general.constant';
import { IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { SliderListPromotionService } from '../providers/slider-list-promotions.service';

@Component({
  selector: 'ca-slider-list-promotions',
  templateUrl: './slider-list-promotions.component.html'
})
export class SliderListPromotionsComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() sliders: any;
  height = '';
  slidersGroups: Array<Array<any>> = [];
  promotionsIds: Array<string> = [];
  pageInfo: IPageInfo;
  SEE_PROMOTIONS = SEE_PROMOTIONS;

  groupSizeOne: Array<Array<any>> = [];
  groupSizeTwo: Array<Array<any>> = [];
  groupSizeThree: Array<Array<any>> = [];

  width: number;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private resizeService: ResizeService,
    private sliderLisPromotionService: SliderListPromotionService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    const slidersTemp =  AngularUtil.toJson(this.sliders, []);
    this.sliders = [];
    if (this.isVisible) {
      this.sliders = slidersTemp;
      this.groupExperiencesNormal();
      this.resizeService.resizeEvent
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(width => {
        this.ngZone.run(() => {
          this.width = width;
          this.height = this.getCarouselHeight();
        });
      });
      this.getPromotions();
    }
  }

  getCarouselHeight(): string {
    const elements = this.elem.nativeElement.querySelectorAll('.g-carousel-content-wrapper');
    const maxHeight: number = [].reduce.call(elements, (oldVal: any, elem: any) => {
      if (oldVal < elem.offsetHeight) {
        oldVal = elem.offsetHeight;
      }

      return oldVal;
    }, this.height.substr(0, 2));

    return `${maxHeight}px`;
  }

  groupExperiencesNormal(): void {
    this.slidersGroups = this.sliders.map((group: any) => {
      return group.promotions.map(promotion => {
        this.promotionsIds.push(promotion.promotion);

        return camelcaseKeys(promotion);
      });
    });
  }

  groupExperiencesMobile(): void {
    this.sliders.forEach((group: any) => {
      group.promotions.forEach(promotion => {
        this.promotionsIds.push(promotion.promotion);

        const newItem = [camelcaseKeys(promotion)];
        this.slidersGroups.push(newItem as any);
      });

    });
  }

  getPromotions(): void {
    this.sliderLisPromotionService
      .getPromotions(this.promotionsIds, this.language, getStorageCurrency())
      .subscribe((res: Array<any>) => {
        this.ngZone.run(() => {
          this.slidersGroups = this.addPromotionInfoToSlide(res);
          this.groupSizeOne = groupCarousel(1, this.slidersGroups);
          this.groupSizeTwo = groupCarousel(2, this.slidersGroups);
          this.groupSizeThree = groupCarousel(3, this.slidersGroups);
        });
      });
  }

  addPromotionInfoToSlide(res: Array<any>): Array<any> {
    const promotions = [];
    this.slidersGroups.forEach((sliderGroup: Array<any>) => {
      sliderGroup.forEach(slider => {
        const promotionData = res.find((item: any) => item.id === slider.promotion);

        promotions.push({
          ...promotionData,
          imagePromotion: promotionData
          && promotionData.infoPromotion
          && promotionData.infoPromotion.imagePromotion
            ? promotionData.infoPromotion.imagePromotion[0]
            : {}
        });
      });

    });

    return promotions;
  }
}
