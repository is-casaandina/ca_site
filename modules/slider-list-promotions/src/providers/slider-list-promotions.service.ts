import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { SliderListPromotionEndpoint } from './endpoints';

@Injectable()
export class SliderListPromotionService {

  constructor(
    private apiService: ApiService
  ) { }

  getPromotions(promotionsIds, language: string, currency: string, showSpin = false): Observable<Array<any>> {
    return this.apiService
      .get(SliderListPromotionEndpoint.promotions, { preloader: showSpin, params: { promotions: promotionsIds, language, currency } });
  }

}
