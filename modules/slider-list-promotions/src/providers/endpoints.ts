import { environment } from '../environments/environment';

export class SliderListPromotionEndpoint {
  public static promotions = `${environment.API_URL}site/pages/promotions/bells`;
}
