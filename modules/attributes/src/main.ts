import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AttributesModule } from './app/attributes.module';

platformBrowserDynamic()
  .bootstrapModule(AttributesModule)
  .catch(err => console.error(err));
