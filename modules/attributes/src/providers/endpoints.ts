import { environment } from '../environments/environment';

export class AttributesEndpoint {
  public static descriptors = `${environment.API_URL}site/descriptors/{descriptorId}`;
}
