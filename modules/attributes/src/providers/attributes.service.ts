import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { AttributesEndpoint } from './endpoints';

@Injectable()
export class AttributesService {

  constructor(
    private apiService: ApiService
  ) { }

  getDescriptor(descriptorId: string): Observable<IDescriptorResponse> {
    return this.apiService.get(AttributesEndpoint.descriptors, { preloader: false, params: {descriptorId} });
  }

}
