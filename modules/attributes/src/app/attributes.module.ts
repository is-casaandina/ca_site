import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { AttributesService } from '../providers/attributes.service';
import { AttributesComponent } from './attributes.component';

@NgModule({
  declarations: [
    AttributesComponent
  ],
  imports: [
    BrowserModule,
    SpinnerModule,
    HttpClientModule,
    NotificationModule,
    PipesModule
  ],
  providers: [
    AttributesService
  ],
  entryComponents: [
    AttributesComponent
  ]
})
export class AttributesModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-attributes';
      const element = createCustomElement(AttributesComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
