import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor, IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { AttributesService } from '../providers/attributes.service';

@Component({
  selector: 'ca-attributes',
  templateUrl: './attributes.component.html'
})
export class AttributesComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  private _backgroundImage: Array<any>;
  @Input()
  set backgroundImage(backgroundImage: Array<any>) {
    this._backgroundImage = backgroundImage;
  }
  get backgroundImage(): Array<any> {
    return this._backgroundImage && typeof this._backgroundImage === 'string'
        && JSON.parse(this._backgroundImage as any) || (this._backgroundImage || []);
  }
  private _items: Array<any>;
  @Input()
  set items(items: Array<any>) {
    this._items = items;
  }
  get items(): Array<any> {
    return this._items && typeof this._items === 'string' && JSON.parse(this._items as any) || (this._items || []);
  }

  pageInfo: IPageInfo;
  descriptorInfo: IDescriptor;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private attributesService: AttributesService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.pageInfo = this.storageService.getItemObject(STORAGE_KEY.pageInfo);
      this.getDescriptor();
    }
  }

  getDescriptor(): void {
    this.attributesService
      .getDescriptor(this.pageInfo.descriptorId)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IDescriptor) => {
        this.ngZone.run(() => {
          this.descriptorInfo = response;
        });
      });
  }
}
