import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { NavbarModule, SectionHeaderModule } from '@ca/library';
import { UrlLinkModule } from '@ca/library/directives';
import { HotelDestinationService } from '../providers/hotel-destination.service';
import { HotelDestinationComponent } from './hotel-destination.component';

@NgModule({
  declarations: [
    HotelDestinationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    SectionHeaderModule,
    NavbarModule,
    UrlLinkModule
  ],
  providers: [
    HotelDestinationService
  ],
  entryComponents: [
    HotelDestinationComponent
  ]
})
export class HotelDestinationModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-hotel-destination'; // Nombre del selector que leera la aplicacion principal
      const element = createCustomElement(HotelDestinationComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
