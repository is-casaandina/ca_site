import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { GENERAL_TEXT, RESERVE_ACTIONS } from '@ca-admin/settings/constants/general.lang';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { getStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import { ITab } from '@ca/library';
import { HotelDestinationService } from '../providers/hotel-destination.service';
@Component({
  selector: 'ca-hotel-destination',
  templateUrl: './hotel-destination.component.html'
})
export class HotelDestinationComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  hotelsDescriptors: any = [];
  tabs: ITab;
  activeParentTab = 0;
  activeChildTab = 0;
  activeHotel: any;
  activeHotelDescriptor: any;
  childTabs: any = [];
  colorInfo: any;
  RESERVE_ACTIONS = RESERVE_ACTIONS;
  GENERAL_TEXT = GENERAL_TEXT;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private hotelDestinationService: HotelDestinationService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.getHotels();
    }
  }

  private getHotels(): void {
    this.hotelDestinationService.getHotels(this.pageInfo.pageId, this.language)
      .subscribe(res => {
        this.ngZone.run(() => {
          this.hotelsDescriptors = res;
          this.getTabs();
          this.onChangeTab(0);
        });
      });
  }

  private getTabs(): void {
    this.tabs = this.hotelsDescriptors.map((descriptor: any) => {
      return {
        tab: descriptor.descriptorName,
        color: descriptor.descriptorColor
      };
    });
  }

  onChangeTab(activeTab: number): void {
    this.activeParentTab = activeTab;

    if (this.hotelsDescriptors.length) {
      this.activeHotelDescriptor = this.hotelsDescriptors[this.activeParentTab];
      this.activeChildTab = 0;
      this.colorInfo = {color: this.activeHotelDescriptor.descriptorColor};
      this.childTabs = this.activeHotelDescriptor.hotels.map((hotel: any) => {
          return {
            tab: hotel.name
          };
      });
      this.onChangeChildTab(0);
    }
  }

  onChangeChildTab(activeTab: number): void {
    this.activeChildTab = activeTab;

    if (this.activeHotelDescriptor.hotels.length) {
      this.activeHotel = this.activeHotelDescriptor.hotels[this.activeChildTab];
      this.getRoibackInfo();
    }
  }

  getRoibackInfo(): void {
    if (this.activeHotel && !this.activeHotel.roibackInfo) {
      this.hotelDestinationService.getRoibackInfo(this.activeHotel.roiback, this.language, getStorageCurrency())
        .subscribe((res: any) => {
          this.ngZone.run(() => {
            this.activeHotel.roibackInfo = res;
            this.getTabs();
          });
        });
    }
  }
}
