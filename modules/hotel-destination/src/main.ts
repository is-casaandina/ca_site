import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HotelDestinationModule } from './app/hotel-destination.module';

platformBrowserDynamic()
  .bootstrapModule(HotelDestinationModule)
  .catch(err => console.error(err));
