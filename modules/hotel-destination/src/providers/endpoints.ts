import { environment } from '../environments/environment';

export class HotelDestinationEndpoint {
  public static hotels = `${environment.API_URL}site/pages/{id}/hotels`;
  public static dataRoiback = `${environment.API_URL}site/pages/roiback/reservations`;
}
