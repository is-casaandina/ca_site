import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { HotelDestinationEndpoint } from './endpoints';

@Injectable()
export class HotelDestinationService {

  constructor(
    private apiService: ApiService
  ) { }

  getHotels(id: string, language: string): Observable<Array<any>> {
    return this.apiService.get(HotelDestinationEndpoint.hotels, { preloader: false, params: {id, language}});
  }

  getRoibackInfo(roiback: string, language: string, currencyIso: string): Observable<Array<any>> {
    return this.apiService.get(HotelDestinationEndpoint.dataRoiback, { preloader: false, params: {roiback, language, currencyIso}});
  }

}
