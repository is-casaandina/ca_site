import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { AngularUtil, StorageService } from 'core/shared/src/helpers/util';
import { IBackgroundImage, Ilink } from 'core/typings';

@Component({
  selector: 'ca-information-brand',
  templateUrl: './information-brand.component.html'
})
export class InformationBrandComponent extends ComponentBase implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() textLink: string;
  @Input() urlLink: Ilink;
  @Input() backgroundImage: IBackgroundImage;
  @Input() detailImage: IBackgroundImage;
  @Input() badge1: string;
  @Input() badge2: string;
  @Input() badge3: string;
  @Input() badge4: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();

    if (this.isVisible) {
      this.backgroundImage = this.backgroundImage ? AngularUtil.toJson(this.backgroundImage as string) : [];
      this.detailImage = this.detailImage ? AngularUtil.toJson(this.detailImage as string) : [];
      this.urlLink = AngularUtil.toJson(this.urlLink);
    }
  }
}
