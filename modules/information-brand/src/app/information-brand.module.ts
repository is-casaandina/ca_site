import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { AwardModule, BenefitModule, SectionHeaderModule } from '@ca/library';
import { UrlLinkModule } from 'core/lib/src/directives';
import { InformationBrandComponent } from './information-brand.component';

@NgModule({
  declarations: [
    InformationBrandComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    SectionHeaderModule,
    AwardModule,
    BenefitModule,
    UrlLinkModule
  ],
  entryComponents: [
    InformationBrandComponent
  ]
})
export class InformationBrandModule {
  constructor(private injector: Injector) {}

	 ngDoBootstrap() {
		const name = 'ca-information-brand';
		const element = createCustomElement(InformationBrandComponent, { injector: this.injector });
		customElements.define(name, element);
	}
}
