import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { InformationBrandModule } from './app/information-brand.module';

platformBrowserDynamic()
  .bootstrapModule(InformationBrandModule)
  .catch(err => console.error(err));
