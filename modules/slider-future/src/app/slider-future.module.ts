import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { CarouselCaptionModule, CarouselControlModule, CarouselModule, SectionHeaderModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { SliderFutureComponent } from './slider-future.component';

@NgModule({
  declarations: [
    SliderFutureComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    CarouselModule,
    CarouselControlModule,
    CarouselCaptionModule,
    ElemBackgroundurlModule,
    PipesModule
  ],
  entryComponents: [SliderFutureComponent]
})
export class SliderFutureModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-slider-future';
      const element = createCustomElement(SliderFutureComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
