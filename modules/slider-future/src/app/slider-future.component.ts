import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { SLD_RESTAURANT_INPUTS } from '@ca-core/shared/settings/constants/general.constant';

@Component({
  selector: 'ca-slider-future',
  templateUrl: './slider-future.component.html'
})
export class SliderFutureComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() sliders: Array<any>;
  SLD_RESTAURANT_INPUTS = SLD_RESTAURANT_INPUTS;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    this.sliders = AngularUtil.toJson(this.sliders, []);
    this.sliders = this.sliders.map((slide: any): any => {
      return camelcaseKeys(slide);
    });
  }
}
