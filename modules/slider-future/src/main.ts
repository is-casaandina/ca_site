import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderFutureModule } from './app/slider-future.module';


platformBrowserDynamic()
  .bootstrapModule(SliderFutureModule)
  .catch(err => console.error(err));
