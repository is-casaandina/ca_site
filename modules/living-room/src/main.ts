import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { LivingRoomModule } from './app/living-room.module';

platformBrowserDynamic()
  .bootstrapModule(LivingRoomModule)
  .catch(err => console.error(err));
