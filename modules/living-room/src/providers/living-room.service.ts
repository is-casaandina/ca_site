import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { ICombo } from 'core/typings';
import { Observable } from 'rxjs';
import { IDestinationHotelRequest, IDestinationHotelResponse } from '../app/living-room';
import { LivingRoomEndPoint } from './endpoints';

@Injectable()
export class LivingRoomServices {

  constructor(
    private apiService: ApiService
  ) { }

  getDestinationHotel(params: IDestinationHotelRequest): Observable<IDestinationHotelResponse> {
    return this.apiService.post(LivingRoomEndPoint.destination, params);
  }

  getDestinations(items: Array<string>): IObservableArray<ICombo> {
    return this.apiService.get(LivingRoomEndPoint.destinations, {
      params: {
        items
      }
    });
  }
}
