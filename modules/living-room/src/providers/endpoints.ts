import { environment } from '../environments/environment';

export class LivingRoomEndPoint {
  public static destination = `${environment.API_URL}site/pages/events/destinations/hotels/living`;
  public static destinations = `${environment.API_URL}site/events/destinations`;
}
