import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IMAGE_SIZE } from '@ca-core/shared/settings/constants/general.constant';
import { FormQuotationComponent, GalleryModalComponent, ITab } from '@ca/library';
import { ICombo, ILivingRooms } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { environment } from '../environments/environment.qa';
import { LivingRoomServices } from '../providers/living-room.service';
import { IDestinationHotelItemResponse, IDestinationHotelRequest, IDestinationHotelResponse, ILivingItem } from './living-room';

@Component({
  selector: 'ca-living-room',
  templateUrl: './living-room.component.html'
})
export class LivingRoomComponent extends ComponentBase implements OnInit {
  @Input() hotels: Array<ILivingRooms> = [];
  destinationActive: ICombo;
  destinationInfoActive: IDestinationHotelResponse;
  destinationsId: Array<string>;
  destinations: Array<ICombo> = [];
  isCollapsed = true;
  isCollapsed2 = true;
  hotelActive: IDestinationHotelItemResponse;
  hotelTabs: Array<ITab>;
  activeTab = 0;
  livingActive: ILivingItem;
  activeImageIdx = 0;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private livingRoomService: LivingRoomServices,
    private modalService: ModalService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    if (this.isVisible) {
      this.hotels = AngularUtil.toJson(this.hotels);
      this.destinationActive = this.hotels.length
        ? { value: this.hotels[0].destination, text: this.hotels[0].destination }
        : {};
      if (!this.destinationInfoActive) {
        this.getDestinationInfo();
        this.destinationsId = this.hotels.map(hotel => hotel.destination);
        this.getDestinations();
      }
    }
  }

  getDestinationsIds(): Array<string> {
    return this.destinationsId = this.hotels.map(hotel => hotel.destination);
  }

  getDestinations(): void {
    this.livingRoomService
      .getDestinations(this.destinationsId)
      .subscribe(res => {
        this.ngZone.run(() => {
          this.destinations = res;
        });
      });
  }

  selectDestination(item: ICombo): void {
    this.destinationActive = item;
    this.getDestinationInfo();
  }

  selectingHotel(index: number): void {
    this.activeTab = index;
    this.livingActive = {} as ILivingItem;
    this.hotelActive = this.destinationInfoActive
      ? this.destinationInfoActive.hotels[index]
      : {} as IDestinationHotelItemResponse;
  }

  selectingLiving(living: ILivingItem): void {
    this.livingActive = living;
    this.activeImageIdx = 0;
  }

  getDestinationInfo(): void {
    const holetCategories = this.hotels.filter(d => d.destination === this.destinationActive.value)
      .map(x => ({ hotelId: x.hotel, category: x.category }));

    const params: IDestinationHotelRequest = {
      destinationId: this.destinationActive.value,
      language: this.language,
      hotelCategories: holetCategories
    };

    this.livingRoomService
      .getDestinationHotel(params)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.ngZone.run(() => {
          this.destinationInfoActive = res;
          this.getHotelTabs();
        });
      });
  }

  getHotelTabs(): void {
    this.hotelTabs = (this.destinationInfoActive && this.destinationInfoActive.hotels.length)
      ? this.destinationInfoActive.hotels.map(hotel => ({ tab: hotel.nameHotel }))
      : [];
    this.selectingHotel(0);
  }

  setActiveImage(index: number): void {
    this.activeImageIdx = index;
  }

  openForm(): void {
    const modalRef = this.modalService.open(FormQuotationComponent, { size: MODAL_BREAKPOINT.lg });
    const activeHotel = this.destinationInfoActive.hotels[this.activeTab];
    modalRef.setPayload({
      recaptchaKey: environment.CAPTCHA_KEY,
      hotels: [{
        text: activeHotel.nameHotel,
        value: activeHotel.id
      }]
    });
  }

  openTour(): void {
    const modalConfig = {
      classDialog: 'multimedia',
      size: IMAGE_SIZE.lg
    };

    const payload = {
      type: 'iframe',
      data: this.hotelActive ? this.hotelActive.urlImage360 : ''
    };

    const modalRef = this.modalService.open(GalleryModalComponent, modalConfig);
    modalRef.setPayload(payload);
    modalRef.result
      .then(() => {
      })
      .catch(err => {
      });
  }
}
