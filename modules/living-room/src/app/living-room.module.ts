import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { FormQuotationModule, GalleryModalModule, NavbarModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { OurLivingRoomServices } from 'modules/our-living-room/src/providers/our-living-room.services';
import { LivingSelectModule } from '../components/living-select.module';
import { LivingRoomServices } from '../providers/living-room.service';
import { LivingRoomComponent } from './living-room.component';

@NgModule({
  declarations: [
    LivingRoomComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule,
    CollapseModule,
    BrowserAnimationsModule,
    NavbarModule,
    ElemBackgroundurlModule,
    LivingSelectModule,
    ModalModule,
    GalleryModalModule,
    FormQuotationModule
  ],
  providers: [
    LivingRoomServices,
    OurLivingRoomServices
  ],
  entryComponents: [LivingRoomComponent]
})
export class LivingRoomModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-living-room';
      const element = createCustomElement(LivingRoomComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
