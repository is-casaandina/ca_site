import { IBackgroundImage } from '@core/typings';

export interface IDestinationHotelRequest {
  destinationId: string;
  language: string;
  hotelCategories: Array<{ hotelId: string, category: string }>;
}

export interface IDestinationHotelResponse {
  destinationName: string;
  hotels: Array<IDestinationHotelItemResponse>;
}

export interface IDestinationHotelItemResponse {
  id: string;
  nameHotel: string;
  urlImage360: string;
  description: string;
  livings: Array<ILivingItem>;
}

export interface ILivingItem {
  name: string;
  sliders: Array<ILivingSlider>;
}

export interface ILivingSlider {
  titleEs: string;
  titleEn: string;
  titlePt: string;
  images: Array<IBackgroundImage>;
}
