import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ICombo } from '@core/typings';

@Component({
  selector: 'ca-living-select',
  templateUrl: './living-select.component.html'
})
export class LivingSelectComponent implements OnInit {
  @Input() isCollapsed = true;
  @Output() selecting: EventEmitter<ICombo> = new EventEmitter();
  @Input() buttonClass: string;
  @Input() valueKey: string;
  @Input() initVal: string;
  @Input() items: Array<ICombo>;
  itemActive: ICombo;

  constructor(
    private cdRef: ChangeDetectorRef
  ) {

  }
  ngOnInit(): void {
    setTimeout(() => {
      if (this.items && this.items.length && this.valueKey) {
        const index = this.items.findIndex(item => item[this.valueKey] === this.initVal);
        this.selectItem(index);
      } else {
        this.selectItem(0);
      }
    });
    this.cdRef.detectChanges();
  }

  displayList(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  selectItem(index: number): void {
    this.itemActive = this.items[index];
    this.selecting.emit(this.itemActive);
    this.isCollapsed = true;
  }

}
