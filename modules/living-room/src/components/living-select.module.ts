import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { HoverCollapseModule } from '@ca/library/directives';
import { LivingSelectComponent } from './living-select.component';

@NgModule({
  declarations: [
    LivingSelectComponent
  ],
  imports: [
    CommonModule,
    CollapseModule,
    ModalModule,
    HoverCollapseModule
  ],
  exports: [LivingSelectComponent]
})
export class LivingSelectModule {
}
