import { MAIN_ENV } from '@ca-site/environments/environment.prod';

export const environment = {
  production: true,
  CAPTCHA_KEY: MAIN_ENV.CAPTCHA_KEY,
  API_URL: MAIN_ENV.API_URL
};
