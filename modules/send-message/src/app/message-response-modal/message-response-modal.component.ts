import { Component, OnInit } from '@angular/core';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NOTIFICATION_TYPES } from '@ca-core/shared/helpers/notification';

@Component({
  selector: 'ca-message-response-modal',
  template: `
    <div class="g-modal-response text-center">
      <div [ngClass]="payload.response">
        <span class="la" [ngClass]="NOTIFICATION_TYPES[payload.response]?.icon"></span>
        <p>{{ payload?.text }}</p>
      </div>
    </div>
  `
})
export class MessageResponseModalComponent implements OnInit, IPayloadModalComponent {

  payload: any;
  NOTIFICATION_TYPES = NOTIFICATION_TYPES;

  constructor(private activeModal: ActiveModal) {}

  ngOnInit(): void {
    setTimeout(() => this.activeModal.dismiss(), 3000);
  }

}
