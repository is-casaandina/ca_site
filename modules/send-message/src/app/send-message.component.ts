import { Component, ElementRef, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RESPONSE_MESSAGE } from '@ca-admin/settings/constants/general.lang';
import { GeneralLang } from '@ca-admin/settings/lang';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { INotify } from '@ca-core/shared/helpers/notification/notification.interface';
import { StorageService, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { OurLivingRoomServices } from 'modules/our-living-room/src/providers/our-living-room.services';
import { zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { SendMessageServices } from '../providers/send-message.service';
import { MessageResponseModalComponent } from './message-response-modal/message-response-modal.component';

@Component({
  selector: 'ca-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.scss']
})
export class SendMessageComponent extends ComponentBase implements OnInit {

  formContact: FormGroup;
  recaptcha: any;
  SITE_KEY = environment.CAPTCHA_KEY;
  GENERAL_LANG = GeneralLang;
  RESPONSE_MESSAGE = RESPONSE_MESSAGE;
  countries: any;
  destinations: any;
  genders: any;
  reasons: any;
  submitted = false;
  showOverlay = false;
  notification: INotify;
  notifyActive = false;
  formLabels = {};

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private sendMessageServices: SendMessageServices,
    private ourLinvingServices: OurLivingRoomServices,
    private newsLetterServices: NewsLetterServices,
    private ngZone: NgZone,
    private formBuild: FormBuilder,
    private modalService: ModalService,
    private notificationService: NotificationService) {
      super(elem, storageService);
    }

  ngOnInit(): void {
    this.setForm();
    this.getSelects();
    this.getNotifies();
    this.getFormLabels();
  }
  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.formContact, controlName);
  }
  sendForm(): void {
    this.submitted = true;
    if (!this.formErrors().number && this.recaptcha) {
      const body = {};
      this.formContact.value.motive = +this.formContact.value.motive;
      Object.keys(this.formContact.value)
      .forEach(key => body[key] = this.formContact.value[key]);

      this.showOverlay = true;
      this.sendMessageServices.saveSubscriber(body)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => this.openModal('success', this.RESPONSE_MESSAGE[this.language].success));

    } else if (this.formErrors().name) {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.existIncorrectFields);
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }
  setLabel(name): string {
    return Object.keys(this.formLabels).length
    ? this.formLabels[name]
    : '';
  }
  private getSelects(): void {
    zip(
      this.newsLetterServices.getCountries(),
      this.ourLinvingServices.getDestinations(),
      this.newsLetterServices.getGenders(this.language),
      this.sendMessageServices.getContactReasons(this.language)
    )
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(response => {
      this.ngZone.run(() => {
        this.countries = response[0];
        this.destinations = response[1];
        this.genders = response[2];
        this.reasons = response[3];
      });
    });
  }

  private setForm(): void {
    this.formContact = this.formBuild.group({
      name: [null, Validators.required],
      email: [null, [
        Validators.required,
        Validators.email
      ]],
      countryId: [null],
      phone: [null],
      pageDestinationId: [null, Validators.required],
      gender: [null],
      motive: [null, Validators.required],
      address: [null],
      message: [null],
      sendNotification: [false],
      sendNew: [false]
    });
  }
  private getNotifies(): void {
    this.notificationService.getNotifies()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<INotify>) => {
        this.ngZone.run(() => {
          if (response.length) {
            this.notification = response[response.length - 1];
            this.notifyActive = true;
            setTimeout(() => {
              this.notifyActive = false;
            }, 2000);
          }
        });
      });
  }
  private formErrors(): any {
    ValidatorUtil.validateForm(this.formContact);

    return ValidatorUtil.formErrors(this.formContact);
  }
  private getFormLabels(): void {
    this.newsLetterServices.getFormLabels({
      code: `contact`,
      language: this.language
    })
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe(response => {
      this.ngZone.run(() => {
        response.forEach(item => {
          this.formLabels[item.name] = item.placeholder || item.text;
        });
      });
    });
  }
  private openModal(response, text): void {
    const modal = this.modalService.open(MessageResponseModalComponent, {
      title: '',
      size: MODAL_BREAKPOINT.sm
    });
    modal.setPayload({
      response,
      lang: this.language,
      text
    });
  }
}
