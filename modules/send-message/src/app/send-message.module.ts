import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ApiModule } from '@ca-core/shared/helpers/api';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { CaInputModule, CaNotifyModule } from '@ca-core/ui/lib/components';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { OurLivingRoomServices } from 'modules/our-living-room/src/providers/our-living-room.services';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { SendMessageServices } from '../providers/send-message.service';
import { MessageResponseModalComponent } from './message-response-modal/message-response-modal.component';
import { SendMessageComponent } from './send-message.component';

@NgModule({
  declarations: [
    SendMessageComponent,
    MessageResponseModalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    CaInputModule,
    ApiModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    ModalModule,
    CaNotifyModule
  ],
  providers: [
    SendMessageServices,
    OurLivingRoomServices,
    NotificationService,
    NewsLetterServices
  ],
  entryComponents: [
    SendMessageComponent,
    MessageResponseModalComponent
  ]
})
export class SendMessageModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-send-message';
    const element = createCustomElement(SendMessageComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
