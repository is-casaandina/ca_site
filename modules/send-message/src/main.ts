import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SendMessageModule } from './app/send-message.module';

platformBrowserDynamic()
.bootstrapModule(SendMessageModule)
  .catch(err => console.error(err));
