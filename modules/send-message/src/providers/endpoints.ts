import { environment } from '../environments/environment';

export class SendMessageEndpoint {
  public static reasons = `${environment.API_URL}site/parameters/contact-reason/{language}`;
  public static sucriber = `${environment.API_URL}api/subscriber/contact`;
}
