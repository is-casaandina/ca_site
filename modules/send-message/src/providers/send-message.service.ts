import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { SendMessageEndpoint } from './endpoints';

@Injectable()
export class SendMessageServices {

  constructor(private apiService: ApiService) {}

  getContactReasons(language: string):  Observable<any> {
    return this.apiService.get(SendMessageEndpoint.reasons, { params: { language }, preloader: false });
  }
  saveSubscriber(params: any):  Observable<any> {
    return this.apiService.post(
        SendMessageEndpoint.sucriber, params);
  }
}
