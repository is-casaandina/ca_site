import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { InfoPromotionModule } from './app/info-promotion.module';

platformBrowserDynamic()
  .bootstrapModule(InfoPromotionModule)
  .catch(err => console.error(err));
