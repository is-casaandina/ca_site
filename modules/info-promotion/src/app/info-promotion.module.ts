import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { InfoPromotionComponent } from './info-promotion.component';

@NgModule({
  declarations: [
    InfoPromotionComponent
  ],
  imports: [
    BrowserModule,
    PipesModule
  ],
  entryComponents: [InfoPromotionComponent]
})
export class InfoPromotionModule {
  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-info-promotion';
    const element = createCustomElement(InfoPromotionComponent, { injector: this.injector });
    customElements.define(name, element);
  }

}
