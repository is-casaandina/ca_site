import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CommonModule } from '@angular/common';
import { createCustomElement } from '@angular/elements';
import { ParagrapghFullComponent } from './paragraph-full.component';

@NgModule({
  imports: [BrowserModule, CommonModule],
  declarations: [ParagrapghFullComponent],
  entryComponents: [ParagrapghFullComponent]
})
export class ParagraphFullModule {

  constructor(private injector: Injector) { }

  ngDoBootstrap(): void {
    const name = 'ca-paragraph-full';
    const element = createCustomElement(ParagrapghFullComponent, { injector: this.injector });
    customElements.define(name, element);
  }
}
