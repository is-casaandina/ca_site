import { Component, ElementRef, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ca-root',
  template: ``,
  encapsulation: ViewEncapsulation.None
})
export class ParagrapghFullComponent implements OnInit {
  @Input() description: string;

  constructor(private elementRef: ElementRef<HTMLElement>) { }

  ngOnInit(): void {
    this.elementRef.nativeElement.innerHTML = this.description;
  }
}
