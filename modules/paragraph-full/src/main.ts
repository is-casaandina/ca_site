import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ParagraphFullModule } from './app/paragraph-full.module';

platformBrowserDynamic()
  .bootstrapModule(ParagraphFullModule)
  .catch(err => console.error(err));
