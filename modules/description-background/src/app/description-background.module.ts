import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { DescriptionBackgroundComponent } from './description-background.component';

@NgModule({
  declarations: [
    DescriptionBackgroundComponent
  ],
  imports: [
    BrowserModule,
    PipesModule,
    ElemBackgroundurlModule
  ],
  providers: [],
  entryComponents: [DescriptionBackgroundComponent]
})
export class DescriptionBackgroundModule {
  constructor(private injector: Injector) {}

	 ngDoBootstrap(): void {
		const name = 'ca-description-background';
		const element = createCustomElement(DescriptionBackgroundComponent, { injector: this.injector });
		customElements.define(name, element);
	}
}
