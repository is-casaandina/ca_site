import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IBackgroundImage } from 'core/typings';

@Component({
  selector: 'ca-description-background',
  templateUrl: './description-background.component.html'
})
export class DescriptionBackgroundComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() description: string;
  @Input() backgroundImage: IBackgroundImage;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    this.backgroundImage = AngularUtil.toJson(this.backgroundImage, {});
  }
}
