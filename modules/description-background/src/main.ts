import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DescriptionBackgroundModule } from './app/description-background.module';

platformBrowserDynamic()
  .bootstrapModule(DescriptionBackgroundModule)
  .catch(err => console.error(err));
