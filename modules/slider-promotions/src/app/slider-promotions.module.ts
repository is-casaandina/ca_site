import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { CarouselControlModule, CarouselModule, SectionHeaderModule } from '@ca/library';
import { ImgSrcsetModule, UrlLinkModule } from '@ca/library/directives';
import { ResizeService } from 'core/shared/src/helpers/util/resize.service';
import { CardPromotionModule } from '../components/card/card-promotion.module';
import { ModalRestrictionModule } from '../components/modal-restriction/modal-restriction.module';
import { SliderPromotionsService } from '../providers/slider-promotions.service';
import { SliderPromotionsComponent } from './slider-promotions.component';

@NgModule({
  declarations: [SliderPromotionsComponent],
  imports: [
    BrowserModule,
    CommonModule,
    SectionHeaderModule,
    CarouselModule,
    NotificationModule,
    SpinnerModule,
    HttpClientModule,
    CarouselControlModule,
    UrlLinkModule,
    CardPromotionModule,
    ImgSrcsetModule,
    PipesModule,
    ModalModule,
    ModalRestrictionModule
  ],
  providers: [
    SliderPromotionsService,
    ResizeService
  ],
  entryComponents: [
    SliderPromotionsComponent
  ]
})
export class SliderPromotionsModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-slider-promotions'; // Nombre del selector que leera la aplicacion principal
    const element = createCustomElement(SliderPromotionsComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
