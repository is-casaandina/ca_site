import { Component, ElementRef, Input, NgZone, OnInit, ViewEncapsulation } from '@angular/core';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { getStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import { ICON_SLIDE_POSITION } from '@ca-core/shared/settings/constants/general.constant';
import { AngularUtil, StorageService } from 'core/shared/src/helpers/util';
import { ResizeService, SCREEN_NAMES } from 'core/shared/src/helpers/util/resize.service';
import { Ilink } from 'core/typings';
import { zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SliderPromotionsService } from '../providers/slider-promotions.service';

@Component({
  selector: 'ca-slider-promotions',
  templateUrl: './slider-promotions.component.html',
  encapsulation: ViewEncapsulation.Emulated
})
export class SliderPromotionsComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() urlLink: Ilink;
  @Input() textLink: string;
  @Input() sliders: Array<any>;
  activeIndex = 0;
  slides: Array<any> = [];
  descriptors: Array<any> = [];
  sliderGroups: Array<any> = [];
  slidesPositions: Array<string>;
  pagesIds: Array<string> = [];
  promotionsIds: Array<string> = [];
  promotionPageId: Array<any> = [];
  bookingUrl: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    protected sliderPromotionsService: SliderPromotionsService,
    protected resizeService: ResizeService,
    protected ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    this.sliders = AngularUtil.toJson(this.sliders, []);
    this.urlLink = AngularUtil.toJson(this.urlLink, {});
    if (this.isVisible) {
      if (this.resizeService.is(SCREEN_NAMES.XS)
        || this.resizeService.is(SCREEN_NAMES.SM)
        || this.resizeService.is(SCREEN_NAMES.MD)) {
        this.groupSlidesOne();
      } else {
        this.sliderGroups = this.groupSlides();
      }
      this.initData();
      this.getBookingUrlService();
    }
  }

  groupSlidesOne(): void {
    this.sliders.forEach((slidersGrouped: any) => {
      const layout = slidersGrouped.type;
      this.slidesPositions = ICON_SLIDE_POSITION[layout];

      Object.keys(slidersGrouped)
        .forEach((idxSlide: string, idx, array) => {
          if (idx + 2 < array.length) {
            slidersGrouped[idxSlide].classNative = this.slidesPositions[idx];
            slidersGrouped[idxSlide].className = this.slidesPositions[idx];
            slidersGrouped[idxSlide].classNameItem = this.slidesPositions[idx].split(' ')
              .map((className: string) => `carousel-item--${className}`)
              .join(' ');
            slidersGrouped[idxSlide].className = `${slidersGrouped[idxSlide].className} active`;
            slidersGrouped[idxSlide].classNameItem = `${slidersGrouped[idxSlide].classNameItem} active`;
            const item = camelcaseKeys(slidersGrouped[idxSlide]);
            this.sliderGroups.push([item]);
            this.promotionPageId.push({
              idPromotion: item.promotion,
              idHotelRest: item.hotelRestaurant
            });
            this.pagesIds.push(item.hotelRestaurant);
            this.promotionsIds.push(item.promotion);
          }
        });
    });
  }

  groupSlides(): Array<any> {
    return this.sliders.map((slidersGrouped: any) => {
      const layout = slidersGrouped.type;
      const slides = [];
      this.slidesPositions = ICON_SLIDE_POSITION[layout];

      Object.keys(slidersGrouped)
        .forEach((idxSlide: string, idx, array) => {
          if (idx + 2 < array.length) {
            slidersGrouped[idxSlide].classNative = this.slidesPositions[idx];
            slidersGrouped[idxSlide].className = this.slidesPositions[idx];
            slidersGrouped[idxSlide].classNameItem = this.slidesPositions[idx].split(' ')
              .map((className: string) => `carousel-item--${className}`)
              .join(' ');
            slidersGrouped[idxSlide].className = `${slidersGrouped[idxSlide].className} active`;
            slidersGrouped[idxSlide].classNameItem = `${slidersGrouped[idxSlide].classNameItem} active`;
            const item = camelcaseKeys(slidersGrouped[idxSlide]);
            slides.push(item);
            this.promotionPageId.push({
              idPromotion: item.promotion,
              idHotelRest: item.hotelRestaurant
            });
            this.pagesIds.push(item.hotelRestaurant);
            this.promotionsIds.push(item.promotion);
          }
        });

      return slides;
    });
  }

  private initData(): void {
    const currency = getStorageCurrency();
    const getPagesInfo$ = this.sliderPromotionsService.getPagesInfo(this.pagesIds, this.language);
    const getPromotionsInfo$ = this.sliderPromotionsService.getPromotionsInfo(this.promotionsIds, this.language, currency);
    const getReservationInfo$ = this.sliderPromotionsService.
      getReservationInfo(AngularUtil.fromJson({ data: this.promotionPageId }), this.language, currency);

    zip(getPagesInfo$, getPromotionsInfo$, getReservationInfo$)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.updateSlides(response[0]);
          this.updateSlidesWithPromotion(response[1]);
          this.updateSlidesReservation(response[2]);
        });
      });
  }

  updateSlidesReservation(reservationsInfo): void {
    this.sliderGroups.forEach(sliderGroup => {
      sliderGroup.forEach(slide => {
        const reservationInfo = reservationsInfo.find(
          (reservation: any) => slide.hotelRestaurant === reservation.idHotelRest && reservation.idPromotion === slide.promotion
        );

        if (reservationInfo) {
          slide.price = reservationInfo.price || 0;
          slide.urlReservation = reservationInfo.urlReservation;
          slide.isRestriction = reservationInfo.isRestriction;
          slide.imageRestrictionFirst = reservationInfo.imageRestriction;
          slide.bodyRestrictionFirst = reservationInfo.bodyRestriction;
          slide.footerRestrictionFirst = reservationInfo.footerRestriction;
          slide.discount = reservationInfo.discount;
          slide.urlPromotion = `${reservationInfo.domain}${reservationInfo.urlPromotion}`;
          slide.days = reservationInfo.days;
          slide.nights = reservationInfo.nights;
        }
      });
    });
  }

  updateSlides(pagesInfo): void {
    this.sliderGroups.forEach(sliderGroup => {
      sliderGroup.forEach(slide => {
        const pageInfo = pagesInfo.find((page: any) => slide.hotelRestaurant === page.id);

        if (pageInfo) {
          slide.descriptorInfo = {
            color: pageInfo.colorDescriptor,
            title: pageInfo.nameDescriptor
          };
          slide.imageMain = pageInfo.imageMain;
          slide.hotelName = pageInfo.name;
          slide.roiback = pageInfo.roiback;
        }
      });
    });
  }

  updateSlidesWithPromotion(promotionsInfo): void {
    this.sliderGroups.forEach(sliderGroup => {
      sliderGroup.forEach(slide => {
        const promotionData = promotionsInfo.find(
          (promotion: any) => slide.promotion === promotion.id
        );

        if (promotionData) {
          slide.title = promotionData.infoPromotion.title;
          slide.urlItem = promotionData.url;
          slide.infoPromotion = promotionData.infoPromotion;
          if (!slide.hotelRestaurant) {
            slide.imageMain = promotionData.infoPromotion.imagePromotion &&
              promotionData.infoPromotion.imagePromotion.length &&
              promotionData.infoPromotion.imagePromotion[0];

            promotionData.infoPromotion.imageRestrictionFirst =
              promotionData.infoPromotion.imageRestrictionFirst || promotionData.infoPromotion.imageRestriction;

            promotionData.infoPromotion.bodyRestrictionFirst =
              promotionData.infoPromotion.bodyRestrictionFirst || promotionData.infoPromotion.bodyRestriction;

            promotionData.infoPromotion.footerRestrictionFirst =
              promotionData.infoPromotion.footerRestrictionFirst || promotionData.infoPromotion.footerRestriction;

            slide.price = promotionData.infoPromotion.price || 0;
            slide.urlReservation = promotionData.url;
            slide.isRestriction = promotionData.infoPromotion.isRestriction;
            slide.imageRestrictionFirst = promotionData.infoPromotion.imageRestriction;
            slide.discount = promotionData.infoPromotion.discount;
            slide.days = promotionData.infoPromotion.days;
            slide.nights = promotionData.infoPromotion.nights;
            slide.descriptorInfo = {};
          }
        }
      });
    });
  }

  private getBookingUrlService(): void {
    this.sliderPromotionsService.getParameter(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(indicator => {
        this.ngZone.run(() => {
          this.bookingUrl = indicator && indicator.url || '';
        });
      });
  }

}
