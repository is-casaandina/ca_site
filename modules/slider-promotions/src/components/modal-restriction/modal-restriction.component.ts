import { Component } from '@angular/core';
import { RESERVE_ACTIONS } from '@ca-admin/settings/constants/general.lang';
import { IPayloadModalComponent } from '@ca-core/shared/helpers/modal';

@Component({
  selector: 'ca-modal-restriction',
  templateUrl: './modal-restriction.component.html'
})
export class ModalRestrictionComponent implements IPayloadModalComponent {

  payload: any;
  RESERVE_ACTIONS = RESERVE_ACTIONS;
  constructor() { }

}
