import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { ElemBackgroundurlModule, ImgSrcsetModule } from '@ca/library/directives';
import { ModalRestrictionComponent } from './modal-restriction.component';

@NgModule({
    imports: [
      CommonModule,
      ElemBackgroundurlModule,
      PipesModule,
      ImgSrcsetModule
    ],
    declarations: [ ModalRestrictionComponent ],
    exports: [ ModalRestrictionComponent ],
    entryComponents: [ ModalRestrictionComponent ]
})
export class ModalRestrictionModule {
}
