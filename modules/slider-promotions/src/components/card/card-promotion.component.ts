import { Component, Input, OnInit } from '@angular/core';
import { GENERAL_TEXT } from '@ca-admin/settings/constants/general.lang';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { BookingUtil, IHotelBooking } from '@ca-core/shared/helpers/util/booking';
import { shareSocialNetworks } from '@ca-core/shared/helpers/util/networks-share';
import { SCREEN_NAMES } from '@ca-core/shared/helpers/util/resize.service';
import { getStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import {
  CARD_RESERVATION,
  CARD_TYPE,
  LOW_PRICE,
  TYPE_DESCRIPTOR_PROMOTION
} from 'core/shared/src/settings/constants/general.constant';
import { ModalRestrictionComponent } from '../modal-restriction/modal-restriction.component';
import { ICardPromotion } from './card-promotion';

@Component({
  selector: 'ca-card-promotion',
  templateUrl: './card-promotion.component.html'
})
export class CardPromotionComponent implements OnInit {

  @Input() data: ICardPromotion;
  @Input() type: string = CARD_TYPE.promotion.name;
  @Input() lang: string;
  @Input() bookingUrl: string;
  @Input()
  get classSize(): string {
    return this._classSize;
  }
  set classSize(value: string) {
    this._classSize = value;
  }

  private _classSize = 's-card-wrapper--sm';
  CARD_TYPE = CARD_TYPE;
  TYPE_DESCRIPTOR_PROMOTION = TYPE_DESCRIPTOR_PROMOTION;
  LOW_PRICE = LOW_PRICE;
  SCREEN_NAMES = SCREEN_NAMES;
  CARD_RESERVATION = CARD_RESERVATION;
  currency = getStorageCurrency();
  GENERAL_TEXT = GENERAL_TEXT;

  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit(): void { }

  share(network): void {
    let url = this.data.urlPromotion || this.data.urlReservation;
    if (url.indexOf('https') === -1) {
      url = `${window.location.origin}/${url}`;
    }
    shareSocialNetworks(network, {
      title: this.data.title,
      url
    });
  }

  openModalRestrictions(): void {
    const modal = this.modalService.open(ModalRestrictionComponent, {
      classDialog: 'site promo',
      classBody: 'p-0',
      classHeader: 'p-0',
      size: MODAL_BREAKPOINT.lg
    });

    modal.setPayload({
      ...this.data.infoPromotion,
      color: this.data.descriptorInfo.color,
      isRestriction: this.data.isRestriction,
      hotelName: this.data.hotelName,
      urlReservation: this.data.urlReservation,
      language: this.lang
    });

    modal.result
      .then(() => { })
      .catch(() => { });
  }

  getBookingUrl(): string {
    const booking: IHotelBooking = {
      nights: this.data.nights,
      days: this.data.days,
      roiback: this.data.roiback,
      urlReservation: this.data.urlReservation
    };

    const url = !this.data.urlReservation && this.bookingUrl && booking.roiback && BookingUtil.getBookingUrl(this.bookingUrl, booking);

    this.data.urlReservation = this.data.urlReservation || url;

    return url;
  }

}
