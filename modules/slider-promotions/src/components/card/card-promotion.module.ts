import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { SafeUrlPipeModule } from '@ca-core/shared/helpers/pipes/safe-url.pipe';
import { CurrencySymbolPipeModule } from '@ca-core/ui/lib/pipes/currency-symbol.pipe';
import { ElemBackgroundurlModule, PromoDetailModule, UrlLinkModule } from '@ca/library/directives';
import { ModalRestrictionModule } from '../modal-restriction/modal-restriction.module';
import { CardPromotionComponent } from './card-promotion.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      UrlLinkModule,
      ElemBackgroundurlModule,
      ModalModule,
      ModalRestrictionModule,
      CurrencySymbolPipeModule,
      SafeUrlPipeModule,
      PromoDetailModule
    ],
    declarations: [ CardPromotionComponent ],
    exports: [ CardPromotionComponent ]
})
export class CardPromotionModule {
}
