import { IBackgroundImage } from '@core/typings';

export interface ICardPromotion {
  title?: string;
  position?: string;
  descriptor?: string;
  type?: string;
  descriptorInfo?: any;
  description?: string;
  hotel?: string;
  urlItem?: string;
  backgroundImage?: any;
  hotelName?: string;
  titleHotel?: string;
  classNative?: string;
  discount?: number;
  entity?: string;
  infoPromotion?: any;
  isRestriction?: boolean;
  imageMain?: any;
  imagePromotion?: Array<IBackgroundImage>;
  price?: string;
  urlPromotion?: string;
  urlReservation?: string;
  nights?: number;
  days?: number;
  roiback?: string;
}
