import { environment } from '../environments/environment';

export class SliderPromotionsEndpoint {
  public static pagesInfo = `${environment.API_URL}site/promotions/detail/ids`;
  public static promotionsInfo = `${environment.API_URL}site/pages/promotions/bells`;
  public static reservations = `${environment.API_URL}site/pages/promotions/details/url`;
  public static parameter = `${environment.API_URL}site/parameters/roiback-hotel/{code}`;
}
