import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IDescriptorResponse } from 'core/typings';
import { Observable } from 'rxjs';
import { SliderPromotionsEndpoint } from './endpoints';

@Injectable()
export class SliderPromotionsService {

  constructor(
    private apiService: ApiService
  ) { }

  getPagesInfo(pagesIds: Array<string>, language: string): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(SliderPromotionsEndpoint.pagesInfo, { params: { ids: pagesIds, language } });
  }

  getPromotionsInfo(promotionsIds, language: string, currency: string, showSpin = false): Observable<Array<any>> {
    return this.apiService
      .get(SliderPromotionsEndpoint.promotionsInfo, { preloader: showSpin, params: { promotions: promotionsIds, language, currency } });
  }

  getReservationInfo(idsString: string, language: string, currency: string): Observable<Array<any>> {
    return this.apiService
      .get(SliderPromotionsEndpoint.reservations, { preloader: false, params: { promotions: idsString, language, currency } });
  }

  getParameter(code: string): Observable<any> {
    return this.apiService.get(SliderPromotionsEndpoint.parameter, { params: { code } });
  }

}
