import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderPromotionsModule } from './app/slider-promotions.module';

platformBrowserDynamic()
  .bootstrapModule(SliderPromotionsModule)
  .catch(err => console.error(err));
