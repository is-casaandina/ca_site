import { Component, NgZone, OnInit } from '@angular/core';
import { StorageService, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IDestinationMap, IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { HotelDirectoryService } from '../providers/hotel-directory.service';

@Component({
  selector: 'ca-hotel-directory',
  templateUrl: './hotel-directory.component.html'
})
export class HotelDirectoryComponent extends UnsubscribeOnDestroy implements OnInit {
  pageInfo: IPageInfo;
  destinationsMap: Array<IDestinationMap> = [];
  hotels: Array<any> = [];
  language: string;

  constructor(
    private hotelDirectoryService: HotelDirectoryService,
    private storageService: StorageService,
    private ngZone: NgZone
  ) {
    super();
  }

  ngOnInit(): void {
    this.pageInfo = this.storageService.getItemObject(STORAGE_KEY.pageInfo);
    this.language = this.pageInfo.language;
    this.getMapDestinations();
    this.getAllhotels();
  }

  private getMapDestinations(): void {
    this.hotelDirectoryService
      .getMapDestinations(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<IDestinationMap>) => {
        this.ngZone.run(() => {
          this.destinationsMap = response;
        });
      });
  }

  private getAllhotels(): void {
    this.hotelDirectoryService
      .getHotels(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => this.hotels = res);
  }
}
