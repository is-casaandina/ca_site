import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { MapDestinationsModule } from '../components/map-destinations/map-destinations.module';
import { HotelDirectoryService } from '../providers/hotel-directory.service';
import { HotelDirectoryComponent } from './hotel-directory.component';

@NgModule({
  declarations: [
    HotelDirectoryComponent
  ],
  imports: [
    BrowserModule,
    MapDestinationsModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule
  ],
  providers: [
    HotelDirectoryService
  ],
  entryComponents: [HotelDirectoryComponent]
})
export class HotelDirectoryModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void {
    const name = 'ca-hotel-directory';
    const element = createCustomElement(HotelDirectoryComponent, {
      injector: this.injector
    });
    customElements.define(name, element);
  }
}
