import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrderByPipeModule } from '@ca-core/shared/helpers/pipes/orderby.pipe';
import { RateModule } from '@ca/library';
import { ElemBackgroundurlModule } from '@ca/library/directives';
import { MapDestinationsComponent } from './map-destinations.component';

@NgModule({
    imports: [ CommonModule, FormsModule, ElemBackgroundurlModule, RateModule, BrowserAnimationsModule, OrderByPipeModule ],
    declarations: [ MapDestinationsComponent ],
    exports: [ MapDestinationsComponent ]
})
export class MapDestinationsModule {
}
