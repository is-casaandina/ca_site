import { Component, Input } from '@angular/core';
import { fadeInOutAnimation } from '@ca-core/shared/helpers/animations/fade.animation';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { DESTINATIONS_MAP, GO_HOTEL } from '@ca-core/shared/settings/constants/general.constant';
import { takeUntil } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HotelDirectoryService } from '../../providers/hotel-directory.service';

@Component({
  selector: 'ca-map-destinations',
  templateUrl: './map-destinations.component.html',
  animations: [fadeInOutAnimation(0, 0.5)]
})
export class MapDestinationsComponent extends UnsubscribeOnDestroy {
  @Input() active = false;
  @Input() mapDestinations: Array<any> = [];
  @Input() hotels: Array<any> = [];
  @Input() language: string;

  BUCKET_SYSTEM = environment.BUCKET_SYSTEM;
  isActiveDetail = false;
  destinationInfo: any;
  hotelInfoActive: any;
  rating: any;
  descriptorColorActive: string;
  DESTINATIONS_MAP = DESTINATIONS_MAP;
  GO_HOTEL = GO_HOTEL;

  activeHotels: boolean;
  currentSelectHotelResponsive: any;

  constructor(private hotelDirectoryService: HotelDirectoryService) {
    super();
  }

  openHotelDesc(destinationId: string, descriptorId: string, descriptorColor: string): void {
    this.descriptorColorActive = descriptorColor;
    this.isActiveDetail = false;
    this.hotelInfoActive = {};
    this.hotelDirectoryService
      .getDestinationInfo(destinationId, descriptorId, this.language)
      .subscribe((response: Array<any>) => {
        this.destinationInfo = response;
        if (response.length) {
          this.setActiveHotel(response[0]);
        }
      });
  }

  setActiveHotel(hotel: any): void {
    this.isActiveDetail = true;
    this.hotelInfoActive = hotel;

    if (hotel && hotel.title1) {
      this.currentSelectHotelResponsive = {
        name: `${hotel.title2} ${hotel.title1}`
      };
    }
  }

  toggle(): void {
    this.activeHotels = !this.activeHotels;
  }

  selectHotel(hotel?: any): void {
    this.currentSelectHotelResponsive = hotel;
    this.toggle();
    this.hotelDirectoryService
      .getContactByHotel(hotel.id, this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        if (response && response.length) {
          this.setActiveHotel(response[0]);
        }
      });
  }
}
