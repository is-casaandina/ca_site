import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';
import { HotelDirectoryEndpoint } from './endpoints';

@Injectable()
export class HotelDirectoryService {
  constructor(private apiService: ApiService) {}

  getDescriptors(): Observable<any> {
    return this.apiService.get(HotelDirectoryEndpoint.descriptors, { preloader: false });
  }

  getMapDestinations(language: string): Observable<any> {
    return this.apiService.get(HotelDirectoryEndpoint.mapsDestinations, { preloader: false, params: { language } });
  }

  getDestinationInfo(destinationId: string, descriptorId: string, language: string): Observable<any> {
    return this.apiService.get(HotelDirectoryEndpoint.destinationInfo, {
      preloader: false,
      params: { language, descriptorId, destinationId }
    });
  }

  getHotels(language: string): Observable<any> {
    return this.apiService.get(HotelDirectoryEndpoint.hotels, { preloader: false, params: { language } });
  }

  getContactByHotel(hotelId: string, language: string): Observable<any> {
    return this.apiService.get(HotelDirectoryEndpoint.contactByHotelId, {
      preloader: false,
      params: { hotelId, language }
    });
  }
}
