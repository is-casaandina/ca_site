import { environment } from '../environments/environment';

export class HotelDirectoryEndpoint {
  public static mapsDestinations = `${environment.API_URL}site/pages/destinations/map`;
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static destinationInfo = `${environment.API_URL}site/pages/hotels/contact`;
  public static hotels = `${environment.API_URL}site/pages/hotels/all/descriptors`;
  public static contactByHotelId = `${environment.API_URL}site/pages/hotels/contact/{hotelId}`;
}
