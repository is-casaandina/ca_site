import { MAIN_ENV } from '@ca-site/environments/environment.qa';

export const environment = {
  production: true,
  API_URL: MAIN_ENV.API_URL,
  BUCKET_SYSTEM: MAIN_ENV.BUCKET_SYSTEM
};
