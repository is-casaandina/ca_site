import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { HotelDirectoryModule } from './app/hotel-directory.module';

platformBrowserDynamic()
  .bootstrapModule(HotelDirectoryModule)
  .catch(err => console.error(err));
