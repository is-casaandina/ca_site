import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { SliderBasicModule } from './app/slider-basic.module';

platformBrowserDynamic()
  .bootstrapModule(SliderBasicModule)
  .catch(err => console.error(err));
