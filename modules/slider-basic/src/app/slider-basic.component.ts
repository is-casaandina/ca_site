import { Component, ElementRef, Input, NgZone, OnInit } from '@angular/core';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { groupCarousel } from '@ca-core/shared/helpers/util/group-carousel';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ca-slider-basic',
  templateUrl: './slider-basic.component.html'
})
export class SliderBasicComponent extends ComponentBase implements OnInit {
  @Input() title: string;
  @Input() sliders: Array<any>;
  slidersGroups: Array<any>;
  width: number;
  groupSizeOne: Array<Array<any>> = [];
  groupSizeTwo: Array<Array<any>> = [];
  groupSizeThree: Array<Array<any>> = [];

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private resizeService: ResizeService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.onInit();
    this.slidersGroups = [];
    if (this.isVisible) {
      this.sliders = AngularUtil.toJson(this.sliders, []);
      this.groupSlidesNormal();
      this.resizeService.resizeEvent
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(width => {
        this.ngZone.run(() => {
          this.width = width;
        });
      });
    }
  }

  groupSlidesNormal(): void {
    this.sliders.forEach((group: any) => {
      group.items.forEach(experience => {
        this.slidersGroups.push(camelcaseKeys(experience));
      });
    });
    this.groupSizeOne = groupCarousel(1, this.slidersGroups);
    this.groupSizeTwo = groupCarousel(2, this.slidersGroups);
    this.groupSizeThree = groupCarousel(3, this.slidersGroups);
  }
}
