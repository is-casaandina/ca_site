import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { CarouselControlModule, CarouselModule, SectionHeaderModule } from '@ca/library';
import { SliderBasicComponent } from './slider-basic.component';

@NgModule({
  declarations: [
    SliderBasicComponent
  ],
  imports: [
    BrowserModule,
    SectionHeaderModule,
    CarouselControlModule,
    CarouselModule
  ],
  entryComponents: [SliderBasicComponent]
})
export class SliderBasicModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap(): void  {
      const name = 'ca-slider-basic';
      const element = createCustomElement(SliderBasicComponent, { injector: this.injector });
      customElements.define(name, element);
    }
}
