# Sites

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# WEB COMPONENTS

En el directorio ./modules se encuentran los web components que deben compilarse para ser leidos en el 'bvlsite', el proceso a nivel de comandos seria el siguiente:

- Para generar el webcomponent como una aplicacion
      `ng generate application <nombre-aplicacion> --style=scss --skipTests=true --prefix ca`

- Registrar la aplicacion y que sea reconocida como web component
      `ng add document-register-element --project <nombre-aplicacion>`

      NOTA: en caso de que el paso anterior falle, manualmente coloque el siguiente objeto en el angular.json, especificamente <nombre-aplicacion>.architect.options.scripts:

      `{ "input": "node_modules/document-register-element/build/document-register-element.js" }`

- Agregar la aplicacion dentro del angular.json
      `ng add ngx-build-plus --project <nombre-aplicacion>`

- Como recomendacion, dentro del componente hacer los siguientes ajustes:
   -  Eliminar el folder <nombre-aplicacion>-e2e, al igual que su referencia en el angular.json
   -  En el archivo main.ts, retirar la condicion del enableProdMode(), junto con la importancion de las          dependencias

   Se puede tomar como ejemplo el componente 'hello-world'

- Una vez que se este satisfecho con el componente, correr el siguiente comando para generar la compilacion del webcomponent:

      `ng build --project <nombre-aplicacion> --prod --extraWebpackConfig ../../../webpack.externals.js --output-hashing none --single-bundle true` 
   
   Se recomienda especificar un comando npm para que se genere la compilacion de dicho componente y a parte se copie al directorio correspondiente, se puede tomar el 'build:hello-world' del package.json como referencia

   IMPORTANTE: Para que compilacion se logre es necesario que exista el archivo webpack.externals.js, el cual permite que las depencias sean leidas desde el proyecto raiz

# LIBRERIA DE COMPONENTES EXTERNOS

Los componentes que estan siendo utilizados por los componentes web, estan siendo empaquetados en una unica libreria. La cual esta siendo maneja como un proyecto de angular en 'core/lib/', dentro de este estan anidados los componentes externos, para agregar un nuevo debe seguirse el siguiente proceso:

  - Generar componente como un proyecto de angular, ubicarlo dentro de la carpeta 'core/lib/'.
  - Incluir dentro de este un archivo que sirva para exporta su modulo, componente, metodos, etc. Se puede colocar uno que tenga como nombre 'public_api.ts', este deberia tener el siguiente codigo:

      `export * from '<nombre del modulo o componente>'`

  - Despues este archivo debe incluirse de la misma forma en el archivo 'public_api.ts' que se encuenta el directorio 'src'.

  - Una vez terminado, en consola ejecutar la siguiente linea de comandos para compilar y ubicar la libreria dentro del proyecto bvlsite:

      `npm run build:lib`
  
  - Ahora, el web component que importe algun componente externo, debera importarlo de la siguiente forma:

    `import { <nombre del componente> } from '@bvl/library'

# BUILD AND UPLOAD WEB COMPONENT

## Only one
```
$ npm run build:comp --comp=<name> --env=<dev|qa|prod>
```

## Multiple
```
$ npm run build:comp --env=<dev|qa|prod>
```

## Multiple - index
```
$ npm run build:comp --index=<number> --env=<dev|qa|prod>
```

### Example
1 Build `all` web component, `dev` mode
```
$ npm run build:comp
```

2 Build `all` web component, `prod` mode
```
$ npm run build:comp --env=prod
```

3 Build web component, `qa` mode
```
$ npm run build:comp --comp=clock --env=qa
```

4 Build `all` web component in `position`, `qa` mode
```
$ npm run build:comp --index=5 --env=qa
```

# BUILD EC2 - MX

## SSH Connection
```
$ chmod 600 ./ec2_ca_site-qa-mx.pem
```
```
$ ssh -i ec2_ca_site-qa-mx.pem ec2-user@ec2-3-82-213-255.compute-1.amazonaws.com
```
```
$ sudo -i
```

## Pull source
```
$ cd ca_site_cms
```
```
$ git pull origin develop
```

## Build docker
```
$ docker-compose build --build-arg env=<dev|qa|prod>
```
```
$ docker-compose stop
```
```
$ docker-compose up -d
```

### Example
1 Build docker, `prod` mode
```
$ docker-compose build --build-arg env=prod
```

### EC2 PROD
QA-Site: 52.205.0.15
QA-CMS:  3.218.5.85
