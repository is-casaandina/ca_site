import { CommonModule, registerLocaleData } from '@angular/common';
import localeEnUS from '@angular/common/locales/en-US-POSIX';
import localeEsPE from '@angular/common/locales/es-PE';
import localeEsPt from '@angular/common/locales/pt-PT';
import { LOCALE_ID, NgModule } from '@angular/core';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';

registerLocaleData(localeEsPt, 'pt');
registerLocaleData(localeEsPE, 'es');
registerLocaleData(localeEnUS, 'en');

const pageInfo = sessionStorage.getItem(STORAGE_KEY.pageInfo);

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: pageInfo
        ? JSON.parse(pageInfo).language
        : 'es'
    }
  ]
})
export class I18nEsPeModule { }
