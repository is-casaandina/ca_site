export class StringUtil {
  public static firstLettersString(value: string): string {
    let firstLetters = '';
    if (value) {
      firstLetters = `${value} `.replace(/([a-zA-Z]{0,} )/g, match => {
        return (match.trim()[0]);
      });
    }

    return firstLetters;
  }

  public static replaceAll(value: string, valueSearch: string, valueReplace: string): string {
    return value.replace(new RegExp(valueSearch, 'g'), valueReplace);
  }

  public static withoutDiacritics(text: string): string {
    // Symbol Ref => https://en.wikipedia.org/wiki/Combining_Diacritical_Marks
    return text.normalize('NFD')
      .replace(new RegExp('[\\u0300-\\u036f]', 'g'), '');
  }

  public static slugify(text: string): string {
    return text.toString()
      .toLowerCase()
      .replace(new RegExp('\\s+', 'g'), '-')           // Replace spaces with -
      .replace(new RegExp('[^\\w\\-]+', 'g'), '')       // Remove all non-word chars
      .replace(new RegExp('\\-\\-+', 'g'), '-')         // Replace multiple - with single -
      .replace(new RegExp('^-+/'), '')             // Trim - from start of text
      .replace(new RegExp('-+$'), '');            // Trim - from end of text
  }

}
