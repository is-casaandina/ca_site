export const groupCarousel = (size: number, experiences: Array<any>): Array<Array<any>> => {
    const groups = [];
    let tmpSize = 0;
    let tmpGroup = [];

    experiences.forEach((experience, index) => {
      tmpSize++;
      if (tmpSize <= size) {
        tmpGroup.push(experience);
      } else {
        tmpGroup = [];
        tmpGroup.push(experience);
        tmpSize = 1;
      }

      if (tmpSize === size || experiences.length - 1 === index) {
        groups.push(tmpGroup);
      }
    });

    return groups;
};
