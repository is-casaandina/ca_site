import { ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { SEARCH_COPY, STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { CaDatepickerRangeComponent } from '@ca-core/ui/lib/components';
import { IAutocompleteSearch, IConfigurationResponse, IRoiback } from '@core/typings';
import { map, takeUntil } from 'rxjs/operators';
import { ComponentBase } from './component-base';
import { StorageService } from './storage-manager';

export class SearchBase extends ComponentBase implements OnInit {

  @ViewChild('dp') datePicker: CaDatepickerRangeComponent;

  registerForm: FormGroup;
  autocomplete: AbstractControl;
  rangepicker: AbstractControl;
  autocompleteHotel: AbstractControl;

  searchUrl: string;
  dateStart: string;
  dateEnd: string;
  dateEndInput: string;
  dateStartInput: string;
  codeDestinationInput: string;
  nameDestinationInput: string;
  today: string;
  hotelParams: any;

  STORAGE_KEY = STORAGE_KEY;
  SEARCH_COPY = SEARCH_COPY;

  isBooking: boolean;

  constructor(
    public searchService: any,
    public fb: FormBuilder,
    public searchEndpoint: string,
    public lang: string,
    public elementRef: ElementRef,
    public ngZone: NgZone,
    public storageService: StorageService
  ) {
    super(elementRef, storageService);
  }

  ngOnInit(): void {
    const now = new Date();
    this.datePicker.input.minDate = { year: now.getFullYear(), month: now.getMonth(), day: now.getDay() };
    this.datePicker.input.maxDate = { year: now.getFullYear() + 10, month: now.getMonth() + 1, day: now.getDate() };

    this.getConfigurations();
    this.today = this.currentDate();
    this.dateStart = this.today;
    this.dateEnd = this.getNextDays(1);
    const rangeValue = {
      from: DateUtil.formatDateYYYYDDMMtoDDMMYYY(this.today),
      to: DateUtil.formatDateYYYYDDMMtoDDMMYYY(this.dateEnd)
    };
    this.registerForm = this.fb.group({
      autocomplete: [''],
      autocompleteHotel: [''],
      code: [''],
      rangepicker: [rangeValue, Validators.required],
      dateStart: [this.today, Validators.required],
      dateEnd: [this.getNextDays(1), Validators.required]
    });
    this.autocomplete = this.registerForm.get('autocomplete');
    this.autocompleteHotel = this.registerForm.get('autocompleteHotel');
    this.rangepicker = this.registerForm.get('rangepicker');
    this.dateStartInput = DateUtil.formatDateYYYYDDMMtoDDMMYYY(this.today);
    this.dateEndInput = DateUtil.formatDateYYYYDDMMtoDDMMYYY(this.getNextDays(1));
    this.autocompleteChange();
    this.afterOnInit();
    this.setDataDefault();
  }

  autocompleteChange(): void {
    this.autocomplete.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((data: IAutocompleteSearch) => {
        this.ngZone.run(() => {
          this.hotelParams = data && data.id && { destinationId: data.id } ;
          this.formatResponse(data);
          this.focus(this.autocomplete);
        });
      });

    this.autocompleteHotel.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((data: IAutocompleteSearch) => {
        this.ngZone.run(() => {
          this.formatResponse(data);
          this.focus(this.autocompleteHotel);
        });
      });
  }

  private formatResponse(data: IAutocompleteSearch): void {
    if (data) {
      if (data.revinate) {
        this.codeDestinationInput = data.roiback;
        this.nameDestinationInput = data.name;
      } else {
        const valuesArr = data.roiback ? data.roiback.split('|') : [];
        this.codeDestinationInput = valuesArr[0];
        this.nameDestinationInput = valuesArr[1] ? valuesArr[1] : valuesArr[0];
      }
    }
  }

  dateRangeChange(values): void {
    const valuesArr = values.split(' - ')
      .map(date => {
        return date.replace(/-/g, '/');
      });
    const dates = values.split(' - ')
      .map(date => {
        return DateUtil.formatDateYYYYDDMMtoDDMMYYY(date);
      });

    this.dateStart = dates[0];
    this.dateStartInput = valuesArr[0];
    if (dates[1]) {
      this.dateEnd = dates[1];
      this.dateEndInput = valuesArr[1];
    }
  }

  getNextDays(nextDays: number, newDate = new Date()): string {
    newDate.setDate(newDate.getDate() + nextDays);

    return DateUtil.formatDateYYYYMMDD(newDate);
  }

  openDatePicker(comp: string, leave = false): void {
    if (this.datePicker) { this.datePicker.leave = leave; }
    (this.elementRef.nativeElement.querySelectorAll(`${comp} .g-datepicker input`)[0] as HTMLElement).click();
    (this.elementRef.nativeElement.querySelectorAll(`${comp} .g-datepicker input`)[0] as HTMLElement).focus();
  }

  currentDate(): string {
    const today = new Date();

    return DateUtil.formatDateYYYYMMDD(today);
  }

  focus(control): void {
    control.markAsTouched({ onlySelf: true });
  }

  getConfigurations(): void {
    this.searchService
      .getConfigurations()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .pipe(map((response: IConfigurationResponse) => response.roiback))
      .subscribe((roiback: IRoiback) => {
        this.ngZone.run(() => {
          this.searchUrl = this.lang === 'es' ? roiback.urlReservationEsp :
            this.lang === 'en' ? roiback.urlReservationEng : roiback.urlReservationPor;
        });
      });
  }

  private setDataDefault(): void {
    if (this.pageInfo && this.pageInfo.roiback) {
      const data = this.pageInfo && {
        roiback: this.pageInfo.roiback,
        revinate: this.pageInfo.revinate,
        name: this.pageInfo.name,
        id: this.pageInfo.pageId
      } as IAutocompleteSearch;

      if (this.isBooking && data && data.revinate) {
        this.autocompleteHotel.setValue(data);
      } else {
        this.autocomplete.setValue(data);
      }

      this.formatResponse(data);
    }else {
      this.codeDestinationInput = 'todos';
      this.autocomplete.setValue({ roiback: 'todos' });
    }
  }

  validForm(): void { }
  afterOnInit(): void { }
}
