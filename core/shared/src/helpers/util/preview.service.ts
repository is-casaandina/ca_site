import { Injectable } from '@angular/core';
import { AngularUtil } from '@ca-core/shared/helpers/util';

@Injectable({
  providedIn: 'root'
})
export class PreviewService {
  constructor() { }

  openPreview(page: any, domain: string): void {
    localStorage.setItem('previewPage', AngularUtil.fromJson(page));
    window.open(`${domain}preview`);
  }

  getPreview<T>(): T {
    const page = localStorage.getItem('previewPage');

    return page ? AngularUtil.toJson(page) : null;
  }

}
