import { DateUtil } from './date';

export class BookingUtil {
  static getBookingUrl(bookingUrl: string, item: IHotelBooking): string {
    if (!item.urlReservation) {
      const beginDate = DateUtil.AddDays(new Date(), Number(item.days || 0));
      const endDate = DateUtil.AddDays(beginDate, Number(item.nights || 1));
      const roiback = item.roiback;

      // tslint:disable
      const beginDateString = beginDate.toISOString().substr(0, 10);
      const endDateString = endDate.toISOString().substr(0, 10);

      return `${this.formatBookingUrl(bookingUrl)}/${roiback}/${beginDateString}/${endDateString}`;
    } else {
      return item.urlReservation;
    }
  }

  private static formatBookingUrl(bookingUrl: string): string {
    const url: string = bookingUrl || '';

    return url && url.endsWith('/') ? url.substring(0, url.length - 1) : url;
  }
}

export interface IHotelBooking {
  urlReservation: string;
  roiback: string;
  days: number;
  nights: number;
}
