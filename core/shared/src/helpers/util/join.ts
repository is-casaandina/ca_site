export const joinSelected = (array: Array<any>, allDefault?: boolean) => {
  const filter = array.filter(item => item.selected);
  const list = !filter.length && allDefault ? array : filter;

  return list.map(item => item.value).join(',');
};
