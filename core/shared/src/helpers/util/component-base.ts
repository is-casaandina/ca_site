import { ElementRef, Input } from '@angular/core';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { IPageInfo } from '@core/typings';

export class ComponentBase extends UnsubscribeOnDestroy {

  template = `<div class="g-component-wrapper g-component-wrapper-empty mb-2">
  <div class="p-2">
    <div class="d-flex justify-content-center align-content-center align-items-center p-1">
      <div class="g-circle-icon ">
        <i class="la la-puzzle-piece"></i>
      </div>
      <span class="g-fs-15 text-center ml-1">
        Este componente esta oculto
      </span>
    </div>
  </div>
</div>`;

  // Parametros
  private _hide: boolean;
  @Input()
  set hide(hide: boolean) {
    this._hide = coerceBooleanProp(hide);
  }
  get hide(): boolean {
    return coerceBooleanProp(this._hide);
  }

  get isSite(): boolean {
    return this.pageInfo.isSite;
  }
  get isVisible(): boolean {
    return !this.hide;
  }

  get language(): string {
    return this.pageInfo.language || 'es';
  }

  get element(): HTMLElement {
    return this.elem.nativeElement;
  }

  // Variables
  pageInfo: IPageInfo;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: any
  ) {
    super();
    this.pageInfo = this.storageService.getItemObject(STORAGE_KEY.pageInfo) || {} as IPageInfo;
  }

  onInit(): void {
    if (this.hide) {
      if (this.isSite) {
        this.elem.nativeElement.closest('section') // Primer elemento section
        .remove(); // Elimina padre
      } else {
        const div = document.createElement('div');
        div.innerHTML = this.template;
        while (this.element.firstChild) {
          this.element.removeChild(this.element.firstChild);
        }
        this.element.appendChild(div.firstChild);
      }
    }
  }

  // Metodos
}
