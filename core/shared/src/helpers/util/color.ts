export const hexToRgb = (hex: string, opacity?: string):  string | null => {
  let result: any = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  result = result ? colorFormater({
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
    a: opacity
  }) : null;

  return result;
};

const colorFormater = ({r, g, b, a}: IRGBAColor): string => {
  return `rgba(${r}, ${g}, ${b}, ${a})`;
}

export interface IRGBAColor {
  r: number;
  g: number;
  b: number;
  a?: string;
}
