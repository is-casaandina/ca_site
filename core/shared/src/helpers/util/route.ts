import { Observable } from 'rxjs';

export class GlobalNavigateUtil {
  static goTo(path: string): void {
    const event = new CustomEvent('custom_navigator', { detail: path });
    document.dispatchEvent(event);
  }

  static onCustomNavigator(): Observable<string> {
    return Observable.create(observable => {
      document.addEventListener('custom_navigator', (event: any) => {
        observable.next(event.detail);
      });
    });
  }
}
