import { AbstractControl, FormArray, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { GeneralLang } from '@ca-core/shared/settings/constants/general.constant';
import { NotificationService } from '../notification';

export class ValidatorUtil {
  public static validateControlByName(form: FormGroup, controlName: any): boolean {
    const control = form.controls[controlName];

    return control && !control.valid && (control.dirty || control.touched);
  }

  public static validateControl(control: AbstractControl): boolean {
    return !control.valid && (control.dirty || control.touched);
  }

  public static validateForm(form: FormGroup): void {
    for (const control in form.controls) {
      if (form.controls.hasOwnProperty(control)) {
        form.controls[control].markAsTouched({ onlySelf: true });
        if ((form.controls[control] as any).controls) { this.validateForm(form.controls[control] as any); }
      }
    }
  }

  public static minChecksRequired(min: number = 1): ValidatorFn {
    const call = ((formArray: FormArray) => {
      const checks = formArray.controls.reduce((prev, next) => {
        return next.value
          ? `${prev}${next.value}`
          : prev;
      }, 0);

      return (checks >= min)
        ? null
        : { required: true };
    });

    return call;
  }

  public static rangeDate(fromControlName: string, toControlName: string): ValidatorFn {
    const call = (formGroup: FormGroup): ValidationErrors | null => {
      const fromControl = formGroup.get(fromControlName);
      const toControl = formGroup.get(toControlName);

      if (!fromControl.value && !toControl.value) {
        return null;
      }

      return (!fromControl.value || !toControl.value || fromControl.value > toControl.value)
        ? { invalidRange: true }
        : null;
    };

    return call;
  }
  public static formErrors(form: FormGroup, notificationService?: NotificationService): any {
    const formControls = Object.keys(form.controls);
    const errors = [];
    const GENERAL_LANG = GeneralLang;

    if (form.errors) {
      const error = Object.keys(form.errors)[0];
      switch (error) {
        case 'notSame':
          notificationService.addWarning(GENERAL_LANG.notifications.fieldsNotEquals.replace('%field%', form.errors.name));
          break;
        default:
          break;
      }
      errors.push({ name: error });

      return { number: errors.filter(item => item).length, name: form.errors.name };
    }

    let name = '';
    formControls.forEach(control => {
      errors.push(form.controls[control].errors);
    });
    errors.filter(e => {
      if (e !== null) {
        switch (Object.keys(e)[0]) {
          case 'required':
            if (notificationService) {
              notificationService.addWarning(GENERAL_LANG.notifications.completeFields);
            }
            break;
          case 'pattern':
          case 'email':
          case 'min':
          case 'max':
            name = Object.keys(e)[0];
            if (notificationService) {
              notificationService.addWarning(GENERAL_LANG.notifications.existIncorrectFields);
            }
            break;
          default:
            break;
        }
      }
    });

    return { number: errors.filter(item => item).length, name };
  }

  public static completeFieldValidator(fromControl: AbstractControl, toControl: AbstractControl): ValidatorFn {
    const call = (formGroup: FormGroup): ValidationErrors | null => {
      const first = fromControl.value;
      const second = toControl.value;

      return (first && !second) || (!first && second) || (first && second)
        ? null
        : { completeField: true };
    };

    return call;
  }

}
