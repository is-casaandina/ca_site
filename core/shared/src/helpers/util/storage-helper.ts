import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IPageInfo } from '@core/typings';

export const getStorageCurrency = () => localStorage.getItem(STORAGE_KEY.currency);
export const setStorageCurrency = (value: string) => localStorage.setItem(STORAGE_KEY.currency, value);

export const getStoragePageInfo = (): string => sessionStorage.getItem(STORAGE_KEY.pageInfo);
export const setStoragePageInfo = (value: IPageInfo) => sessionStorage.setItem(STORAGE_KEY.pageInfo, value as any);
