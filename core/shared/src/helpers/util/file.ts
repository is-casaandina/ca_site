import { Observable } from 'rxjs';
import { StringUtil } from './string';
declare var saveAs;

export class FileUtil {
  public dataURItoBlob(dataURI): Blob {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    const byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    const mimeString = dataURI
      .split(',')[0]
      .split(':')[1]
      .split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    const bb = new Blob([ab], { type: mimeString });

    return bb;
  }

  download(base64: string, type: string, nameFile: string, extension = ''): void {
    try {
      const mimeType = type === 'pdf' ? 'application/pdf' : type;
      const isIE = navigator.userAgent.indexOf('MSIE ') > -1 || navigator.userAgent.indexOf('Trident/') > -1;
      const blobFile = this.dataURItoBlob(`data:${mimeType};base64,${base64}`);
      this.downloadBlob(blobFile, `${StringUtil.slugify(nameFile)}${isIE ? `.${type}` : extension}`);
    } catch (error) {}
  }

  downloadBlob(blob: Blob, filename: string, element?: HTMLAnchorElement): void {
    saveAs(blob, filename);
  }

  generateFile(blob: Blob, name: string): File {
    let newFile: File;

    try {
      newFile = new File([blob], name);
    } catch (error) {
      const newBlob = new Blob([blob], { type: blob.type });
      newBlob['lastModifiedDate'] = new Date();
      newBlob['name'] = name;

      newFile = newBlob as any;
    }

    return newFile;
  }

  fileToBase64(file: File): Observable<string> {
    const reader = new FileReader();

    return Observable.create(observable => {
      reader.onload = () => {
        const uri = reader.result;
        const base64 = uri.toString()
        .split(',')[1];
        observable.next(base64);
      };

      reader.readAsDataURL(file);
    });
  }

  base64ToFile(base64: string, fileName: string): File {
    const base64Parts = base64.split(',');
    const fileFormat = base64Parts[0].split(';')[1];
    const fileContent = base64Parts[1];
    const file = new File([fileContent], fileName, { type: fileFormat });

    return file;
  }

  getNewMockFile(name: string, size: number, type: string): File {
    return {
      name,
      size,
      type
    } as File;
  }
}
