import { IPageInfo } from 'core/typings';

export const addElementToRef = (webComponent: any, clear?: boolean, element?: any): void => {
  if (clear) {
    webComponent.parent.innerHTML = '';
  }

  webComponent.parent.insertAdjacentHTML(webComponent.position, webComponent.html);
};

export const addScriptTag = (names: Array<string>, url?: string): void => {
  names.forEach(fileName => {
    const scriptId = `${fileName}_script`;
    const tmpScript = document.getElementById(scriptId);
    if (!tmpScript) {
      const scriptTag = document.createElement(`script`);
      scriptTag.setAttribute('src', `${url || 'assets/web-components/'}${fileName}.js`);
      scriptTag.setAttribute('type', 'text/javascript');
      scriptTag.setAttribute('id', `${fileName}_script`);
      scriptTag.setAttribute('defer', '');
      document.head.appendChild(scriptTag);
    }
  });
};

export const formatPageInfo = (page: any, language: string, isSite = false, payload: any, name: string): IPageInfo => {
  return {
    pageId: page.id,
    language,
    descriptorId: page.descriptorId,
    roiback: page.roiback,
    revinate: page.revinate,
    contact: page.contact,
    beginDate: page.beginDate,
    firstExpiration: page.firstExpiration,
    secondExpiration: page.secondExpiration,
    countdown: page.countdown,
    isSite,
    payload,
    name
  } as IPageInfo;
};
