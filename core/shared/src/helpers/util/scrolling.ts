import { Observable } from 'rxjs';

export class ScrollUtil {

  static goTo(top: number): Observable<any> {
    window.scrollTo({
      behavior: 'smooth',
      left: 0,
      top
    });

    return Observable.create(observer => {
      window.onscroll = () => {
        const scrollY = window.scrollY || window.pageYOffset;
        if (scrollY === top) {
          observer.next();
          observer.complete();
          window.onscroll = null;
        }
      };
    });
  }

  static isScrolledIntoView(el): boolean {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;

    // INFO: Only completely visible elements return true:
    // return (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // INFO: Partially visible elements return true:
    return elemTop < window.innerHeight && elemBottom >= 0;
  }

}
