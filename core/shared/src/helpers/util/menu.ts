import { IMenuSite } from '@ca-core/shared/statemanagement/models/menu-site.interface';

export class MenuUtil {
  public static menuTree(list: Array<IMenuSite>): Array<IMenuSite> {
    const map = {};
    let node: IMenuSite;
    const roots: Array<IMenuSite> = [];
    let i: number;

    for (i = 0; i < list.length; i += 1) {
      map[list[i].id] = i; // initialize the map
      list[i].children = []; // initialize the children
    }
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      node.collapse = true;
      node.sectionFooter = !!node.sectionFooter;
      node.order = node.order > -1 ? node.order : 999999999;

      if (node.parentId) {
        // if you have dangling branches check that map[node.parentId] exists
        const parent = list[map[node.parentId]];
        if (parent) {
          parent.children.push(node);
        }
      } else {
        roots.push(node);
      }
    }

    return roots;
  }
}
