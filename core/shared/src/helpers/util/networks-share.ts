export const shareSocialNetworks = (network, data: { title: string; url: string }) => {
  let url: string;
  switch (network) {
    case 'facebook':
      url = `https://www.facebook.com/sharer.php?u=${data.url}`;
      break;
    case 'twitter':
      url = `https://twitter.com/intent/tweet?text=${data.title}&url=${data.url}`;
      break;
    default:
  }

  const ua = navigator.userAgent;
  const isiOsMobile = navigator.platform.match(/iPhone|iPod|iPad/);
  const isAndroid = ua.match(/Android/);

  if (!isAndroid && !isiOsMobile) {
    const windowModal = window.open(url, '_blank', 'height=500,width=520,top=200,left=300,resizable');
    if (windowModal.focus) {
      windowModal.focus();
    }
  } else {
    const link = document.createElement('a');
    link.href = url;
    link.target = '_blank';
    link.click();
  }
};
