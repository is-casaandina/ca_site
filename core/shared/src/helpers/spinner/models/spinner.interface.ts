export interface ISpinnerConfig {
  spinnerClass?: string;
  spinnerIconClass?: string;
  spinnerImg?: string;
}

export interface IContainer {
  element: any;
  class: string;
}
