import { Component, ElementRef, OnInit, ViewEncapsulation } from '@angular/core';
import { LOADING_TEXT } from '@ca-admin/settings/constants/general.constant';
import { StorageService } from '../../util';
import { ComponentBase } from '../../util/component-base';
import { ISpinnerConfig } from './../models/spinner.interface';

@Component({
  selector: 'cashared-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent extends ComponentBase implements OnInit {

  config: ISpinnerConfig = {
    spinnerIconClass: 'la la-spinner la-spin g-fs-60 g-cl-primary'
  };

  LOADING_TEXT = LOADING_TEXT;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void { }

}
