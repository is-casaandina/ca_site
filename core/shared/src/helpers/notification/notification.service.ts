import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { NOTIFICATION_TYPES } from './notification-types.constants';
import {
  AlertNotify,
  ErrorNotify,
  INotify,
  MessageNotify,
  SuccessNotify,
  WarningNotify
} from './notification.interface';

@Injectable()
export class NotificationService {

  private notifies: Array<INotify>;
  notifiesBehaviorSubject: BehaviorSubject<Array<INotify>>;

  /**
   * Inicializador del servicios de Notificaciones
   */
  constructor() {
    this.notifies = new Array<AlertNotify>();
    this.notifiesBehaviorSubject = new BehaviorSubject<Array<AlertNotify>>(this.notifies);
  }

  /**
   * Agregar una notificación de Éxito
   * @param message Descripción de notificación de Éxito
   */
  addSuccess(message: string): void {
    this.add(new SuccessNotify(message));
  }

  /**
   * Agregar una notificación de Alerta
   * @param message Descripción de notificación de Alerta
   */
  addAlert(message: string): void {
    this.add(new AlertNotify(message));
  }

  /**
   * Agreagr una notificación tipo Mensaje
   * @param message Descripción de notificación tipo Mensaje
   */
  addMessage(message: string): void {
    this.add(new MessageNotify(message));
  }

  /**
   * Agregar una notificación tipo Advertencia
   * @param message Descripción de notificación tipo Advertencia
   */
  addWarning(message: string, data?: object): void {
    this.add(new WarningNotify(message, data));
  }

  /**
   * Agregar una notificación de tipo Error
   * @param message Descripción de notificación tipo Error
   */
  addError(message: string, data?: object): void {
    this.add(new ErrorNotify(message, data));
  }

  /**
   * Retorna todas las notificaciones
   */
  getNotifies(): BehaviorSubject<Array<INotify>> {
    return this.notifiesBehaviorSubject;
  }

  /**
   * Retorna todas las notificaciones tipo Alerta
   */
  getAlerts(): Array<AlertNotify> {
    return this.notifies.filter((n: INotify) => n.type === NOTIFICATION_TYPES.alert.code);
  }

  /**
   * Retorna la última notificación de Alerta
   */
  getLastAlert(): ErrorNotify {
    const errors = this.getAlerts();

    return errors[errors.length - 1];
  }

  /**
   * Retorna todas las notificaciones tipo Mensaje
   */
  getMessages(): Array<MessageNotify> {
    return this.notifies.filter((n: INotify) => n.type === NOTIFICATION_TYPES.message.code);
  }

  /**
   * Retorna todas las notificaciones tipo Advertencia
   */
  getWarnings(): Array<WarningNotify> {
    return this.notifies.filter((n: INotify) => n.type === NOTIFICATION_TYPES.warning.code);
  }

  /**
   * Retorna todas las notificaciones tipo Error
   */
  getErrors(): Array<ErrorNotify> {
    return this.notifies.filter((n: INotify) => n.type === NOTIFICATION_TYPES.error.code);
  }

  /**
   * Retorna la última notificación de Error
   */
  getLastError(): ErrorNotify {
    const errors = this.getErrors();

    return errors[errors.length - 1];
  }

  /**
   * Agrega un mensaje de advertencia por no tener permiso para realizar alguna acción
   */
  notPermission(): void {
    this.addWarning('You do not have permission to perform this action');
  }

  private add(notify: INotify): void {
    this.notifies = [...this.notifies, notify];
    this.getNotifies()
      .next(this.notifies);
  }
}
