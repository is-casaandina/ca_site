import { NOTIFICATION_TYPES } from './notification-types.constants';

export interface INotify {
  type: string;
  title: string;
  description: string;
  date: number;
  data?: object;
}

export class SuccessNotify implements INotify {
  type: string;
  title: string;
  description: string;
  date: number;
  data?: object;

  constructor(description: string) {
    this.title = 'Warning';
    this.description = description;
    this.type = NOTIFICATION_TYPES.success.code;
    this.date = Date.now();
  }
}

export class AlertNotify implements INotify {
  type: string;
  title: string;
  description: string;
  date: number;

  constructor(...args: Array<string>) {
    const title = args.length > 1 ? args[0] : 'Alert';
    this.title = title;
    this.description = args[args.length - 1];
    this.type = NOTIFICATION_TYPES.alert.code;
    this.date = Date.now();
  }
}

export class MessageNotify implements INotify {
  type: string;
  title: string;
  description: string;
  date: number;

  constructor(...args: Array<string>) {
    const title = args.length > 1 ? args[0] : 'Message';
    this.title = title;
    this.description = args[args.length - 1];
    this.type = NOTIFICATION_TYPES.message.code;
    this.date = Date.now();
  }
}

export class WarningNotify implements INotify {
  type: string;
  title: string;
  description: string;
  date: number;
  data?: object;

  constructor(description: string, data?: object) {
    this.title = 'Warning';
    this.description = description;
    this.type = NOTIFICATION_TYPES.warning.code;
    this.date = Date.now();
    this.data = data;
  }
}

export class ErrorNotify implements INotify {
  type: string;
  title: string;
  description: string;
  date: number;
  data?: object;

  constructor(description: string, data?: object) {
    this.title = 'Error';
    this.description = description;
    this.type = NOTIFICATION_TYPES.error.code;
    this.date = Date.now();
    this.data = data;
  }
}
