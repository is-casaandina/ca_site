import { Directive, ElementRef, Input, NgModule, OnChanges, Renderer2 } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[iconFile]'
})
export class IconFileDirective implements OnChanges {

  // url
  @Input() iconFile: string;

  constructor(
    private elm: ElementRef,
    private renderer2: Renderer2
  ) { }

  ngOnChanges(): void {
    this.setIcon();
  }

  private setIcon(): void {
    const extS = (this.iconFile || '').split('.');
    const ext = extS[extS.length - 1] || '';

    const deft = this.getIconByType('file');
    const icon = this.getIconByType(ext) || deft;
    icon.split(' ')
      .forEach(ico => {
        this.renderer2.addClass(this.elm.nativeElement, ico);
      });
  }

  private getIconByType(type: string): string {
    const icons = {
      file: 'la la-file',
      doc: 'la la-file-word-o',
      dot: 'la la-file-word-o',
      docx: 'la la-file-word-o',
      dotx: 'la la-file-word-o',
      docm: 'la la-file-word-o',
      dotm: 'la la-file-word-o',
      xls: 'la la-file-excel-o',
      xlt: 'la la-file-excel-o',
      xla: 'la la-file-excel-o',
      xlsx: 'la la-file-excel-o',
      xltx: 'la la-file-excel-o',
      xlsm: 'la la-file-excel-o',
      xltm: 'la la-file-excel-o',
      xlam: 'la la-file-excel-o',
      xlsb: 'la la-file-excel-o',
      ppt: 'la la-file-powerpoint-o',
      pot: 'la la-file-powerpoint-o',
      pps: 'la la-file-powerpoint-o',
      ppa: 'la la-file-powerpoint-o',
      pptx: 'la la-file-powerpoint-o',
      potx: 'la la-file-powerpoint-o',
      ppsx: 'la la-file-powerpoint-o',
      ppam: 'la la-file-powerpoint-o',
      pptm: 'la la-file-powerpoint-o',
      potm: 'la la-file-powerpoint-o',
      ppsm: 'la la-file-powerpoint-o',
      pdf: 'la la-file-pdf-o'
    };

    return icons[type];
  }
}

@NgModule({ declarations: [IconFileDirective], exports: [IconFileDirective] })
export class IconFileDirectiveModule { }
