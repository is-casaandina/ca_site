import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, NgModule, Output } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { filter, map, pairwise } from 'rxjs/operators';

interface IScrollPosition {
  sH: number;
  sT: number;
  cH: number;
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[infiniteScroller]'
})
export class InfiniteScrollerDirective implements AfterViewInit {

  @Output() infiniteScroller: EventEmitter<any> = new EventEmitter<any>();
  @Input() offsetBottom = 300;

  private scrollEvent$: Observable<any>;

  constructor(private elm: ElementRef) { }

  ngAfterViewInit(): void {
    this.streamScrollEvents();
  }

  private streamScrollEvents(): void {
    this.scrollEvent$ = fromEvent(this.elm.nativeElement, 'scroll');
    this.scrollEvent$
      .pipe(
        map((e: any): IScrollPosition => ({
          sH: e.target.scrollHeight,
          sT: e.target.scrollTop,
          cH: e.target.clientHeight
        })),
        pairwise(),
        filter(positions => this.isUserScrollingDown(positions) && this.isScrollExpectedPercent(positions[1]))
      )
      .subscribe(res => {
        this.infiniteScroller.next();
      });
  }

  private isUserScrollingDown(positions: Array<IScrollPosition>): boolean {
    return positions[0].sT < positions[1].sT;
  }

  private isScrollExpectedPercent(position: IScrollPosition): boolean {
    return position.sT + position.cH >= position.sH;
  }

}

@NgModule({ declarations: [InfiniteScrollerDirective], exports: [InfiniteScrollerDirective] })
export class InfiniteScrollerDirectiveModule { }
