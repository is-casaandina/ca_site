import { HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

export class GeneralResponse<T> {
  message: string;
  data: T;

  constructor(data?: T, message?: string) {
    this.message = message;
    this.data = data;
  }

}

export class PaginationResponse<T> {
  content: Array<T>;
  first: boolean;
  last: boolean;
  totalElements: number;
  totalPages: number;
  byService?: boolean;
}

export interface IObservableArray<T> extends Observable<Array<T>> { }
export interface ICaObservable<T> extends Observable<GeneralResponse<T>> { }
export interface ICaObservableEvent<T> extends Observable<HttpEvent<GeneralResponse<T>>> { }
export interface ICaObservableArray<T> extends Observable<GeneralResponse<Array<T>>> { }
export interface ICaObservablePagination<T> extends Observable<PaginationResponse<T>> { }
