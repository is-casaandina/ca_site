import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from '@angular/animations';

export function fadeAnimation(): AnimationTriggerMetadata {
  return trigger('fadeAnimation', [
    state('in', style({ opacity: 1, display: 'block' })),
    state('out', style({ opacity: 0, display: 'none' })),

    transition('in => out', [style({ opacity: 1 }), animate(250, style({ opacity: 0 }))]),

    transition('out => in', [style({ opacity: 0 }), animate(250, style({ opacity: 1 }))])
  ]);
}

export function fadeAnimationWithoutDisplay(): AnimationTriggerMetadata {
  return trigger('fadeAnimationWithoutDisplay', [
    state('in', style({ opacity: 1 })),
    state('out', style({ opacity: 0 })),

    transition('in => out', [style({ opacity: 1 }), animate(250, style({ opacity: 0 }))]),

    transition('out => in', [style({ opacity: 0 }), animate(250, style({ opacity: 1 }))])
  ]);
}

export function fadeInOutAnimation(timeOut = 0, timeIn = 1): AnimationTriggerMetadata {
  return trigger('fadeInOutAnimation', [
    state(
      'void',
      style({
        opacity: 0.5
      })
    ),
    transition('* => void', animate(`${timeOut}s ease-out`)),
    transition('void => *', animate(`${timeIn}s ease-in`))
  ]);
}
