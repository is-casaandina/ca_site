import { Injectable } from '@angular/core';
import { SweetAlertOptions, SweetAlertType } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalConfirmService {

  constructor() { }

  private _swalOptions(
    type: SweetAlertType,
    title: string,
    html: string | HTMLElement | JQuery = null,
    confirmButtonText: string = 'ACEPTAR',
    options?: SweetAlertOptions): SweetAlertOptions {

    let sweetAlertOptions: SweetAlertOptions = {
      type,
      title,
      html,
      confirmButtonText,
      showCancelButton: true
    };

    if (options) {
      sweetAlertOptions = { ...sweetAlertOptions, ...options };
    }

    return sweetAlertOptions;
  }

  success(
    title: string, textOrHtml?: string | HTMLElement | JQuery, confirmButtonText?: string, options?: SweetAlertOptions): SweetAlertOptions {
    return this._swalOptions('success', title, textOrHtml, confirmButtonText, options);
  }

  error(
    title: string, textOrHtml?: string | HTMLElement | JQuery, confirmButtonText?: string, options?: SweetAlertOptions): SweetAlertOptions {
    return this._swalOptions('error', title, textOrHtml, confirmButtonText, options);
  }

  warning(
    title: string, textOrHtml?: string | HTMLElement | JQuery, confirmButtonText?: string, options?: SweetAlertOptions): SweetAlertOptions {
    return this._swalOptions('warning', title, textOrHtml, confirmButtonText, options);
  }

  info(
    title: string, textOrHtml?: string | HTMLElement | JQuery, confirmButtonText?: string, options?: SweetAlertOptions): SweetAlertOptions {
    return this._swalOptions('info', title, textOrHtml, confirmButtonText, options);
  }

  question(
    title: string, textOrHtml?: string | HTMLElement | JQuery, confirmButtonText?: string, options?: SweetAlertOptions): SweetAlertOptions {
    return this._swalOptions('question', title, textOrHtml, confirmButtonText, options);
  }
}
