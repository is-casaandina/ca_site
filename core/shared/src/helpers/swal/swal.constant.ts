export const SWAL = {
  defaultOptions: {
    customContainerClass: '',
    customClass: 'g-modal-dialog',
    reverseButtons: true,
    buttonsStyling: false,
    cancelButtonClass: 'g-btn g-btn-outline',
    confirmButtonClass: 'g-btn g-btn-primary'
  },
  types: {
    success: 'success',
    error: 'error',
    warning: 'warning',
    info: 'info',
    question: 'question'
  }
};
