import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileSizePipe } from './file-size.pipe';
import { GroupByPipe } from './group-by.pipe';
import { MaxCharPipe } from './max-char.pipe';
import { SafeHtmlPipe, SafeHtmlPipeModule } from './safe-html.pipe';
import { SafeUrlPipe, SafeUrlPipeModule } from './safe-url.pipe';
@NgModule({
  imports: [
    CommonModule,
    SafeUrlPipeModule,
    SafeHtmlPipeModule
  ],
  declarations: [
    FileSizePipe,
    GroupByPipe,
    MaxCharPipe
  ],
  exports: [
    FileSizePipe,
    GroupByPipe,
    MaxCharPipe,
    SafeUrlPipe,
    SafeHtmlPipe
  ]
})
export class PipesModule { }
