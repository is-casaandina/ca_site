import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { IMenuSite } from '@ca-core/shared/statemanagement/models/menu-site.interface';

@Pipe({ name: 'menuSection' })
export class MenuSectionPipe implements PipeTransform {
  transform(menus: Array<IMenuSite>, footer: boolean): Array<IMenuSite> {
    return (menus || []).filter(menu => !!menu.sectionFooter === footer);
  }

}

@NgModule({
  declarations: [MenuSectionPipe],
  exports: [MenuSectionPipe]
})
export class MenuSectionPipeModule { }
