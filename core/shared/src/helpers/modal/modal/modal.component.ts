import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { INotify } from '../../notification/notification.interface';
import { BUTTON_ACTIONS, MODAL_BREAKPOINT, SOURCE } from '../constants/modal.constants';
import { IModalConfig } from '../models/modal.interface';

@Component({
  selector: 'cashared-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent extends UnsubscribeOnDestroy implements OnInit {

  // tslint:disable-next-line:no-output-rename
  @Output('dismiss') dismissEvent = new EventEmitter();

  activeModal: boolean;
  notification: INotify;
  config: IModalConfig;

  notifyActive: boolean;

  BUTTON_ACTIONS = BUTTON_ACTIONS;
  SOURCE = SOURCE;
  MODAL_BREAKPOINT = MODAL_BREAKPOINT;

  constructor(
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getNotifies();
  }

  /**
   * Emit output dismissEvent to subscribe it in the modalRef (models/modal-ref)
   */
  dismiss(reason?: any): void {
    this.dismissEvent.emit(reason);
  }

  private getNotifies(): void {
    this.notificationService.getNotifies()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<INotify>) => {
        if (response.length && this.activeModal) {
          this.notification = response[response.length - 1];
          this.notifyActive = true;
          setTimeout(() => {
            this.notifyActive = false;
          }, this.config.notificationDuration || 2000);
        }
        this.activeModal = true;
      });
  }

}
