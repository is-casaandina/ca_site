import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaNotifyModule } from '@ca-core/ui/lib/components';
import { ModalComponent } from './modal/modal.component';
import { ModalService } from './services/modal.service';

@NgModule({
  declarations: [ModalComponent],
  imports: [
    CommonModule,
    CaNotifyModule
  ],
  entryComponents: [ModalComponent],
  providers: [
    ModalService
  ]
})
export class ModalModule { }
