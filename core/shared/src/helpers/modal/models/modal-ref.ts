import { ComponentRef, NgZone } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';
import { ContentRef } from './content-ref';
import { IModalConfig } from './modal.interface';

export class ModalRef {
  private resolve: (result?: any) => void;
  private reject: (reason?: any) => void;

  /**
   * The instance of component used as modal's content.
   * Undefined when a TemplateRef is used as modal's content.
   */
  get componentInstance(): any {
    if (this.contentRef.componentRef) {
      return this.contentRef.componentRef.instance;
    }
  }

  /**
   * A Promise that is resolved when the modal is closed and rejected when the modal is dismissed.
   */
  result: Promise<any>;

  constructor(
    private modalCmptRef: ComponentRef<ModalComponent>,
    private contentRef: ContentRef,
    private ngZone: NgZone
  ) {
    /**
     * Subsribe output dismissEvent of modalComponent(modal/modal.component)
     */
    this.modalCmptRef.instance.dismissEvent.subscribe((reason: any) => {
      this.dismiss(reason);
    });
    /**
     * Create the Promise to return the result of the button actions
     */
    this.result = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
    this.result.then(null, () => { });
  }

  /**
   * Closes the modal with an optional 'result' value.
   * The 'ModalRef.result' promise will be resolved with provided value.
   */
  close(result?: any): void {
    if (this.modalCmptRef) {
      this.resolve(result);
    }
  }

  /**
   * Dismisses the modal with an optional 'reason' value.
   * The 'ModalRef.result' promise will be rejected with provided value.
   */
  dismiss(reason?: any): void {
    if (this.modalCmptRef) {
      this.reject(reason);
    }
  }

  /**
   * Remove and destroy the modalCmpRef
   * Destroy the contentRef
   */
  removeModalElements(): void {
    const modalNativeEl = this.modalCmptRef.location.nativeElement;
    modalNativeEl.parentNode.removeChild(modalNativeEl);
    this.modalCmptRef.destroy();

    if (this.contentRef && this.contentRef.viewRef) {
      this.contentRef.viewRef.destroy();
    }

    this.modalCmptRef = null;
    this.contentRef = null;
  }

  setConfig(config?: IModalConfig): void {
    if (this.modalCmptRef) {
      this.ngZone.run(() => {
        this.modalCmptRef.instance.config = config || {};
      });
    }
  }

  setPayload<T>(payload: T): void {
    if (this.componentInstance) {
      this.ngZone.run(() => {
        this.componentInstance.payload = payload;
      });
    }
  }
}
