import { Size } from '@ca-core/ui/common';

export interface IModalConfig {
  classModal?: string;
  classHeader?: string;
  classBody?: string;
  classDialog?: string;
  title?: string;
  hideHeader?: boolean;
  notificationDuration?: number;
  size?: Size;
  source?: string;
  hideClose?: boolean;
}

export interface IPayloadModalComponent {
  payload: any;
}
