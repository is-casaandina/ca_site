export const BUTTON_ACTIONS = {
  cancel: 'cancel',
  save: 'save',
  acept: 'acept'
};

export const SOURCE = {
  site: 'site',
  admin: 'admin'
};

export enum MODAL_BREAKPOINT {
  sm = 'sm',
  md = 'md',
  lg = 'lg'
}
