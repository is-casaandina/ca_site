import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  Injectable,
  Injector,
  NgZone,
  Renderer2,
  RendererFactory2
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { ModalComponent } from '../modal/modal.component';
import { ActiveModal } from '../models/active-modal';
import { ContentRef } from '../models/content-ref';
import { ModalRef } from '../models/modal-ref';
import { IModalConfig } from '../models/modal.interface';

/** @dynamic */
@Injectable()
export class ModalService {

  private static modalRefs: Array<ModalRef> = [];
  private renderer2: Renderer2;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private applicationRef: ApplicationRef,
    private rendererFactory2: RendererFactory2,
    private ngZone: NgZone,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.renderer2 = this.rendererFactory2.createRenderer(null, null);
  }

  open(content: any, config?: IModalConfig): ModalRef {
    const containerElement = this.document.body;

    const activeModal = new ActiveModal();
    const contentRef = this.getContentRef(this.componentFactoryResolver, this.injector, content, activeModal);
    const modalCmptRef = this.attachModalComponent(this.componentFactoryResolver, containerElement, contentRef);

    const modalRef: ModalRef = new ModalRef(modalCmptRef, contentRef, this.ngZone);
    ModalService.modalRefs.push(modalRef);

    const removeModalElements = () => {
      const index = ModalService.modalRefs.indexOf(modalRef);
      ModalService.modalRefs.splice(index, 1);

      let time = 0;
      if (!ModalService.modalRefs.length) {
        this.renderer2.removeClass(this.document.body, 'g-modal-open');
        time = 700;
      }
      // removeModalElements after 700ms for the effect css
      setTimeout(() => modalRef.removeModalElements(), time);
    };
    modalRef.result.then(removeModalElements, removeModalElements);
    modalRef.setConfig(config);

    activeModal.close = (result: any) => { modalRef.close(result); };
    activeModal.dismiss = (reason: any) => { modalRef.dismiss(reason); };

    // Added class to the body after 300ms for the effect css
    setTimeout(() => {
      this.renderer2.addClass(this.document.body, 'g-modal-open');
    }, 300);

    return modalRef;
  }

  isActiveModal(): boolean {
    return ModalService.modalRefs.length > 0;
  }

  private getContentRef(
    componentFactoryResolver: ComponentFactoryResolver,
    contentInjector: Injector,
    content: any,
    activeModal: ActiveModal
  ): ContentRef {
    // add condition for return text/template/component
    return this.createFromComponent(componentFactoryResolver, contentInjector, content, activeModal);
  }

  private createFromComponent(
    componentFactoryResolver: ComponentFactoryResolver,
    contentInjector: Injector,
    content: any,
    activeModal: ActiveModal): ContentRef {
    // Create a component reference from the component
    // The resolveComponentFactory() method takes a component and returns the recipe for how to create a component.
    const contentFactory = componentFactoryResolver.resolveComponentFactory(content);
    const newContentInjector = Injector.create({
      providers: [
        {
          provide: ActiveModal,
          useValue: activeModal
        }
      ],
      parent: contentInjector
    });
    const componentRef = contentFactory.create(newContentInjector); // En este punto se crea el content por tanto ingresa a la clase
    // Attach component to the appRef so that it's inside the ng component tree
    this.applicationRef.attachView(componentRef.hostView);

    // Instance my class ContentRef for have in stock my 3 values: nodes(nativeElement), refView, componentRef
    return new ContentRef([[componentRef.location.nativeElement]], componentRef.hostView, componentRef);
  }

  private attachModalComponent(
    componentFactoryResolver: ComponentFactoryResolver,
    containerElement: any,
    contentRef: any): ComponentRef<ModalComponent> {
    // Create a component reference from the component
    // The resolveComponentFactory() method takes a component and returns the recipe for how to create a component.
    const modalFactory = componentFactoryResolver.resolveComponentFactory(ModalComponent);
    // Added el content in my ModalComponent(<ng-content></ng-content>)
    const modalRef = modalFactory.create(this.injector, contentRef.nodes);
    // Attach modal to the appRef so that it's inside the ng component tree
    this.applicationRef.attachView(modalRef.hostView);
    // Append DOM element to the body or another specific section
    containerElement.appendChild(modalRef.location.nativeElement);

    // Return modalRef
    return modalRef;
  }

}
