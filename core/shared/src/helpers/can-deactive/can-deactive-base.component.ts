import { HostListener } from '@angular/core';
import { UnsubscribeOnDestroy } from '../util';

export abstract class ComponentCanDeactivate extends UnsubscribeOnDestroy {

  abstract canDeactivate(): boolean;

  @HostListener('window:beforeunload', ['$event']) private beforeUnloadNotification($event: any): void {
    if (!this.canDeactivate()) {
      $event.returnValue = true;
    } else {
      this.unload();
    }
  }

  @HostListener('window:unload', ['$event']) private unloadNotification($event: any): void {
    this.unload();
  }

  abstract unload(): void;

}
