import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ComponentCanDeactivate } from './can-deactive-base.component';

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<ComponentCanDeactivate> {

  private first = true;

  canDeactivate(component: ComponentCanDeactivate): boolean {
    if (!component.canDeactivate() && this.first) {
      this.first = false;
      if (confirm('Es posible que los cambios que implementaste no se puedan guardar.')) {
        component.unload();

        return true;
      } else {
        return false;
      }
    }

    component.unload();

    return true;
  }

}
