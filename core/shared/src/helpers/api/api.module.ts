import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { NotificationModule } from '../notification';
import { ApiService } from './api.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    SpinnerModule,
    NotificationModule
  ],
  providers: [
    ApiService
  ],
  exports: [
    HttpClientModule
  ]
})
export class ApiModule { }
