const isFunction = fn => typeof fn === 'function';

const doUnsubscribe = subscription => {
  // tslint:disable-next-line:no-unused-expression
  subscription &&
    isFunction(subscription.unsubscribe) &&
    subscription.unsubscribe();
};

const doUnsubscribeIfArray = subscriptionsArray => {
  // tslint:disable-next-line:no-unused-expression
  Array.isArray(subscriptionsArray) &&
    subscriptionsArray.forEach(doUnsubscribe);
};

export const UnSubscribe = ({
  blackList = [],
  arrayName = '',
  event = 'ngOnDestroy'
} = {}): any => {
  // tslint:disable-next-line:only-arrow-functions
  return function(constructor: Function): any {
    const original = constructor.prototype[event];

    constructor.prototype[event] = function(): any {
      if (arrayName) {
        // tslint:disable-next-line:no-invalid-this
        doUnsubscribeIfArray(this[arrayName]);
        // tslint:disable-next-line:no-unused-expression
        isFunction(original) && original.apply(this, arguments);

        return;
      }

      for (const propName in this) {
        if (blackList.includes(propName)) { continue; }

        const property = this[propName];
        doUnsubscribe(property);
      }
      // tslint:disable-next-line:no-unused-expression
      isFunction(original) && original.apply(this, arguments);
    };
  };
};
