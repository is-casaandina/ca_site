import { ISpinnerConfig } from '@ca-core/shared/helpers/spinner';

export const ICON_SLIDE_POSITION = {
  layout1: ['lg', 'md', 'xs', 'xs'],
  layout2: ['lg', 'lg'],
  layout3: ['md', 'xs', 'xs', 'md', 'xs', 'xs'],
  layout4: ['md', 'md', 'md', 'md'],
  layout5: ['lg', 'md', 'md'],
  layout6: ['xs', 'xs', 'xs', 'xs', 'xs', 'xs', 'xs', 'xs'],
  layout7: ['xs', 'xs', 'md', 'xs', 'xs', 'md'],
  layout8: ['md', 'lg right', 'md'],
  layout9: ['xs', 'xs', 'lg right', 'xs', 'xs'],
  layout10: ['lg', 'xs', 'xs', 'xs', 'xs'],
  layoutgeneral: ['sm', 'sm', 'sm']
};

export const DESCRIPTOR_TYPE_ID = {
  hotel: 'Hoteles',
  restaurant: 'Restaurantes',
  go: 'Ir a sección principal',
  url: 'Url'
};

export const CARD_TYPE = {
  promotion: {
    name: 'promotion',
    es: {
      textLink: 'Ver Condiciones',
      shareText: 'Compartir'
    },
    en: {
      textLink: 'See Promotion',
      shareText: 'Share'
    },
    pt: {
      textLink: 'Veja promoção',
      shareText: 'partilhar'
    }
  },
  experience: {
    name: 'experience',
    es: {
      textLink: 'Ver Experiencia',
      shareText: 'Compartir'
    },
    en: {
      textLink: 'See Experience',
      shareText: 'Share'
    },
    pt: {
      textLink: 'veja a experiência',
      shareText: 'partilhar'
    }
  },
  restrictions: {
    name: 'restrictions',
    es: {
      textLink: 'Términos y Restricciones',
      shareText: 'Compartir'
    },
    en: {
      textLink: 'Terms and Restrictions',
      shareText: 'Share'
    },
    pt: {
      textLink: 'Termos e restrições',
      shareText: 'partilhar'
    }
  }
};

export const FORM_TYPE = {
  image: 'formImage',
  imgName: 'image',
  video: 'formVideo',
  vidName: 'video',
  iframe: 'formIframe',
  ifrName: 'iframe',
  mapName: 'map'
};

export enum LANGUAGE_TYPES_CODE {
  es = 'es',
  en = 'en',
  pt = 'pt'
}

export const SLIDER_TIMER = 5000;

export const STORAGE_KEY = {
  pageInfo: 'pageInfo',
  userToken: 'USER_TOKEN',
  // userProfile: 'USER_PROFILE',
  // userPermissions: 'USER_PERMISSIONS',
  // logoInfo: 'LOGO_INFO',
  configurations: 'CONFIGURATIONS',
  descriptor: 'DESCRIPTOR',
  currency: 'ca_currency'
};

export const SPINNER_CONFIG: ISpinnerConfig = {
  spinnerIconClass: 'caicon-ca'
};

export const SPINNER_CONFIG_SITE: ISpinnerConfig = {
  spinnerClass: 's-loader',
  spinnerImg: 'assets/images/casa-andina.svg'
};

export const PAGE_TITLES = {
  benefits: {
    es: {
      title: 'Beneficios de reservar On-line'
    },
    en: {
      title: 'Benefits of booking online'
    },
    pt: {
      title: 'Benefícios da reserva online'
    }
  }
};

export const SLD_RESTAURANT_INPUTS = {
  direction: {
    reverse: 'reverse',
    normal: 'normal'
  },
  types: {
    large: 'large',
    normal: 'normal'
  },
  pageTypes: {
    hotel: 'hotel',
    restaurant: 'restaurant'
  }
};

export const BANNER_COLOR = {
  white: '#ffffff',
  deft: '#d69c4f'
};

export const SERVICE_CODE = {
  hotel: 1,
  room: 2
};

export const NAV_HEADER = {
  room: 'room',
  activites: 'activites'
};

export enum IMAGE_SIZE {
  sm = 'sm',
  md = 'md',
  lg = 'lg'
}

export const FEATURE_TYPES = {
  benefit: 'benefit'
};

export const BANNER_HEIGHT = {
  sm: 461,
  md: 600
};

export const OPINION_INFO = {
  bodyLength: 140
};

export const COLORS = {
  default: '#d69c4f',
  white: '#ffffff'
};

export const SEARCH_COPY = {
  labels: {
    hotel: {
      es: 'HOTEL',
      en: 'HOTEL',
      pt: 'HOTEL'
    },
    destination: {
      es: 'DESTINO, HOTEL',
      en: 'DESTINATION, HOTEL',
      pt: 'DESTINO, HOTEL'
    },
    destination2: {
      es: 'DESTINO',
      en: 'DESTINATION',
      pt: 'DESTINO'
    },
    entry: {
      es: 'ENTRADA',
      en: 'CHECK-IN',
      pt: 'ENTRADA'
    },
    leave: {
      es: 'SALIDA',
      en: 'CHECK-OUT',
      pt: 'SALIDA'
    },
    code: {
      es: 'Código',
      en: 'Code',
      pt: 'Code'
    },
    promotionalCode: {
      es: 'Código promocional',
      en: 'Promotional code',
      pt: 'Código promocional'
    }
  },
  placeholders: {
    hotel: {
      es: `Busca 'Standard, Select, Premium'`,
      en: `Search 'Standard, Select, Premium'`,
      pt: `Fanfarrão 'Standard, Select, Premium'`
    },
    destination: {
      es: 'Ingrese su destino',
      en: 'enter your destination',
      pt: 'insira seu destino'
    },
    destination2: {
      es: `Busca 'Arequipa'`,
      en: `Search 'Arequipa'`,
      pt: `Fanfarrão 'Arequipa'`
    },
    code: {
      es: 'Ingresa tu código',
      en: 'Enter your code',
      pt: 'Insira seu código'
    },
    promotionalCode: {
      es: 'Opcional',
      en: 'Optional',
      pt: 'Opcional'
    }
  },
  buttons: {
    search: {
      es: 'BUSCAR',
      en: 'SEARCH',
      pt: 'ACHAR'
    }
  }
};

export const GeneralLang = {
  notifications: {
    success: 'Se realizó correctamente',
    notSaveCorrectly: 'No se guardo correctamente',
    completeFields: 'Completa los campos',
    existIncorrectFields: 'Existen campos incorrectos',
    thereAreChangesWithoutSaving: 'Existen cambios sin guardar',
    fieldsNotEquals: 'Los campos de %field% deben ser iguales'
  }
};
export const DESTINATIONS_MAP = {
  es: {
    mapTitle: 'ENCUENTRA TU HOTEL',
    subtitle: 'Elige un destino y comienza a vivir tu mejor experiencia',
    contactTitle: 'Directorio de hoteles Casa Andina',
    contactSubtitle: 'Elige un destino y comienza a vivir tu mejor experiencia',
    arrive: 'Como llegar',
    reserve: 'Reservar',
    selected: 'Selecciona un hotel'
  },
  en: {
    mapTitle: 'FIND YOUR HOTEL',
    subtitle: 'Choose a destination and start living the best experience',
    contactTitle: 'Hotel Directory Casa Andina',
    contactSubtitle: 'Choose a destination and start living the best experience',
    arrive: 'How to arrive',
    reserve: 'Reserve',
    selected: 'Choose a hotel'
  },
  pt: {
    mapTitle: 'Encontre o seu hotel',
    contactTitle: 'Directório de hotéis Casa Andina',
    subtitle: 'Escolha um destino e comece a viver sua melhor experiência',
    contactSubtitle: 'Escolha um destino e comece a viver sua melhor experiência',
    arrive: 'Como chegar',
    reserve: 'Reservar',
    selected: 'Escolha um hotel'
  }
};

export const TYPE_DESCRIPTORS_ID = {
  hotel: 'dad19020-cf17-4435-9cbe-38ab9d34bf85',
  restaurant: '0f9e341c-11dc-48db-9efe-88ae7202c64b'
};

export const GO_HOTEL = {
  es: 'Ir a hotel',
  en: 'Go to hotel',
  pt: 'Ir para o hotel'
};

export const SEE_PROMOTIONS = {
  es: 'Ver promociones',
  en: 'See promotions',
  pt: 'veja promoções'
};

export const TYPE_DESCRIPTOR_PROMOTION = {
  hotels: 'Hoteles',
  restaurants: 'Restaurantes'
};

export const LOW_PRICE = {
  from: {
    es: 'Desde',
    en: 'From',
    pt: 'De'
  }
};

export const AWARDS = {
  es: 'Premios y certificados',
  en: 'Awards and Certifications',
  pt: 'prêmios e certificações'
};

export const CURRENCY_SYMBOl = {
  PEN: 'S/',
  USD: '$',
  BRL: 'R$'
};

export const CURRENCY_KEYS = {
  PEN: 'PEN',
  USD: 'USD',
  BRL: 'BRL'
};

export const CARD_RESERVATION = {
  es: {
    reserve: 'Reservar'
  },
  en: {
    reserve: 'Book'
  },
  pt: {
    reserve: 'Reservar'
  }
};
