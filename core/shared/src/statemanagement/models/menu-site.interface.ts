import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';

export interface IMenuSite {
  id?: string;
  name: string;
  type: MENU_SITE_TYPE;
  action: MENU_SITE_ACTION;
  order?: number;
  languageCode: LANGUAGE_TYPES_CODE;
  createdDate?: string;
  updatedDate?: string;
  pageId: string;
  parentId?: string;
  pagePath?: string;
  sectionFooter?: boolean;
  external?: boolean;
  phone?: string;
  email?: string;

  position?: number;
  hover?: boolean;
  active?: boolean;
  collapse?: boolean;
  children?: Array<IMenuSite>;
}

export enum MENU_SITE_ACTION {
  BOOKING = 'booking',
  REDIRECT = 'redirect',
  PROMOTION_ALL = 'promotion_all'
}

export enum MENU_SITE_TYPE {
  HEADER = 'header',
  FOOTER = 'footer'
}
