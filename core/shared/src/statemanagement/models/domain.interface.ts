import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';

export interface IDomain {
  id?: string;
  path: string;
  tax: number;
  state: number;
  promotionAllId: string;
  createdDate: string;
  language: LANGUAGE_TYPES_CODE;
  currency: string;
  location?: string;
  locationPath?: string;
  locationPathFormat?: string;
  hideLanguage?: boolean;
  promotionPaths?: Array<IPromotionPath>;
  isCurrent?: boolean;
}

export interface IPromotionPath {
  language: string;
  path: string;
}
