import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SectionHeaderComponent } from './section-header.component';

@NgModule({
    imports: [ CommonModule ],
    declarations: [ SectionHeaderComponent ],
    exports: [ SectionHeaderComponent ]
})
export class SectionHeaderModule { }
