import { Component, Input, OnInit } from '@angular/core';
import { shareSocialNetworks } from '@ca-core/shared/helpers/util/networks-share';
import { SCREEN_NAMES } from '@ca-core/shared/helpers/util/resize.service';
import { CARD_RESERVATION, CARD_TYPE, LOW_PRICE, TYPE_DESCRIPTOR_PROMOTION } from 'core/shared/src/settings/constants/general.constant';
import { ICard } from './card';

@Component({
  selector: 'ca-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  private _classSize = 's-card-wrapper--sm';
  CARD_TYPE = CARD_TYPE;
  TYPE_DESCRIPTOR_PROMOTION = TYPE_DESCRIPTOR_PROMOTION;
  LOW_PRICE = LOW_PRICE;
  SCREEN_NAMES = SCREEN_NAMES;
  CARD_RESERVATION = CARD_RESERVATION;
  @Input() data: ICard;
  @Input() type: string = CARD_TYPE.promotion.name;
  @Input() lang: string;

  get classSize(): string {
    return this._classSize;
  }
  @Input()
  set classSize(value: string) {
    this._classSize = value;
  }

  ngOnInit(): void {
  }
  share(network): void {
    const domain = JSON.parse(sessionStorage.getItem('CONFIGURATIONS')).domain;
    const url = `${domain}${(this.data.urlItem as any).url}`;
    shareSocialNetworks(network, { title: this.data.title, url });
  }
}
