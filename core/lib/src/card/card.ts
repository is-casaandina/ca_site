export interface ICard {
  title?: string;
  position?: string;
  descriptor?: string;
  type?: string;
  descriptorInfo?: any;
  description?: string;
  hotel?: string;
  urlItem?: string;
  backgroundImage?: any;
  hotelName?: string;
  classNative?: string;
  discount?: number;
  urlReservation?: number;
}
