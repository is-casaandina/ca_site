import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ElemBackgroundurlModule, UrlLinkModule } from '../directives';
import { PromoDetailModule } from '../directives/promo-detail';
import { CardComponent } from './card.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      UrlLinkModule,
      ElemBackgroundurlModule,
      PromoDetailModule
    ],
    declarations: [ CardComponent ],
    exports: [ CardComponent ]
})
export class CardModule {
}
