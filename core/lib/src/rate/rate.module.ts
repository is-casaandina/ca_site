import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HoverCollapseModule } from '../directives';
import { RateComponent } from './rate.component';

@NgModule({
    imports: [ CommonModule, FormsModule, HoverCollapseModule ],
    declarations: [ RateComponent ],
    exports: [ RateComponent ]
})
export class RateModule {
}
