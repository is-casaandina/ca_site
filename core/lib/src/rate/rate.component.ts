import { Component, ElementRef, Input } from '@angular/core';
import { RATE_TEXT } from '@ca-admin/settings/constants/general.lang';
import { StorageService } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { IDescriptor, IOpinionRate } from 'core/typings';

@Component({
  selector: 'ca-rate',
  templateUrl: './rate.component.html'
})
export class RateComponent extends ComponentBase {
  cardWidth: string;
  state = false;
  RATE_TEXT = RATE_TEXT;

  @Input() opinionsRate: IOpinionRate;
  @Input() descriptorInfo?: IDescriptor;
  @Input() lang: string;

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService) {
    super(elem, storageService);
  }
}
