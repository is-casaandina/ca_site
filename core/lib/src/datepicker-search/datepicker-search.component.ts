import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ca-datepicker-search',
  templateUrl: './datepicker-search.component.html'
})
export class DatepickerSearchComponent implements OnInit {
  @Input() label: string;
  @Input() fbControl: AbstractControl;
  @Input() type: string;
  @Input() minDate: string;
  model: any;
  dateFormated: any;
  parsedMinDate: string;

  constructor(private parserFormatter: NgbDateParserFormatter) {
  }

  ngOnInit(): void {
    this.model = this.parserFormatter.parse(this.fbControl.value);
    this.dateFormated = this.fbControl.value;
  }

  parseStringToNbdate(dateStr: string): NgbDateStruct {
    return this.parserFormatter.parse(dateStr);
  }

  selectingDate(): void {
    this.dateFormated =  this.parserFormatter.format(this.model);
    this.fbControl.setValue(this.dateFormated);
    if (this.type === 'max') {
      this.parsedMinDate = this.model;
    }
  }
}
