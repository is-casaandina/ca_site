import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { I18nEsPeModule } from '@ca-core/shared/helpers/i18n-es-pe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DatepickerSearchComponent } from './datepicker-search.component';

@NgModule({
    imports: [
      CommonModule,
      NgbModule,
      ReactiveFormsModule,
      FormsModule,
      I18nEsPeModule
    ],
    declarations: [ DatepickerSearchComponent ],
    exports: [ DatepickerSearchComponent ]
})
export class DatepickerSearchModule {
}
