import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input } from '@angular/core';
import { ScrollUtil } from '@ca-core/shared/helpers/util/scrolling';
import { DESCRIPTOR_TYPE_ID } from 'core/shared/src/settings/constants/general.constant';

/** @dynamic */
@Component({
  selector: 'ca-slider-button',
  templateUrl: './slider-button.component.html'
})
export class SliderButtonComponent {
  @Input() slider: any;
  @Input() isCollapsed: boolean;

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) { }

  doAction(typeDescriptor, urlButton = null): void {
    console.log(typeDescriptor);
    if (typeDescriptor === DESCRIPTOR_TYPE_ID.go) {
      this.goToSection();
    } else if (typeDescriptor === DESCRIPTOR_TYPE_ID.url){
        location.href = urlButton
    }else {
      this.isCollapsed = !this.isCollapsed;
    }
  }

  goToSection(): void {
    const elem: any = this.document.querySelector('[master]');
    const height = elem.offsetTop;

    ScrollUtil.goTo(height)
      .subscribe(() => { });
  }
}
