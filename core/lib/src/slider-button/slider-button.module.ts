import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { HoverCollapseModule, UrlLinkModule } from '../directives';
import { SliderButtonComponent } from './slider-button.component';

@NgModule({
    imports: [
      CommonModule,
      CollapseModule,
      HoverCollapseModule,
      UrlLinkModule
     ],
    declarations: [ SliderButtonComponent ],
    exports: [ SliderButtonComponent ]
})
export class SliderButtonModule {
}
