import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VideoAutoplayDirective } from './video-autoplay.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [VideoAutoplayDirective],
  exports: [VideoAutoplayDirective]
})
export class VideoAutoplayModule {}
