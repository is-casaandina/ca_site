import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[caVideoAutoplay]'
})
export class VideoAutoplayDirective implements OnInit {
  constructor(private elementRef: ElementRef<HTMLVideoElement>) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.elementRef.nativeElement.play().then(() => {});
    }, 1000);
  }
}
