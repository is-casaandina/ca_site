import {
  Directive,
  ElementRef,
  HostListener,
  Renderer2
} from '@angular/core';
import { ResizeService, SCREEN_NAMES } from '@ca-core/shared/helpers/util/resize.service';

@Directive({
  selector: '[caHoverCollapse]'
})
export class HoverCollapseDirective {

  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2,
    private resizeService: ResizeService) {
  }

  @HostListener('click') onClick(): void {
    if (!this.resizeService.is(SCREEN_NAMES.XL)) {
      const list = this.elementRef.nativeElement.querySelector('.g-collapse-list') as HTMLElement;
      if (list) {
        list.classList.contains('active')
          ? this.renderer2.removeClass(list, 'active')
          : this.renderer2.addClass(list, 'active');
      }
    }
  }

  @HostListener('mouseenter') onMouseOver(): void {
    if (this.resizeService.is(SCREEN_NAMES.XL)) {
      const list = this.elementRef.nativeElement.querySelector('.g-collapse-list');
      if (list) {
        this.renderer2.addClass(list, 'active');
      }
    }
  }

  @HostListener('mouseleave') onMouseOut(): void {
    if (this.resizeService.is(SCREEN_NAMES.XL)) {
    }
    const list = this.elementRef.nativeElement.querySelector('.g-collapse-list');
    if (list) {
      this.renderer2.removeClass(list, 'active');
    }
  }
}
