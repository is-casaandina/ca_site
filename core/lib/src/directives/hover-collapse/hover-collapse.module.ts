import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HoverCollapseDirective } from './hover-collapse.directive';

@NgModule({
  declarations: [HoverCollapseDirective],
  imports: [
    CommonModule
  ],
  exports: [HoverCollapseDirective]
})
export class HoverCollapseModule { }
