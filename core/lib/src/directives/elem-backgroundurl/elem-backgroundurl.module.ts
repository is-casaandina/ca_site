import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ElemBackgroundurlDirective } from './elem-backgroundurl.directive';

@NgModule({
  declarations: [ElemBackgroundurlDirective],
  imports: [
    CommonModule
  ],
  exports: [ElemBackgroundurlDirective]
})
export class ElemBackgroundurlModule { }
