
export interface IImageDetail {
  imageUri: string;
  size: number;
  height: number;
  width: number;
}

export interface IImage {
  name: string;
  small: IImageDetail;
  medium: IImageDetail;
  large: IImageDetail;
  selected?: boolean;
}
