import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { ResizeService, SCREEN_NAMES } from '@ca-core/shared/helpers/util/resize.service';
import { IBackgroundImage } from 'core/typings';

@Directive({
  selector: '[caBackgroundurl]'
})
export class ElemBackgroundurlDirective {
  private _caBackgroundurl: IBackgroundImage;
  @Input()
  set caBackgroundurl(caBackgroundurl: IBackgroundImage) {
    this._caBackgroundurl = caBackgroundurl;
    this.init();
  }
  get caBackgroundurl(): IBackgroundImage {
    return this._caBackgroundurl;
  }

  constructor(private elementRef: ElementRef, private renderer2: Renderer2, private resizeService: ResizeService) {}

  private init(): void {
    if (!this.caBackgroundurl && (this.caBackgroundurl && !this.caBackgroundurl.name)) {
      return void 0;
    }

    const img = this.getImage();
    const xsmall = this.getXSmall();
    this.loadImage(xsmall, true);
    this.preLoadImage(img);
  }

  private preLoadImage(image: string): void {
    const newImg = new Image();
    newImg.onload = () => {
      this.loadImage(image);
    };
    newImg.src = image;
  }

  private loadImage(image: string, filter?: boolean): void {
    this.renderer2.setStyle(this.elementRef.nativeElement, 'background-image', `url(${image})`);
    if (filter) {
      this.renderer2.setStyle(this.elementRef.nativeElement, 'filter', 'blur(10px)');

      return void 0;
    }

    this.renderer2.removeStyle(this.elementRef.nativeElement, 'filter');
  }

  private getImage(): string {
    const sizeImage = this.resizeService.is(SCREEN_NAMES.XS)
      ? 'small'
      : this.resizeService.is(SCREEN_NAMES.SM) || this.resizeService.is(SCREEN_NAMES.MD)
      ? 'medium'
      : 'large';

    return (this.caBackgroundurl && this.caBackgroundurl[sizeImage] && this.caBackgroundurl[sizeImage].imageUri) || '';
  }

  private getXSmall(): string {
    const tmp = this.caBackgroundurl.large.imageUri.split('/');
    const tmpName = this.caBackgroundurl.name.split('.');
    tmpName[0] = `${tmpName[0]}-xsmall`;
    tmp[tmp.length - 1] = tmpName.join('.');

    return tmp.join('/');
  }
}
