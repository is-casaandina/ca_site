import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { PLATFORMS, PlatformService } from 'core/shared/src/helpers/util/platform.service';
import { IBackgroundImage } from 'core/typings';

@Directive({
  selector: '[caImgSrcset]'
})
export class ImgSrcsetDirective implements OnInit {

  private _caImgSrcset: IBackgroundImage;
  @Input()
  set caImgSrcset(caImgSrcset: IBackgroundImage) {
    this._caImgSrcset = caImgSrcset;
    this.setInfo();
  }
  get caImgSrcset(): IBackgroundImage {

    return this._caImgSrcset;
  }

  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2,
    private platformService: PlatformService
  ) { }

  ngOnInit(): void {
    this.setInfo();
  }

  setInfo(): void {
    if (this.caImgSrcset && this.caImgSrcset.large && this.caImgSrcset.medium && this.caImgSrcset.small) {
      this.renderer2.setAttribute(
        this.elementRef.nativeElement,
        'srcset',
        `${this.caImgSrcset.large.imageUri}, ${this.caImgSrcset.medium.imageUri}, ${this.caImgSrcset.small.imageUri}`
      );

      const sizeImage = (this.platformService.is(PLATFORMS.IE))
                          ? 'large'
                          : 'small';
      this.renderer2.setAttribute(this.elementRef.nativeElement, 'src', this.caImgSrcset[sizeImage].imageUri);
      this.renderer2.setAttribute(this.elementRef.nativeElement, 'title', this.caImgSrcset.properties.title);
      this.renderer2.setAttribute(this.elementRef.nativeElement, 'alt', this.caImgSrcset.properties.alt);
    } else {
      this.renderer2.setAttribute(
        this.elementRef.nativeElement,
        'srcset',
        `'', '', ''`
      );

      this.renderer2.setAttribute(this.elementRef.nativeElement, 'src', '');
      this.renderer2.setAttribute(this.elementRef.nativeElement, 'title', '');
      this.renderer2.setAttribute(this.elementRef.nativeElement, 'alt', '');
    }
  }
}
