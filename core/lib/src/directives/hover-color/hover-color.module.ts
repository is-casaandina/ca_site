import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HoverColorDirective } from './hover-color.directive';

@NgModule({
  declarations: [HoverColorDirective],
  imports: [
    CommonModule
  ],
  exports: [HoverColorDirective]
})
export class HoverColorModule { }
