import {
  Directive,
  ElementRef,
  Input,
  Renderer2,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[caHoverColor]'
})
export class HoverColorDirective {
  @Input() caHoverColor: string;

  constructor(private elementRef: ElementRef, private renderer2: Renderer2) {}

  @HostListener('mouseover') onMouseOver(): void {
    this.renderer2.setStyle(
      this.elementRef.nativeElement,
      'background-color',
      this.caHoverColor || ''
    );

    this.renderer2.setStyle(
      this.elementRef.nativeElement,
      'color',
      '#ffffff' || ''
    );
  }

  @HostListener('mouseout') onMouseOut(): void {
    const hasActiveCls  = this.elementRef.nativeElement.className.indexOf('active') !== -1;

    this.renderer2.setStyle(
      this.elementRef.nativeElement,
      'background-color',
      hasActiveCls ? this.caHoverColor : ''
    );

    this.renderer2.setStyle(
      this.elementRef.nativeElement,
      'color',
      hasActiveCls ? '#ffffff' : this.caHoverColor
    );
  }
}
