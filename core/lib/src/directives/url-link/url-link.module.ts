import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UrlLinkDirective } from './url-link.directive';

@NgModule({
  declarations: [UrlLinkDirective],
  imports: [
    CommonModule
  ],
  exports: [UrlLinkDirective]
})
export class UrlLinkModule { }
