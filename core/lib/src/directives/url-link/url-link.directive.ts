import { Directive, ElementRef, HostListener, Input, OnInit, Renderer2 } from '@angular/core';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { Ilink } from 'core/typings';

@Directive({
  selector: '[caUrlLink]'
})
export class UrlLinkDirective implements OnInit {
  private _caUrlLink: Ilink;
  @Input()
  set caUrlLink(caUrlLink: Ilink) {
    this.setAttributeLink(caUrlLink);
    this._caUrlLink = caUrlLink;
  }
  get caUrlLink(): Ilink {
    return this._caUrlLink;
  }

  @Input() isLinkable = true;

  constructor(private elementRef: ElementRef, private renderer2: Renderer2) {}

  ngOnInit(): void {
    this.setAttributeLink(this.caUrlLink);
  }

  setAttributeLink(caUrlLinkBefore): void {
    if (!this.isLinkable) {
      return void 0;
    }

    const urlLink = (caUrlLinkBefore && caUrlLinkBefore.url) || 'javascript:void(0)';

    if (caUrlLinkBefore && caUrlLinkBefore.isExternal) {
      this.renderer2.setAttribute(this.elementRef.nativeElement, 'target', '_blank');
    }
    this.renderer2.setAttribute(this.elementRef.nativeElement, 'href', urlLink);
  }

  @HostListener('click', ['$event']) click(event: MouseEvent): void {
    event.preventDefault();
    if (!this.caUrlLink || (this.caUrlLink && !this.caUrlLink.url)) {
      return void 0;
    }

    if (!this.caUrlLink.isExternal) {
      GlobalNavigateUtil.goTo(this.caUrlLink.url);

      return void 0;
    }

    if (!this.isLinkable) {
      window.open(this.caUrlLink.url, '_blank');
    }
  }
}
