import {
  Directive,
  ElementRef,
  HostListener
} from '@angular/core';

@Directive({
  selector: '[caPromoDetail]'
})
export class PromoDetailDirective {

  private promoElem = this.elementRef.nativeElement;

  constructor(private elementRef: ElementRef) {}

  @HostListener('click') onClick(): void {
    if (this.promoElem) {
      const elemClass = this.promoElem.classList;
      elemClass.contains('show-detail')
        ? elemClass.remove('show-detail')
        : elemClass.add('show-detail');
    }
  }

  @HostListener('mouseleave') onLeave(): void {
    if (this.promoElem) {
      this.promoElem.classList.remove('show-detail');
    }
}

}
