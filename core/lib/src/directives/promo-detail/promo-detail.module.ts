import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PromoDetailDirective } from './promo-detail.directive';

@NgModule({
  declarations: [PromoDetailDirective],
  imports: [
    CommonModule
  ],
  exports: [PromoDetailDirective]
})
export class PromoDetailModule { }
