import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MymeTypeDirective } from './myme-type.directive';

@NgModule({
  declarations: [MymeTypeDirective],
  imports: [
    CommonModule
  ],
  exports: [MymeTypeDirective]
})
export class MymeTypeModule { }
