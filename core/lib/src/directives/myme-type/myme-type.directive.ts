import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[caMymeType]'
})
export class MymeTypeDirective implements OnInit {

  @Input() caMymeType: string;
  @Input() caVideoUrl: string;

  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) { }

  ngOnInit(): void {
    if (!this.caMymeType) {
      this.caMymeType =  this.validURL(this.caVideoUrl) ? this.getExtension(this.caVideoUrl) : 'video/mp4';
    }

    this.renderer2.setAttribute(this.elementRef.nativeElement, 'type', this.caMymeType);
  }

  private validURL(str: string): boolean {
    const pattern = new RegExp('^(https?:\\/\\/)?' +
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' +
      '((\\d{1,3}\\.){3}\\d{1,3}))' +
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' +
      '(\\?[;&a-z\\d%_.~+=-]*)?' +
      '(\\#[-a-z\\d_]*)?$', 'i');

    return !!pattern.test(str);
  }

  getExtension(url: string): string {
    const urlParts = url.split('.');
    let extension = urlParts.length ? urlParts[urlParts.length - 1] : 'mp4';
    extension = `video/${extension}`;

    return extension;
  }
}
