import { Component, EventEmitter, Input, Output } from '@angular/core';
import { COLORS, DESCRIPTOR_TYPE_ID } from '@ca-core/shared/settings/constants/general.constant';

@Component({
  selector: 'ca-banner-caption',
  templateUrl: './banner-caption.component.html'
})
export class BannerCaptionComponent {

  @Input() title: string;
  @Input() subtitle: string;
  @Input() isCollapsed: boolean;
  private _slider: any;
  @Input()
  set slider(slider: any) {
    this.setInfo(slider);
    this._slider = slider;

    
  }
  get slider(): any {
    return this._slider;
  }
  @Output() updateHoverState = new EventEmitter<boolean>();

  constructor() {
    
   }

  textAlign: string;
  DESCRIPTOR_TYPE_ID = DESCRIPTOR_TYPE_ID;
  COLORS = COLORS;
  titlecss : any; 
  cssarray : any;
  plantilacss : any
  csstext : any
  setInfo(slider): void {
    var arrayStyle = []
    this.textAlign = slider ? `g-main-banner-title--${slider.align}` : '';
   
    if(slider.classCss){
      if(slider.areaCss){

        arrayStyle = slider.areaCss.split('.')
        console.log(arrayStyle);

        var styleEl = document.createElement('style');
        document.head.appendChild(styleEl);
        styleEl.appendChild(document.createTextNode(''));
        
       
        for (let i = 1; i < arrayStyle.length; i++) {
          this.isValidRule(styleEl.sheet,'.'+arrayStyle[i],i -1)
        }

        
      } 
    }
  }

  mouseHoverState(state: boolean): void {
    this.updateHoverState.emit(state);
  }

  isValidRule(file,rule,i) {
    try {

      ( file as CSSStyleSheet).insertRule(rule,i) 
      return true
    } catch (err) {
      return false
    }
  }





}
