import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { SliderButtonModule } from '../slider-button/public_api';
import { BannerCaptionComponent } from './banner-caption.component';

@NgModule({
    imports: [
      CommonModule,
      SliderButtonModule,
      CollapseModule
    ],
    declarations: [ BannerCaptionComponent ],
    exports: [ BannerCaptionComponent ]
})
export class BannerCaptionModule {

}
