import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CarouselControlModule, CarouselControlService } from '../carousel-control/public_api';
import { CarouselSlideDirective } from './carousel-slide.directive';
import { CarouselComponent } from './carousel.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      CarouselControlModule
    ],
    declarations: [ CarouselComponent, CarouselSlideDirective ],
    providers: [CarouselControlService],
    exports: [ CarouselComponent, CarouselSlideDirective ]
})
export class CarouselModule {
}
