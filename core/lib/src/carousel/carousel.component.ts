import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { takeUntil } from 'rxjs/operators';
import { CarouselControlComponent, CarouselControlService } from '../carousel-control/public_api';

@Component({
  selector: 'ca-carousel',
  templateUrl: './carousel.component.html'
})
export class CarouselComponent extends UnsubscribeOnDestroy implements AfterViewInit, OnInit {

  @ViewChild('controls') carouselControls: CarouselControlComponent;

  @Input() sliders?: Array<any>;
  @Input() height: string;
  @Input() animation: boolean;
  @Output() watchActiveVal = new EventEmitter<number>();
  @Input() controlColor?: string;
  private _updateWidth = false;
  @Input()
  set updateWidth(updateWidth: boolean) {
    this._updateWidth = updateWidth;
    if (updateWidth) { this.getWidth(); }
  }
  get updateWidth(): boolean {
    return this._updateWidth;
  }
  elementWidthInPx = '';
  elementWidthSum = 0;
  elementWidth = 0;
  transitionNormal = 'transform 1500ms ease 0s';
  transitionFast = 'transform 500ms ease 0s';
  transitionTime = 'transform 1500ms ease 0s';

  currentWidth: number;
  constructor(
    private carouselControlService: CarouselControlService,
    private elem: ElementRef,
    private renderer2: Renderer2,
    private ngZone: NgZone,
    private resizeService: ResizeService
  ) {
    super();
  }

  ngOnInit(): void {
    this.resizeService.resizeEvent
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.ngZone.run(() => {
          this.superLeftMove();
        });
      });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.carouselControlService.moveDirection
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe((moveInfo: any) => {
          this.ngZone.run(() => {
            this.elementWidth = this.getWidth();
            if (moveInfo.direction === 'L') {
              this.leftMove();
            } else if (moveInfo.direction === 'R') {
              this.rightMove();
            } else {
              this.superLeftMove();
            }
          });
        });
      if (window.innerWidth <= 991) {
        setTimeout(() => {
          this.initSwiper();
        }, 500);
      }
      this.elementWidth = this.getWidth();
      this.elementWidthInPx = `translateX(0px)`;
    }, 500);

    setTimeout(() => {
      this.setSectionColor();
    }, 1000);
  }

  getWidth(): number {
    return this.elem.nativeElement.querySelector('.carousel-group-item')
      ? this.elem.nativeElement.querySelector('.carousel-group-item').offsetWidth
      : 0;
  }

  rightMove(): void {
    this.transitionTime = this.transitionFast;
    this.elementWidthSum = this.elementWidthSum - this.elementWidth;
    this.elementWidthInPx = `translateX(${this.elementWidthSum}px)`;
  }

  leftMove(): void {
    this.transitionTime = this.transitionFast;
    this.elementWidthSum = this.elementWidth + this.elementWidthSum;
    this.elementWidthInPx = `translateX(${this.elementWidthSum}px)`;
  }

  superLeftMove(): void {
    this.transitionTime = this.transitionFast;
    this.elementWidthSum = 0;
    this.elementWidthInPx = `translateX(-${this.elementWidthSum}px)`;
  }

  setSectionColor(): void {
    const sectionElem = this.elem.nativeElement
      .closest('.g-section') as HTMLElement;
    const sectionElemColor = sectionElem ? sectionElem.style.backgroundColor : '';
    const carouselAfterElem = this.elem.nativeElement.querySelector('.g-carousel-content-after');
    const carouselBeforeElem = this.elem.nativeElement.querySelector('.g-carousel-content-before');
    if (carouselAfterElem) {
      this.renderer2.setStyle(carouselAfterElem, 'background-color', sectionElemColor);
    }
    if (carouselBeforeElem) {
      this.renderer2.setStyle(carouselBeforeElem, 'background-color', sectionElemColor);
    }
  }

  private initSwiper(): void {
    const carouselContent: HTMLElement = this.elem.nativeElement.querySelector('.g-carousel-content');
    if (carouselContent) {
      carouselContent.addEventListener('touchstart', event => {
        const timerSucriber = this.carouselControls.timerSubscription;
        if (timerSucriber) { timerSucriber.unsubscribe(); }
        const dFlex: HTMLElement = this.elem.nativeElement.querySelector('.d-flex');
        const elementWidth = this.getWidth();
        const initTransform = dFlex.style.transform;
        const totalWidth = this.elem.nativeElement.querySelector('.g-carousel-content--wrap').offsetWidth - elementWidth;
        const currentTransformValue = +initTransform
          .split('(')[1]
          .replace('px)', '');
        let moving;

        const onTouchEnd = eventOut => {

          if (moving) {
            const byInitPoint = moving.calcMovingWithInitPoint;
            if (byInitPoint < -100 && moving.value > (totalWidth * -1)) {
              this.carouselControls.next();
            } else if (byInitPoint > 100 && moving.value <= 0) {
              this.carouselControls.prev();
            } else {
              dFlex.style.transform = initTransform;
            }
          }
          this.carouselControls.startAnimation();
          event.target.removeEventListener('touchmove', onTouchMove);
          event.target.removeEventListener('touchend', onTouchEnd);
        };
        const onTouchMove = eventMove => {
          moving = (() => {
            const calcMovingWithInitPoint = eventMove.touches[0].clientX - event.changedTouches[0].clientX;

            return {
              value: currentTransformValue + calcMovingWithInitPoint,
              calcMovingWithInitPoint
            };
          })();

          if (moving.value <= 0 && moving.value > (totalWidth * -1)) {
            dFlex.style.transform = `translateX(${moving.value}px)`;
          }
        };
        event.target.addEventListener('touchmove', onTouchMove);
        event.target.addEventListener('touchend', onTouchEnd);
      });
    }
  }
}
