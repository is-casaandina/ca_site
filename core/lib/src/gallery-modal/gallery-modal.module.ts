import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { CarouselControlModule } from '../carousel-control/public_api';
import { MymeTypeModule } from '../directives';
import { GalleryModalComponent } from './gallery-modal.component';

@NgModule({
    imports: [
      CommonModule,
      CarouselControlModule,
      PipesModule,
      MymeTypeModule
    ],
    declarations: [ GalleryModalComponent ],
    exports: [ GalleryModalComponent ],
    entryComponents: [ GalleryModalComponent ]
})
export class GalleryModalModule {

}
