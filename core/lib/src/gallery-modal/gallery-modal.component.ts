import { AfterViewInit, Component, NgZone, OnInit } from '@angular/core';
import { FORM_TYPE } from 'core/shared/src/settings/constants/general.constant';
import { CarouselControlService } from '../carousel-control/public_api';

@Component({
  selector: 'ca-gallery-modal',
  templateUrl: './gallery-modal.component.html'
})
export class GalleryModalComponent implements OnInit, AfterViewInit {
  height: string;
  payload?: any;
  activeSlide: number;
  FORM_TYPE = FORM_TYPE;

  constructor(private carouselCtrlService: CarouselControlService, private ngZone: NgZone) {}

  ngOnInit(): void {
    this.activeSlide = this.payload ? this.payload.activeSlide : 0;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.carouselCtrlService.moveDirection.subscribe((moveInfo: any) => {
        this.ngZone.run(() => {
          this.activeSlide = moveInfo.active;
        });
      });
    }, 0);
  }
}
