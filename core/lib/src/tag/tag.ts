export interface ITag {
  title?: string;
  url?: ITagUrl;
}

export interface ITagUrl {
  url?: string;
  isExternal: boolean;
}
