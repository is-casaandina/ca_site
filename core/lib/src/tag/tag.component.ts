import { Component, Input } from '@angular/core';
import { ITag } from './tag';

@Component({
  selector: 'ca-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent {
  @Input() data: ITag;

  constructor() { }

}
