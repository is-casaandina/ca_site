import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CaDatepickerModule, CaInputModule } from '@ca-core/ui/lib/components';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { FormNewsletterComponent } from './form-newsletter.component';
import { TermsModalComponent } from './terms-modal/terms-modal.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      CaDatepickerModule,
      CaInputModule,
      ReactiveFormsModule,
      CaDatepickerModule,
      RecaptchaModule,
      RecaptchaFormsModule
    ],
    providers: [ NewsLetterServices ],
    declarations: [ FormNewsletterComponent, TermsModalComponent ],
    exports: [ FormNewsletterComponent ],
    entryComponents: [ FormNewsletterComponent, TermsModalComponent ]
})
export class FormNewsletterModule { }
