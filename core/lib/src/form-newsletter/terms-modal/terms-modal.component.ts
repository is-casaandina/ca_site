import { Component, OnInit } from '@angular/core';
import { IPayloadModalComponent } from '@ca-core/shared/helpers/modal';

@Component({
  selector: 'ca-modal-restriction',
  template: `<p>{{ payload.content }}</p>`
})
export class TermsModalComponent implements OnInit, IPayloadModalComponent {

  payload: any;

  constructor() {}

  ngOnInit(): void {}

}
