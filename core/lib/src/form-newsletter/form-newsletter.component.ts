import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActiveModal, IPayloadModalComponent, MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { StorageService, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { GeneralLang } from '@ca-core/shared/settings/constants/general.constant';
import { CaDatepickerComponent } from '@ca-core/ui/lib/components';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { takeUntil } from 'rxjs/operators';
import { INewsLetterRequest, ISimpleList, TYPE_PERSON } from './newsletter';
import { TermsModalComponent } from './terms-modal/terms-modal.component';

@Component({
  selector: 'ca-form-newsletter',
  templateUrl: './form-newsletter.component.html',
  styleUrls: ['./form-newsletter.component.scss']
})
export class FormNewsletterComponent extends ComponentBase implements OnInit, AfterViewInit, IPayloadModalComponent {

  @ViewChild('dp') CaDatePicker: CaDatepickerComponent;

  formNewsLetter: FormGroup;
  showOverlay = false;
  GENERAL_LANG = GeneralLang;
  countries: Array<ISimpleList>;
  segments: Array<ISimpleList>;
  genders: Array<ISimpleList>;
  payload: any;
  juridicPerson: boolean;
  recaptcha: any;
  recaptchaKey: string;
  formLabels = [];

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private newsLetterServices: NewsLetterServices,
    private formBuild: FormBuilder,
    private activeModal: ActiveModal,
    private notificationService: NotificationService,
    private modalService: ModalService
  ) {
    super(elem, storageService);
  }

  ngOnInit(): void {
    this.juridicPerson = this.payload.typePerson === TYPE_PERSON.LEGAL_PERSON;
    this.formNewsLetter = this.getFormGroup(this.payload.typePerson);
    this.getFormLabels();
    this.getCountries();
    this.getSegments();
    this.getGenders();
    this.recaptchaKey = this.payload.recaptchaKey;
  }
  ngAfterViewInit(): void {
    if (this.CaDatePicker) {
      const now = this.CaDatePicker.now;
      this.CaDatePicker.datePicker.minDate = { year: now.getFullYear() - 100, month: now.getMonth() + 1, day: now.getDate() };
      this.CaDatePicker.datePicker.maxDate = { year: now.getFullYear() - 18, month: now.getMonth() + 1, day: now.getDate() };
    }
  }
  setLabel(name): string {
    return Object.keys(this.formLabels).length && this.formLabels[name]
    ? this.formLabels[name].text
    : '';
  }
  sendForm(): void {
    if (!this.formErrors().number && this.recaptcha) {
      const body: INewsLetterRequest = {} as INewsLetterRequest;
      Object.keys(this.formNewsLetter.value)
        .forEach(key => {
          if (key === 'birthdate') {
            const value = this.formNewsLetter.value[key];
            const datePipe = new DatePipe('en-US');
            const birthDate = new Date(value.year, value.month, value.day);
            body[key] = datePipe.transform(birthDate, 'yyyy-MM-dd');
          } else {
            body[key] = this.formNewsLetter.value[key];
          }
        });
      this.showOverlay = true;
      this.newsLetterServices.saveSubscriber(body, this.payload.typePerson)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          response => {
            this.notificationService.addSuccess(response.message);
            setTimeout(() => {
              this.showOverlay = false;
              this.activeModal.dismiss();
            }, 3000);
          },
          err => {
            this.notificationService.addWarning(err.message);
            this.showOverlay = false;
          }
        );
    }
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.formNewsLetter, controlName);
  }

  openModal(content): void {
    const modal = this.modalService.open(TermsModalComponent, {
      title: '',
      size: MODAL_BREAKPOINT.lg
    });
    modal.setPayload({ content });
  }
  private checkEmails(form: FormGroup): any {
    const formValue = form.value;

    return formValue.email === formValue.repeatEmail ? null : { notSame: true, name: 'email' };
  }

  private formErrors(): any {
    ValidatorUtil.validateForm(this.formNewsLetter);

    return ValidatorUtil.formErrors(this.formNewsLetter, this.notificationService);
  }

  private getCountries(): void {
    this.newsLetterServices.getCountries()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.countries = response);
  }

  private getSegments(): void {
    this.newsLetterServices.getSegments(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.segments = response);
  }

  private getGenders(): void {
    this.newsLetterServices.getGenders(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.genders = response);
  }

  private getFormLabels(): void {
    this.newsLetterServices.getFormLabels({
        code: `newsletter-${ this.juridicPerson && TYPE_PERSON.LEGAL_PERSON || TYPE_PERSON.PERSON }`,
        language: this.language
      })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        response.forEach(item => {
          this.formLabels[item.name] = {
            text: item.placeholder || item.text,
            type: item.type
          };
        });
      });
  }

  private getFormGroup(typePerson: TYPE_PERSON): FormGroup {
    if (typePerson === TYPE_PERSON.LEGAL_PERSON) {
      return this.formBuild.group({
        name: [null, Validators.required],
        surname: [null, Validators.required],
        email: [this.payload.email || null, [
          Validators.required,
          Validators.email
        ]],
        repeatEmail: [null, Validators.required],
        phone: [null, [
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/)
        ]],
        businessName: [null, Validators.required],
        ruc: [null, Validators.required],
        segment: [null, Validators.required],
        countryId: [null, Validators.required],
        city: [null, Validators.required],
        gender: [null, Validators.required],
        birthdate: [null, Validators.required],
        sendNotification: [false],
        sendNew: [false]
      },
        // tslint:disable-next-line: no-unbound-method
        { validator: this.checkEmails });
    } else {
      return this.formBuild.group({
        name: [null, Validators.required],
        email: [this.payload.email || null, [
          Validators.required,
          Validators.email
        ]],
        phone: [null, [
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/)
        ]],
        countryId: [null, Validators.required],
        city: [null, Validators.required],
        gender: [null, Validators.required],
        sendNotification: [false],
        sendNew: [false]
      });
    }
  }
}
