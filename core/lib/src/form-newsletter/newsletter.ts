export interface ISimpleList {
  name?: string;
  code?: string;
  text?: string;
  value?: string;
}

export interface INewsLetterRequest {
  name: string;
  surname: string;
  email: string;
  businessName: string;
  ruc: number;
  phone: number;
  countryId: string;
  segment: number;
  city: string;
  gender: number;
  birthdate: string;
  sendNotification: boolean;
  sendNew: boolean;
}
export enum TYPE_PERSON {
  PERSON = 'person',
  LEGAL_PERSON = 'legal-person'
}
export interface ILabelsRequest {
  code: string;
  language: string;
}
