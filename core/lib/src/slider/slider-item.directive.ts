import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[caSliderItem]'
})
export class SliderItemDirective {
  @Input() typeSlide: string;
  constructor() { }

}
