export interface Slider {
  path: string;
  alt: string;
}
