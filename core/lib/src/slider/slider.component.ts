import {
  AfterContentInit,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild
} from '@angular/core';

import { FORM_TYPE, SLIDER_TIMER } from '@ca-core/shared/settings/constants/general.constant';
import { interval, Observable, Subscription } from 'rxjs';
import { SliderItemDirective } from './slider-item.directive';
let firstClick;
let movingOpacity = 1;

@Component({
  selector: 'ca-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements AfterContentInit, OnInit {
  private stopAutoplay = false;
  private timerSubscrition: Subscription;

  @ContentChildren(SliderItemDirective, { read: ElementRef }) sliders: QueryList<ElementRef>;
  @Input() color?: string;
  @Input() animation: boolean;
  @Input() buttonHovered: boolean;
  @Input() slides?: any;
  @Input() videoPosition?: number;
  @Output() watchActiveVal = new EventEmitter<number>();
  @ViewChild('slider') carouselContainer: ElementRef<HTMLDivElement>;

  activeIndex = 0;
  slideHeight: string;
  timer: Observable<any>;
  transitionClass = '';
  animationWait = 0;

  constructor(public elem: ElementRef) {}

  ngOnInit(): void {
    this.timer = interval(SLIDER_TIMER);
  }

  setActiveSlide(num = 0): void {
    this.activeIndex = num;
    this.watchActiveVal.emit(num);
    if (this.timerSubscrition) {
      this.timerSubscrition.unsubscribe();
      this.startAnimation();
    }
  }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.setActiveSlide();
      if (this.animation) {
        this.startAnimation();
      }
      if (this.slides && this.slides.length > 1 && window.innerWidth <= 991) {
        this.initSwiper();
      }
    }, 2000);
  }

  startAnimation(): void {
    this.timerSubscrition = this.timer.subscribe(() => {
      if (!this.stopAutoplay && !this.buttonHovered) {
        let newIndex = 0;
        if (this.activeIndex + 1 < this.slides.length) {
          newIndex = this.activeIndex + 1;
        } else if (this.activeIndex + 1 === this.slides.length) {
          newIndex = 0;
        }
        if (this.slides[this.activeIndex].type === FORM_TYPE.video && this.animationWait < 3) {
          this.animationWait++;

          return;
        }

        this.animationWait = 0;
        this.setActiveSlide(newIndex);
      }
    });
  }

  private initSwiper(): void {
    this.elem.nativeElement.addEventListener('touchstart', event => {
      if (this.timerSubscrition) {
        this.timerSubscrition.unsubscribe();
      }
      const img: HTMLElement = this.elem.nativeElement.querySelector('.banner-item.active .item-media');
      const defaultTransform = `translateX(-50%) translateY(-50%)`;
      this.stopAutoplay = true;
      firstClick = event.touches[0].clientX;

      const onTouchEnd = eventOut => {
        movingOpacity = 1;
        img.style.opacity = '1';
        const diff = firstClick - eventOut.changedTouches[0].clientX;
        const calcDiff = diff < 0 ? diff * -1 : diff;
        if (calcDiff > 100) {
          this.setActiveSlide(
            (() => {
              if (firstClick > eventOut.changedTouches[0].clientX) {
                return this.activeIndex < this.slides.length - 1 ? this.activeIndex + 1 : 0;
              } else {
                return this.activeIndex > 0 ? this.activeIndex - 1 : this.slides.length - 1;
              }
            })()
          );
        }
        img.style.transform = defaultTransform;
        this.stopAutoplay = false;
        event.target.removeEventListener('touchmove', onTouchMove);
        event.target.removeEventListener('touchend', onTouchEnd);
      };

      const onTouchMove = eventMove => {
        const moving = (() => {
          let calcMovingWithInitPoint = eventMove.touches[0].clientX - event.changedTouches[0].clientX;
          calcMovingWithInitPoint =
            calcMovingWithInitPoint < 0 ? calcMovingWithInitPoint + 15 : calcMovingWithInitPoint - 15;
          const movingPercent = +((calcMovingWithInitPoint * 0.1) / 100).toFixed(3) * -1;

          return { value: calcMovingWithInitPoint, percent: movingPercent };
        })();

        movingOpacity = moving.percent > 0 ? movingOpacity - moving.percent : movingOpacity + moving.percent;

        if (movingOpacity > 0.2) {
          img.style.opacity = String(movingOpacity);
        }
      };
      event.target.addEventListener('touchmove', onTouchMove);
      event.target.addEventListener('touchend', onTouchEnd);
    });
  }
}
