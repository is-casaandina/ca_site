import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StrPadPipeModule } from '@ca-core/ui/lib/pipes';
import { SliderItemDirective } from './slider-item.directive';
import { SliderComponent } from './slider.component';

@NgModule({
    imports: [ CommonModule, StrPadPipeModule ],
    declarations: [ SliderComponent, SliderItemDirective],
    exports: [ SliderComponent, SliderItemDirective ]
})
export class SliderModule {
}
