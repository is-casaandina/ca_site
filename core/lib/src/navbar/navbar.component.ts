import {
  AfterContentInit,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  Output,
  QueryList,
  ViewChild
} from '@angular/core';
import { IDescriptor } from 'core/typings';
import { ITab } from './navbar';
import { NavbarContentDirective } from './navbar-content.directive';

@Component({
  selector: 'ca-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements AfterContentInit {
  @ContentChildren(NavbarContentDirective, { read: ElementRef }) items: QueryList<ElementRef>;
  @Output() watchActiveVal = new EventEmitter<number>();
  @ViewChild('navbarContent') navbarContent: ElementRef<HTMLDivElement>;
  @Input() tabs: Array<ITab>;
  @Input() type: string;
  @Input() classLi: string;
  @Input() activeIndex = 0;

  showListOption = false;

  private _descriptorInfo: any;

  get descriptorInfo(): IDescriptor {
    return this._descriptorInfo;
  }
  @Input()
  set descriptorInfo(descriptorInfo: IDescriptor) {
    this._descriptorInfo = descriptorInfo;
  }

  constructor(private ngZone: NgZone) {}

  selectTab(index: number = 0): void {
    this.showListOption = false;
    if (this.activeIndex !== index) {
      this.activeIndex = index;
      this.watchActiveVal.emit(index);
    }
  }

  getActiveNav(activeIndex): any {
    return this.tabs && this.tabs.length ? this.tabs[activeIndex].tab : '';
  }

  ngAfterContentInit(): void {
    this.activeIndex = 0;
    this.watchActiveVal.emit(0);
  }

  toggle(): void {
    this.ngZone.run(() => {
      this.showListOption = !this.showListOption;
    });
  }
}
