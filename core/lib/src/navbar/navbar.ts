import { IDescriptor } from 'core/typings';

export interface ITab {
  tab: string;
  color?: string;
  descriptorInfo?: IDescriptor;
  class?: string;
}
