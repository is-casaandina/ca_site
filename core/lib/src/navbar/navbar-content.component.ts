import { Component, Input } from '@angular/core';

@Component({
  selector: 'ca-navbar-content',
  template: `
    <ng-content></ng-content>
  `
})
export class NavbarContentComponent {
  @Input() active = false;
}
