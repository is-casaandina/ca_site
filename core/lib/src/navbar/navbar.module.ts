import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HoverColorModule } from '@ca/library/directives';
import { SideArrowsModule } from '../side-arrows/side-arrows.module';
import { NavbarContentComponent } from './navbar-content.component';
import { NavbarContentDirective } from './navbar-content.directive';
import { NavbarComponent } from './navbar.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      HoverColorModule,
      SideArrowsModule
    ],
    declarations: [ NavbarComponent, NavbarContentComponent, NavbarContentDirective],
    exports: [ NavbarComponent, NavbarContentComponent, NavbarContentDirective ]
})
export class NavbarModule {
}
