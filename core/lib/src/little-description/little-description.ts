export interface ILittleDescription {
  descriptionLeft?: string;
  descriptionRight?: string;
}
