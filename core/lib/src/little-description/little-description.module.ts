import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LittlDescriptionComponent } from './little-description.component';

@NgModule({
    imports: [ CommonModule, FormsModule ],
    declarations: [ LittlDescriptionComponent ],
    exports: [ LittlDescriptionComponent ]
})
export class CardModule {
}
