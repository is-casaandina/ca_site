import { Component, Input } from '@angular/core';
import { CARD_TYPE } from 'core/shared/src/settings/constants/general.constant';
import { ICard } from '../card/card';

@Component({
  selector: 'ca-little-description',
  templateUrl: './little-description.component.html'
})
export class LittlDescriptionComponent {
  CARD_TYPE = CARD_TYPE;
  @Input() data: ICard;

}
