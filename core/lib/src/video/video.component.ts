import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ca-video',
  templateUrl: './video.component.html'
})
export class VideoComponent implements OnInit {

  @Input() videoUri: string;
  @Input() mimeType: string;
  @Input() autoplay: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
