import { Component, Input, OnInit } from '@angular/core';
import { FORM_TYPE } from '@ca-core/shared/settings/constants/general.constant';

@Component({
  selector: 'ca-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent  implements OnInit {
  @Input() data: any;
  FORM_TYPE = FORM_TYPE;
  videoType = 'mp4';

  ngOnInit(): void {
    if (this.data.type === FORM_TYPE.video && this.data.backgroundVideo.mimeType) {
      this.videoType = this.data.backgroundVideo.mimeType;
    }
  }
}
