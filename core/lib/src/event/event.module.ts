import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ImgSrcsetModule, UrlLinkModule } from '@ca/library/directives';
import { EventComponent } from './event.component';

@NgModule({
    imports: [
      CommonModule,
      ImgSrcsetModule,
      UrlLinkModule
    ],
    declarations: [
      EventComponent
    ],
    exports: [ EventComponent ]
})
export class EventModule { }
