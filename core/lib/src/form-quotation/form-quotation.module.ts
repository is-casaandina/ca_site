import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CaDatepickerModule, CaInputModule } from '@ca-core/ui/lib/components';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { Ng5SliderModule } from 'ng5-slider';
import { HoverCollapseModule } from '../directives';
import { FormQuotationComponent } from './form-quotation.component';

@NgModule({
    imports: [
      CommonModule,
      Ng5SliderModule,
      FormsModule,
      CaDatepickerModule,
      CaInputModule,
      NoopAnimationsModule,
      ReactiveFormsModule,
      RecaptchaModule,
      RecaptchaFormsModule,
      HoverCollapseModule
    ],
    declarations: [ FormQuotationComponent ],
    exports: [ FormQuotationComponent ],
    entryComponents: [ FormQuotationComponent ],
    providers: [ NewsLetterServices ]
})
export class FormQuotationModule { }
