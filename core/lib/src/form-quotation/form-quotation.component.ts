import { Component, ElementRef, HostListener, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GENERAL_TEXT } from '@ca-admin/settings/constants/general.lang';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { StorageService, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { ComponentBase } from '@ca-core/shared/helpers/util/component-base';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { joinSelected } from '@ca-core/shared/helpers/util/join';
import { ResizeService, SCREEN_NAMES } from '@ca-core/shared/helpers/util/resize.service';
import { GeneralLang } from '@ca-core/shared/settings/constants/general.constant';
import { CaDatepickerRangeComponent, CaDatetimepickerComponent } from '@ca-core/ui/lib/components';
import { NewsLetterServices } from '@ca-site/providers/newsletter';
import { IOutLivinRoomList } from 'modules/our-living-room/src/app/our-living-room';
import { OurLivingRoomServices } from 'modules/our-living-room/src/providers/our-living-room.services';
import { Options } from 'ng5-slider';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ca-form-quotation',
  templateUrl: './form-quotation.component.html',
  styleUrls: ['./form-quotation.component.scss']
})
export class FormQuotationComponent extends ComponentBase implements OnInit, IPayloadModalComponent {

  @ViewChild('dp') datePicker: CaDatepickerRangeComponent;
  @ViewChild('tp') dateTimePicker: CaDatetimepickerComponent;

  payload: any;
  minRange = 1;
  maxRange = 1;
  totalElements: number;
  options: Options;
  hotels: Array<IOutLivinRoomList>;
  dateRange: string;
  dayInitDate: string;
  comments: string;
  initDate = null;
  endDate = null;
  initTime = `00:00`;
  endTime = `00:00`;
  submitted = false;

  armedTypes: Array<IOutLivinRoomList>;
  foodOptions: Array<IOutLivinRoomList>;
  audiovisualEquipments: Array<IOutLivinRoomList>;
  formQuotation: FormGroup;
  GENERAL_LANG = GeneralLang;
  GENERAL_TEXT = GENERAL_TEXT;
  showOverlay = false;
  recaptcha: any;
  recaptchaKey: string;
  joinedHotel: string;
  formLabels = {};

  constructor(
    protected elem: ElementRef<HTMLElement>,
    protected storageService: StorageService,
    private formBuild: FormBuilder,
    private activeModal: ActiveModal,
    private notificationService: NotificationService,
    private ourLinvingServices: OurLivingRoomServices,
    private newsLetterServices: NewsLetterServices,
    private elementRef: ElementRef<HTMLDivElement>,
    private resizeService: ResizeService,
    private ngZone: NgZone
  ) {
    super(elem, storageService);
    this.formQuotation = this.formBuild.group({
      fullName: [null, Validators.required],
      email: [null, [
        Validators.required,
        Validators.email
      ]],
      phone: [null, [
        Validators.required,
        Validators.pattern(/^-?(0|[1-9]\d*)?$/)
      ]]
    });
  }

  @HostListener('document:click', ['$event']) documentClick(ev): void {
    if (!this.resizeService.is(SCREEN_NAMES.LG)) {
      const range = this.datePicker.input;
      const time = this.dateTimePicker;
      const target = ev.target as Element;

      if (!target.closest(`.s-range.datePicker`)) {
        range.close();
      }
      if (!target.closest(`.s-range.timePicker`)) {
        time.close();
      }
    }
  }

  ngOnInit(): void {
    const now = this.datePicker.now;
    this.datePicker.fromDate = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() };
    this.datePicker.toDate = { year: now.getFullYear(), month: now.getMonth(), day: now.getDate() + 1 };

    this.datePicker.input.minDate = { year: now.getFullYear(), month: now.getMonth(), day: now.getDay() };
    this.datePicker.input.maxDate = { year: now.getFullYear() + 10, month: now.getMonth() + 1, day: now.getDate() };

    this.getFormLabels();
    this.getPayloadData();
    this.getArmedTypes();
    this.getFoodOptions();
    this.getAudiovisualEquipments();
  }

  setLabel(name): string {
    return Object.keys(this.formLabels).length
      ? this.formLabels[name]
      : '';
  }

  openDatePicker(comp, leave = false): void {
    if (this.datePicker) { this.datePicker.leave = leave; }
    (this.elementRef.nativeElement.querySelectorAll(`${comp} .g-datepicker input`)[0] as HTMLElement).click();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.formQuotation, controlName);
  }

  sendForm(): void {
    this.submitted = true;
    if (!this.formErrors().number && this.recaptcha && this.checkExtraFields() === 0) {
      const body = {
        idPageHotel: this.joinedHotel || this.hotels[0].value,
        quantityAssistants: this.maxRange,
        beginDate: DateUtil.formatDateYYYYMMDD(this.initDate),
        endDate: DateUtil.formatDateYYYYMMDD(this.endDate),
        beginHour: this.initTime,
        endHour: this.endTime,
        additionalComments: this.comments,
        armedTypes: this.getNameSelected(this.armedTypes),
        foodOptions: this.getNameSelected(this.foodOptions),
        audiovisualEquipments: this.getNameSelected(this.audiovisualEquipments),
        subscriberShortRequest: {
          name: this.formQuotation.controls.fullName.value,
          email: this.formQuotation.controls.email.value,
          phone: this.formQuotation.controls.phone.value
        }
      };
      this.showOverlay = true;
      this.ourLinvingServices.saveQuote(body)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          response => {
            this.ngZone.run(() => {
              this.notificationService.addSuccess(response.message);
              setTimeout(() => {
                this.showOverlay = false;
                this.activeModal.dismiss();
              }, 3000);
            });
          },
          err => {
            this.notificationService.addWarning(err.message);
            this.showOverlay = false;
          }
        );
    } else if (!this.recaptcha) {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  selectedLength(type: string): number {
    if (this[type]) {
      const filter = this[type].filter(item => item.selected);

      return filter ? filter.length : 0;
    }

    return 0;
  }

  seletedHotel(): void {
    setTimeout(() => { this.joinedHotel = joinSelected(this.hotels as any); });
  }

  dateRangeChange(value): void {
    const dates = value.split(' - ')
      .map(date => {
        const splitedDate = date.split('-');

        return new Date(splitedDate[2], splitedDate[1] - 1, splitedDate[0]);
      });

    this.initDate = dates[0];
    if (dates[1]) { this.endDate = dates[1]; }
  }

  timeRangeChange(times): void {
    this.initTime = `${(`0${times[0].getHours()}`).slice(-2)}:${(`0${times[0].getMinutes()}`).slice(-2)}`;
    this.endTime = `${(`0${times[1].getHours()}`).slice(-2)}:${(`0${times[1].getMinutes()}`).slice(-2)}`;
  }

  getDate(value, model, type?): string {
    if (value === 'toLocaleString') {
      return this[model][value]('default', { [type]: 'short' })
        .replace('.', '');
    }

    return this[model][value]();
  }

  private getPayloadData(): void {
    if (this.payload) {
      const payloadKeys = Object.keys(this.payload);
      if (payloadKeys.length) {
        payloadKeys.forEach(item => {
          this[item] = this.payload[item];
        });
      }
      if (!this.payload.maxRange && !this.payload.minRange) {
        this.getRange();
      }
    }
  }

  private getArmedTypes(): void {
    this.ourLinvingServices.getArmedTypes(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.armedTypes = response.map(item => {
            return {
              text: item,
              value: item,
              selected: false
            };
          });
        });
      });
  }

  private getFoodOptions(): void {
    this.ourLinvingServices.getFoodOptions(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.foodOptions = response.map(item => {
            return {
              text: item,
              value: item,
              selected: false
            };
          });
        });
      });
  }

  private getAudiovisualEquipments(): void {
    this.ourLinvingServices.getAudiovisualEquipments(this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.audiovisualEquipments = response.map(item => {
            return {
              text: item,
              value: item,
              selected: false
            };
          });
        });
      });
  }

  private getRange(): void {
    this.ourLinvingServices.getRange()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          this.minRange = response.theaterMin;
          this.maxRange = response.theaterMax;
          this.options = {
            floor: 1,
            ceil: response.theaterMax
          };
        });
      });
  }

  private checkExtraFields(): number {
    let extraRequired = ['joinedHotel', 'initDate', 'endDate'];
    extraRequired.forEach(field => {
      if (this[field]) {
        extraRequired = extraRequired.filter(item => item !== field);
      }
    });

    return extraRequired.length;
  }

  private formErrors(): any {
    ValidatorUtil.validateForm(this.formQuotation);

    return ValidatorUtil.formErrors(this.formQuotation, this.notificationService);
  }

  private getNameSelected(object): Array<string> {
    return object.filter(item => item.selected)
      .map(item => item.text);
  }

  private getFormLabels(): void {
    this.newsLetterServices.getFormLabels({
      code: `quote-event`,
      language: this.language
    })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => {
        this.ngZone.run(() => {
          response.forEach(item => {
            this.formLabels[item.name] = item.placeholder || item.text;
          });
        });
      });
  }
}
