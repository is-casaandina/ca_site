import { Component, Input, OnInit } from '@angular/core';
import { INotificationModal } from './notification-modal';

@Component({
  selector: 'ca-notification-modal',
  templateUrl: './notification-modal.component.html'
})
export class NotificationModalComponent implements OnInit {
  @Input() data: INotificationModal;
  payload: INotificationModal;

  ngOnInit() {
  }

}
