import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotificationModalComponent } from './notification-modal.component';

@NgModule({
    imports: [ CommonModule ],
    declarations: [ NotificationModalComponent ],
    exports: [ NotificationModalComponent ],
    entryComponents: [ NotificationModalComponent ]
})
export class NotificationModalModule {
}
