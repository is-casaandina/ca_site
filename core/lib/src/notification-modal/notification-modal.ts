export interface INotificationModal {
  title?: string;
  detail?: string;
}
