import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AwardComponent } from './award.component';

@NgModule({
    imports: [ CommonModule ],
    declarations: [ AwardComponent ],
    exports: [ AwardComponent ]
})
export class AwardModule {
}
