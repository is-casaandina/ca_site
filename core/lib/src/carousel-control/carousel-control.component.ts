import { AfterViewInit, Component, Input, NgZone, OnInit } from '@angular/core';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { interval, Observable, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CarouselControlService } from './carousel-control.service';

@Component({
  selector: 'ca-carousel-controls',
  templateUrl: './carousel-control.component.html'
})
export class CarouselControlComponent extends UnsubscribeOnDestroy implements OnInit, AfterViewInit {
  @Input() active = 0;
  @Input() lgth: number;
  @Input() color: string;
  @Input() animation: boolean;
  private _isActiveModal: boolean;
  @Input()
  set isActiveModal(isActiveModal: boolean) {
    if (isActiveModal) {
      this.stopAnimation();
    } else {
      this.startAnimation();
    }
    this._isActiveModal = isActiveModal;
  }
  get isActiveModal(): boolean {
    return this._isActiveModal;
  }
  timerSubscription: Subscription;
  slides: any;
  timer: Observable<number>;
  startDirection = 'R';
  isHovered = false;

  constructor(
    private carouselControlService: CarouselControlService,
    private ngZone: NgZone
  ) {
    super();
  }

  ngOnInit(): void {
    this.timer = interval(6000);
    this.carouselControlService.hoverState
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((isHovered: boolean) => {
        this.ngZone.run(() => {
          this.isHovered = isHovered;
        });
      });
  }

  ngAfterViewInit(): void {
    this.startAnimation();
  }

  next(): void {
    if (this.lgth > this.active + 1) {
      this.active++;
      const direction = 'R';
      this.carouselControlService.move(direction, this.active);
    }
  }

  prev(): void {
    if (this.active !== 0) {
      this.active--;
      const direction = 'L';
      this.carouselControlService.move(direction, this.active);
    }
  }

  superPrev(): void {
    if (this.active !== 0) {
      this.active = 0;
      const direction = 'SL';
      this.carouselControlService.move(direction, this.active);
    }
  }

  startAnimation(): void {
    if (this.timer && this.animation) {
      this.timerSubscription = this.timer
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(() => {
          this.ngZone.run(() => {
            if (!this.isHovered) {
              this.doAnimation();
            }
          });
        });
    }
  }

  doAnimation(): void {
    if (this.active + 1 === this.lgth) {
      this.startDirection = 'R';
      this.superPrev();
      this.prev();
    } else if (this.active === 0) {
      this.startDirection = 'R';
      this.next();
    } else {
      this.startDirection === 'R' ? this.next() : this.prev();
    }
  }

  stopAnimation(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

}
