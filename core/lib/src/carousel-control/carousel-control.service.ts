import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class CarouselControlService {

  moveDirection: EventEmitter<any> = new EventEmitter();
  hoverState: EventEmitter<boolean> = new EventEmitter();

  move(direction: string, active: number): void {
    const moveInfo = {
      direction,
      active
    };
    this.moveDirection.emit(moveInfo);
  }

  hoveredState(isHovered): void {
    this.hoverState.emit(isHovered);
  }
}
