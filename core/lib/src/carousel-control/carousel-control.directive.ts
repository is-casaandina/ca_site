import { Directive, HostListener, OnInit } from '@angular/core';
import { CarouselControlService } from './carousel-control.service';

@Directive({
  selector: '[caCarouselHover]'
})
export class CarouselControlDirective implements OnInit {

  constructor(
    private carouselControlService: CarouselControlService
  ) { }

  ngOnInit(): void {
  }

  @HostListener('mouseenter') onMouseOver(): void {
    this.carouselControlService.hoveredState(true);
  }

  @HostListener('mouseleave') onMouseOut(): void {
    this.carouselControlService.hoveredState(false);
  }

}
