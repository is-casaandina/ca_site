import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StrPadPipeModule } from '@ca-core/ui/lib/pipes';
import { CarouselControlComponent } from './carousel-control.component';
import { CarouselControlDirective } from './carousel-control.directive';
import { CarouselControlService } from './carousel-control.service';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      StrPadPipeModule
    ],
    declarations: [
      CarouselControlComponent,
      CarouselControlDirective
    ],
    exports: [
      CarouselControlComponent,
      CarouselControlDirective
    ],
    providers: [ CarouselControlService ]
})
export class CarouselControlModule {
}
