import { Component, Input } from '@angular/core';
import { SLD_RESTAURANT_INPUTS } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor } from 'core/typings';
import { CarouselCaption } from './carousel-caption';

@Component({
  selector: 'ca-carousel-caption',
  templateUrl: './carousel-caption.component.html'
})
export class CarouselCaptionComponent {
  @Input() data: CarouselCaption;
  @Input() type?: string;
  @Input() heightType?: string;
  @Input() direction: number;
  @Input() pageType?: string;
  @Input() descriptorInfo?: IDescriptor;
  @Input() index: number;
  SLD_RESTAURANT_INPUTS = SLD_RESTAURANT_INPUTS;

}
