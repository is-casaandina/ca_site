import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CarouselCaptionComponent } from './carousel-caption.component';

@NgModule({
    imports: [ CommonModule, FormsModule ],
    declarations: [ CarouselCaptionComponent ],
    exports: [ CarouselCaptionComponent ]
})
export class CarouselCaptionModule {
}
