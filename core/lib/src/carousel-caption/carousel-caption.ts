export interface CarouselCaption {
  width: string;
  title: string;
  position: string;
  descriptor: string;
  type: string;
  descriptorInfo: any;
  className: string;
}
