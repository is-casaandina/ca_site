import { Component, Input, OnInit } from '@angular/core';
import { IOpinion, IOpinionModal } from 'core/typings';
import { IMAGE_SIZE, OPINION_INFO } from '@ca-core/shared/settings/constants/general.constant';
import { ModalService } from '@ca-core/shared/helpers/modal';

@Component({
  selector: 'ca-opinion',
  templateUrl: './opinion.component.html'
})
export class OpinionComponent implements OnInit {
  @Input() data: IOpinion;
  OPINION_INFO = OPINION_INFO;
  cardWidth: string;
  payload: IOpinionModal;

  constructor(
    private modalService: ModalService,
    ) {}

  ngOnInit(): void {
    if (this.payload) {
      this.data = this.payload.data;
    }
  }

  openOpinionDetail(): void {
    if (this.data.body.length < OPINION_INFO.bodyLength + 1 || this.data.modalView) {
      return;
    }

    const modalConfig = {
      classDialog: 'multimedia',
      size: IMAGE_SIZE.sm
    };

    const modalRef = this.modalService.open(OpinionComponent, modalConfig);
    modalRef.setPayload({
      data: { ...this.data, modalView: true}
    });
    modalRef.result
      .then(() => {
      })
      .catch(err => {

      });
  }
}
