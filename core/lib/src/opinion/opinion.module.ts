import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { OpinionComponent } from './opinion.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      PipesModule,
      ModalModule,
      HttpClientModule,
      NotificationModule,
      SpinnerModule
    ],
    declarations: [ OpinionComponent ],
    exports: [ OpinionComponent ],
    entryComponents: [ OpinionComponent ]
})
export class OpinionModule {
}
