export interface IBenefit {
  icon?: string;
  title?: string;
  subtitle?: string;
  value?: string;
}
