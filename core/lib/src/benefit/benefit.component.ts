import { Component, Input } from '@angular/core';
import { FEATURE_TYPES } from '@ca-core/shared/settings/constants/general.constant';
import { IDescriptor } from 'core/typings';
import { IBenefit } from './benefit';

@Component({
  selector: 'ca-benefit',
  templateUrl: './benefit.component.html'
})
export class BenefitComponent {
  @Input() data: IBenefit;
  @Input() descriptorInfo: IDescriptor;
  @Input() type?: string;
  FEATURE_TYPES = FEATURE_TYPES;
}
