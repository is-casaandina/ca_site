import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BenefitComponent } from './benefit.component';


@NgModule({
    imports: [ CommonModule, FormsModule ],
    declarations: [ BenefitComponent ],
    exports: [ BenefitComponent ]
})
export class BenefitModule {

}
