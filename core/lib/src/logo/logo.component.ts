import { Component, Input, OnInit } from '@angular/core';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { IMAGES } from '@ca-site/settings/constants/images.constant';
import { ILogoInfo } from '@core/typings';
import { DESCRIPTOR_TYPE_ID } from 'core/shared/src/settings/constants/general.constant';

@Component({
  selector: 'ca-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  @Input() descriptorInfo?: any;
  @Input() typeDescriptor: string;
  @Input() logoInfo: ILogoInfo;
  IMAGES = IMAGES;
  backgroundColor: string;
  DESCRIPTOR_TYPE_ID = DESCRIPTOR_TYPE_ID;

  constructor() { }

  ngOnInit(): void {
  }

  goToHome(): void {
    GlobalNavigateUtil.goTo('/');
  }

}
