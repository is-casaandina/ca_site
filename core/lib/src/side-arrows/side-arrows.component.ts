import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ca-side-arrows',
  templateUrl: './side-arrows.component.html',
  styleUrls: ['./side-arrows.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideArrowsComponent implements OnInit {

  @Input() scrollableElem: HTMLElement;
  fullLeftScroll = 0;
  showSideArrow = false;
  arrowDirection = 'right';

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.changeDetectorRef.detectChanges();
    window.addEventListener('resize', () => this.changeDetectorRef.detectChanges());
    this.scrollableElem.addEventListener('scroll', ev => {
      this.toggleSideArrows();
      this.changeDetectorRef.detectChanges();
    });
  }

  moveScroll(): void {
    this.fullLeftScroll = this.scrollableElem.scrollWidth - this.scrollableElem.clientWidth;
    this.scrollableElem.scrollTo({
      behavior: 'smooth',
      left: this.arrowDirection === 'right'
        ? this.fullLeftScroll
        : 0,
      top: 0
    });
  }
  toggleSideArrows(): void {
    this.arrowDirection = this.scrollableElem.scrollLeft >= this.fullLeftScroll
    ? 'left'
    : 'right';
  }
  scrollableTable(): boolean {
    return this.scrollableElem.scrollWidth > this.scrollableElem.clientWidth;
  }
}
