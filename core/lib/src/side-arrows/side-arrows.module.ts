import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SideArrowsComponent } from './side-arrows.component';

@NgModule({
    imports: [
      CommonModule
    ],
    declarations: [ SideArrowsComponent ],
    exports: [ SideArrowsComponent ]
})
export class SideArrowsModule {
}
