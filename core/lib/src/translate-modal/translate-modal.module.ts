import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { DragScrollModule } from 'ngx-drag-scroll';
import { CarouselControlModule } from '../carousel-control/public_api';
import { TranslateModalComponent } from './translate-modal.component';

@NgModule({
    imports: [
      CommonModule,
      CarouselControlModule,
      DragScrollModule,
      PipesModule
    ],
    declarations: [ TranslateModalComponent ],
    exports: [ TranslateModalComponent ],
    entryComponents: [ TranslateModalComponent ]
})
export class TranslateModalModule {

}
