import { IBackgroundImage } from './general';

export interface IPageInfo {
  pageId: string;
  language: string;
  descriptorId: string;
  revinate: string;
  contact: IContact;
  roiback: string;
  beginDate: string;
  firstExpiration: string;
  secondExpiration: string;
  countdown: boolean;
  isSite: boolean;
  payload: IPageInfoData;
  currency: string;
  name: string;
}

export interface IContact {
  title1: string;
  title2: string;
  languages: Array<IlangugeContact>;
}

export interface IlangugeContact {
  language: string;
  details: Array<IDetailContact>;
}

export interface ILanguageContact {
  language: string;
  details: Array<IDetailContact>;
}

export interface IDetailContact {
  description: string;
  icon: string;
  label: string;
  type: number;
}

export interface IPageInfoData {
  caInfoPromotion: ICaInfoPromotion;
}

export interface ICaInfoPromotion {
  bodyRestriction: string;
  description: string;
  descriptionLeft: string;
  descriptionRight: string;
  descount: number;
  footerRestriction: string;
  imagePromotion: Array<IBackgroundImage>;
  imageRestriction: Array<IBackgroundImage>;
  isRestriction: boolean;
  price: number;
  title: string;
  urlReservation: string;
}
