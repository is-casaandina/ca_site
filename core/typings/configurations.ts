export interface IConfigurationResponse {
  company: string;
  copyright: string;
  css: string;
  defaultLanguage: string;
  icon: string;
  keywords: string;
  languages: Array<string>;
  iconsPay: Array<string>;
  logo: string;
  metadata: IMetadata;
  roiback: IRoiback;
  roibackPrice: IRoibackPrice;
  sitename: string;
  socialNetworking: ISocialNetworking;
  fontIcons: string;
  script: IScript;
}

export interface IMetadata {
  title: string;
  description: string;
}

export interface ISocialNetworking {
  urlFacebook: string;
  urlTwitter: string;
  urlInstagram: string;
  urlYoutube: string;
  urlFlickr: string;
  urlWhatsapp: string;
}

export interface IRoiback {
  urlReservationEsp: string;
  urlReservationEng: string;
  urlReservationPor: string;
}

export interface IRoibackPrice {
  brl: number;
  pen: number;
  usd: number;
}

export interface IScript {
  body: string;
  footer: string;
  header: string;
  tracing: string;
}
