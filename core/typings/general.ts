export interface IBanner {
  type: string;
  backgroundImage?: Array<IBackgroundProperty>;
  backgroundVideo?: IBackgroundVideo;
  align?: string;
  alignImage?: string;
  subtitle: string;
  title: string;
  color?: string;
  isButton: boolean;
  typeDescriptor: string;
  descriptor: string;
  textButton: string;
  descriptorInfo?: IDescriptor;
  colorTitle?: string;
  pagesDescriptor?: Array<any>;
  isFrame?: boolean;
  hideOpacity?: boolean;
  externalLink?: Ilink;
}

export interface IActionServiceResponse {
  name?: string;
}
export interface IRestaurant {
  descriptor: string;
  backgroundImage?: Array<IBackgroundImage>;
  title?: string;
  descriptionOne?: string;
  descriptionTwo?: string;
  textLink?: string;
  urlLink?: string;
}

export interface IBackgroundImage {
  name?: string;
  properties?: IBackgroundProperty;
  small?: IImage;
  medium?: IImage;
  large?: IImage;
}

export interface IBackgroundVideo extends IBackgroundImage {
  mimeType?: string;
  uri: string;
  createDate: string;
  url: string;
  size: number;
}

export interface IBackgroundProperty {
  title?: string;
  alt?: string;
  description?: string;
}

export interface IImage {
  name?: string;
  width?: number;
  height?: number;
  imageUri?: string;
  size?: number;
}

export interface IDescriptor extends IDescriptorResponse {
  colorRgba?: string;
  colorRgbaLow?: string;
}

export interface IOpinionRate {
  ratingType?: string;
  avarageRating?: number;
  ratingsByReviewSites: Array<IReviewsiteRate>;
  rankingMessage: string;
}

export interface IOpinion {
  title?: string;
  body?: string;
  author?: string;
  dateReview?: number;
  rating?: number;
  reviewSite?: any;
  modalView?: boolean;
}

export interface IReviewsiteRate {
  reviewSiteName?: string;
  avarageRating?: number;
  rankingMessage?: string;
}

export interface IDescriptorResponse {
  id: string;
  name: string;
  url: string;
  urlBanner?: string;
  typographyStyle?: string;
  color: string;
  title: string;
  createdDate?: string;
  updatedDate?: string;
  category?: string;
  categoryName?: string;
}

export interface IServiceRequest {
  items?: Array<string>;
  language?: string;
}

export interface IService {
  icon: string;
  text: string;
  value: string;
}

export interface IOpinionModal extends IOpinion {
  data: IOpinion;
}

export interface Ilink {
  url: string;
  isExternal: boolean;
}

export interface IHotel {
  value: string;
  text: string;
}

export interface ILivingRooms {
  country: string;
  destination: string;
  hotel: string;
  category: string;
}

export interface ILivingRoom {
  livingRoom: string;
}

export interface ICombo {
  value?: string;
  text?: string;
  name?: string;
}

export interface IDestinationMap {
  className: string;
  descriptors: Array<IDescriptorMap>;
  destination: string;
  destinationId: string;
}

export interface IDescriptorMap {
  destination: string;
  destinationId: string;
}

export interface IMoveInfo {
  direction: string;
  active: number;
}

export interface ILogoInfo {
  url: string;
  alt: string;
}

export interface IAutocompleteSearch {
  categoryId: string;
  destinationId: string;
  destinationName: string;
  id: string;
  lastUpdate: string;
  name: string;
  revinate: string;
  roiback: string;
}
