import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StrPadPipe } from './str-pad.pipe';

@NgModule({
   imports: [
      CommonModule
   ],
   declarations: [
      StrPadPipe
   ],
   exports: [
      StrPadPipe
   ]
})

export class StrPadPipeModule {}
