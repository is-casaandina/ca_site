import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'strPad'})
export class StrPadPipe implements PipeTransform {
  transform(value: any[], length: number): string {
    const pad: string = this.getPad(length);
    return (pad + value).slice(-2);
  }


  getPad(length: number): string {
    let pad = '';
    for (let i = 0; i < length; i++) {
      pad += '0';
    }
    return pad;
  }
}
