import { CommonModule } from '@angular/common';
import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CURRENCY_KEYS, CURRENCY_SYMBOl } from '@ca-core/shared/settings/constants/general.constant';

@Pipe({
  name: 'currencySymbol'
})
export class CurrencySymbolPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return CURRENCY_KEYS.USD === value ? CURRENCY_SYMBOl.USD :
      CURRENCY_KEYS.BRL === value ? CURRENCY_SYMBOl.BRL : CURRENCY_SYMBOl.PEN;
  }

}

@NgModule({
  imports: [CommonModule],
  declarations: [CurrencySymbolPipe],
  exports: [CurrencySymbolPipe]
})
export class CurrencySymbolPipeModule { }
