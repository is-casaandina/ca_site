import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithoutResultsComponent } from './without-results/without-results.component';
import { WithoutResultsDirective } from './without-results.directive';

@NgModule({
  declarations: [WithoutResultsComponent, WithoutResultsDirective],
  imports: [
    CommonModule
  ],
  entryComponents: [WithoutResultsComponent],
  exports: [WithoutResultsDirective]
})
export class WithoutResultsModule { }
