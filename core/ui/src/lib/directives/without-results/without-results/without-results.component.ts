import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ca-without-results',
  templateUrl: './without-results.component.html',
  styleUrls: ['./without-results.component.css']
})
export class WithoutResultsComponent implements OnInit {

  @Input()
  protected _description: string;
  get description(): string {
    return this._description;
  }
  set description(value: string) {
    this._description = value;
  }

  constructor() { }

  ngOnInit(): void { }

}
