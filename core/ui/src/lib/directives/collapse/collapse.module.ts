import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapsePaddingDirective } from './collapse-padding.directive';
import { CollapseDirective } from './collapse.directive';

@NgModule({
  declarations: [CollapseDirective, CollapsePaddingDirective],
  imports: [
    CommonModule
  ],
  exports: [CollapseDirective, CollapsePaddingDirective]
})
export class CollapseModule { }
