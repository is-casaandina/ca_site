import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollapseCardDirective } from './collapse-card.directive';

@NgModule({
  declarations: [CollapseCardDirective],
  imports: [
    CommonModule
  ],
  exports: [CollapseCardDirective]
})
export class CollapseCardModule { }

