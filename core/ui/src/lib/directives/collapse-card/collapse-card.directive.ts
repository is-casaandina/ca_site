import { Directive, HostListener , ElementRef, Renderer2, Input, AfterContentInit } from '@angular/core';

@Directive({
  selector: '[collapseCard]'
})
export class CollapseCardDirective implements AfterContentInit {

  @Input() show: boolean =  true;
  constructor(private el: ElementRef, private renderer2: Renderer2) {}
  @HostListener ('click') onclick() {
    this.show = !this.show;
    this.collapse();
  }

  collapse(): void {
    this.show ?
      this.renderer2.addClass(this.el.nativeElement, 'active') :
      this.renderer2.removeClass(this.el.nativeElement, 'active');
  }

  ngAfterContentInit() {
    this.collapse();
  }
}
