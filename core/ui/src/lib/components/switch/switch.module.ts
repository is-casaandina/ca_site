import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaSwitchComponent } from './switch.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CaSwitchComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CaSwitchComponent]
})
export class CaSwitchModule { }
