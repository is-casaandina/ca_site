import { DatePipe } from '@angular/common';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

// tslint:disable-next-line:interface-name
export interface NgbTimeStruct {
  hour: number;
  minute: number;
  second: number;
}

@Component({
  selector: 'ca-datetimepicker',
  templateUrl: './datetimepicker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaDatetimepickerComponent),
      multi: true
    },
    DatePipe
  ]
})
export class CaDatetimepickerComponent implements OnInit, ControlValueAccessor {

  protected _value: any;
  @Input()
  get value(): any {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    this.propagateChange(this._value);
  }

  protected _date: NgbDateStruct = { year: 0, month: 0, day: 0 };
  get date(): NgbDateStruct {
    return this._date;
  }
  set date(date: NgbDateStruct) {
    this._date = date;
    this.setTmp();
  }

  protected _time: NgbTimeStruct = { hour: 0, minute: 0, second: 0 };
  get time(): NgbTimeStruct {
    return this._time;
  }
  set time(time: NgbTimeStruct) {
    this._time = time;
    this.setTmp();
    this.emitTime();
  }

  protected _endTime: NgbTimeStruct = { hour: 0, minute: 0, second: 0 };
  get endTime(): NgbTimeStruct {
    return this._endTime;
  }
  set endTime(endTime: NgbTimeStruct) {
    this._endTime = endTime;
    this.emitTime();
  }

  protected _error = false;
  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }

  @Input() placeholder: string;
  @Input() initDate = true;
  @Input() timeRange = false;
  @Output() timeSelected: EventEmitter<any>;

  isCollapsed: boolean;
  valueTmp: Date;

  constructor() {
    this.isCollapsed = true;
    this.timeSelected = new EventEmitter<any>();
  }

  ngOnInit(): void {
    if (this.initDate) {
      const now = new Date();
      this.setDateTime(now);
      this.value = now;
    } else {
      this.date = {} as NgbDateStruct;
    }
  }

  focusInput(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  apply(): void {
    this.value = this.getDatetime()
      .toISOString();
    this.close();
  }

  clean(): void {
    this.value = null;
    this.date = { year: 0, month: 0, day: 0 } as NgbDateStruct;
    this.time = { hour: 0, minute: 0, second: 0 } as NgbTimeStruct;
  }

  setTmp(): void {
    this.valueTmp = this.getDatetime();
  }

  close(): void {
    this.isCollapsed = true;
  }

  getLabelDatetimepicker(): string {
    return this.value;
  }

  private setValue(): void { }
  /* Takes the value  */
  writeValue(value: any): void {
    if (value) {
      this._value = value;
      this.setDateTime(value);
      this.setTmp();
    } else {
      this.date = null;
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

  private getDatetime(): Date {
    return !this.date || (this.date && !this.date.year) ? null :
      new Date(
        this.date.year,
        this.date.month ? this.date.month - 1 : this.date.month,
        this.date.day,
        this.time.hour,
        this.time.minute,
        this.time.second
      );
  }

  private setDateTime(date: Date): void {
    this.date.year = date.getFullYear();
    this.date.month = date.getMonth() + 1;
    this.date.day = date.getDate();
    this.time.hour = date.getHours();
    this.time.minute = date.getMinutes();

    this.endTime.hour = date.getHours();
    this.endTime.minute = date.getMinutes();
  }

  private getEndDatetime(): Date {
    return !this.date ? null : new Date(this.date.year, this.date.month, this.date.day,
      this.endTime.hour, this.endTime.minute, this.endTime.second);
  }

  private emitTime(): void {
    this.timeSelected.emit([this.getDatetime(), this.getEndDatetime()]);
  }
}
