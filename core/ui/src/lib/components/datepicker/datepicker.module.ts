import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { I18nEsPeModule } from '@ca-core/shared/helpers/i18n-es-pe';
import { NgbDatepickerModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { CollapseModule } from '../../directives';
import { PROVIDER_I18N_ES_PE } from './constants/provider.constants';
import { CaDatepickerRangeComponent } from './datepicker-range/datepicker-range.component';
import { CaDatepickerComponent } from './datepicker/datepicker.component';
import { CaDatetimepickerComponent } from './datetimepicker/datetimepicker.component';

@NgModule({
  declarations: [
    CaDatepickerComponent,
    CaDatepickerRangeComponent,
    CaDatetimepickerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    I18nEsPeModule,
    CollapseModule
  ],
  exports: [
    CaDatepickerComponent,
    CaDatepickerRangeComponent,
    CaDatetimepickerComponent
  ],
  providers: [
    PROVIDER_I18N_ES_PE
  ]
})
export class CaDatepickerModule { }
