import { Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgbDate, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ca-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaDatepickerComponent),
      multi: true
    }
  ]
})
export class CaDatepickerComponent implements OnInit, ControlValueAccessor {

  @ViewChild('dp') datePicker: NgbInputDatepicker;
  now = new Date();

  protected _placeholder: string;
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value || '';
  }

  protected _model: any;
  @Input()
  get model(): any {
    return this._model;
  }
  set model(value: any) {
    this._model = value;
    this.propagateChange(this._model);
  }

  @Output() dateSelect: EventEmitter<NgbDate>;

  constructor() {
    this.dateSelect = new EventEmitter<NgbDate>();
  }

  ngOnInit(): void {
    this.datePicker.maxDate = { year: this.now.getFullYear() + 1, month: this.now.getMonth() + 1, day: this.now.getDate() };
  }

  oDateSelect(date: NgbDate): void {
    this.dateSelect.next(date);
  }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value !== undefined) {
      this._model = value;
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void {}

}
