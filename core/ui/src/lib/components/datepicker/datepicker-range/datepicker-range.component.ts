import { DatePipe } from '@angular/common';
import { Component, ElementRef, EventEmitter, forwardRef, Input, NgZone, Output, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { PROVIDER_I18N_ES_PE, PROVIDER_NGB_NATIVE_ADAPTER, PROVIDER_PARSE_FORMAT } from '../constants/provider.constants';

@Component({
  selector: 'ca-datepicker-range',
  templateUrl: './datepicker-range.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaDatepickerRangeComponent),
      multi: true
    },
    PROVIDER_NGB_NATIVE_ADAPTER,
    PROVIDER_PARSE_FORMAT,
    DatePipe,
    PROVIDER_I18N_ES_PE
  ]
})
export class CaDatepickerRangeComponent implements ControlValueAccessor {

  @ViewChild('dp') input: NgbInputDatepicker;
  @ViewChild('myRangeInput') myRangeInput: ElementRef;

  protected _value: any;
  @Input()
  get value(): any {
    return this._value || {};
  }
  set value(value: any) {
    this._value = value;
    this.propagateChange(this._value);
  }

  protected _placeholder: string;
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value || '';
  }
  @Input() leave = false;
  @Input() minValue: string;
  @Output() dateSelect: EventEmitter<string>;

  model: any;
  hoveredDate: NgbDate;
  fromDate: any;
  toDate: any;

  now = new Date();
  equals = (one: NgbDate, two: NgbDate) => one.equals(two);
  before = (one: NgbDate, two: NgbDate) => one.before(two);
  after = (one: NgbDate, two: NgbDate) => one.after(two);

  isHovered = date =>
    this.fromDate && !this.toDate && this.hoveredDate && this.after(date, this.fromDate) && this.before(date, this.hoveredDate)
  isInside = date => this.after(date, this.fromDate) && this.before(date, this.toDate);
  isFrom = date => this.equals(date, this.fromDate);
  isTo = date => this.equals(date, this.toDate);
  isRange = date => this.isFrom(date) || this.isTo(date) || this.isInside(date) || this.isHovered(date);

  constructor(
    private renderer: Renderer2,
    private parserFormatter: NgbDateParserFormatter,
    private ngZone: NgZone
  ) {
    this.dateSelect = new EventEmitter<string>();
  }

  isDisabled(date: NgbDateStruct): boolean {
    return new Date(`${date.year}-${date.month}-${date.day}`).getTime() < new Date(this.minValue).getTime();
  }

  onDateSelection(date: NgbDate): void {
    this.ngZone.run(() => {
      if (!(this.minValue && this.isDisabled(date))) {
        this.value = this.value || {};

        let parsed = '';
        if (!this.fromDate && !this.toDate) {
          this.fromDate = this.leave ? this.fromDate : date;
        } else if (this.fromDate && !this.toDate && (this.equals(date, this.fromDate) || this.after(date, this.fromDate))) {
          this.toDate = date;
          this.input.close();
        } else {
          this.toDate = this.leave ? date : null;
          this.fromDate = this.leave ? this.fromDate : date;

          if (this.leave) { this.input.close(); }
        }
        if (this.fromDate) {
          this.value.from = this.parserFormatter.format(this.fromDate);
          parsed += this.value.from;
        }
        if (this.toDate) {
          this.value.to = this.parserFormatter.format(this.toDate);
          parsed += ` - ${this.value.to}`;
        }

        if (this.toDate && this.fromDate && (this.equals(this.toDate, this.fromDate) || this.after(this.toDate, this.fromDate))) {
          this.renderer.setProperty(this.myRangeInput.nativeElement, 'value', parsed);
          this.dateSelect.next(parsed);
        }
      }

    });
  }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value) {
      this._value = value;
      if (this.value && this.value.from && this.value.to) {
        this.onDateSelection(this.fromModel(this.value.from));
        this.onDateSelection(this.fromModel(this.value.to));
        setTimeout(() => {
          const model = `${this.value.from} - ${this.value.to}`;
          this.renderer.setProperty(this.myRangeInput.nativeElement, 'value', model);
        }, 500);
      }
    } else {
      this.model = null;
      this.fromDate = null;
      this.toDate = null;
    }
  }

  setHoveredDate(date?: NgbDate): void {
    this.ngZone.run(() => {
      this.hoveredDate = date;
    });
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

  private fromModel(value: string): NgbDate {
    const date = DateUtil.stringDDMMYYYYToDate(value);

    return date ? new NgbDate(
      date.getUTCFullYear(),
      date.getUTCMonth() + 1,
      date.getUTCDate()
    ) : null;
  }
}
