import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaButtonComponent } from './button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CaButtonComponent],
  exports: [CaButtonComponent]
})
export class CaButtonModule { }
