import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input
} from '@angular/core';

import {
  Block,
  CanBlock,
  CanDisable,
  CanTheme,
  ColorPalette,
  mixinBlock,
  mixinDisabled,
  mixinTheme,
  ThemePalette
} from '@ca-core/ui/common';

export class ButtonBase {
  constructor(public _elementRef: ElementRef) {
    this.init();
  }

  init(): void {
    this._elementRef.nativeElement.classList.add('g-btn');
  }
}

export const _ButtonMixinBase = mixinBlock(mixinDisabled(mixinTheme(ButtonBase)));

@Component({
  selector: 'button[ca-button], a[ca-button]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CaButtonComponent extends _ButtonMixinBase implements CanTheme, CanDisable, CanBlock {

  @Input() theme: ThemePalette = 'primary';
  @Input() color: ColorPalette;
  @Input() block: Block;

  constructor(elementRef: ElementRef) {
    super(elementRef);
  }

}
