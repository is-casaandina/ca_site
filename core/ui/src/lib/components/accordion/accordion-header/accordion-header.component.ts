import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ca-accordion-header',
  templateUrl: './accordion-header.component.html'
})
export class CaAccordionHeaderComponent implements OnInit {

  @Output() oToggle: EventEmitter<boolean>;

  constructor() {
    this.oToggle = new EventEmitter<boolean>();
  }

  ngOnInit(): void { }

}
