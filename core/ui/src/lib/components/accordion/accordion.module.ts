import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseModule } from '../../directives';
import { CaAccordionContentComponent } from './accordion-content/accordion-content.component';
import { CaAccordionGroupComponent } from './accordion-group/accordion-group.component';
import { CaAccordionHeaderComponent } from './accordion-header/accordion-header.component';
import { CaAccordionComponent } from './accordion/accordion.component';

@NgModule({
  declarations: [
    CaAccordionComponent,
    CaAccordionGroupComponent,
    CaAccordionHeaderComponent,
    CaAccordionContentComponent
  ],
  imports: [
    CommonModule,
    CollapseModule
  ],
  exports: [
    CaAccordionComponent,
    CaAccordionGroupComponent,
    CaAccordionHeaderComponent,
    CaAccordionContentComponent
  ]
})
export class CaAccordionModule { }
