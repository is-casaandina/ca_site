import { AfterContentChecked, Component, ContentChildren, Input, OnInit, QueryList } from '@angular/core';
import { Subscription } from 'rxjs';
import { CaAccordionGroupComponent } from '../accordion-group/accordion-group.component';

@Component({
  selector: 'ca-accordion',
  templateUrl: './accordion.component.html'
})
export class CaAccordionComponent implements OnInit, AfterContentChecked {

  @ContentChildren(CaAccordionGroupComponent) caAccordionGroups: QueryList<CaAccordionGroupComponent>;

  @Input() customClass: string;

  protected accordionGroups: Array<CaAccordionGroupComponent>;
  protected accordionGroupsSubs: Array<Subscription> = [];

  constructor() {
    this.accordionGroups = [];
  }

  ngOnInit(): void { }

  ngAfterContentChecked(): void {
    if (this.caAccordionGroups && this.accordionGroups.length !== this.caAccordionGroups.toArray().length) {
      this.accordionGroupsSubs.forEach(sub => sub.unsubscribe());
      this.accordionGroupsSubs = [];

      this.accordionGroups = this.caAccordionGroups.toArray();
      this.accordionGroups.forEach((fv, fk) => {
        if (fv.active) {
          fv.accordionContent.collapse = !fv.active;
        }
        const sub = fv.accordionHeader.oToggle
          .subscribe(() => {
            this.toggle(fk);
          });
        this.accordionGroupsSubs.push(sub);
      });
    }
  }

  toggle(currentIndexGroup: number): void {
    this.accordionGroups.forEach((fv, fk) => {
      const active = (fk !== currentIndexGroup)
                            ? false
                            : !fv.active;
      fv.active = active;
      fv.accordionContent.collapse = !active;
    });
  }

}
