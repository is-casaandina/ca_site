
export { CaAccordionContentComponent as CaAccordionContent } from './accordion-content/accordion-content.component';
export { CaAccordionHeaderComponent as CaAccordionHeader } from './accordion-header/accordion-header.component';
export { CaAccordionGroupComponent as CaAccordionGroup } from './accordion-group/accordion-group.component';
export { CaAccordionComponent as CaAccordion } from './accordion/accordion.component';
export { CaAccordionModule } from './accordion.module';
