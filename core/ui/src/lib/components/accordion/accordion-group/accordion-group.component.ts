import { Component, ContentChild, ElementRef, HostBinding, Input, OnInit, Renderer2 } from '@angular/core';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { CaAccordionContentComponent } from '../accordion-content/accordion-content.component';
import { CaAccordionHeaderComponent } from '../accordion-header/accordion-header.component';

@Component({
  selector: 'ca-accordion-group',
  templateUrl: './accordion-group.component.html'
})
export class CaAccordionGroupComponent implements OnInit {

  @HostBinding('attr.class') attrClass = 'g-acordion-list-item';

  @ContentChild(CaAccordionHeaderComponent) accordionHeader: CaAccordionHeaderComponent;
  @ContentChild(CaAccordionContentComponent) accordionContent: CaAccordionContentComponent;

  protected _active: boolean;
  @Input()
  get active(): boolean {
    return this._active;
  }
  set active(value: boolean) {
    this._active = coerceBooleanProp(value);
    const actionType = (this._active) ? 'addClass' : 'removeClass';
    setTimeout(() => {
      this._renderer2[actionType](this._elementRef.nativeElement, 'active');
    }, 100);
  }

  constructor(
    private _elementRef: ElementRef,
    private _renderer2: Renderer2
  ) { }

  ngOnInit(): void { }

}
