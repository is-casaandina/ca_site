import { Component, Input, OnInit } from '@angular/core';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';

@Component({
  selector: 'ca-accordion-content',
  templateUrl: './accordion-content.component.html'
})
export class CaAccordionContentComponent implements OnInit {

  protected _collpase: boolean;
  @Input()
  get collapse(): boolean {
    return this._collpase;
  }
  set collapse(value: boolean) {
    this._collpase = coerceBooleanProp(value);
  }

  /** auto padding */
  @Input() ap = true;
  @Input() classOpen: string;

  constructor() {
    this._collpase = true;
  }

  ngOnInit(): void { }

}
