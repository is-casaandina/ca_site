import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';

@Component({
  selector: 'ca-card-cell',
  templateUrl: './card-cell.component.html',
  styleUrls: ['./card-cell.component.css']
})
export class CaCardCellComponent implements OnInit {

  @HostBinding('attr.class') attrClass = 'g-card-info';

  @Input() label: string;

  protected _borderRight: boolean;
  @Input()
  get borderRight(): boolean {
    return this._borderRight;
  }
  set borderRight(value: boolean) {
    this._borderRight = coerceBooleanProp(value);
    if (this._borderRight) {
      this.attrClass +=  ` ${this.attrClass}-border`;
    }
  }

  constructor() { }

  ngOnInit(): void { }

}
