import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaCardCellComponent } from './card-cell/card-cell.component';
import { CaCardContentComponent } from './card-content/card-content.component';
import { CaCardHeaderComponent } from './card-header/card-header.component';
import { CaCardWrapperComponent } from './card-wrapper/card-wrapper.component';
import { CaCardComponent } from './card/card.component';

@NgModule({
  declarations: [
    CaCardComponent,
    CaCardHeaderComponent,
    CaCardContentComponent,
    CaCardWrapperComponent,
    CaCardCellComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CaCardComponent,
    CaCardHeaderComponent,
    CaCardContentComponent,
    CaCardWrapperComponent,
    CaCardCellComponent
  ]
})
export class CaCardModule { }
