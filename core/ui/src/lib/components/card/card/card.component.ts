import { Component, OnInit, Input } from '@angular/core';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';

@Component({
  selector: 'ca-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CaCardComponent implements OnInit {

  protected _accordion: boolean;
  @Input()
  get accordion(): boolean {
    return this._accordion;
  }
  set accordion(value: boolean) {
    this._accordion = coerceBooleanProp(value);
  }

  protected _disabled: boolean;
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProp(value);
  }

  constructor() {
    this._disabled = false;
  }

  ngOnInit(): void {
  }

}
