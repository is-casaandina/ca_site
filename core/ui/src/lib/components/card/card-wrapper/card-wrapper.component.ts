import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'ca-card-wrapper',
  templateUrl: './card-wrapper.component.html',
  styleUrls: ['./card-wrapper.component.css']
})
export class CaCardWrapperComponent implements OnInit {

  @HostBinding('attr.class') attrClass = 'g-card-wrapper';

  constructor() { }

  ngOnInit(): void { }

}
