import { Component, OnInit, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'ca-card-content',
  templateUrl: './card-content.component.html',
  styleUrls: ['./card-content.component.css']
})
export class CaCardContentComponent implements OnInit {

  protected _class: string;
  @HostBinding('attr.class')
  @Input()
  get class(): string {
    return this._class;
  }
  set class(value: string) {
    this._class = (value) ? `${this._class} ${value}` : 'g-card-body';
  }

  constructor() {
    this._class = 'g-card-body';
  }

  ngOnInit(): void { }

}
