import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';

@Component({
  selector: 'ca-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaRadioButtonComponent),
      multi: true
    }
  ]
})
export class CaRadioButtonComponent implements OnInit {

  protected _model: string;
  @Input()
  get model(): string {
    return this._model;
  }
  set model(value: string) {
    this._model = value;
    this.propagateChange(this._model);
  }

  protected _value: string;
  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
  }

  protected _disabled: boolean;
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProp(value);
  }

  constructor() {
    this._disabled = false;
  }

  ngOnInit(): void { }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value !== undefined) {
      this._model = value;
    }
  }

  propagateChange = (_: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void {}

}
