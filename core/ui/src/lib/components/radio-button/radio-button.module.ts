import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaRadioButtonComponent } from './radio-button.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CaRadioButtonComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CaRadioButtonComponent]
})
export class CaRadioButtonModule { }
