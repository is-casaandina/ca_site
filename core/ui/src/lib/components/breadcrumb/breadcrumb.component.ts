import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ca-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class CaBreadcrumbComponent implements OnInit {

  @Input() list: Array<string>;

  constructor() { }

  ngOnInit(): void { }

}
