import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaBreadcrumbComponent } from './breadcrumb.component';

@NgModule({
  declarations: [ CaBreadcrumbComponent],
  imports: [
    CommonModule
  ],
  exports: [CaBreadcrumbComponent]
})
export class CaBreadcrumbModule { }
