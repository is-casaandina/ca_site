import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ca-paginator',
  templateUrl: './paginator.component.html'
})
export class CaPaginatorComponent implements OnInit {

  protected _page: number;
  @Input()
  get page(): number {
    return this._page;
  }
  set page(value: number) {
    this._page = value;
  }

  protected _maxSize: number | string;
  @Input()
  get maxSize(): number | string {
    return this._maxSize;
  }
  set maxSize(value: number | string) {
    this._maxSize = value;
  }

  protected _pageSize: number;
  @Input()
  get pageSize(): number {
    return this._pageSize;
  }
  set pageSize(value: number) {
    this._pageSize = value;
  }

  protected _collectionSize: number | string;
  @Input()
  get collectionSize(): number | string {
    return this._collectionSize;
  }
  set collectionSize(value: number | string) {
    this._collectionSize = value;
  }

  @Output() pageChange: EventEmitter<number>;

  constructor() {
    this._page = 1;
    this._maxSize = 4;
    this._pageSize = 10;
    this.pageChange = new EventEmitter<number>(true);
  }

  ngOnInit(): void { }

  oPageChange(event: any): void {
    this.pageChange.emit(event);
  }

}
