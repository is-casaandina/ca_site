import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { CaPaginatorComponent } from './paginator.component';

@NgModule({
  declarations: [CaPaginatorComponent],
  imports: [
    CommonModule,
    NgbPaginationModule
  ],
  exports: [CaPaginatorComponent]
})
export class CaPaginatorModule { }
