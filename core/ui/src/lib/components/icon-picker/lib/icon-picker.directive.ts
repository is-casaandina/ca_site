import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ReflectiveInjector,
  ViewContainerRef
} from '@angular/core';

import { IIcon } from './icon';
import { IconPickerComponent } from './icon-picker.component';

@Directive({
  selector: '[iconPicker]'
})
export class IconPickerDirective implements OnInit {
  @Input() icons: Array<IIcon> = [];
  @Input() iconPicker: string;
  @Input() ipPlaceHolder = 'Buscar icono...';
  @Input() ipPosition = 'bottom';
  @Input() ipFallbackIcon: string;
  @Input() ipHeight = 'auto';
  @Input() ipMaxHeight = '224px';
  @Input() ipWidth = '230px';

  @Output() iconPickerSelect = new EventEmitter<string>(true);

  private dialog: IconPickerComponent;
  private created: boolean;
  private cmpRef: ComponentRef<IconPickerComponent>;

  constructor(
    private vcRef: ViewContainerRef,
    private el: ElementRef,
    private cfr: ComponentFactoryResolver
  ) {
    this.created = false;
  }

  ngOnInit(): void {
    this.iconPicker = this.iconPicker || this.ipFallbackIcon;
  }

  @HostListener('click', ['$event.target']) onClick(): void {
    this.openDialog();
  }

  openDialog(): void {
    if (!this.created) {
      this.created = true;
      const vcRef = this.vcRef;
      const compFactory = this.cfr.resolveComponentFactory(IconPickerComponent);
      const injector = ReflectiveInjector.fromResolvedProviders([], vcRef.parentInjector);
      const cmpRef = vcRef.createComponent(compFactory, 0, injector, []);
      cmpRef.instance.setDialog(this, this.el, this.iconPicker, this.ipPosition, this.ipHeight, this.ipMaxHeight,
        this.ipWidth, this.ipPlaceHolder, this.ipFallbackIcon);
      this.dialog = cmpRef.instance;

      if (this.vcRef !== vcRef) {
        cmpRef.changeDetectorRef.detectChanges();
      }

      this.cmpRef = cmpRef;
    } else if (this.dialog) {
      this.dialog.openDialog(this.iconPicker);
    }
  }

  iconSelected(icon: string): void {
    this.iconPickerSelect.emit(icon);
  }

}
