import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[text]'
})
export class TextDirective {
  @Output() newValue = new EventEmitter<any>();
  @Input() text: any;

  @HostListener('input', ['$event']) changeInput(event: any): void {
    this.newValue.emit(event.target.value);
  }

}
