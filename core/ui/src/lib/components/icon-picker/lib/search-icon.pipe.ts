import { Pipe, PipeTransform } from '@angular/core';
import { IIcon } from './icon';

@Pipe({
  name: 'searchIcon'
})

export class SearchIconPipe implements PipeTransform {

  transform(value: Array<IIcon>, search: string): any {
    if (!search) {
      return value;
    }

    const searchValue = this.clean(search);

    return value.filter(icon => {
      let keep = false;
      if (icon.name) {
        keep = keep || this.clean(icon.name)
          .includes(searchValue);
      }
      if (icon.className) {
        keep = keep || this.clean(icon.className)
          .includes(searchValue);
      }
      if (icon.filters) {
        keep = keep || icon.filters.some(val => this.clean(val)
          .includes(searchValue));
      }

      return keep;
    });
  }

  clean(value: string): string {
    return value.trim()
      .toLowerCase();
  }
}
