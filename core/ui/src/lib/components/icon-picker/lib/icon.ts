export class IIcon {
  name: string;
  className: string;
  filters?: Array<string>;
}
