import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IIcon } from './icon';
import { IconPickerDirective } from './icon-picker.directive';

@Component({
  selector: 'icon-picker',
  templateUrl: './icon-picker.component.html',
  styleUrls: ['./icon-picker.component.scss']
})
export class IconPickerComponent implements OnInit {

  @ViewChild('dialogPopup') dialogElement: any;

  ipPosition: string;
  ipHeight: number;
  ipMaxHeight: number;
  ipWidth: number;
  ipPlaceHolder: string;
  ipFallbackIcon: string;

  show: boolean;
  selectedIcon: IIcon;

  private directiveInstance: any;
  private initialIcon: string;
  private directiveElementRef: ElementRef;

  search = '';
  icons: Array<IIcon> = [];

  constructor(
    private el: ElementRef,
    private cdr: ChangeDetectorRef
  ) {
  }

  setDialog(
    instance: IconPickerDirective, elementRef: ElementRef, icon: string, ipPosition: string, ipHeight: string, ipMaxHeight: string,
    ipWidth: string, ipPlaceHolder: string, ipFallbackIcon: string): void {

    this.directiveInstance = instance;
    this.icons = instance.icons;

    this.directiveElementRef = elementRef;
    this.ipPosition = ipPosition;
    this.ipHeight = parseInt(ipHeight, 10);
    this.ipMaxHeight = parseInt(ipMaxHeight, 10);
    this.ipWidth = parseInt(ipWidth, 10);
    if (!this.ipWidth) {
      this.ipWidth = elementRef.nativeElement.offsetWidth;
    }
    this.ipPlaceHolder = ipPlaceHolder;
    this.ipFallbackIcon = ipFallbackIcon;
  }

  ngOnInit(): void {
    this.openDialog(this.initialIcon);
  }

  setInitialIcon(icon: string): void {
    this.initialIcon = icon;
    this.selectedIcon = this.icons
      .find(el => el ? el.className === icon : false);
  }

  openDialog(icon: string): void {
    this.setInitialIcon(icon);
    this.openIconPicker();
  }

  setSearch(val: string): void {
    this.search = val;
  }

  selectIcon(icon: IIcon): void {
    this.directiveInstance.iconSelected(icon.className);
    this.closeIconPicker();
  }

  openIconPicker(): void {
    if (!this.show) {
      this.show = true;
      this.cdr.detectChanges();
    }
  }

  closeIconPicker(): void {
    if (this.show) {
      this.show = false;
      this.cdr.detectChanges();
    }
  }

  getIcons(): void {

  }

}
