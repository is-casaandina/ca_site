import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IIcon } from './lib';

@Component({
  selector: 'ca-icon-picker',
  templateUrl: './icon-picker.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => IconPickerComponent),
      multi: true
    }
  ]
})
export class IconPickerComponent implements OnInit, ControlValueAccessor {

  @Input() icons: Array<IIcon> = [];

  protected _value: string;
  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
    this.propagateChange(this._value);
  }

  form: FormGroup;
  icon: string;

  ngOnInit(): void { }

  onIconPickerDefaultSelect(icon: string): void {
    this.value = icon;
  }

  /* Takes the value  */
  writeValue(value: string): void {
    if (value !== undefined) {
      this._value = value;
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

}
