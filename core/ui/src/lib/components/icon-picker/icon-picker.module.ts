import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { InfiniteScrollerDirectiveModule } from '@ca-core/shared/helpers/directives/infinite-scroller.directive';
import { IconPickerComponent as IconPicker } from './icon-picker.component';
import { IconPickerComponent } from './lib/icon-picker.component';
import { IconPickerDirective } from './lib/icon-picker.directive';
import { SearchIconPipe } from './lib/search-icon.pipe';
import { TextDirective } from './lib/text.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InfiniteScrollerDirectiveModule
  ],
  declarations: [
    IconPickerComponent,
    IconPickerDirective,
    TextDirective,
    SearchIconPipe,
    IconPicker
  ],
  exports: [
    IconPicker
  ],
  entryComponents: [
    IconPickerComponent
  ]
})
export class CaIconPickerModule {
}
