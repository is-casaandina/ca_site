import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaLinkComponent } from './link.component';

@NgModule({
  declarations: [CaLinkComponent],
  imports: [
    CommonModule
  ],
  exports: [CaLinkComponent]
})
export class CaLinkModule { }
