import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CaCheckboxComponent } from './checkbox.component';

@NgModule({
  declarations: [CaCheckboxComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [CaCheckboxComponent]
})
export class CaCheckboxModule { }
