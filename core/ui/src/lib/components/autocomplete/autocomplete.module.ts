import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { CaAutocompleteComponent } from './autocomplete.component';

@NgModule({
  declarations: [CaAutocompleteComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbTypeaheadModule
  ],
  exports: [CaAutocompleteComponent]
})
export class CaAutocompleteModule { }
