import { Component, ElementRef, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'ca-autocomplete, ca-autocom',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CaAutocompleteComponent),
      multi: true
    }
  ]
})
export class CaAutocompleteComponent extends UnsubscribeOnDestroy implements OnInit, ControlValueAccessor, OnDestroy {

  @ViewChild('autoComplete') autoComplete: ElementRef;

  protected _placeholder: string;
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value || '';
  }

  protected _value: string;
  @Input()
  get value(): string {
    return this._value;
  }
  set value(value: string) {
    this._value = value;
    this.propagateChange(this._value);
  }

  protected _disabled: boolean;
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProp(value);
  }

  protected _readonly: boolean;
  @Input()
  get readonly(): boolean {
    return this._readonly;
  }
  set readonly(value: boolean) {
    this._readonly = coerceBooleanProp(value);
  }

  protected _error: boolean;
  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }

  @Input() startLength: number;
  @Input() dataList: Array<any>;
  @Input() matchField: string;
  @Input() textField: string;

  @Output() selectItem: EventEmitter<any>;

  list: Array<any>;
  iconSearch: boolean;

  constructor() {
    super();
    this._placeholder = '';
    this._disabled = false;
    this._readonly = false;
    this._error = false;
    this.iconSearch = true;
    this.startLength = 2;
    this.selectItem = new EventEmitter<any>();
  }

  ngOnInit(): void { }

  search = (text$: Observable<string>) =>
    text$.pipe(
      takeUntil(this.unsubscribeDestroy$),
      distinctUntilChanged(),
      map(term => {
        if (term.length >= this.startLength) {
          return this.dataList.filter(item => {
            const matchField = item[this.matchField].toLowerCase();

            return matchField.includes(term.toLowerCase());
          });
        }

        return [];
      })
    )

  formatter = (item: any) => item[this.textField];

  oSelectItem(event): void {
    this._disabled = true;
    this.iconSearch = false;
    this.selectItem.next(event);
  }

  delete(): void {
    this._value = null;
    this.propagateChange(null);
    this._disabled = false;
    this.iconSearch = true;
    setTimeout(() => {
      this.autoComplete.nativeElement.focus();
    }, 0);
  }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value !== undefined) {
      this._value = value;
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

  ngOnDestroy(): void {
    if (!!this.selectItem) { this.selectItem.unsubscribe(); }
  }

}
