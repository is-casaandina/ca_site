import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberDirectiveModule } from '@ca-core/shared/helpers/directives/number.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { CaInputSearchComponent } from './input-search/input-search.component';
import { CaInputComponent } from './input/input.component';
import { CaPasswordComponent } from './password/password.component';

@NgModule({
  declarations: [CaInputComponent, CaPasswordComponent, CaInputSearchComponent, AutocompleteComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NumberDirectiveModule
  ],
  exports: [CaInputComponent, CaPasswordComponent, CaInputSearchComponent, AutocompleteComponent]
})
export class CaInputModule { }
