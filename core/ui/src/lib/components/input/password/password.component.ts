import { Component, OnInit } from '@angular/core';
import { CaInputBaseComponent, providerInputBase } from '../input-base.component';

@Component({
  selector: 'ca-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css'],
  providers: [ providerInputBase(CaPasswordComponent) ]
})
export class CaPasswordComponent extends CaInputBaseComponent implements OnInit {

  viewPassword: boolean;

  constructor() {
    super();
    this.viewPassword = false;
    this._type = 'password';
  }

  ngOnInit(): void { }

  toggleViewPassword(): void {
    this.viewPassword = (this._type === 'password');
    this._type = (this.viewPassword)
                  ? 'text'
                  : 'password';
  }

}
