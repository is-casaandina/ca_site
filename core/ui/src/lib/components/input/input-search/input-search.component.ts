import { Component, OnInit } from '@angular/core';
import { CaInputBaseComponent, providerInputBase } from '../input-base.component';

@Component({
  selector: 'ca-input-search',
  templateUrl: './input-search.component.html',
  providers: [providerInputBase(CaInputSearchComponent)]
})
export class CaInputSearchComponent extends CaInputBaseComponent implements OnInit {
  constructor() {
    super();
  }

  ngOnInit(): void {}
}
