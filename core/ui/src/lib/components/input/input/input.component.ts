import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { CaInputBaseComponent, providerInputBase } from '../input-base.component';

@Component({
  selector: 'ca-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css'],
  providers: [providerInputBase(CaInputComponent)]
})
export class CaInputComponent extends CaInputBaseComponent implements OnInit {

  protected _type = 'text';
  @Input()
  get type(): string {
    return this._type;
  }
  set type(type: string) {
    this._type = type || 'text';
  }

  protected _accept;
  @Input()
  get accept(): string {
    return this._accept;
  }
  set accept(accept: string) {
    this._accept = accept || null;
  }

  protected _multipled;
  @Input()
  get multipled(): boolean {
    return this._multipled;
  }
  set multipled(multipled: boolean) {
    this._multipled = multipled;
  }

  protected _pattern;
  @Input()
  get pattern(): string {
    return this._pattern;
  }
  set pattern(pattern: string) {
    this._pattern = pattern;
  }

  protected _error = false;
  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }

  protected _maxlength;
  @Input()
  get maxlength(): number {
    return this._maxlength;
  }
  set maxlength(maxlength: number) {
    this._maxlength = maxlength;
  }

  protected _min;
  @Input()
  get min(): number {
    return this._min;
  }
  set min(min: number) {
    this._min = min;
  }

  @Input() Number: boolean;
  @Input() id: any;
  @Input() autocomplete = 'on';
  @Input() prefix: string;
  @Input() box: boolean;

  @Output() onchange: EventEmitter<any> = new EventEmitter<any>();
  @Output() onfocus: EventEmitter<any> = new EventEmitter<any>();
  @Output() onblur: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    super();
  }

  onChangeInput(event: any): void {
    this.onchange.emit(event);
  }

  onFocusInput(event: any): void {
    this.onfocus.emit(event);
  }

  onBlurInput(event: any): void {
    this.onblur.emit(event);
  }

  ngOnInit(): void { }

}
