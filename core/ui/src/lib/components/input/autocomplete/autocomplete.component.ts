import { Component, Input, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { merge, Observable, of, Subject } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'ca-autocomplete',
  styleUrls: ['./autocomplete.component.scss'],
  template: `
    <input
      class="g-input g-input-invert"
      [attr.placeholder]="placeholder || ''"
      [formControl]="controlAutocomplete"
      [ngbTypeahead]="search"
      [inputFormatter]="inputFormatter"
      [resultFormatter]="resultFormatter"
      (focus)="focus($event.target.value)"
      (blur)="blur()"
      (selectItem)="selecting($event)"
    />
  `
})
export class AutocompleteComponent implements OnInit {
  @Input() placeholder: string;
  @Input() language: string;
  @Input() request: string;
  @Input() endPoint: string;
  @Input() fbControl: any;
  @Input() initOnFocus: boolean;
  @Input() customParams: any;

  @Input() appendList: { field: string; value: string; appendText: string };

  controlAutocomplete: FormControl;

  private list: Array<any> = [];
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  constructor(private apiService: ApiService, private fb: FormBuilder, private ngZone: NgZone) {}

  ngOnInit(): void {
    this.controlAutocomplete = this.fb.control('autocomplete');

    const data = this.fbControl.value;
    if (data) {
      this.controlAutocomplete.setValue(data);
      this.list.push(data);
    }
  }

  blur(): void {
    const data = this.list.find(item => item.name === (this.controlAutocomplete.value || {}).name);
    this.controlAutocomplete.setValue(data);
    this.fbControl.setValue(data);
  }

  focus(value: string): void {
    if (this.initOnFocus) {
      this.focus$.next(value);
    }
  }

  selecting(ev: NgbTypeaheadSelectItemEvent): void {
    this.fbControl.setValue(ev.item);
  }

  search = (text$: Observable<string>) => {
    const inputFocus$ = this.focus$;
    const debouncedText$ = text$.pipe(
      debounceTime(200),
      distinctUntilChanged()
    );

    return merge(debouncedText$, inputFocus$).pipe(
      switchMap(term =>
        this.getdata(term).pipe(
          map(list => {
            list = list || [];
            if (this.appendList) {
              list = list.map(item => {
                if (item[this.appendList.field] === this.appendList.value) {
                  item.name = `${item.name} ${this.appendList.appendText}`;
                }

                return item;
              });
            }
            this.list = list;

            return this.list;
          }),
          catchError(() => {
            return of([]);
          })
        )
      )
    );
  }

  private getdata(value: string): IObservableArray<any> {
    const params = {
      params: {
        name: value,
        limit: 10
      }
    };

    if (this.customParams) {
      Object.keys(this.customParams).forEach(key => (params.params[key] = this.customParams[key]));
    }

    return this.apiService.get<IObservableArray<any>>(this.endPoint, params);
  }

  inputFormatter = (x: any) => {
    return x.name;
  }

  resultFormatter = (x: any) => {
    return x.name ? x.name : x;
  }
}
