import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NOTIFICATION_TYPES } from '@ca-core/shared/helpers/notification';

@Component({
  selector: 'ca-notify',
  templateUrl: './notify.component.html'
})
export class CaNotifyComponent implements OnInit {

  @ViewChild('notify') notify: ElementRef;
  @ViewChild('icon') icon: ElementRef;

  protected _previousNotify;

  protected _type: string;
  @Input()
  get type(): string {
    return this._type;
  }
  set type(value: string) {
    const notify = Object.keys(NOTIFICATION_TYPES)
      .find((fv, fk) => {
        return NOTIFICATION_TYPES[fv].code === value;
      });
    if (NOTIFICATION_TYPES[notify]) {
      if (this._previousNotify) {
        this.notify.nativeElement.classList.remove(`g-main-notification-${this._previousNotify.color}`);
        this.icon.nativeElement.classList.remove(this._previousNotify.icon);
      }
      this.notify.nativeElement.classList.add(`g-main-notification-${NOTIFICATION_TYPES[notify].color}`);
      this.icon.nativeElement.classList.add(NOTIFICATION_TYPES[notify].icon);
      this._previousNotify = NOTIFICATION_TYPES[notify];
    }
    this._type = value;
  }

  protected _active: boolean;
  @Input()
  get active(): boolean {
    return this._active;
  }
  set active(value: boolean) {
    const action = (value)
      ? 'add'
      : 'remove';
    this.notify.nativeElement.classList[action]('active');
    this._active = value;
  }

  constructor() { }

  ngOnInit(): void { }

}
