import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaNotifyComponent } from './notify.component';

@NgModule({
  declarations: [CaNotifyComponent],
  imports: [
    CommonModule
  ],
  exports: [CaNotifyComponent]
})
export class CaNotifyModule { }
