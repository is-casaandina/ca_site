import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ca-alert-error',
  templateUrl: './alert-error.component.html',
  styleUrls: ['./alert-error.component.css']
})
export class CaAlertErrorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
