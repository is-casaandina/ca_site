import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaAlertErrorComponent } from './alert-error.component';

@NgModule({
  declarations: [CaAlertErrorComponent],
  imports: [
    CommonModule
  ],
  exports: [CaAlertErrorComponent]
})
export class CaAlertErrorModule { }
