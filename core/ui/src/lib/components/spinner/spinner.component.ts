import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ca-spinner',
  template: `
  <div class="text-center mt-2">
    <div class="spinner-grow text-warning" role="status">
      <span class="sr-only">Cargando...</span>
    </div>
  </div>
  `
})
export class CaSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
