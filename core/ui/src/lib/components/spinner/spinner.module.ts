import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaSpinnerComponent } from './spinner.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CaSpinnerComponent],
  exports: [CaSpinnerComponent]
})
export class CaSpinnerModule { }
