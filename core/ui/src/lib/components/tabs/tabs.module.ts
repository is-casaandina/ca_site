import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CaTabContentComponent } from './tab-content/tab-content.component';
import { CaTabHeaderComponent } from './tab-header/tab-header.component';

@NgModule({
  declarations: [
    CaTabHeaderComponent,
    CaTabContentComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    CaTabHeaderComponent,
    CaTabContentComponent
  ]
})
export class CaTabsModule { }
