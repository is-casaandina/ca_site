export { CaTabContentComponent as CaTabContent } from './tab-content/tab-content.component';
export { CaTabHeaderComponent as CaTabHeader } from './tab-header/tab-header.component';
export { CaTabsModule } from './tabs.module';
