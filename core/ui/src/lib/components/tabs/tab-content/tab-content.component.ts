import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ca-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.css']
})
export class CaTabContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }

}
