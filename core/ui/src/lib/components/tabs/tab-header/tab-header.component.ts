import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ca-tab-header',
  templateUrl: './tab-header.component.html'
})
export class CaTabHeaderComponent implements OnInit {

  @Input() tabItemsList: Array<any>;

  constructor() { }

  ngOnInit(): void { }

}
