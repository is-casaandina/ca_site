import { ElementRef } from '@angular/core';

import { coerceBooleanProp } from '../helpers';
import { Constructor } from './constructor';

export interface CanOutline {
  outline: boolean;
}

export interface HasElementRef {
  _elementRef: ElementRef;
}

export type Outline = boolean;

export function mixinOutline<T extends Constructor<HasElementRef>>(base: T): Constructor<CanOutline> & T {
  return class extends base {
    private _outline: any;

    get outline(): Outline {
      return this._outline;
    }
    set outline(value: Outline) {
      const outline = coerceBooleanProp(value);
      if (this._outline !== outline) {
        if (outline) {
          this._elementRef.nativeElement.classList.add('g-btn-outline');
        } else {
          this._elementRef.nativeElement.classList.remove('g-btn-outline');
        }

        this._outline = outline;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);
    }
  };
}
