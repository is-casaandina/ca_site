import { ElementRef } from '@angular/core';

import { Constructor } from './constructor';
import { ThemePalette } from './theme';

export interface CanColor {
  color: ColorPalette;
}

export interface HasElementRef {
  _elementRef: ElementRef;
}

export type ColorPalette = 'primary' | undefined;

export function mixinColor<T extends Constructor<HasElementRef>>(
  base: T,
  defaultColor?: ColorPalette
): Constructor<CanColor> & T {

  return class extends base {

    private _color: ColorPalette;

    get color(): ColorPalette { return this._color; }
    set color(value: ColorPalette) {
      const colorPalette = value || defaultColor;

      if (colorPalette !== this._color) {

        if (this._color) {
          this._elementRef.nativeElement.classList.remove(`g-btn-${this._color}`);
        }
        if (colorPalette) {
          this._elementRef.nativeElement.classList.add(`g-btn-${colorPalette}`);
        }

        this._color = colorPalette;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);

      this.color = defaultColor;
    }
  };
}
