import { ElementRef } from '@angular/core';

import { coerceBooleanProp } from '../helpers';
import { Constructor } from './constructor';

export interface CanBlock {
  block: Block;
}

export interface HasElementRef {
  _elementRef: ElementRef;
}

export type Block = boolean;

export function mixinBlock<T extends Constructor<HasElementRef>>(base: T): Constructor<CanBlock> & T {
  return class extends base {
    private _block: Block;

    get block(): Block {
      return this._block;
    }
    set block(value: Block) {
      const block = coerceBooleanProp(value);
      if (this._block !== block) {
        if (block) {
          this._elementRef.nativeElement.classList.add('g-block');
        } else {
          this._elementRef.nativeElement.classList.remove('g-block');
        }

        this._block = block;
      }
    }

    constructor(...args: Array<any>) {
      super(...args);
    }
  };
}
