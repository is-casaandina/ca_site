import { Injectable } from '@angular/core';
import { IScript } from '@core/typings';

@Injectable({
  providedIn: 'root'
})
export class ScriptService {

  addScript(scripts: IScript): void {
    if (scripts && scripts.header) {
      const collection = this.stringToElements(scripts.header);
      collection.forEach(element => document.head.appendChild(element));
    }
    if (scripts && scripts.body) {
      const collection = this.stringToElements(scripts.body);
      collection.forEach(element => document.body.appendChild(element));
    }
    if (scripts && scripts.tracing) {
      const collection = this.stringToElements(scripts.tracing);
      collection.forEach(element => document.head.appendChild(element));
    }
    if (scripts && scripts.footer) {
      const collection = this.stringToElements(scripts.footer);
      collection.forEach(element => document.body.appendChild(element));
    }
  }

  private stringToElements(scripts: string): Array<Element> {
    const div = document.createElement('div');
    div.innerHTML = scripts;

    return Array.from(div.children)
      .map((element: any) => {
        if (element.tagName === 'SCRIPT') {
          const scriptTag = document.createElement(`script`);
          scriptTag.innerHTML = element.innerHTML;
          element.getAttributeNames()
            .forEach(attr => scriptTag.setAttribute(attr, element.getAttribute(attr)));
          element = scriptTag;
        }

        return element;
      });
  }

}
