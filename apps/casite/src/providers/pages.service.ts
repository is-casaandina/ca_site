import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { IDomain } from '@ca-core/shared/statemanagement/models/domain.interface';
import { IMenuSite } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { IPageResponse } from '@ca-site/statemanagement/page.interface';
import { IConfigurationResponse, IDescriptor } from 'core/typings';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PageEndPoints } from './endpoints';

@Injectable({
  providedIn: 'root'
})
export class PageServices {
  pageEndpoints = PageEndPoints;
  configurations: IConfigurationResponse;
  configurationSub: BehaviorSubject<IConfigurationResponse> = new BehaviorSubject<IConfigurationResponse>(null);
  currencySub: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  languageSub: BehaviorSubject<LANGUAGE_TYPES_CODE> = new BehaviorSubject<LANGUAGE_TYPES_CODE>(null);

  private domain: IDomain;

  constructor(
    private apiService: ApiService
  ) { }

  getPageError(code: string, language: string): Observable<IPageResponse> {
    return this.apiService.get(this.pageEndpoints.pageError, { params: { code, language } });
  }

  getPages(options: { params: { path?: string, language: string, id?: string } }): Observable<IPageResponse> {
    return this.apiService.get(this.pageEndpoints.pages, options);
  }

  getMenu(language: string): IObservableArray<IMenuSite> {
    return this.apiService.get(this.pageEndpoints.menu, { params: { language } });
  }

  getDescriptors(): IObservableArray<IDescriptor> {
    return this.apiService.get(this.pageEndpoints.descriptors);
  }

  getModalNotifications(urlParams: { pageId: string; language: string; descriptorId: string }): Observable<any> {
    return this.apiService.get(this.pageEndpoints.notification, { params: { language: urlParams.language, pageId: urlParams.pageId } });
  }

  getConfigurations(): Observable<IConfigurationResponse> {
    if (this.configurations) { return of(this.configurations); }

    return this.apiService.get<Observable<IConfigurationResponse>>(this.pageEndpoints.configuration)
      .pipe(map(res => {
        this.configurations = res;
        this.configurationSub.next(res);

        return res;
      }));
  }

  getConfigurationsSub(): BehaviorSubject<IConfigurationResponse> {
    return this.configurationSub;
  }

  getContactUrl(): Observable<any> {
    return this.apiService.get(this.pageEndpoints.contactUrl);
  }

  getDomainByPath(path: string, preloader = false): Observable<IDomain> {
    if (this.domain) { return of(this.domain); }

    return this.apiService.get<Observable<IDomain>>(this.pageEndpoints.domainByPath, { params: { path }, preloader })
      .pipe(map(res => {
        this.domain = res;

        return res;
      }));
  }

}
