import {
  Inject,
  Injectable,
  RendererFactory2,
  ViewEncapsulation
} from '@angular/core';
import { DOCUMENT, Meta, Title } from '@angular/platform-browser';
import { IMetadata, IPathPage } from '../statemanagement/page.interface';

@Injectable({
  providedIn: 'root'
})
export class SEOService {
  constructor(
    private metaService: Meta,
    private titleService: Title,
    private rendererFactory: RendererFactory2,
    // tslint:disable-next-line: deprecation
    @Inject(DOCUMENT) private document
  ) {}

  setData(metaData: IMetadata): void {
    // Setear Titulo
    this.addPageTitle(metaData.title);
    const metas = [];
    // Meta Descripción
    metas.push({ name: 'description', content: metaData.description });

    // Open Graph data
    metas.push({ property: 'og:title', content: this.getOgTitle(metaData) });
    metas.push({ property: 'og:locale', content: metaData.language });
    metas.push({ property: 'og:type', content: metaData.ogType });
    metas.push({ property: 'og:url', content: this.getOgUrl(metaData) });
    metas.push({ property: 'og:image', content: this.getOgImage(metaData) });
    metas.push({
      property: 'og:description',
      content: this.getOgDescription(metaData)
    });
    metas.push({ property: 'og:site_name', content: metaData.ogSitename });

    // Twitter Card Data
    metas.push({ name: 'twitter:card', content: metaData.twitterCard });
    metas.push({
      name: 'twitter:title',
      content: this.getTwitterTitle(metaData)
    });
    metas.push({
      name: 'twitter:description',
      content: this.getTwitterDescription(metaData)
    });
    metas.push({
      name: 'twitter:image',
      content: this.getTwitterImage(metaData)
    });

    // Set Metadata
    this.addMetaTags(metas);

    // Set Links
    this.addTag({ rel: 'canonical', href: this.getCanonical(metaData) }, true);
  }

  setLinksLang(paths: Array<IPathPage>): void {
    (paths || []).forEach(path => {
      this.addTag(
        {
          rel: 'alternate',
          href: `${this.getDomain()}/${path.path}`,
          hreflang: path.language
        },
        true
      );
    });
  }

  setHtmlLang(language: string): void {
    document.documentElement.lang = language;
  }

  private getDomain(): string {
    return `${location.protocol}//${location.hostname}${
      location.port ? `:${location.port}` : ''
    }`;
  }

  private getCanonical(metaData: IMetadata): string {
    return metaData.canonicalLink || this.document.URL;
  }

  private getOgTitle(metaData: IMetadata): string {
    return metaData.ogTitle || metaData.title;
  }

  private getOgUrl(metaData: IMetadata): string {
    return metaData.ogUrl || this.document.URL;
  }

  private getOgDescription(metaData: IMetadata): string {
    return metaData.ogDescription || metaData.description;
  }

  private getOgImage(metaData: IMetadata): string {
    return metaData.ogImage || metaData.image;
  }

  private getTwitterTitle(metaData: IMetadata): string {
    return metaData.twitterTitle || metaData.ogTitle || metaData.title;
  }

  private getTwitterDescription(metaData: IMetadata): string {
    return (
      metaData.twitterDescription ||
      metaData.ogDescription ||
      metaData.description
    );
  }

  private getTwitterImage(metaData: IMetadata): string {
    return metaData.twitterImage || metaData.ogImage || metaData.image;
  }

  addMetaTags(
    metas: Array<{ name?: string; property?: string; content: string }>
  ): void {
    metas
      .filter(item => !!item.content)
      .forEach(item => {
        if (item.name) {
          try {
            this.metaService.removeTag(`name='${item.name}'`);
          } catch (error) {}
          if (item.content) {
            this.metaService.addTag(
              { name: item.name, content: item.content },
              true
            );
          }
        } else {
          try {
            this.metaService.removeTag(`property='${item.property}'`);
          } catch (error) {}
          if (item.content) {
            this.metaService.addTag(
              { property: item.property, content: item.content },
              true
            );
          }
        }
      });
  }

  addPageTitle(title: string): void {
    this.titleService.setTitle(title);
  }

  addTag(tag: LinkDefinition, forceCreation?: boolean): void {
    try {
      const renderer = this.rendererFactory.createRenderer(this.document, {
        id: '-1',
        encapsulation: ViewEncapsulation.None,
        styles: [],
        data: {}
      });

      const link = renderer.createElement('link');
      const head = this.document.head;
      // const selector = this._parseSelector(tag);

      if (head === null) {
        throw new Error('<head> not found within DOCUMENT.');
      }

      Object.keys(tag)
        .forEach((prop: string) => {
          return renderer.setAttribute(link, prop, tag[prop]);
        });

      // [TODO]: get them to update the existing one (if it exists) ?
      renderer.appendChild(head, link);
    } catch (e) {
      console.error('Error within linkService : ', e);
    }
  }

  // private _parseSelector(tag: LinkDefinition): string {
  //   // Possibly re-work this
  //   const attr: string = tag.rel ? 'rel' : 'hreflang';

  //   return `${attr}="${tag[attr]}"`;
  // }
}

export declare type LinkDefinition = {
  charset?: string;
  crossorigin?: string;
  href?: string;
  hreflang?: string;
  media?: string;
  rel?: string;
  rev?: string;
  sizes?: string;
  target?: string;
  type?: string;
} & {
  [prop: string]: string;
};
