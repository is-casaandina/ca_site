import { Injectable } from '@angular/core';

@Injectable()
export class GeoLocationService {

  constructor(
  ) { }

  getLanguage(): string {
    const language = navigator.languages && navigator.languages[0] ||
      navigator.language ||
      window.navigator['userLanguage'];

    return this.getLanguageAbr(language || '');
  }

  getLanguageAbr(lang: string): string {
    const langAbr: string = (lang.indexOf('en') !== -1) ? 'en' :
      (lang.indexOf('pt') !== -1) ? 'pt' : 'es';

    return langAbr;
  }

}
