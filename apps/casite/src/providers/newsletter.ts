import { Injectable } from '@angular/core';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservable, IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { ILabelsRequest, INewsLetterRequest, ISimpleList, TYPE_PERSON } from '@ca/library/form-newsletter/newsletter';
import { PageEndPoints } from './endpoints';

@Injectable()
export class NewsLetterServices {

  constructor(private apiService: ApiService) { }

  getFormLabels(params: ILabelsRequest): IObservableArray<any> {
    return this.apiService.get(PageEndPoints.formTemplate, { params });
  }
  getCountries(): IObservableArray<ISimpleList> {
    return this.apiService.get(PageEndPoints.countries);
  }
  getSegments(language: string): IObservableArray<ISimpleList> {
    return this.apiService.get(PageEndPoints.segments, { params: { language } });
  }
  getGenders(language: string): IObservableArray<ISimpleList> {
    return this.apiService.get(PageEndPoints.genders, { params: { language } });
  }
  saveSubscriber(params: INewsLetterRequest, type: TYPE_PERSON): ICaObservable<string> {
    return this.apiService.post(`${PageEndPoints.subscriber}${type}`, params);
  }
}
