import { MAIN_ENV } from '../environments/environment';

export class PageEndPoints {
  public static configuration = `${MAIN_ENV.API_URL}site/configurations`;
  public static pages = `${MAIN_ENV.API_URL}site/pages`;
  public static pageError = `error/{code}/{language}`;
  public static notification = `${MAIN_ENV.API_URL}api/page/notification/site/{language}/{pageId}`;
  public static countries = `${MAIN_ENV.API_URL}site/countries/select`;
  public static genders = `${MAIN_ENV.API_URL}site/parameters/gender/{language}`;
  public static segments = `${MAIN_ENV.API_URL}site/parameters/segment/{language}`;
  public static subscriber = `${MAIN_ENV.API_URL}api/subscriber/`;
  public static menu = `${MAIN_ENV.API_URL}site/menus/`;
  public static descriptors = `${MAIN_ENV.API_URL}api/descriptors`;
  public static autocompleteDestination = `${MAIN_ENV.API_URL}site/pages/autocomplete/destinations`;
  public static autocompleteHotels = `${MAIN_ENV.API_URL}site/pages/autocomplete/hotels`;
  public static formTemplate = `${MAIN_ENV.API_URL}site/form/template`;
  public static contactUrl = `${MAIN_ENV.API_URL}site/configurations/contact/page`;
  public static domainByPath = `${MAIN_ENV.API_URL}site/domains`;
}
