import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AngularUtil } from '@ca-core/shared/helpers/util';
import {
  getStorageCurrency,
  getStoragePageInfo,
  setStorageCurrency
} from '@ca-core/shared/helpers/util/storage-helper';
import { CURRENCY_KEYS, LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { IDomain } from '@ca-core/shared/statemanagement/models/domain.interface';
import { PageServices } from '@ca-site/providers/pages.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DomainResolve implements Resolve<any> {
  nextUrl: string;

  constructor(private pageService: PageServices) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDomain> {
    let domain = window.location.origin;
    domain = domain.endsWith('/') ? domain : `${domain}/`;

    this.nextUrl = state.url;

    return this.pageService.getDomainByPath(domain).pipe(
      map(res => {
        res = this.setData(res);

        return res;
      }),
      catchError(err => {
        const res = this.setData({} as IDomain);

        return of(res);
      })
    );
  }

  private setData(res: IDomain): IDomain {
    const defaultCurrency = getStorageCurrency();
    const defaultLanguage = this.getLanguage();

    res.currency = defaultCurrency || res.currency || CURRENCY_KEYS.PEN;
    res.language = defaultLanguage || res.language || LANGUAGE_TYPES_CODE.es;

    res.location = this.nextUrl;
    res.locationPath = this.getPath();
    res.locationPathFormat = this.getPathWithLanguage(res.language);

    this.pageService.languageSub.next(res.language);
    setStorageCurrency(res.currency);

    return res;
  }

  private getLanguage(): LANGUAGE_TYPES_CODE {
    const pageInfo = getStoragePageInfo();
    const pathLanguage = this.getLanguageFromPath();

    return pathLanguage || (pageInfo && (AngularUtil.toJson(pageInfo, {}).language as LANGUAGE_TYPES_CODE));
  }

  private getPathWithLanguage(language: LANGUAGE_TYPES_CODE): string {
    let path = this.getPath();
    if (language && !this.getLanguageFromPath() && path.length > 1) {
      path = `${language}/${path}`;
    }

    return path;
  }

  private getLanguageFromPath(): LANGUAGE_TYPES_CODE {
    return AngularUtil.enumToArray(LANGUAGE_TYPES_CODE).find(
      code => this.nextUrl.startsWith(`/${code}/`) || this.nextUrl === `/${code}`
    );
  }

  private getPath(): string {
    return this.nextUrl.length > 1
      ? this.nextUrl.length < 4 && this.getLanguageFromPath()
        ? '/'
        : this.nextUrl.substring(1).split('?')[0]
      : '/';
  }
}
