import { enableProdMode } from '@angular/core';

import { MAIN_ENV } from './environments/environment';

if (MAIN_ENV.production) {
  enableProdMode();
}

export { AppServerModule } from './app/app.server.module';
