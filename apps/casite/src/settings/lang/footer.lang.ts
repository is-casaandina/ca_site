export const FooterLang = {
  socialNetwork: {
    es: 'Redes sociales',
    en: 'Social networks',
    pt: 'Redes sociais'
  },
  subscribe: {
    es: 'Suscribete',
    en: 'Subscribe',
    pt: 'Subscrever'
  },
  person: {
    es: 'Persona',
    en: 'Person',
    pt: 'Pessoa'
  },
  natural: {
    es: 'Natural',
    en: 'Natural',
    pt: 'Natural'
  },
  legalPerson: {
    es: 'Jurídica',
    en: 'Legal entity',
    pt: 'Jurídica'
  },
  email: {
    es: 'Ingresa tu email',
    en: 'Enter your email',
    pt: 'Digite seu email'
  },
  paymentMethods: {
    es: 'Medios de pago',
    en: 'Paymnent methods',
    pt: 'Meios de pagamento'
  },
  copyright: {
    es: 'Todos los derechos reservados - Casa Andina Hoteles',
    en: 'All direct reserved - Casa Andina Hoteles',
    pt: 'Todos os direitos reservados - Casa Andina Hoteles'
  }
};
