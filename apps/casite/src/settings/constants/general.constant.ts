export const ASSETS_PATH = {
  images: 'assets/images/',
  videos: 'assets/videos/'
};

export const CA = {
  logoSmall: 'https://s3.amazonaws.com/multimediadev.casa-andina.com/images/main/ca-brand-white.png'
};

export const COLORS_DESCRIPTORS = {
  colors: {
    select: '#008eaa',
    premium: '#002855',
    standard: '#ff4200',
    normal: '#000000'
  },
  logoColors: {
    select: '#008eaa',
    premium: '#002855',
    standard: '#ff4200',
    normal: '#000000'
  },
  corners: {
    select: '#008eaa',
    premium: '#002855',
    standard: '#ff4200',
    normal: '#d69c4f'
  }
};
