import { ASSETS_PATH } from './general.constant';

export const IMAGES = {
  logoCa: {
    src: `${ASSETS_PATH.images}casa-andina-logo.svg`,
    alt: 'Logo Casa Andina'
  }
};
