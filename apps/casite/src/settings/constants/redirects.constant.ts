export const urlsRedirect = [
  { urlOld: '/', urlNew: '/es/home' },
  { urlOld: '/arco', urlNew: '/es/arco' },
  {
    urlOld: '/beneficios-exclusivos-por-comprar-en-nuestra-web',
    urlNew: '/es/beneficios-exclusivos-por-comprar-en-nuestra-web'
  },
  { urlOld: '/contacto', urlNew: '/es/contacto' },
  { urlOld: '/destinos', urlNew: '/es/destinos' },
  { urlOld: '/destinos/arequipa', urlNew: '/es/destinos/arequipa' },
  {
    urlOld: '/destinos/arequipa/hotel-arequipa-peru_casa-andina-premium',
    urlNew: '/es/destinos/arequipa/hoteles/casa-andina-premium-arequipa'
  },
  {
    urlOld: '/destinos/arequipa/casa-andina-select-arequipa',
    urlNew: '/es/destinos/arequipa/hoteles/casa-andina-select-arequipa'
  },
  {
    urlOld: '/destinos/arequipa/standard-arequipa',
    urlNew: '/es/destinos/arequipa/hoteles/casa-andina-standard-arequipa'
  },
  { urlOld: '/destinos/chiclayo', urlNew: '/es/destinos/chiclayo' },
  {
    urlOld: '/destinos/chiclayo/hotel-chiclayo-peru_casa-andina-select-chiclayo',
    urlNew: '/es/destinos/chiclayo/hoteles/casa-andina-select-chiclayo'
  },
  { urlOld: '/destinos/chincha', urlNew: '/es/destinos/chincha' },
  { urlOld: '/destinos/chincha/standard-chincha', urlNew: '/es/destinos/chincha/hoteles/casa-andina-standard-chincha' },
  { urlOld: '/destinos/colca', urlNew: '/es/destinos/colca' },
  { urlOld: '/destinos/colca/standard-colca', urlNew: '/es/destinos/colca/hoteles/casa-andina-standard-colca' },
  { urlOld: '/destinos/cusco', urlNew: '/es/destinos/cusco' },
  {
    urlOld: '/destinos/cusco/hotel-cusco-peru_casa-andina-premium',
    urlNew: '/es/destinos/cusco/hoteles/casa-andina-premium-cusco'
  },
  {
    urlOld: '/destinos/cusco/standard-cusco-catedral',
    urlNew: '/es/destinos/cusco/hoteles/casa-andina-standard-cusco-catedral'
  },
  {
    urlOld: '/destinos/cusco/standard-cuzco-koricancha',
    urlNew: '/es/destinos/cusco/hoteles/casa-andina-standard-cusco-koricancha'
  },
  {
    urlOld: '/destinos/cusco/standard-cusco-plaza',
    urlNew: '/es/destinos/cusco/hoteles/casa-andina-standard-cusco-plaza'
  },
  {
    urlOld: '/destinos/cusco/standard-cusco-san-blas',
    urlNew: '/es/destinos/cusco/hoteles/casa-andina-standard-cusco-san-blas'
  },
  { urlOld: '/destinos/lima', urlNew: '/es/destinos/lima' },
  { urlOld: '/destinos/machu-picchu', urlNew: '/es/destinos/machu-picchu' },
  {
    urlOld: '/destinos/machu-picchu/casa-andina-standard-machu-picchu',
    urlNew: '/es/destinos/machu-picchu/hoteles/casa-andina-standard-machu-picchu'
  },
  {
    urlOld: '/destinos/miraflores/hotel-miraflores-lima-peru_casa-andina-premium',
    urlNew: '/es/destinos/miraflores/hoteles/casa-andina-premium-miraflores'
  },
  {
    urlOld: '/destinos/miraflores/hotel-miraflores-lima_casa-andina-select-miraflores',
    urlNew: '/es/destinos/miraflores/hoteles/casa-andina-select-miraflores'
  },
  {
    urlOld: '/destinos/miraflores/standard-miraflores-centro',
    urlNew: '/es/destinos/miraflores/hoteles/casa-andina-standard-miraflores-centro'
  },
  {
    urlOld: '/destinos/miraflores/standard-miraflores-san-antonio',
    urlNew: '/es/destinos/miraflores/hoteles/casa-andina-standard-miraflores-san-antonio'
  },
  { urlOld: '/destinos/moquegua', urlNew: '/es/destinos/moquegua' },
  {
    urlOld: '/destinos/moquegua/hotel-moquegua-casa-andina',
    urlNew: '/es/destinos/moquegua/hoteles/casa-andina-select-moquegua'
  },
  { urlOld: '/destinos/nasca', urlNew: '/es/destinos/nasca' },
  { urlOld: '/destinos/nasca/standard-nasca', urlNew: '/es/destinos/nasca/hoteles/casa-andina-standard-nasca' },
  { urlOld: '/destinos/piura', urlNew: '/es/destinos/piura' },
  {
    urlOld: '/destinos/piura/casa-andina-premium-piura',
    urlNew: '/es/destinos/piura/hoteles/casa-andina-premium-piura'
  },
  {
    urlOld: '/destinos/piura/casa-andina-standard-piura',
    urlNew: '/es/destinos/piura/hoteles/casa-andina-standard-piura'
  },
  { urlOld: '/destinos/pucallpa', urlNew: '/es/destinos/pucallpa' },
  {
    urlOld: '/destinos/pucallpa/hotel-pucallpa-peru_casa-andina-select-pucallpa',
    urlNew: '/es/destinos/pucallpa/hoteles/casa-andina-select-pucallpa'
  },
  { urlOld: '/destinos/puno', urlNew: '/es/destinos/puno' },
  {
    urlOld: '/destinos/puno/hotel-puno-peru_casa-andina-premium',
    urlNew: '/es/destinos/puno/hoteles/casa-andina-premium-puno'
  },
  { urlOld: '/destinos/puno/standard-puno', urlNew: '/es/destinos/puno/hoteles/casa-andina-standard-puno' },
  { urlOld: '/destinos/tacna', urlNew: '/es/destinos/tacna' },
  {
    urlOld: '/destinos/tacna/casa-andina-select-hotel-tacna',
    urlNew: '/es/destinos/tacna/hoteles/casa-andina-select-tacna'
  },
  { urlOld: '/destinos/talara', urlNew: '/es/destinos/talara' },
  {
    urlOld: '/destinos/talara/casa-andina-standard-talara',
    urlNew: '/es/destinos/talara/hoteles/casa-andina-standard-talara'
  },
  { urlOld: '/destinos/trujillo', urlNew: '/es/destinos/trujillo' },
  {
    urlOld: '/destinos/trujillo/hotel-trujillo-peru_casa-andina-premium',
    urlNew: '/es/destinos/trujillo/hoteles/casa-andina-premium-trujillo'
  },
  {
    urlOld: '/destinos/trujillo/casa-andina-standard-trujillo',
    urlNew: '/es/destinos/trujillo/hoteles/casa-andina-standard-trujillo-plaza'
  },
  { urlOld: '/destinos/valle-sagrado', urlNew: '/es/destinos/valle-sagrado' },
  {
    urlOld: '/destinos/valle-sagrado/hotel-cusco-valle-sagrado_casa-andina-premium',
    urlNew: '/es/destinos/valle-sagrado/hoteles/casa-andina-premium-valle-sagrado-hotel-&-villas'
  },
  { urlOld: '/destinos/zorritos-tumbes', urlNew: '/es/destinos/zorritos-tumbes' },
  {
    urlOld: '/destinos/zorritos-tumbes/hotel-tumbes-peru_casa-andina-select-tumbes',
    urlNew: '/es/destinos/zorritos-tumbes/hoteles/select-zorritos-tumbes'
  },
  { urlOld: '/dog-friendly', urlNew: '/es/dog-friendly' },
  { urlOld: '/eventos-y-reuniones', urlNew: '/es/eventos-y-reuniones' },
  { urlOld: '/corporativo', urlNew: '/es/eventos-y-reuniones/corporativo' },
  { urlOld: '/sociales', urlNew: '/es/eventos-y-reuniones/sociales' },
  { urlOld: '/libro-de-reclamaciones', urlNew: '/es/libro-de-reclamaciones' },
  { urlOld: '/promociones', urlNew: '/es/ofertas' },
  { urlOld: '/promociones/combate-de-angamos', urlNew: '/es/ofertas/combate-de-angamos' },
  { urlOld: '/promociones/compra-anticipada', urlNew: '/es/ofertas/compra-anticipada' },
  { urlOld: '/promociones/estadia-minima-2-noches', urlNew: '/es/ofertas/estadia-minima-2-noches' },
  { urlOld: '/promociones/estadia-minima-3-noches', urlNew: '/es/ofertas/estadia-minima-3-noches' },
  { urlOld: '/promociones/semana-santa', urlNew: '/es/politica-web-de-privacidad' },
  { urlOld: '/promociones/ultimo-minuto', urlNew: '/es/preguntas-frecuentes' },
  { urlOld: '/promociones/vacaciones-de-octubre', urlNew: '/es/preguntas-frecuentes/actividades-y-tours' },
  { urlOld: '/promociones/verano', urlNew: '/es/preguntas-frecuentes/contactos' },
  { urlOld: '/promociones/wow-junio', urlNew: '/es/preguntas-frecuentes/infraestructura-y-facilidades' },
  { urlOld: '/preguntas-frecuentes/politica-reserva', urlNew: '/es/preguntas-frecuentes/politica-reserva' },
  { urlOld: '/casa-andina-premium', urlNew: '/es/premium' },
  { urlOld: '/sobre-nosotros', urlNew: '/es/quienes-somos' },
  { urlOld: '/reservaciones', urlNew: '/es/reservaciones' },
  { urlOld: '/responsabilidad-social', urlNew: '/es/responsabilidad-social' },
  { urlOld: '/restaurantes', urlNew: '/es/restaurantes' },
  { urlOld: '/casa-andina-select', urlNew: '/es/select' },
  { urlOld: '/casa-andina-standard', urlNew: '/es/standard' },
  { urlOld: '/terminos-y-condiciones', urlNew: '/es/terminos-y-condiciones' },
  { urlOld: '/en', urlNew: '/en/home' },
  {
    urlOld: '/en/destinations/arequipa/hotel-arequipa-peru_casa-andina-premium',
    urlNew: '/en/destinations/arequipa/hotels/casa-andina-premium-arequipa'
  },
  {
    urlOld: '/en/destinations/arequipa/casa-andina-select-arequipa',
    urlNew: '/en/destinations/arequipa/hotels/casa-andina-select-arequipa'
  },
  {
    urlOld: '/en/destinations/arequipa/standard-arequipa',
    urlNew: '/en/destinations/arequipa/hotels/casa-andina-standard-arequipa'
  },
  {
    urlOld: '/en/destinations/chiclayo/hotel-chiclayo-peru_casa-andina-select-chiclayo',
    urlNew: '/en/destinations/chiclayo/hotels/casa-andina-select-chiclayo'
  },
  {
    urlOld: '/en/destinations/chincha/standard-chincha',
    urlNew: '/en/destinations/chincha/hotels/casa-andina-standard-chincha'
  },
  {
    urlOld: '/en/destinations/colca/standard-colca',
    urlNew: '/en/destinations/colca/hotels/casa-andina-standard-colca'
  },
  {
    urlOld: '/en/destinations/cusco/hotel-cusco-peru_casa-andina-premium',
    urlNew: '/en/destinations/cusco/hotels/casa-andina-premium-cusco'
  },
  {
    urlOld: '/en/destinations/cusco/standard-cusco-catedral',
    urlNew: '/en/destinations/cusco/hotels/casa-andina-standard-cusco-catedral'
  },
  {
    urlOld: '/en/destinations/cusco/standard-cuzco-koricancha',
    urlNew: '/en/destinations/cusco/hotels/casa-andina-standard-cusco-koricancha'
  },
  {
    urlOld: '/en/destinations/cusco/standard-cusco-plaza',
    urlNew: '/en/destinations/cusco/hotels/casa-andina-standard-cusco-plaza'
  },
  {
    urlOld: '/en/destinations/cusco/standard-cusco-san-blas',
    urlNew: '/en/destinations/cusco/hotels/casa-andina-standard-cusco-san-blas'
  },
  {
    urlOld: '/en/destinations/machu-picchu/casa-andina-standard-machu-picchu',
    urlNew: '/en/destinations/machu-picchu/hotels/casa-andina-standard-machu-picchu'
  },
  {
    urlOld: '/en/destinations/miraflores/hotel-miraflores-lima-peru_casa-andina-premium',
    urlNew: '/en/destinations/miraflores/hotels/casa-andina-premium-miraflores'
  },
  {
    urlOld: '/en/destinations/miraflores/hotel-miraflores-lima_casa-andina-select-miraflores',
    urlNew: '/en/destinations/miraflores/hotels/casa-andina-select-miraflores'
  },
  {
    urlOld: '/en/destinations/miraflores/standard-miraflores-centro',
    urlNew: '/en/destinations/miraflores/hotels/casa-andina-standard-miraflores-centro'
  },
  {
    urlOld: '/en/destinations/miraflores/standard-miraflores-san-antonio',
    urlNew: '/en/destinations/miraflores/hotels/casa-andina-standard-miraflores-san-antonio'
  },
  {
    urlOld: '/en/destinations/moquegua/hotel-moquegua-casa-andina',
    urlNew: '/en/destinations/moquegua/hotels/casa-andina-select-moquegua'
  },
  {
    urlOld: '/en/destinations/nasca/standard-nasca',
    urlNew: '/en/destinations/nasca/hotels/casa-andina-standard-nasca'
  },
  {
    urlOld: '/en/destinations/piura/casa-andina-premium-piura',
    urlNew: '/en/destinations/piura/hotels/casa-andina-premium-piura'
  },
  {
    urlOld: '/en/destinations/piura/casa-andina-standard-piura',
    urlNew: '/en/destinations/piura/hotels/casa-andina-standard-piura'
  },
  {
    urlOld: '/en/destinations/pucallpa/hotel-pucallpa-peru_casa-andina-select-pucallpa',
    urlNew: '/en/destinations/pucallpa/hotels/casa-andina-select-pucallpa'
  },
  {
    urlOld: '/en/destinations/puno/hotel-puno-peru_casa-andina-premium',
    urlNew: '/en/destinations/puno/hotels/casa-andina-premium-puno'
  },
  { urlOld: '/en/destinations/puno/standard-puno', urlNew: '/en/destinations/puno/hotels/casa-andina-standard-puno' },
  {
    urlOld: '/en/destinations/tacna/casa-andina-select-hotel-tacna',
    urlNew: '/en/destinations/tacna/hotels/casa-andina-select-tacna'
  },
  {
    urlOld: '/en/destinations/talara/casa-andina-standard-talara',
    urlNew: '/en/destinations/talara/hotels/casa-andina-standard-talara'
  },
  {
    urlOld: '/en/destinations/trujillo/hotel-trujillo-peru_casa-andina-premium',
    urlNew: '/en/destinations/trujillo/hotels/casa-andina-premium-trujillo'
  },
  {
    urlOld: '/en/destinations/trujillo/casa-andina-standard-trujillo',
    urlNew: '/en/destinations/trujillo/hotels/casa-andina-standard-trujillo-plaza'
  },
  {
    urlOld: '/en/destinations/valle-sagrado/hotel-cusco-valle-sagrado_casa-andina-premium',
    urlNew: '/en/destinations/valle-sagrado/hotels/casa-andina-premium-valle-sagrado-hotel-&-villas'
  },
  {
    urlOld: '/en/destinations/zorritos-tumbes/hotel-tumbes-peru_casa-andina-select-tumbes',
    urlNew: '/en/destinations/zorritos-tumbes/hotels/select-zorritos-tumbes'
  },
  { urlOld: '/en/corporativo', urlNew: '/en/events-and-meetings/corporate' },
  { urlOld: '/en/social', urlNew: '/en/events-and-meetings/social' },
  { urlOld: '/en/deals/combate-de-angamos-holiday', urlNew: '/en/offers-and-deals/combate-de-angamos' },
  { urlOld: '/en/deals/early-booking', urlNew: '/en/offers-and-deals/early-booking' },
  { urlOld: '/en/deals/last-minute', urlNew: '/en/offers-and-deals/last-minute' },
  { urlOld: '/en/deals/minimum-stay-2-night', urlNew: '/en/offers-and-deals/minimum-stay-2-night' },
  { urlOld: '/en/deals/minimum-stay-3-night', urlNew: '/en/offers-and-deals/minimum-stay-3-night' },
  { urlOld: '/en/deals/octobers-holidays', urlNew: '/en/offers-and-deals/october-holidays' },
  { urlOld: '/en/casa-andina-select', urlNew: '/en/select' },
  { urlOld: '/en/casa-andina-standard', urlNew: '/en/standard' },
  { urlOld: '/pr', urlNew: '/pt/home' },
  {
    urlOld: '/pr/beneficios-exclusivos-por-comprar-en-nuestra-web',
    urlNew: '/pt/beneficios-exclusivos-por-comprar-en-nuestra-web'
  },
  { urlOld: '/pr/contato', urlNew: '/pt/contato' },
  { urlOld: '/pr/destinacoes', urlNew: '/pt/destinacoes' },
  { urlOld: '/pr/destinacoes/arequipa', urlNew: '/pt/destinacoes/arequipa' },
  {
    urlOld: '/pr/destinacoes/arequipa/hotel-arequipa-peru_casa-andina-premium',
    urlNew: '/pt/destinacoes/arequipa/hoteis/casa-andina-premium-arequipa'
  },
  {
    urlOld: '/pr/destinacoes/arequipa/casa-andina-select-arequipa',
    urlNew: '/pt/destinacoes/arequipa/hoteis/casa-andina-select-arequipa'
  },
  {
    urlOld: '/pr/destinacoes/arequipa/standard-arequipa',
    urlNew: '/pt/destinacoes/arequipa/hoteles/casa-andina-standard-arequipa'
  },
  { urlOld: '/pr/destinacoes/chiclayo', urlNew: '/pt/destinacoes/chiclayo' },
  {
    urlOld: '/pr/destinacoes/chiclayo/hotel-chiclayo-peru_casa-andina-select-chiclayo',
    urlNew: '/pt/destinacoes/chiclayo/hoteis/casa-andina-select-chiclayo'
  },
  { urlOld: '/pr/destinacoes/chincha', urlNew: '/pt/destinacoes/chincha' },
  {
    urlOld: '/pr/destinacoes/chincha/standard-chincha',
    urlNew: '/pt/destinacoes/chincha/hoteis/casa-andina-standard-chincha'
  },
  { urlOld: '/pr/destinacoes/colca', urlNew: '/pt/destinacoes/colca' },
  { urlOld: '/pr/destinacoes/colca/standard-colca', urlNew: '/pt/destinacoes/colca/hoteis/casa-andina-standard-colca' },
  { urlOld: '/pr/destinacoes/cusco', urlNew: '/pt/destinacoes/cusco' },
  {
    urlOld: '/pr/destinacoes/cusco/hotel-cusco-peru_casa-andina-premium',
    urlNew: '/pt/destinacoes/cusco/hoteis/casa-andina-premium-cusco'
  },
  {
    urlOld: '/pr/destinacoes/cusco/standard-cusco-catedral',
    urlNew: '/pt/destinacoes/cusco/hoteis/casa-andina-standard-cusco-catedral'
  },
  {
    urlOld: '/pr/destinacoes/cusco/standard-cuzco-koricancha',
    urlNew: '/pt/destinacoes/cusco/hoteis/casa-andina-standard-cusco-koricancha'
  },
  {
    urlOld: '/pr/destinacoes/cusco/standard-cusco-plaza',
    urlNew: '/pt/destinacoes/cusco/hoteis/casa-andina-standard-cusco-plaza'
  },
  {
    urlOld: '/pr/destinacoes/cusco/standard-cusco-san-blas',
    urlNew: '/pt/destinacoes/cusco/hoteis/casa-andina-standard-cusco-san-blas'
  },
  { urlOld: '/pr/destinacoes/lima', urlNew: '/pt/destinacoes/lima' },
  { urlOld: '/pr/destinacoes/machu-picchu', urlNew: '/pt/destinacoes/machu-picchu' },
  {
    urlOld: '/pr/destinacoes/machu-picchu/casa-andina-standard-machu-picchu',
    urlNew: '/pt/destinacoes/machu-picchu/hoteis/casa-andina-standard-machu-picchu'
  },
  {
    urlOld: '/pr/destinacoes/miraflores/hotel-miraflores-lima-peru_casa-andina-premium',
    urlNew: '/pt/destinacoes/miraflores/hoteis/casa-andina-premium-miraflores'
  },
  {
    urlOld: '/pr/destinacoes/miraflores/hotel-miraflores-lima_casa-andina-select-miraflores',
    urlNew: '/pt/destinacoes/miraflores/hoteis/casa-andina-select-miraflores'
  },
  {
    urlOld: '/pr/destinacoes/miraflores/standard-miraflores-centro',
    urlNew: '/pt/destinacoes/miraflores/hoteis/casa-andina-standard-miraflores-centro'
  },
  {
    urlOld: '/pr/destinacoes/miraflores/standard-miraflores-san-antonio',
    urlNew: '/pt/destinacoes/miraflores/hoteis/casa-andina-standard-miraflores-san-antonio'
  },
  { urlOld: '/pr/destinacoes/moquegua', urlNew: '/pt/destinacoes/moquegua' },
  {
    urlOld: '/pr/destinacoes/moquegua/hotel-moquegua-casa-andina',
    urlNew: '/pt/destinacoes/moquegua/hoteis/casa-andina-select-moquegua'
  },
  { urlOld: '/pr/destinacoes/nasca', urlNew: '/pt/destinacoes/nasca' },
  { urlOld: '/pr/destinacoes/nasca/standard-nasca', urlNew: '/pt/destinacoes/nasca/hoteis/casa-andina-standard-nasca' },
  { urlOld: '/pr/destinacoes/piura', urlNew: '/pt/destinacoes/piura' },
  {
    urlOld: '/pr/destinacoes/piura/casa-andina-premium-piura',
    urlNew: '/pt/destinacoes/piura/hoteis/casa-andina-premium-piura'
  },
  {
    urlOld: '/pr/destinacoes/piura/casa-andina-standard-piura',
    urlNew: '/pt/destinacoes/piura/hoteis/casa-andina-standard-piura'
  },
  { urlOld: '/pr/destinacoes/pucallpa', urlNew: '/pt/destinacoes/pucallpa' },
  {
    urlOld: '/pr/destinacoes/pucallpa/hotel-pucallpa-peru_casa-andina-select-pucallpa',
    urlNew: '/pt/destinacoes/pucallpa/hoteis/casa-andina-select-pucallpa'
  },
  { urlOld: '/pr/destinacoes/puno', urlNew: '/pt/destinacoes/puno' },
  {
    urlOld: '/pr/destinacoes/puno/hotel-puno-peru_casa-andina-premium',
    urlNew: '/pt/destinacoes/puno/hoteis/casa-andina-premium-puno'
  },
  { urlOld: '/pr/destinacoes/puno/standard-puno', urlNew: '/pt/destinacoes/puno/hoteis/casa-andina-standard-puno' },
  { urlOld: '/pr/destinacoes/tacna', urlNew: '/pt/destinacoes/tacna' },
  {
    urlOld: '/pr/destinacoes/tacna/casa-andina-select-hotel-tacna',
    urlNew: '/pt/destinacoes/tacna/hoteis/casa-andina-select-tacna'
  },
  { urlOld: '/pr/destinacoes/talara', urlNew: '/pt/destinacoes/talara' },
  {
    urlOld: '/pr/destinacoes/talara/casa-andina-standard-talara',
    urlNew: '/pt/destinacoes/talara/hoteis/casa-andina-standard-talara'
  },
  { urlOld: '/pr/destinacoes/trujillo', urlNew: '/pt/destinacoes/trujillo' },
  {
    urlOld: '/pr/destinacoes/trujillo/casa-andina-standard-trujillo',
    urlNew: '/pt/destinacoes/trujillo/hoteis/casa-andina-standard-trujillo-plaza'
  },
  {
    urlOld: '/pr/destinacoes/trujillo/hotel-trujillo-peru_casa-andina-premium',
    urlNew: '/pt/destinacoes/trujillo/hoteis/casa-andina-premium-trujillo'
  },
  { urlOld: '/pr/destinacoes/valle-sagrado', urlNew: '/pt/destinacoes/valle-sagrado' },
  {
    urlOld: '/pr/destinacoes/valle-sagrado/hotel-cusco-valle-sagrado_casa-andina-premium',
    urlNew: '/pt/destinacoes/valle-sagrado/hoteis/casa-andina-premium-valle-sagrado-hotel-&-villas'
  },
  { urlOld: '/pr/destinacoes/zorritos-tumbes', urlNew: '/pt/destinacoes/zorritos-tumbes' },
  {
    urlOld: '/pr/destinacoes/zorritos-tumbes/hotel-tumbes-peru_casa-andina-select-tumbes',
    urlNew: '/pt/destinacoes/zorritos-tumbes/hoteis/select-zorritos-tumbes'
  },
  { urlOld: '/pr/dog-friendly', urlNew: '/pt/dog-friendly' },
  { urlOld: '/pr/livro-de-reclamacoes', urlNew: '/pt/livro-de-reclamacoes' },
  { urlOld: '/pr/promocoes', urlNew: '/pt/ofertas' },
  { urlOld: '/pr/promocoes/feria-de-combate-de-angamos', urlNew: '/pt/ofertas/combate-de-angamos' },
  { urlOld: '/pr/promocoes/compra-antecipada', urlNew: '/pt/ofertas/compra-antecipada' },
  { urlOld: '/pr/promocoes/estadia-minima-2-noites', urlNew: '/pt/ofertas/estadia-minima-2-noites' },
  { urlOld: '/pr/promocoes/estadia-minima-3-noites', urlNew: '/pt/ofertas/estadia-minima-3-noites' },
  { urlOld: '/pr/promocoes/ferias-de-outubro', urlNew: '/pt/ofertas/ferias-de-octubre' },
  { urlOld: '/pr/promocoes/promocao-de-ultimo-minuto', urlNew: '/pt/ofertas/promocao-de-ultimo-minuto' },
  { urlOld: '/pr/perguntas-frequentes', urlNew: '/pt/perguntas-frequentes' },
  { urlOld: '/pr/perguntas-frequentes/actividades-y-tours', urlNew: '/pt/perguntas-frequentes/actividades-e-passeios' },
  { urlOld: '/pr/perguntas-frequentes/contactos', urlNew: '/pt/perguntas-frequentes/contatos' },
  {
    urlOld: '/pr/perguntas-frequentes/infraestructura-y-facilidades',
    urlNew: '/pt/perguntas-frequentes/infra-estrutura-e-instalacoes'
  },
  { urlOld: '/pr/perguntas-frequentes/politica-reserva', urlNew: '/pt/perguntas-frequentes/politica-reserva' },
  { urlOld: '/pr/politica-web-de-privacidad', urlNew: '/pt/politica-web-de-privacidad' },
  { urlOld: '/pr/casa-andina-premium', urlNew: '/pt/premium' },
  { urlOld: '/pr/quem-somos', urlNew: '/pt/quem-somos' },
  { urlOld: '/pr/reservaciones', urlNew: '/pt/reservas' },
  { urlOld: '/pr/responsabilidad-social', urlNew: '/pt/responsabilidade-social' },
  { urlOld: '/pr/restaurantes', urlNew: '/pt/restaurantes' },
  { urlOld: '/pr/eventos-e-reunioes', urlNew: '/pt/reunioes-e-eventos' },
  { urlOld: '/pr/corporativo', urlNew: '/pt/reunioes-e-eventos/corporativo' },
  { urlOld: '/pr/social', urlNew: '/pt/reunioes-e-eventos/sociais' },
  { urlOld: '/pr/casa-andina-select', urlNew: '/pt/select' },
  { urlOld: '/pr/solicitud-de-derechos-arco', urlNew: '/pt/solicitud-de-derechos-arco' },
  { urlOld: '/pr/casa-andina-standard', urlNew: '/pt/standard' },
  { urlOld: '/pr/termos-e-condicoes', urlNew: '/pt/terminos-y-condiciones' }
];
