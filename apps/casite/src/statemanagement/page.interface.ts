export interface IPageContentLanguage {
  language: string;
  canonical: string;
  path: string;
  pagePath: string;
  parentPath: string;
  metadata: IMetadata;
  social: string;
  title: string;
  category: string;
  tags: Array<string>;
  content: IContent;
  name?: string;
  data?: any;
}

export interface IPageResponse {
  name: string;
  pageType: number;
  type: number;
  state: number;
  owner?: string;
  user?: string;
  createdDate?: string;
  modifiedDate?: string;
  id?: string;
  categoryId?: string;
  languages: Array<IPageContentLanguage>;
  descriptorId?: string;
  revinate?: string;
  contact?: IContact;

  invalidCountry?: true;
  countryRequest?: string;
  countryName?: string;

  paths: Array<IPathPage>;
}

export interface IPathPage {
  language: string;
  path: string;
}

export interface IContact {
  title1: string;
  title2: string;
  languages: Array<IlangugeContact>;
}

export interface IlangugeContact {
  language: string;
  details: Array<IDetailContact>;
}

export interface IDetailContact {
  description: string;
  icon: string;
  label: string;
  type: number;
}

export interface IContent {
  html: string;
  refComponents: Array<string>;
  data?: any;
}

export interface IMetadata {
  // title: string;
  // description: string;
  // keywords: string;
  // robots: string;
  // nositelinksearchbox: string;
  // nosnippet: string;
  // autor: string;
  // subject: string;
  language?: string;
  // revisitafter: string;
  image?: string;
  title?: string;
  titleSnippet?: string;
  description?: string;
  ogSitename?: string;
  ogType?: string;
  ogUrl?: string;
  ogTitle?: string;
  ogDescription?: string;
  ogImage?: string;
  twitterCard?: string;
  twitterTitle?: string;
  twitterDescription?: string;
  twitterImage?: string;
  canonicalLink?: string;
  keywords?: string;
  slug?: string;
}
