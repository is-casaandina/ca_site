import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainLayoutComponent } from '@ca-site/layouts/main-layout/main-layout.component';
import { RedirectGuard } from '../guards/redirect.guard';
import { DomainResolve } from '../resolves/domain.resolve';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    resolve: [DomainResolve],
    canActivate: [RedirectGuard],
    children: [
      {
        path: 'preview',
        resolve: [DomainResolve],
        canActivate: [RedirectGuard],
        loadChildren: '../main/views/page/page.module#PageModule'
      },
      {
        path: '**',
        resolve: [DomainResolve],
        canActivate: [RedirectGuard],
        pathMatch: 'full',
        loadChildren: '../main/views/page/page.module#PageModule'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always',
      scrollPositionRestoration: 'enabled'
    })
  ],
  exports: [
    RouterModule
  ]
})
export class RoutesModule { }
