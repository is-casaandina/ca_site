import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';

@Component({
  selector: 'casite-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(private router: Router) {
    this.onCustomNavigator();
  }

  private onCustomNavigator(): void {
    GlobalNavigateUtil.onCustomNavigator().subscribe(path => {
      this.router.navigate([path || '/']);
    });
  }
}
