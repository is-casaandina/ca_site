import { CommonModule, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ModalModule, ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { SPINNER_CONFIG_SITE } from '@ca-core/shared/settings/constants/general.constant';
import { MainLayoutModule } from '@ca-site/layouts/main-layout/main-layout.module';
import { GeoLocationService } from '@ca-site/providers/geolocation.service';
import { NotificationModalModule } from '@ca/library';
import { RoutesModule } from '../routes/routes.module';
import { AppComponent } from './app.component';

import localeEnUS from '@angular/common/locales/en-US-POSIX';
import localeEsPE from '@angular/common/locales/es-PE';
import localeEsPt from '@angular/common/locales/pt-PT';

registerLocaleData(localeEsPt, 'pt');
registerLocaleData(localeEsPE, 'es');
registerLocaleData(localeEnUS, 'en');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NotificationModule,
    HttpClientModule,
    NotificationModalModule,
    ModalModule,
    RoutesModule,
    MainLayoutModule,
    SpinnerModule.forRoot(SPINNER_CONFIG_SITE)
  ],
  providers: [
    ModalService,
    GeoLocationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
