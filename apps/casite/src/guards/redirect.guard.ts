import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { urlsRedirect } from '@ca-site/settings/constants/redirects.constant';

@Injectable({
  providedIn: 'root'
})
export class RedirectGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const index = urlsRedirect.findIndex(r => r.urlOld === state.url);
    if (index > -1) {
      this.router.navigateByUrl(urlsRedirect[index].urlNew);

      return false;
    } else {
      return true;
    }
  }
}
