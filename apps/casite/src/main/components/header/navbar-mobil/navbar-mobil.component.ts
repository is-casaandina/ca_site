import { Component, Input } from '@angular/core';
import { RESERVE_ACTIONS } from '@ca-admin/settings/constants/general.lang';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'casite-navbar-mobil',
  templateUrl: './navbar-mobil.component.html'
})
export class NavbarMobilComponent {

  @Input() showBooking: boolean;
  @Input() language: LANGUAGE_TYPES_CODE;
  @Input() contactConfig: any;
  @Input() contactPath: Array<any>;

  RESERVE_ACTIONS = RESERVE_ACTIONS;

  activeSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  openBookingSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  openContactInfo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  toggle(): void {
    this.activeSub.next(!this.activeSub.getValue());
  }

  openBooking(): void {
    this.openBookingSub.next(true);
  }

  goToHome(): void {
    GlobalNavigateUtil.goTo('/');
  }

  goContactForm(): void {
    GlobalNavigateUtil.goTo(this.contactPath.find(item => item.language === this.language).path);
  }

  contactInfoToggle(): void {
    this.openContactInfo.next(!this.openContactInfo.getValue());
  }

}
