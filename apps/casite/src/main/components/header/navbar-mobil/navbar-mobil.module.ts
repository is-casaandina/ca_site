import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavbarMobilComponent } from './navbar-mobil.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NavbarMobilComponent],
  exports: [NavbarMobilComponent]
})
export class NavbarMobilModule { }
