import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { StorageService, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { PlatformService } from '@ca-core/shared/helpers/util/platform.service';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { IMenuSite, MENU_SITE_ACTION } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { PageServices } from '@ca-site/providers/pages.service';
import { takeUntil } from 'rxjs/operators';
import { MenuComponent } from './menu/menu.component';
import { NavbarMobilComponent } from './navbar-mobil/navbar-mobil.component';

@Component({
  selector: 'casite-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild(NavbarMobilComponent) navbar: NavbarMobilComponent;
  @ViewChild(MenuComponent) menu: MenuComponent;

  @Input() header: Array<IMenuSite>;
  @Input() language: LANGUAGE_TYPES_CODE;
  @Input() descriptorsIdColor: { [key: string]: string };

  get currentPath(): string {
    return this.getPath();
  }
  contactConfig: any;
  contactPath: Array<any>;

  constructor(
    private location: Location,
    private router: Router,
    private platformService: PlatformService,
    private resizeService: ResizeService,
    private pageService: PageServices,
    protected storageService: StorageService
  ) {
    super();
  }

  ngOnInit(): void {
    const conf = this.storageService.getItemObject<any>('CONFIGURATIONS');
    if (conf) { this.contactConfig = conf.contact; }
    this.onMobilActive();
    this.handlerRouterChange();
    this.getContactPath();
  }

  private getPath(): string {
    const locationPath = this.location.path() || '';

    return locationPath.length > 0 ? locationPath.substring(1)
      .split('?')[0] : '/';
  }

  private onMobilActive(): void {
    this.navbar.activeSub
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(active => {
        if (this.menu.activeNavbar !== active) {
          this.menu.activeNavbar = active;
          this.menu.updateChange(0);
          if (this.menu.items) {
            const bookingItem = this.menu.items.find(item => item.action === MENU_SITE_ACTION.BOOKING);
            bookingItem.active = !active ? active : bookingItem.active;
          }
        }
      });

    this.navbar.openBookingSub
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(active => {
        if (active) {
          this.menu.activeBooking();
          this.navbar.activeSub.next(true);
        }
      });

    this.menu.onAutoCollapse
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        const bookingItem = this.menu.items.find(item => item.action === MENU_SITE_ACTION.BOOKING);
        bookingItem.active = false;
        this.navbar.activeSub.next(false);
      });
  }

  private handlerRouterChange(): void {
    this.router
      .events
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.navbar.activeSub.next(false);
        }
      });
  }

  get isMobile(): boolean {
    return this.platformService.isMobile || this.resizeService.currentWidth < 1199;
  }

  private getContactPath(): void {
    this.pageService.getContactUrl()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.contactPath = response);
  }
}
