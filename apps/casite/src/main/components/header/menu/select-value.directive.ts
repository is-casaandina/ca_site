import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[casiteSelectValue]'
})
export class SelectValueDirective implements OnInit {

  select: HTMLSelectElement;

  constructor(private elem: ElementRef) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.select = this.elem.nativeElement;
      this.setValue(this.select.value);

      this.select.onchange = () => {
        this.setValue(this.select.querySelector('option:checked')
          .getAttribute('value'));
      };
    });
  }

  private setValue(selectedValue): void {
    const firstOption = this.select.querySelectorAll('option')[0];
    firstOption.value = selectedValue;
    firstOption.innerHTML = selectedValue;
    this.select.value = selectedValue;
  }
}
