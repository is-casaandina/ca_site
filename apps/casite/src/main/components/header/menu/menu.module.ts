import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MenuSectionPipeModule } from '@ca-core/shared/helpers/pipes/menu-section.pipe';
import { OrderByPipeModule } from '@ca-core/shared/helpers/pipes/orderby.pipe';
import { SafeUrlPipeModule } from '@ca-core/shared/helpers/pipes/safe-url.pipe';
import { BookingModule } from '@ca-site/components/booking/booking.module';
import { MenuComponent } from './menu.component';
import { SelectValueDirective } from './select-value.directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MenuSectionPipeModule,
    BookingModule,
    SafeUrlPipeModule,
    OrderByPipeModule
  ],
  declarations: [MenuComponent, SelectValueDirective],
  exports: [MenuComponent]
})
export class MenuModule { }
