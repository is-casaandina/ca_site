import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CURRENCY_COPYS, GENERAL_TEXT, LANGUAGES_COPYS } from '@ca-admin/settings/constants/general.lang';
import { StorageService, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { PlatformService } from '@ca-core/shared/helpers/util/platform.service';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { getStorageCurrency, setStorageCurrency } from '@ca-core/shared/helpers/util/storage-helper';
import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { IMenuSite, MENU_SITE_ACTION } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { PageServices } from '@ca-site/providers/pages.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'casite-menu',
  templateUrl: './menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      .bullet::before {
        content: '• ';
        font-size: 16px;
      }
    `
  ]
})
export class MenuComponent extends UnsubscribeOnDestroy implements OnInit {
  @Input() items: Array<IMenuSite>;
  @Input() language: LANGUAGE_TYPES_CODE;
  @Input() contactPath: Array<any>;

  currency = getStorageCurrency();

  private _currentPath: string;
  @Input()
  set currentPath(currentPath) {
    this._currentPath = currentPath;
    this.updateChange();
  }
  get currentPath(): string {
    return this._currentPath;
  }

  @Input() descriptorsIdColor: { [key: string]: string };
  @Input() contactConfig: any;

  activeNavbar: boolean;
  contactNav: boolean;
  MENU_SITE_ACTION = MENU_SITE_ACTION;
  GENERAL_TEXT = GENERAL_TEXT;
  CURRENCY_COPYS = CURRENCY_COPYS;
  LANGUAGES_COPYS = LANGUAGES_COPYS;
  showSelect = false;

  onAutoCollapse: Subject<any> = new Subject<any>();
  hideLanguage: boolean;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private resizeService: ResizeService,
    private platformService: PlatformService,
    private pageService: PageServices,
    protected storageService: StorageService,
    private activedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.hideLanguage = this.activedRoute.snapshot.data[0].hideLanguage;
  }

  updateChange(timer: number = 250): void {
    setTimeout(() => {
      this.changeDetectorRef.detectChanges();
    }, timer);
  }
  hasActiveChild(parent: IMenuSite): boolean {
    return !!parent.children.find(child => child.active);
  }

  bookingActive(item: IMenuSite): boolean {
    return item.action === MENU_SITE_ACTION.BOOKING && item.active;
  }

  isActive(item: IMenuSite, parent: IMenuSite): boolean {
    const isMobil = this.isMobile;
    const active = this.isSameRoute(item);
    const activeBrother = (parent.children || []).filter(brother => brother !== item).find(brother => brother.active);

    if (!isMobil) {
      parent.active = active || !!activeBrother;
    } else if ((active || !!activeBrother) && !parent.active) {
      parent.active = true;
      parent.collapse = false;
    }

    return active;
  }

  onHover(item: IMenuSite): void {
    const isMobil = this.isMobile;

    if (item.action !== MENU_SITE_ACTION.BOOKING && !isMobil) {
      item.hover = true;
      if (item.children && item.children.length) {
        this.items
          .filter(brother => brother.action === MENU_SITE_ACTION.BOOKING)
          .forEach(header => (header.active = false));
      }
    }
  }

  onLeave(item: IMenuSite): void {
    const isMobil = this.isMobile;

    if (!isMobil) {
      item.hover = false;
    }
  }

  isSameRoute(item: IMenuSite): boolean {
    const active = this.currentPath === item.pagePath || this.hasActiveChild(item);
    if (item.action !== MENU_SITE_ACTION.BOOKING) {
      item.active = active;
    }

    return active;
  }

  goContactForm(): void {
    GlobalNavigateUtil.goTo(this.contactPath.find(item => item.language === this.language).path);
  }

  goTo(item: IMenuSite, event: MouseEvent, level: number): void {
    if (event) {
      event.preventDefault();
    }
    if (item.action || (!item.action && item.children.length)) {
      const isMobil = this.isMobile;
      if (isMobil) {
        this.goToMobil(item, level);
      } else {
        this.goToDesktop(item, true);
      }
    }
  }

  private goToDesktop(item: IMenuSite, deactive: boolean): void {
    this.items.forEach(header => (header.hover = false));

    if (item.action === MENU_SITE_ACTION.BOOKING) {
      item.active = !item.active;
    } else {
      const url = item.action === MENU_SITE_ACTION.REDIRECT ? item.pagePath : '';
      if (url && !item.external) {
        GlobalNavigateUtil.goTo(url);
        this.currentPath = url;
        this.items
          .filter(tmp => tmp.action === MENU_SITE_ACTION.BOOKING)
          .forEach(tmp => {
            if (deactive) {
              tmp.active = false;
            }
          });
      } else if (url) {
        window.open(url, '_blank');
      }
    }
    this.updateChange();
  }

  private goToMobil(item: IMenuSite, level: number): void {
    let url: string;
    switch (level) {
      case 1:
        const filters = this.items.filter(brother => brother !== item);
        if (!item.children.length) {
          this.goToDesktop(item, false);
          filters.forEach(parent => {
            this.unactive(parent);
            this.collapse(parent);
          });
        } else {
          item.collapse = !item.collapse;
          filters.forEach(parent => {
            this.collapse(parent);
          });
        }
        this.updateChange();
        break;
      case 2:
        if (!item.children.length) {
          url = item.action === MENU_SITE_ACTION.REDIRECT ? item.pagePath : '';
          if (url && !item.external) {
            GlobalNavigateUtil.goTo(url);
            this.currentPath = url;
            this.items
              .filter(brother => brother.children.indexOf(item) < 0)
              .forEach(brother => {
                this.unactive(brother);
                this.collapse(brother);
              });
          } else if (url) {
            window.open(url, '_blank');
          }
        } else {
          item.collapse = !item.collapse;
          this.items
            .find(brother => brother.children.indexOf(item) > -1)
            .children.filter(brother => brother !== item)
            .forEach(brother => this.collapse(brother));
          this.updateChange();
        }
        break;
      case 3:
        url = item.action === MENU_SITE_ACTION.REDIRECT ? item.pagePath : '';
        if (url) {
          if (item.external) {
            window.open(url, '_blank');
          } else {
            GlobalNavigateUtil.goTo(url);
            this.currentPath = url;
            const grandparent = this.items.find(
              brother => !!brother.children.find(child => child.children.indexOf(item) > -1)
            );
            grandparent.children
              .filter(child => child.children.indexOf(item) < 0)
              .forEach(child => {
                this.unactive(child);
                this.collapse(child);
              });

            this.items
              .filter(child => child !== grandparent)
              .forEach(child => {
                this.unactive(child);
                this.collapse(child);
              });
          }
        }
        break;
      case 4:
        url = item.pagePath;
        if (url && !item.external) {
          GlobalNavigateUtil.goTo(url);
          this.currentPath = url;
          this.activeNavbar = false;
          this.onAutoCollapse.next();
          // this.items
          //   .filter(brother => brother.children.indexOf(item) < 0)
          //   .forEach(brother => {
          //     this.unactive(brother);
          //     this.collapse(brother);
          //   });
        } else if (url) {
          window.open(url, '_blank');
        }
        break;
      default:
        break;
    }
  }
  setCurrency(currency: string): void {
    setStorageCurrency(currency);
    this.pageService.currencySub.next(currency);
  }

  toggle(): void {
    this.onAutoCollapse.next();
  }

  activeMenuBooking(ev): void {
    this.toggle();
  }

  setLanguage(language: LANGUAGE_TYPES_CODE): void {
    this.pageService.languageSub.next(language);
  }

  activeBooking(): void {
    const booking = this.items.find(item => item.action === MENU_SITE_ACTION.BOOKING);
    if (booking) {
      this.goTo(booking, null, 1);
    }
  }

  get isMobile(): boolean {
    return this.platformService.isMobile || this.resizeService.currentWidth < 1199;
  }

  private unactive(item: IMenuSite): void {
    item.active = false;
    item.children.forEach(it => this.unactive(it));
  }

  private collapse(item: IMenuSite): void {
    item.collapse = true;
    item.children.forEach(it => this.collapse(it));
  }
}
