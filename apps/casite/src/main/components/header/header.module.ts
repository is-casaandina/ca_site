import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavbarFixedDirectiveModule } from '@ca-core/shared/helpers/directives/navbar-fixed.directive';
import { HeaderComponent } from './header.component';
import { MenuModule } from './menu/menu.module';
import { NavbarMobilModule } from './navbar-mobil/navbar-mobil.module';

@NgModule({
  imports: [
    CommonModule,
    MenuModule,
    NavbarFixedDirectiveModule,
    NavbarMobilModule
  ],
  declarations: [HeaderComponent],
  exports: [HeaderComponent]
})
export class HeaderModule { }
