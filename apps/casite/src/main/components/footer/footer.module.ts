import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MenuSectionPipeModule } from '@ca-core/shared/helpers/pipes/menu-section.pipe';
import { OrderByPipeModule } from '@ca-core/shared/helpers/pipes/orderby.pipe';
import { SafeHtmlPipeModule } from '@ca-core/shared/helpers/pipes/safe-html.pipe';
import { SafeUrlPipeModule } from '@ca-core/shared/helpers/pipes/safe-url.pipe';
import { FormNewsletterModule } from '@ca/library';
import { FooterComponent } from './footer.component';

@NgModule({
  imports: [
    CommonModule,
    MenuSectionPipeModule,
    FormNewsletterModule,
    FormsModule,
    OrderByPipeModule,
    SafeUrlPipeModule,
    SafeHtmlPipeModule
  ],
  declarations: [FooterComponent],
  exports: [FooterComponent]
})
export class FooterModule { }
