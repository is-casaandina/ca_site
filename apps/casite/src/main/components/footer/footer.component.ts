import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { PlatformService } from '@ca-core/shared/helpers/util/platform.service';
import { ResizeService } from '@ca-core/shared/helpers/util/resize.service';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { IMenuSite, MENU_SITE_ACTION } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { MAIN_ENV } from '@ca-site/environments/environment';
import { PageServices } from '@ca-site/providers/pages.service';
import { FooterLang } from '@ca-site/settings/lang/footer.lang';
import { FormNewsletterComponent } from '@ca/library';
import { TYPE_PERSON } from '@ca/library/form-newsletter/newsletter';
import { IConfigurationResponse } from '@core/typings';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'casite-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent extends UnsubscribeOnDestroy implements OnInit {

  @Input() footer: Array<IMenuSite>;
  @Input() language: LANGUAGE_TYPES_CODE;

  configurations: IConfigurationResponse;

  typePerson: TYPE_PERSON = TYPE_PERSON.PERSON;
  email: string;

  MENU_SITE_ACTION = MENU_SITE_ACTION;
  FooterLang = FooterLang;

  constructor(
    private pageService: PageServices,
    private modalService: ModalService,
    private resizeService: ResizeService,
    private platformService: PlatformService
  ) {
    super();
  }

  ngOnInit(): void {
    this.pageService.getConfigurationsSub()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.configurations = res;
        if (this.configurations && this.configurations.iconsPay) {
          this.configurations.iconsPay = this.configurations.iconsPay.filter(icon => icon);
        }
      });
  }

  collapse(item: IMenuSite): void {
    if (this.isMobile) {
      this.footer
        .filter(child => child !== item)
        .forEach(child => child.collapse = true);

      item.collapse = !item.collapse;
    }
  }

  goTo(item: IMenuSite, event: MouseEvent): void {
    if (event) {
      event.preventDefault();
    }

    const url = item.action === MENU_SITE_ACTION.REDIRECT ? item.pagePath : '';
    if (url && !item.external) {
      GlobalNavigateUtil.goTo(url);
    } else if (url) {
      window.open(url, '_blank');
    }
  }

  openNewsLetterForm(): void {
    const modalRef = this.modalService.open(FormNewsletterComponent, { size: 'md' });
    modalRef.setPayload({
      typePerson: this.typePerson,
      email: this.email || '',
      recaptchaKey: MAIN_ENV.CAPTCHA_KEY
    });
    modalRef.result
      .then(response => { })
      .catch(err => { });
  }

  get isMobile(): boolean {
    return this.platformService.isMobile || this.resizeService.currentWidth < 576;
  }

}
