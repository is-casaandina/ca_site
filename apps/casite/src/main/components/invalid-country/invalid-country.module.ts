import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { InvalidCountryComponent } from './invalid-country.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [InvalidCountryComponent],
  entryComponents: [InvalidCountryComponent],
  exports: [InvalidCountryComponent]
})
export class InvalidCountryModule { }
