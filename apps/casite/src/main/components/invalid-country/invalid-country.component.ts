import { Component, OnInit } from '@angular/core';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { IPageResponse } from '@ca-site/statemanagement/page.interface';

@Component({
  selector: 'casite-invalid-country',
  templateUrl: './invalid-country.component.html'
})
export class InvalidCountryComponent implements OnInit, IPayloadModalComponent {

  payload: IPageResponse;

  constructor(
    private activeModal: ActiveModal
  ) { }

  ngOnInit(): void {
  }

  home(): void {
    GlobalNavigateUtil.goTo('/');
    this.activeModal.close();
  }
}
