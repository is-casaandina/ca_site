import { Component, ElementRef, EventEmitter, HostBinding, Input, NgZone, OnChanges, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { StorageService, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { SearchBase } from '@ca-core/shared/helpers/util/search-base';
import { IMenuSite } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { PageEndPoints } from '@ca-site/providers/endpoints';
import { SearchService } from 'modules/search/src/providers/search.service';

@Component({
  selector: 'casite-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent extends SearchBase implements OnChanges {

  @HostBinding('class') class = 'booking-fade container';
  @Input() item: IMenuSite;
  @Input() languageCustom: string;

  @Output() activeBooking = new EventEmitter<boolean>(false);

  endpointHotel: string;
  constructor(
    public searchService: SearchService,
    public fb: FormBuilder,
    public elementRef: ElementRef,
    public ngZone: NgZone,
    public storageService: StorageService
  ) {
    super(
      searchService,
      fb,
      PageEndPoints.autocompleteDestination,
      '',
      elementRef,
      ngZone,
      storageService
    );
    this.lang = this.languageCustom;
    this.endpointHotel = PageEndPoints.autocompleteHotels;
    this.isBooking = true;
  }

  ngOnChanges(): void {
    this.lang = this.languageCustom;
  }

  afterOnInit(): void {
    this.registerForm.setValidators(ValidatorUtil.completeFieldValidator(this.autocomplete, this.autocompleteHotel));
  }

  toggle(): void {
    this.item.active = !this.item.active;
    this.activeBooking.emit(this.item.active);
  }
}
