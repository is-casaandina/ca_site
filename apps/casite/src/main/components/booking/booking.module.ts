import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { CaDatepickerModule, CaInputModule } from '@ca-core/ui/lib/components';
import { BookingComponent } from './booking.component';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    NotificationModule,
    ReactiveFormsModule,
    FormsModule,
    CaInputModule,
    CaDatepickerModule
  ],
  declarations: [BookingComponent],
  exports: [BookingComponent]
})
export class BookingModule { }
