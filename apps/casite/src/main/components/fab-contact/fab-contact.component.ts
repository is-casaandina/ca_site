import { Component, OnInit } from '@angular/core';
import { PageServices } from '@ca-site/providers/pages.service';

@Component({
  selector: 'casite-fab-contact',
  templateUrl: './fab-contact.component.html',
  styleUrls: ['./fab-contact.component.scss']
})
export class FabContactComponent implements OnInit {

  whatsappLink: string;

  constructor(
    private pageService: PageServices
  ) { }

  ngOnInit(): void {
    this.pageService.getConfigurationsSub()
      .subscribe(configuration => {
        this.whatsappLink = configuration && configuration.socialNetworking && configuration.socialNetworking.urlWhatsapp;
      });
  }

}
