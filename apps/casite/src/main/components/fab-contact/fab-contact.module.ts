import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FabContactComponent } from './fab-contact.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FabContactComponent],
  exports: [FabContactComponent]
})
export class FabContactModule { }
