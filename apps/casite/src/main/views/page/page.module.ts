import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { InvalidCountryModule } from '@ca-site/components/invalid-country/invalid-country.module';
import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';

@NgModule({
  declarations: [PageComponent],
  imports: [
    CommonModule,
    PageRoutingModule,
    InvalidCountryModule,
    ModalModule
  ]
})
export class PageModule { }
