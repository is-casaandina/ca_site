import { isPlatformBrowser } from '@angular/common';
import { Component, ElementRef, Inject, Input, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeAnimationWithoutDisplay } from '@ca-core/shared/helpers/animations/fade.animation';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { ModalRef } from '@ca-core/shared/helpers/modal/models/modal-ref';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { StorageService, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { PreviewService } from '@ca-core/shared/helpers/util/preview.service';
import { GlobalNavigateUtil } from '@ca-core/shared/helpers/util/route';
import { addElementToRef, addScriptTag, formatPageInfo } from '@ca-core/shared/helpers/util/web-components';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { InvalidCountryComponent } from '@ca-site/components/invalid-country/invalid-country.component';
import { MAIN_ENV } from '@ca-site/environments/environment';
import { PageServices } from '@ca-site/providers/pages.service';
import { ScriptService } from '@ca-site/providers/script.service';
import { SEOService } from '@ca-site/providers/seo.service';
import { IPageContentLanguage, IPageResponse } from '@ca-site/statemanagement/page.interface';
import { NotificationModalComponent } from '@ca/library';
import { IPageInfo } from 'core/typings';
import { takeUntil } from 'rxjs/operators';
import { FileEndpoint } from '@ca-admin/endpoints';

@Component({
  selector: 'casite-page',
  templateUrl: './page.component.html',
  animations: [fadeAnimationWithoutDisplay()]
})
export class PageComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('view') viewPage: ElementRef<HTMLElement>;

  @Input() title: string;
  private language: string;
  private path: string;
  private modalRef: ModalRef;
  private response: IPageResponse;

  html: string;
  isPreview: boolean;
  page_editor: any;
  id_page:any;
  ruta_s3 =  null;
  ruta_s3_complete= null;

  constructor(
    private pagesServices: PageServices,
    private modalService: ModalService,
    private storageService: StorageService,
    private seoService: SEOService,
    private scriptService: ScriptService,
    private previewService: PreviewService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    private spinnerService: SpinnerService,
    private elementRef: ElementRef,
    @Inject(PLATFORM_ID) private platformId
  ) {
    super();
  }

  ngOnInit(): void {
    this.isPreview = this.router.url.startsWith('/preview');
    this.language = this.activedRoute.snapshot.data[0].language;
    this.path = this.activedRoute.snapshot.data[0].locationPath;

    this.getConfigurations();
    this.currencyChange();
    this.languageChange();

    if (!this.isPreview) {
      this.getPage();
    } else {
      const page = this.previewService.getPreview<IPageResponse>();
      this.addPage(page);
    }

    //setTimeout(() => {
      //this.cargandoJS();
    //}, 3000);

  }

  private currencyChange(): void {
    this.pagesServices.currencySub.pipe(takeUntil(this.unsubscribeDestroy$)).subscribe(currency => {
      if (this.response && currency) {
        // this.spinnerService.showSpinner();
        this.html = null;
        this.viewPage.nativeElement.innerHTML = '';
        setTimeout(() => {
          this.addPage(this.response);
        }, 100);
      }
    });
  }

  private languageChange(): void {
    this.pagesServices.languageSub.pipe(takeUntil(this.unsubscribeDestroy$)).subscribe(language => {
      if (this.response && language && language !== this.language) {
        this.language = language;
        const newPath = this.response.paths.find(item => item.language === language);
        if (!this.isPreview) {
          GlobalNavigateUtil.goTo(newPath.path);
        } else {
          this.getPageByIdAndLanguage();
        }
      }
    });
  }

  private getPage(): void {
    // this.spinnerService.showSpinner();
    if (this.path !== 'null') {
      this.pagesServices
        .getPages({ params: { path: this.path, language: this.language } })
        .subscribe(response => this.addPage(response), err => this.getPageError(err));
    } else {
      this.getPageError({ status: 404 });
    }
  }

  private getPageByIdAndLanguage(): void {
    // this.spinnerService.showSpinner();
    this.pagesServices
      .getPages({ params: { id: this.response.id, language: this.language } })
      .subscribe(response => this.addPage(response), err => this.getPageError(err));
  }

  private getPageError(err: any): void {
    this.pagesServices
      .getPageError(err.status, this.language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        response => {
          if (response && response.languages) {
            this.html = null;
            this.viewPage.nativeElement.innerHTML = '';
            setTimeout(() => this.addPage(response), 100);
          } else {
            this.hideSpinner();
          }
        },
        () => this.hideSpinner()
      );
  }

  private addPage(response: IPageResponse): string {
    this.response = response;
    const page: IPageContentLanguage = response.languages && response.languages[0];
    if (page) {
      const pageInfo: IPageInfo = formatPageInfo(response, page.language, true, page.data, page.title);
      this.storageService.setItem(STORAGE_KEY.pageInfo, pageInfo);

      this.addMeta(page);
      this.seoService.setLinksLang(response.paths);
      if (isPlatformBrowser(this.platformId)) {
        this.html = page.content.html;
        //console.log("hmtl pagina",this.html);
        if(this.html != ''){
          this.cargandoJS();
        }
        addElementToRef({
          html: page.content.html,
          position: 'beforeend',
          parent: this.viewPage.nativeElement
        });
        addScriptTag(page.content.refComponents, MAIN_ENV.BUCKET_WC);
        this.getModalsNotification(pageInfo);
        this.verifyValidCountry();
      }

      this.hideSpinner();

      return page.path;
    }
  }

  cargandoJS(){
    
    this.page_editor = JSON.parse(sessionStorage.getItem('pageInfo'));
    console.log("id de pagina", JSON.parse(sessionStorage.getItem('pageInfo')));
    this.id_page = this.page_editor.pageId;
    console.log("id de pagina 2",this.id_page);
    let id_page_js =  this.id_page+'.js';
    this.ruta_s3 = FileEndpoint.js_ruta;
    let n = /-/gi;
    let nombre_sin_guion = id_page_js.replace(n,"");
    this.ruta_s3_complete = this.ruta_s3+'/'+nombre_sin_guion;

    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src ='';
    //s.src = "https://s3.amazonaws.com/multimediadev.casa-andina.com/filejs/7cda7184a9804b8586120724c823dc70.js";
    var aleatorio =  Math.round(Math.random() * 1000);
    //console.log("aletorio", aleatorio);
    s.src = this.ruta_s3_complete+'?'+aleatorio;
    this.elementRef.nativeElement.appendChild(s);
  
}

  private verifyValidCountry(): void {
    if (this.response.invalidCountry) {
      const modal = this.modalService.open(InvalidCountryComponent, {
        title: 'Página inválida',
        hideClose: true
      });
      modal.setPayload(this.response);
      modal.result.then(() => {}).catch(() => {});
    }
  }

  private addMeta(page: IPageContentLanguage): void {
    const meta = page.metadata;
    meta.language = page.language;
    this.seoService.setData(page.metadata);
    this.seoService.setHtmlLang(page.language);
  }

  private getModalsNotification(pageInfo: IPageInfo): void {
    this.pagesServices.getModalNotifications(pageInfo).subscribe((notifications: Array<any>) => {
      notifications.forEach(notification => {
        const config = {
          title: notification.title,
          source: 'site'
        };
        this.modalRef = this.modalService.open(NotificationModalComponent, config);
        this.modalRef.setPayload(notification);
        this.modalRef.result.then(() => {}).catch(err => {});
      });
    });
  }

  private getConfigurations(): void {
    this.pagesServices.getConfigurations().subscribe(res => {
      this.setFavicon(res.icon);
      this.setIconFonts(res.fontIcons);
      this.scriptService.addScript(res.script);
      this.storageService.setItem(STORAGE_KEY.configurations, res);
    });
  }

  private setFavicon(href: string): void {
    const linkElement = document.createElement('link');
    linkElement.setAttribute('rel', 'icon');
    linkElement.setAttribute('type', 'image/png');
    linkElement.setAttribute('href', href);
    document.head.appendChild(linkElement);
  }

  private setIconFonts(href: string): void {
    const linkElement = document.createElement('link');
    linkElement.setAttribute('rel', 'stylesheet');
    linkElement.setAttribute('type', 'text/css');
    linkElement.setAttribute('href', href);
    document.head.appendChild(linkElement);
  }

  private hideSpinner(): void {
    setTimeout(() => {
      this.spinnerService.hideSpinner();
      this.spinnerService.hideSpinner();
    }, 600);
  }
}
