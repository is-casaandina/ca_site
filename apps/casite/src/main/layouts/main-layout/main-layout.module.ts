import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FabContactModule } from '@ca-site/components/fab-contact/fab-contact.module';
import { FooterModule } from '@ca-site/components/footer/footer.module';
import { HeaderModule } from '@ca-site/components/header/header.module';
import { MainLayoutComponent } from './main-layout.component';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    HeaderModule,
    FooterModule,
    FabContactModule,
    BrowserAnimationsModule
  ]
})
export class MainLayoutModule { }
