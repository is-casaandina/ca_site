import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router, RouterEvent } from '@angular/router';
import { MenuUtil } from '@ca-core/shared/helpers/util/menu';
import { LANGUAGE_TYPES_CODE } from '@ca-core/shared/settings/constants/general.constant';
import { IPromotionPath } from '@ca-core/shared/statemanagement/models/domain.interface';
import {
  IMenuSite,
  MENU_SITE_ACTION,
  MENU_SITE_TYPE
} from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { PageServices } from '@ca-site/providers/pages.service';
import { IDescriptor } from '@core/typings';

@Component({
  selector: 'casite-main-layout',
  templateUrl: './main-layout.component.html'
})
export class MainLayoutComponent implements OnInit {
  header: Array<IMenuSite>;
  footer: Array<IMenuSite>;
  language: LANGUAGE_TYPES_CODE;
  descriptorsIdColor: { [key: string]: string };

  showLayout: boolean;

  promotionPaths: Array<IPromotionPath>;

  constructor(
    private pageService: PageServices,
    private activedRoute: ActivatedRoute,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId
  ) {
    setTimeout(() => (this.showLayout = isPlatformBrowser(this.platformId)), 500);
  }

  ngOnInit(): void {
    this.language = this.activedRoute.snapshot.data[0].language;
    this.promotionPaths = this.activedRoute.snapshot.data[0].promotionPaths || [];

    this.getMenu();
    this.getdescriptors();
    this.languageChange();
    this.onRouterBack();
  }

  private languageChange(): void {
    this.pageService.languageSub.subscribe(language => {
      if (language && language !== this.language) {
        (window as any).language = language;
        this.language = language;
        this.getMenu();
      }
    });
  }

  private onRouterBack(): void {
    let isBack: boolean;

    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationStart && event.restoredState) {
        isBack = true;
      }

      if (event instanceof NavigationEnd) {
        if (isBack) {
          this.language = this.activedRoute.snapshot.data[0].language;
        }

        isBack = false;
      }
    });
  }

  private getMenu(): void {
    const promotion = this.promotionPaths.find(promo => promo.language === this.language);
    this.pageService.getMenu(this.language).subscribe(menus => {
      menus = menus || [];
      menus = (menus || []).map(menu => {
        if (menu.action === MENU_SITE_ACTION.PROMOTION_ALL) {
          menu.pagePath = !!promotion ? promotion.path : menu.pagePath;
          menu.external = !!promotion ? false : menu.external;
          menu.action = !!promotion ? MENU_SITE_ACTION.REDIRECT : null;
        }

        return menu;
      });
      this.header = MenuUtil.menuTree(menus.filter(menu => menu.type === MENU_SITE_TYPE.HEADER));
      this.footer = MenuUtil.menuTree(menus.filter(menu => menu.type === MENU_SITE_TYPE.FOOTER));
    });
  }

  private getdescriptors(): void {
    this.pageService.getDescriptors().subscribe(res => this.makeDescriptorObjectId(res));
  }

  private makeDescriptorObjectId(descriptors: Array<IDescriptor>): void {
    this.descriptorsIdColor = descriptors.reduce(
      (initVal, currentVal) => {
        initVal[currentVal.id] = currentVal.color;

        return initVal;
      },
      {} as any
    );
  }
}
