import * as express from 'express';
import dotenv from 'dotenv';
import compression from 'compression';

import { AdminRoutes } from './src/routes/AdminRoutes';
import { ErrorRoutes } from './src/routes/ErrorRoutes';
import { Logger } from './src/utils/Logger';
import { S3FilesRoutes } from './src/routes/S3FilesRoutes';
import { SeoRoutes } from './src/routes/SeoRoutes';
import { SiteRoutes } from './src/routes/SiteRoutes';
import { UniversalEngine } from './src/engine/UniversalEngine';

dotenv.config();

const app = express();
const PORT = process.env.PORT || 4000;
const ENV = process.env.ENV || 'dev';

app.use(compression());

Logger.init(app);
S3FilesRoutes.init(app, ENV);
UniversalEngine.init(app);
AdminRoutes.init(app);
SeoRoutes.init(app, ENV);
ErrorRoutes.init(app, ENV);
SiteRoutes.init(app);

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT} => mode: ${ENV}`);
});
