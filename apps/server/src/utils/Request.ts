export const allowedExt = ['.js', '.ico', '.css', '.png', '.jpg', '.jpeg', '.woff2', '.woff', '.ttf', '.svg', 'eot'];

export class Request {
  static isFile(req): boolean {
    return !!allowedExt.find(ext => req.url && req.url.match(ext));
  }

  static isBrowser(req): boolean {
    const userAgent = req.header('user-agent') || '';

    return userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i);
  }
}
