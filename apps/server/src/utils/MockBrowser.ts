import * as mockBrowser from 'mock-browser';
const mock = new mockBrowser.mocks.MockBrowser();

global['document'] = mock.getDocument();
global['navigator'] = mock.getNavigator();
global['window'] = mock.getWindow();
global['sessionStorage'] = mock.getSessionStorage();
global['location'] = mock.getLocation();
global['ErrorEvent'] = null;
