import * as express from 'express';
import { join } from 'path';
import { Request } from '../utils/Request';

const DIST_ADMIN_FOLDER = join(process.cwd(), 'dist/caadmin');

export class AdminRoutes {
  static init(app: any): void {
    app.use('/admin', express.static(DIST_ADMIN_FOLDER));
    app.get('/admin/**', this.renderIndexFn);
  }

  private static renderIndexFn(req, res): void {
    if (Request.isFile(req)) {
      res.sendFile(`${DIST_ADMIN_FOLDER}/${req.url}`);
    } else {
      res.sendFile(`${DIST_ADMIN_FOLDER}/index.html`);
    }
  }
}
