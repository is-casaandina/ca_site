import { IPageResponse } from '@ca-site/statemanagement/page.interface';

const downLoadFile = require('../../../../scripts/download-file');
const config = require('../../../../scripts/config/config.json');
const pathError = config.error.directory;
const pathS3 = config.s3.directory.publicSeo;

export class ErrorRoutes {

  static init(app: any, env: string): void {
    app.get('/error/:code/:language', (req, res) => {
      const code = req.params.code;
      const languageCode = req.params.language;

      downLoadFile.download_file(`${pathS3[env]}${pathError[env]}/${code}.json`)
        .then(response => {
          const tmp = response.toString();
          try {
            const page: IPageResponse = JSON.parse(tmp);
            page.paths = (page.languages || []).map(item => ({ language: item.language, path: item.path }));
            page.languages = (page.languages || []).filter(item => item.language === languageCode);
            page.id = (page as any)._id;

            delete (page as any)._id;

            res.json(page);
          } catch (error) {
            res.send(tmp);
          }
        })
        .catch(error => {
          res.send(error)
        })
    });
  }

}
