const downLoadFile = require('../../../../scripts/download-file');
const config = require('../../../../scripts/config/config.json');
const pathSeo = config.seo;
const pathS3 = config.s3.directory.publicSeo;

export class SeoRoutes {

  static init(app: any, env: string): void {
    Object.keys(pathSeo)
      .forEach(key => {
        const seoConfig = pathSeo[key];
        app.get(`/${key}`, (req, res) => {
          downLoadFile.download_file(`${pathS3[env]}${seoConfig.directory[env]}`)
            .then(response => {
              res.writeHead(200, {
                'Content-Type': seoConfig.contectType,
                'Content-Length': response.length
              });

              res.end(response, 'binary');
            })
            .catch(error => {
              res.send(error)
            })
        });
      });
  }
}
