import * as express from 'express';
import s3Proxy from 's3-proxy';

const config = require('../../../../scripts/config/config.json');

export class S3FilesRoutes {
  static configuration: any;

  static init(app: any, env: string): void {
    this.configuration = {
      bucket: config.s3.bucketName[env],
      region: config.s3.region,
      accessKeyId: config.s3.accessKeyId,
      secretAccessKey: config.s3.secretAccessKey,
      overrideCacheControl: 'max-age=31557600'
    };

    this.mailingRoute(app);
    this.staticRoute(app);
    this.webComponentRoute(app);
  }

  private static staticRoute(app: any): void {
    const mailing = express();
    const conf = { ...this.configuration, prefix: 'static' };
    mailing.get('/*', s3Proxy(conf));

    app.use('/static/', mailing);
  }

  private static mailingRoute(app: any): void {
    const statics = express();
    const conf = { ...this.configuration, prefix: 'static/mailing' };
    statics.get('/*', s3Proxy(conf));

    app.use('/mailing/', statics);
  }

  private static webComponentRoute(app: any): void {
    const webComponenrt = express();
    const conf = { ...this.configuration, prefix: 'system/web-components' };
    webComponenrt.get('/*', s3Proxy(conf));

    app.use('/web-components/', webComponenrt);
  }
}
