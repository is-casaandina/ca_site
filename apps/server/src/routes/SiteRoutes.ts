import { join } from 'path';
import { Request } from '../utils/Request';

const DIST_SITE_FOLDER = join(process.cwd(), 'dist/casite');

export class SiteRoutes {
  static init(app: any): void {
    // Serve static files from /casite
    // app.get('*', express.static(DIST_SITE_FOLDER));
    // All regular routes use the Universal engine
    app.get('*.*', this.staticFiles.bind(this));
    app.get('*', this.renderIndexFn.bind(this));
  }

  private static staticFiles(req, res): void {
    if (req.url.endsWith('/')) {
      req.url = req.url.slice(0, req.url.length - 1);
    }

    res.sendFile(`${DIST_SITE_FOLDER}/${req.url}`);
  }

  private static renderIndexFn(req, res): void {
    if (Request.isBrowser(req)) {
      res.sendFile(`${DIST_SITE_FOLDER}/index.html`);
    } else {
      res.render('index', { req, res }, (error, html) => res.send(html));
    }
  }
}
