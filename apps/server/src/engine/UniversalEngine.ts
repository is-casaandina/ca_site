import 'zone.js/dist/zone-node';
import '../utils/MockBrowser';

import { join } from 'path';
import { enableProdMode } from '@angular/core';
// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
// * NOTE :: leave this as require() since this file is built Dynamically from webpack
import { AppServerModuleNgFactory, LAZY_MODULE_MAP } from './../../../../dist/server/main';

export class UniversalEngine {
  static DIST_SITE_FOLDER = join(process.cwd(), 'dist/casite');

  static init(app: any): void {
    // Faster server renders w/ Prod mode (dev mode never needed)
    enableProdMode();
    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    app.engine(
      'html',
      ngExpressEngine({
        bootstrap: AppServerModuleNgFactory,
        providers: [provideModuleMap(LAZY_MODULE_MAP)]
      })
    );

    app.set('view engine', 'html');
    app.set('views', this.DIST_SITE_FOLDER);
  }
}
