import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { CreatePublicationGuard } from '@ca-admin/guards/create-publication.guard';
import { AuthGuard, PermissionGuard } from '@ca-admin/guards/public-api';
import { EditorLayoutComponent } from '@ca-admin/layouts/editor-layout/editor-layout.component';
import { LoginLayoutComponent } from '@ca-admin/layouts/login-layout/login-layout.component';
import { MainLayoutComponent } from '@ca-admin/layouts/main-layout/main-layout.component';
import { MENU_CODE } from '@ca-admin/settings/constants/menu.constant';
import { UserPermissionsResolve } from '../providers/resolve';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'view-editor',
    loadChildren: '../main/views/view-editor/view-editor.module#ViewEditorModule'
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        loadChildren: '../main/views/login/login.module#LoginModule'
      },
      {
        path: 'recovery',
        loadChildren: '../main/views/recovery/recovery.module#RecoveryModule'
      },
      {
        path: 'recovery-password',
        loadChildren: '../main/views/reset-password/reset-password.module#ResetPasswordModule'
      },
      {
        path: 'passwordchange',
        canActivate: [AuthGuard],
        loadChildren: '../main/views/password-change/password-change.module#PasswordChangeModule'
      }
    ]
  },
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    resolve: {
      userPermissions: UserPermissionsResolve
    },
    children: [
      {
        path: 'users-roles',
        loadChildren: '../main/views/users-roles/users-roles.module#UsersRolesModule'
      },
      {
        path: 'home',
        data: {
          menuCode: MENU_CODE.home
        },
        loadChildren: '../main/views/home/home.module#HomeModule'
      },
      {
        path: 'menu-tray',
        data: {
          menuCode: MENU_CODE.menu
        },
        loadChildren: '../main/views/menu/menu.module#MenuModule'
      },
      {
        path: 'configurations',
        data: {
          menuCode: MENU_CODE.configurations
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/configurations/configurations.module#ConfigurationsModule'
      },
      {
        path: 'page-tray',
        data: {
          menuCode: MENU_CODE.pages
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'page-error-tray',
        data: {
          menuCode: MENU_CODE.pagesError
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'subscribers-tray',
        data: {
          menuCode: MENU_CODE.subscribers
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/subscribers-tray/subscribers-tray.module#SubscribersTrayModule'
      },
      {
        path: 'promotions-tray',
        data: {
          menuCode: MENU_CODE.promotions
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'hotels-tray',
        data: {
          menuCode: MENU_CODE.hotels
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'destinations-tray',
        data: {
          menuCode: MENU_CODE.destinations
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'restaurants-tray',
        data: {
          menuCode: MENU_CODE.restaurants
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'events-tray',
        data: {
          menuCode: MENU_CODE.events
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/page-tray/page-tray.module#PageTrayModule'
      },
      {
        path: 'multimedia',
        data: {
          menuCode: MENU_CODE.multimedia
        },
        canActivate: [PermissionGuard],
        loadChildren: '../main/views/multimedia/multimedia.module#MultimediaModule'
      },
      {
        path: 'profile',
        loadChildren: '../main/views/profile/profile.module#ProfileModule'
      }
    ]
  },
  {
    path: 'editor',
    component: EditorLayoutComponent,
    resolve: {
      userPermissions: UserPermissionsResolve
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'create',
        canActivate: [CreatePublicationGuard],
        children: [
          {
            path: 'publication',
            loadChildren: '../main/views/editor/create-publication/create-publication.module#CreatePublicationModule'
          }
        ]
      },
      {
        path: 'edit',
        children: [
          {
            path: 'publication/:idPage',
            loadChildren: '../main/views/editor/publication/publication.module#PublicationModule'
          }
        ]
      },
      {
        path: 'see',
        children: [
          {
            path: 'publication/:idPage',
            data: {
              seePublication: true
            },
            loadChildren: '../main/views/editor/publication/publication.module#PublicationModule'
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always',
      scrollPositionRestoration: 'enabled'
    })
  ],
  exports: [
    RouterModule
  ]
})
export class RoutesModule { }
