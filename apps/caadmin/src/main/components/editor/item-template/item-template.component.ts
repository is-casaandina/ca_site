import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TemplatePreviewComponent } from '@ca-admin/components/modals/template-preview/template-preview.component';
import { PageService, TemplateService } from '@ca-admin/services';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ITemplateResponse } from '@ca-admin/statemanagement/models/template.interface';
import { IModalConfig, MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-item-template',
  templateUrl: './item-template.component.html'
})
export class ItemTemplateComponent extends UnsubscribeOnDestroy implements OnInit {

  @Input() template: ITemplateResponse;

  GENERAL_LANG = GeneralLang;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private pageService: PageService,
    private templateService: TemplateService
  ) {
    super();
  }

  ngOnInit(): void { }

  selectTemplate(template: ITemplateResponse): void {
    this.templateService.byCode(template.code)
      .pipe(
        switchMap(res => this.pageService.createPage(res, true)),
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(response => {
        const queryParams = { state: response.data.state };
        this.pageService.setPageEditor(null);
        this.pageService.setPageStorage(null);
        this.router.navigate(['editor/edit/publication', response.data.id], { queryParams });
      });
  }

  templatePreview(template: ITemplateResponse): void {
    const modalConfig = {
      titleText: template.name,
      size: MODAL_BREAKPOINT.lg,
      classBody: 'px-5 py-3'
    } as IModalConfig;
    const modal = this.modalService.open(TemplatePreviewComponent, modalConfig);
    modal.setPayload(template);
    modal.result
      .then(() => {
        this.selectTemplate(template);
      }, () => {
        return false;
      });
  }

}
