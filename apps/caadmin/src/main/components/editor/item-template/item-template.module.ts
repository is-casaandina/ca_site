import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TemplatePreviewModule } from '@ca-admin/components/modals/template-preview/template-preview.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule } from '@ca-core/ui/lib/components';
import { ItemTemplateComponent } from './item-template.component';

@NgModule({
  declarations: [ItemTemplateComponent],
  imports: [
    CommonModule,
    CaButtonModule,
    ModalModule,
    TemplatePreviewModule
  ],
  exports: [ItemTemplateComponent]
})
export class ItemTemplateModule { }
