import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EditorContactModule } from '@ca-admin/components/modals/editor-contact/editor-contact.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaCheckboxModule, CaDatepickerModule, CaInputModule } from '@ca-core/ui/lib/components';
import { EditorFormHeaderComponent } from './editor-form-header.component';

@NgModule({
  imports: [
    CommonModule,
    CaInputModule,
    CaButtonModule,
    ReactiveFormsModule,
    EditorContactModule,
    ModalModule,
    CaDatepickerModule,
    CaCheckboxModule
  ],
  declarations: [EditorFormHeaderComponent],
  exports: [EditorFormHeaderComponent]
})
export class EditorFormHeaderModule { }
