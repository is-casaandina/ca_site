import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { environment } from '@ca-admin/environments/environment';
import { PageService } from '@ca-admin/services';
import {
  EDITOR_FORM,
  EDITOR_PAGE_MODAL_DEFINITION,
  EDITOR_PAGE_PROPERTY,
  EDITOR_PAGE_PROPERTY_DEFINITION,
  EDITOR_PAGE_PROPERTY_TYPE
} from '@ca-admin/settings/constants/editor.constant';
import { CATEGORY, LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { ApiService } from '@ca-core/shared/helpers/api';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { DateUtil } from '@ca-core/shared/helpers/util/date';

@Component({
  selector: 'caadmin-editor-form-header',
  templateUrl: './editor-form-header.component.html'
})
export class EditorFormHeaderComponent implements OnInit {

  @Input() category: CATEGORY;
  @Output() changeData: EventEmitter<any> = new EventEmitter<any>();

  private data: any = {};

  categoryId: FormControl;
  properties: Array<EDITOR_PAGE_PROPERTY>;
  reloaders: Array<EDITOR_PAGE_PROPERTY>;
  changeKeys: Array<{ from: EDITOR_PAGE_PROPERTY, to: EDITOR_PAGE_PROPERTY }>;

  modals: typeof EDITOR_PAGE_MODAL_DEFINITION;

  type = EDITOR_PAGE_PROPERTY_TYPE;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private notificationService: NotificationService,
    private pageService: PageService,
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
    this.setFormStructure();
  }

  setFormStructure(): void {
    this.categoryId = this.fb.control(this.category);
    const page = this.pageService.getPageEditor();

    const data = EDITOR_FORM.find(form => form.category === this.category);
    if (data) {
      this.properties = data.properties || [];
      this.reloaders = data.reloaders || [];
      this.changeKeys = data.changeKey || [];

      const modals = data.modals || [];
      this.modals = modals.map(modal => EDITOR_PAGE_MODAL_DEFINITION.find(item => item.modal === modal));

      this.properties.forEach(property => {
        const tmpProp = this.getPropertyKey(property);

        const tmp: string = page[tmpProp];
        const value = this.getType(property) === this.type.DATETIME && tmp ? DateUtil.setTimezoneDate(tmp) : tmp;
        this[property] = this.fb.control(value || null);
      });
      this.properties
        .filter(property => this.getEndpoint(property))
        .forEach(property => {
          this.setDataList(property);
        });

      this.onChanges();
    }
  }

  private setDataList(property: EDITOR_PAGE_PROPERTY): void {
    const data = EDITOR_PAGE_PROPERTY_DEFINITION.find(item => item.property === property);
    const endpoint = `${environment.API_URL}${data.endpoint}`;
    const params = {
      language: LANGUAGE_TYPES_CODE.es
    };

    if (data.dependence) {
      const controlDependence = this.getControl(data.dependence);
      const control = this.getControl(property);

      if (controlDependence.value) {
        params[data.dependence] = controlDependence.value;
        this.getDataEndpoint(endpoint, params, property, false);
      } else {
        control.disable();
      }

      controlDependence.valueChanges
        .subscribe(() => {
          const value = controlDependence.value;
          params[data.dependence] = value;
          if (!value) {
            control.disable();
          } else {
            control.enable();
          }
          this.getDataEndpoint(endpoint, params, property);
        });
    } else {
      this.getDataEndpoint(endpoint, params, property, false);
    }
  }

  private getDataEndpoint(endpoint: string, params: any, property: EDITOR_PAGE_PROPERTY, reset: boolean = true): void {
    if (reset) {
      this.getControl(property)
        .setValue(null);
    }
    this.apiService.get<IObservableArray<any>>(endpoint, { params })
      .subscribe(res => {
        this[`list${property}`] = res || [];
      });
  }

  private onChanges(): void {
    this.properties.forEach(property => {
      const control = this.getControl(property);
      control.valueChanges
        .subscribe(() => {
          const isReloader = this.reloaders.find(reloader => reloader === property);
          if (isReloader && control.value !== this.data[property]) {
            this.pageService.reloadPublicationEdit()
              .next();
          }

          property = this.getPropertyKey(property);

          this.data[property] = control.value;
          this.changeData.next(this.data);
        });
    });
  }

  getControl(property: EDITOR_PAGE_PROPERTY): FormControl {
    return this[property];
  }

  getLabel(property: EDITOR_PAGE_PROPERTY): string {
    const data = EDITOR_PAGE_PROPERTY_DEFINITION.find(item => item.property === property);

    return data && data.label || '';
  }

  getPlaceholder(property: EDITOR_PAGE_PROPERTY): string {
    const data = EDITOR_PAGE_PROPERTY_DEFINITION.find(item => item.property === property);

    return data && data.placeholder || '';
  }

  getType(property: EDITOR_PAGE_PROPERTY): EDITOR_PAGE_PROPERTY_TYPE | string {
    const data = EDITOR_PAGE_PROPERTY_DEFINITION.find(item => item.property === property);

    return data && data.type || '';
  }

  getEndpoint(property: EDITOR_PAGE_PROPERTY): EDITOR_PAGE_PROPERTY_TYPE | string {
    const data = EDITOR_PAGE_PROPERTY_DEFINITION.find(item => item.property === property);

    return data && data.endpoint || '';
  }

  isRequired(property: EDITOR_PAGE_PROPERTY): boolean {
    const data = EDITOR_PAGE_PROPERTY_DEFINITION.find(item => item.property === property);

    return data && data.required;
  }

  getList(property: EDITOR_PAGE_PROPERTY): Array<any> {
    return this[`list${property}`] || [];
  }

  isValid(): any {
    let propertyMiss: EDITOR_PAGE_PROPERTY;

    (this.properties || []).forEach(property => {
      const value = this.getControl(property).value;
      if (this.isRequired(property) && !propertyMiss && !value) {
        propertyMiss = property;
      }
    });

    if (propertyMiss) {
      this.notificationService.addWarning(`El campo ${this.getLabel(propertyMiss)} es requerido.`);
    }

    return !propertyMiss;
  }

  openModal(modal: typeof EDITOR_PAGE_MODAL_DEFINITION[0]): void {
    this.modalService.open(modal.component, {
      title: modal.title,
      size: MODAL_BREAKPOINT.lg
    })
      .result
      .then(() => { })
      .catch(() => { });
  }

  private getPropertyKey(property: EDITOR_PAGE_PROPERTY, reverse?: boolean): EDITOR_PAGE_PROPERTY {
    const prop = this.changeKeys.find(item => reverse ? item.to === property : item.from === property);

    return prop ? reverse ? prop.from : prop.to : property;
  }

}
