import { Location } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { GeneralService, PageService } from '@ca-admin/services';
import { PAGE_CODE_STATES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { PreviewService } from '@ca-core/shared/helpers/util/preview.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'caadmin-editor-header',
  templateUrl: './editor-header.component.html'
})
export class EditorHeaderComponent implements OnInit {
  @ViewChild('swalCancel') private swalCancel: SwalComponent;

  @Input() pageState: number;
  @Input() seePublication: boolean;

  @Output() save: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() publish: EventEmitter<any> = new EventEmitter<any>();
  @Output() update: EventEmitter<any> = new EventEmitter<any>();

  swalCancelOptions: SweetAlertOptions;
  GENERAL_LANG = GeneralLang;

  constructor(
    private location: Location,
    private pageService: PageService,
    private previewService: PreviewService,
    private swalConfirmService: SwalConfirmService,
    private generalService: GeneralService
  ) { }

  PAGE_CODE_STATES = PAGE_CODE_STATES;

  ngOnInit(): void {
    this.swalCancelOptions = this.swalConfirmService.warning(this.GENERAL_LANG.modalConfirm.titleCancelChange);
  }

  back(): void {
    this.location.back();
  }

  saveFn(): void {
    this.save.next();
  }

  cancelAlert(): void {
    this.swalCancel.show();
  }

  cancelChanges(): void {
    this.cancel.next();
  }

  publishChanges(): void {
    if (this.isPublish) {
      this.update.next();
    } else {
      this.publish.next();
    }
  }

  isPublish(): boolean {
    const page = this.pageService.getPageStorage();

    return page && (page.state === PAGE_CODE_STATES.published || page.publish);
  }

  openPreview(): void {
    const config = this.generalService.getConfigurationStorage();
    const page = this.pageService.getPageEditor();
    this.previewService.openPreview(page, config.domain);
  }

}
