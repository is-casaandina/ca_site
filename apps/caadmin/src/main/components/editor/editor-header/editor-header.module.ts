import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { EditorHeaderComponent } from './editor-header.component';

@NgModule({
  imports: [
    CommonModule,
    SweetAlert2Module
  ],
  declarations: [EditorHeaderComponent],
  exports: [EditorHeaderComponent]
})
export class EditorHeaderModule { }
