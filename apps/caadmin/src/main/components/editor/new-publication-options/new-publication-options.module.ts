import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '@ca-admin/components/dynamic-form';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaLinkModule } from '@ca-core/ui/lib/components';
import { ComponentEditComponent } from './components/component-edit/component-edit.component';
import { NewPublicationPopupComponent } from './components/new-publication-popup/new-publication-popup.component';
import { EditTemplateDirective } from './directives/edit-template.directive';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    DynamicFormModule,
    CaButtonModule,
    CaLinkModule,
    ReactiveFormsModule
  ],
  declarations: [
    EditTemplateDirective,
    NewPublicationPopupComponent,
    ComponentEditComponent
  ],
  entryComponents: [
    NewPublicationPopupComponent,
    ComponentEditComponent
  ],
  exports: [
    EditTemplateDirective
  ]
})
export class NewPublicationOptionsModule { }
