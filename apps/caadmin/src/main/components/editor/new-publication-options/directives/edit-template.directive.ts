import {
  AfterViewInit,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  Renderer2,
  ViewContainerRef
} from '@angular/core';
import { ComponentService } from '@ca-admin/services/component.service';
import { IComponentResponse } from '@ca-admin/statemanagement/models/component.interface';
import { IProperty } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { IContent } from '@ca-admin/statemanagement/models/page.interface';
import { AngularUtil } from '@ca-core/shared/helpers/util';
import { camelcaseKeys } from '@ca-core/shared/helpers/util/object-to-calmelcase';
import { addScriptTag } from '@ca-core/shared/helpers/util/web-components';
import * as _ from 'underscore';
import { NewPublicationPopupComponent } from '../components/new-publication-popup/new-publication-popup.component';

// INFO: Activa la funcionalidad para agregar multiples componentes en una sola sección
const MULTI_COMPONENTS = false;

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[editTemplate]'
})
export class EditTemplateDirective implements AfterViewInit {
  @Output() edit: EventEmitter<IContent> = new EventEmitter<IContent>();
  @Input() pageId: string;
  @Input() language: string;
  @Input() hideEdit: boolean;
  @Input() hideDelete: boolean;
  @Input() emptyElement: HTMLDivElement;
  @Input() refComponents: Array<string>;

  private key = 'keyc';
  private empty = 'empty';
  private datac = 'datac';

  private popupRefs: Array<{ ts: NewPublicationPopupComponent; html: HTMLDivElement }> = [];

  constructor(
    private elementRef: ElementRef<HTMLDivElement>,
    private container: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    private renderer: Renderer2,
    private componentService: ComponentService
  ) {}

  ngAfterViewInit(): void {
    const elements = this.elementRef.nativeElement.querySelectorAll(`[${this.key}]`);
    const emptyElements = this.elementRef.nativeElement.querySelectorAll(`[${this.empty}]`);

    elements.forEach(element => {
      const componentKey = element.getAttribute(this.key) || this.getTimeStamp();
      element.setAttribute(this.key, componentKey);
      const newElement = this.addOptions(element, componentKey);
      element.replaceWith(newElement);
    });
    emptyElements.forEach(element => {
      const emptyElement = this.emptyElement.cloneNode(true) as HTMLDivElement;
      element.replaceWith(emptyElement);
      this.onDropNewComponent(emptyElement);
    });

    this.formAndEmitEdit();
  }

  private addOptions(element: Element, componentKey?: string): HTMLDivElement {
    const cloneElementBase = element.cloneNode(true);
    const factory = this.resolver.resolveComponentFactory<NewPublicationPopupComponent>(NewPublicationPopupComponent);

    const referentPopUp = this.container.createComponent(factory);
    const referentPopUpHtml = referentPopUp.location.nativeElement as HTMLDivElement;
    const referentPopUpTs = referentPopUp.instance;

    referentPopUpTs.pageId = this.pageId;
    referentPopUpTs.language = this.language;
    referentPopUpTs.hideEdit = this.hideEdit || false;
    referentPopUpTs.componentCode = (cloneElementBase as HTMLElement).localName;
    referentPopUpTs.componentKey = componentKey;

    this.popupRefs.push({ ts: referentPopUpTs, html: referentPopUpHtml });

    referentPopUpHtml.getElementsByClassName('ng-content')[0].replaceWith(cloneElementBase);

    this.renderer.addClass(referentPopUpHtml, 'g-component-wrapper');

    this.onHover(referentPopUpHtml, referentPopUpTs);
    this.onEdit(referentPopUpHtml, referentPopUpTs);
    if (MULTI_COMPONENTS) {
      this.onDropAppendComponent(referentPopUpHtml);
    }

    return referentPopUpHtml;
  }

  private onHover(referentPopUpHtml: HTMLDivElement, referentPopUpTs: NewPublicationPopupComponent): void {
    referentPopUpHtml.onmouseover = () => {
      this.completeStructure();
    };
  }

  private onEdit(referentPopUpHtml: HTMLDivElement, referentPopUpTs: NewPublicationPopupComponent): void {
    referentPopUpTs.onEdit.subscribe(htmlString => {
      const div = this.getDivWithComponent(htmlString, referentPopUpTs.componentKey);
      const originElem = referentPopUpHtml.firstElementChild;

      const isMaster = originElem.getAttribute('master') !== null;
      const isData = originElem.getAttribute(this.datac) !== null;

      if (isMaster) {
        div.setAttribute('master', '');
      }
      if (isData) {
        div.setAttribute(this.datac, '');
      }

      originElem.replaceWith(div);
      this.formAndEmitEdit();
    });
  }

  private onDropNewComponent(emptyElement: HTMLDivElement): void {
    this.onDragOver(emptyElement);
    emptyElement.ondrop = ev => {
      ev.preventDefault();
      const component: IComponentResponse = JSON.parse(ev.dataTransfer.getData('text') || null);
      if (component) {
        const currentTarget = ev.currentTarget as HTMLDivElement;
        const componentElement = this.getDivWithComponent(component.template);
        const newTarget = this.addOptions(componentElement, this.getTimeStamp());
        currentTarget.replaceWith(newTarget);

        this.addNewWebComponent(component.ref);
      }
    };
  }

  private onDropAppendComponent(referentPopUpHtml: HTMLDivElement): void {
    referentPopUpHtml.ondragover = ev => ev.preventDefault();
    referentPopUpHtml.ondrop = ev => {
      ev.preventDefault();
      const component: IComponentResponse = JSON.parse(ev.dataTransfer.getData('text') || null);
      if (component) {
        const currentTarget = ev.currentTarget as HTMLDivElement;
        const componentElement = this.getDivWithComponent(component.template);
        const newElement = this.addOptions(componentElement, null);
        const parentElement = currentTarget.parentElement;

        const nodes = Array.from(parentElement.childNodes);
        const index = nodes.indexOf(currentTarget);

        parentElement.insertBefore(newElement, nodes[index + 1]);
        this.addNewWebComponent(component.ref);
      }
    };
  }

  private addNewWebComponent(ref: string): void {
    addScriptTag([ref]);
    this.addRefComponent(ref);
    this.formAndEmitEdit();
  }

  private onDragOver(element: HTMLDivElement): void {
    element.ondragover = ev => {
      ev.preventDefault();
      this.renderer.addClass(element, 'g-component-wrapper-drag');
    };
    element.ondragleave = ev => {
      ev.preventDefault();
      this.renderer.addClass(element, 'g-component-wrapper-drag');
    };
  }

  private getDivWithComponent(template: string, componentKey?: string): Element {
    let div: Element = document.createElement('div');
    div.innerHTML = template;
    div = div.firstElementChild;
    div.setAttribute(this.key, componentKey || this.getTimeStamp());

    return div;
  }

  private formAndEmitEdit(): void {
    const allElements = this.elementRef.nativeElement.cloneNode(true) as HTMLDivElement;
    const elements = allElements.querySelectorAll(`[${this.key}]`);
    const empties = allElements.querySelectorAll(`[${this.empty}]`);
    const elementsData = allElements.querySelectorAll(`[${this.datac}]`);
    const data = {};
    elements.forEach(element => {
      const parent = element.parentElement;
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }

      element
        .getAttributeNames()
        .filter(name => name.startsWith('_nghost') || name.startsWith('ng-version') || name.startsWith('class'))
        .forEach(attr => element.removeAttribute(attr));

      parent.replaceWith(element);
    });

    elementsData.forEach(element => {
      data[element.localName] = {};
      element
        .getAttributeNames()
        .filter(
          attr =>
            !attr.startsWith('_nghost') &&
            !attr.startsWith('ng-version') &&
            !attr.startsWith('class') &&
            !attr.startsWith(this.datac) &&
            !attr.startsWith(this.key)
        )
        .forEach(attr => {
          const value = element.getAttribute(attr);
          data[element.localName][attr] = AngularUtil.toJson(value);
        });
    });

    empties.forEach(element => {
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }
      const attrs = element
        .getAttributeNames()
        .filter(name => name.startsWith('_ngcontent') || name.startsWith('class'));
      attrs.forEach(attr => element.removeAttribute(attr));
    });

    this.edit.next({
      html: allElements.innerHTML,
      refComponents: this.refComponents,
      data: camelcaseKeys(data)
    });
  }

  private addRefComponent(currentRef: string): void {
    const index = this.refComponents.indexOf(currentRef);
    if (index < 0) {
      this.refComponents.push(currentRef);
    }
  }

  private completeStructure(): void {
    this.popupRefs.forEach(popup => {
      const component = this.getComponent(popup.ts.componentCode, popup.html.firstElementChild);
      popup.ts.component = component;
    });
  }

  private getComponent(componentCode: string, element: Element): IComponentResponse {
    const structure = _.clone(this.getStructure(componentCode));
    const data = this.getValues(element);
    if (structure && structure.properties) {
      structure.properties.map(property => (property.code ? this.getValueFromProperty(data, property) : property));

      return structure;
    }
  }

  private getValueFromProperty(data: any, property: IProperty): IProperty {
    const value = data[property.code];
    // tslint:disable-next-line:prefer-conditional-expression
    if (
      (property.type === 'array' ||
        property.type === 'image' ||
        property.type === 'video' ||
        property.type === 'url' ||
        property.type === 'table') &&
      value
    ) {
      property.value = AngularUtil.toJson(value, null);
    } else {
      property.value = value;
    }

    return property;
  }

  private getStructure(componentCode: string): IComponentResponse {
    return this.componentService.getComponentsEditor().find(component => component.code === componentCode);
  }

  private getValues(element: Element): any {
    const data = {};
    const keys = Object.keys(element.attributes);
    keys.forEach(key => {
      const attribute = element.attributes[key];
      if (attribute.value) {
        data[attribute.localName] = attribute.value;
      }
    });

    return data;
  }

  private getTimeStamp(): string {
    const date = new Date();

    return (date.getTime() + Math.floor(Math.random() * 100 + 1)).toString();
  }
}
