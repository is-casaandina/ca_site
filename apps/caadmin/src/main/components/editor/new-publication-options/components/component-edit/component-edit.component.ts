import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicFormComponent } from '@ca-admin/components/dynamic-form';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IComponentResponse } from '@ca-admin/statemanagement/models/component.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { IPayloadModalComponent } from '@ca-core/shared/helpers/modal/models/modal.interface';
import { ComponentService } from '../../../../../../providers/services/component.service';
import { FileService } from '../../../../../../providers/services/file.service';

@Component({
  selector: 'caadmin-component-edit',
  templateUrl: './component-edit.component.html'
})
export class ComponentEditComponent implements OnInit, IPayloadModalComponent {

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  GENERAL_LANG = GeneralLang;

  payload: IComponentResponse;

  constructor(
    private activeModal: ActiveModal,
    private componentService: ComponentService,
    private fileService: FileService,
  ) { }

  ngOnInit(): void {
  }

  close(confirm?: boolean): void {
    if (confirm) {
      this.dynamicForm.formatForm();
      if(this.componentService.file != undefined || this.componentService.name != undefined){
        console.log("file", this.componentService.file[0])
        this.fileService.uploadJs(this.componentService.file[0],this.componentService.name)
        .subscribe(
        event => {
          console.log("envio js",event)
        },
        err => {
          console.log("error envio js", err);
        });
      }
    } else {
      this.activeModal.close();
    }
  }

  formSubmitted(data: Array<IComponentResponse>): void {
    this.activeModal.close(data[0].template);
  }

}
