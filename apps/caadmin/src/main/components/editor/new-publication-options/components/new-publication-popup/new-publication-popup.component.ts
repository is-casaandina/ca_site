import { Component, ElementRef, EventEmitter, OnInit, Renderer2, ViewChild } from '@angular/core';
// import { TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { IComponentResponse } from '@ca-admin/statemanagement/models/component.interface';
import { IModalConfig, MODAL_BREAKPOINT, ModalRef, ModalService } from '@ca-core/shared/helpers/modal';
import { ComponentEditComponent } from '../component-edit/component-edit.component';

@Component({
  selector: 'caadmin-new-publication-popup',
  templateUrl: './new-publication-popup.component.html'
})
export class NewPublicationPopupComponent implements OnInit {

  @ViewChild('btnEdit') btnEdit: ElementRef<HTMLDivElement>;

  onEdit: EventEmitter<string> = new EventEmitter<string>();
  /** pageId & language necessary for comments */
  pageId: string;
  language: string;
  /** Necessary for seePublication */
  hideEdit: boolean;
  hideView = true;
  // Example: ca-admin
  componentCode: string;
  // Referencia al componente en el template, para uso de comentarios
  componentKey: string;

  private modalConfig: IModalConfig;
  private modalRef: ModalRef;

  private _component: IComponentResponse;
  set component(component: IComponentResponse) {
    // const typesCode = [TYPES_CODE.static, TYPES_CODE.common];
    // if (this.btnEdit && component && (typesCode.includes(component.type) || this.hideEdit)) {
    //   this.renderer2.removeChild(this.elementRef.nativeElement, this.btnEdit.nativeElement);
    // }
    const hasProperties = component && component.properties && component.properties.length;
    if (this.btnEdit && (!hasProperties || this.hideEdit)) {
      this.renderer2.removeChild(this.elementRef.nativeElement, this.btnEdit.nativeElement);
    }
    this._component = component;
  }
  get component(): IComponentResponse {
    return this._component;
  }

  constructor(
    private elementRef: ElementRef,
    private modalService: ModalService,
    private renderer2: Renderer2
  ) { }

  ngOnInit(): void { }

  openModalEdit(): void {
    this.modalConfig = {
      size: MODAL_BREAKPOINT.lg
    };
    this.modalConfig.title = this.component.name;
    this.modalRef = this.modalService.open(ComponentEditComponent, this.modalConfig);
    this.modalRef.setPayload(this.component);
    this.modalRef.result
      .then(result => {
        if (result) { this.onEdit.next(result); }
      })
      .catch(() => { });
  }

}
