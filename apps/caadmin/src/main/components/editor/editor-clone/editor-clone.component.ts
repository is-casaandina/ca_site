import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LanguageFormatPipe } from '@ca-admin/pipes/language-format.pipe';
import { LANGUAGE_TYPES, LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'caadmin-editor-clone',
  templateUrl: './editor-clone.component.html'
})
export class EditorCloneComponent implements OnInit {
  @Output() cloned: EventEmitter<any> = new EventEmitter<any>();
  languages = LANGUAGE_TYPES;

  from: LANGUAGE_TYPES_CODE;
  to: LANGUAGE_TYPES_CODE;

  cloneSwal: SweetAlertOptions;

  constructor(
    private swalConfirmService: SwalConfirmService,
    private languageFormatPipe: LanguageFormatPipe
  ) { }

  ngOnInit(): void {
    this.formatAlert();
  }

  formatAlert(): void {
    const from = this.languageFormatPipe.transform(this.from);
    const to = this.languageFormatPipe.transform(this.to);

    let text = `Si clona la información de la pagina, los datos del destino se perderán. `;
    text += 'Recordar corregir los enlaces y textos del nuevo idioma.<br>';
    this.cloneSwal = this.swalConfirmService
      .warning(text, `¿Seguro de clonar de <strong>${from}</strong> a <strong>${to}</strong>?`
      );
  }

  clone(): void {
    this.cloned.next({ from: this.from, to: this.to });
  }
}
