import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CaAdminPipesModule } from '@ca-admin/pipes/pipes.module';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { EditorCloneComponent } from './editor-clone.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SweetAlert2Module,
    CaAdminPipesModule
  ],
  declarations: [EditorCloneComponent],
  exports: [EditorCloneComponent]
})
export class EditorCloneModule { }
