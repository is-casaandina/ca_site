import { Component, OnInit, ViewChild } from '@angular/core';
import { RoomService } from '@ca-admin/services/room.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { STRUCTURE_COMPONENT_ROOM } from '@ca-admin/settings/constants/structures.contant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ILivingRoomResponse } from '@ca-admin/statemanagement/models/room.interface';
import { IComponent } from '@ca-admin/statemanagement/models/template.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { AngularUtil, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { DynamicFormComponent } from '../dynamic-form';

@Component({
  selector: 'caadmin-room-form',
  templateUrl: './room-form.component.html'
})
export class RoomFormComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {
  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;
  payload: ILivingRoomResponse;

  GENERAL_LANG = GeneralLang;

  titlePage = 'Agregar nueva Sala';

  structure: Array<IComponent> = null;

  constructor(
    private activeModal: ActiveModal,
    private roomService: RoomService
  ) {
    super();
  }

  ngOnInit(): void {
    this.setDataForm();
    if (this.payload) {
      this.titlePage = 'Editar Sala';
    }
  }

  setDataForm(): void {
    const structure = AngularUtil.clone(STRUCTURE_COMPONENT_ROOM) as IComponent;
    if (this.payload) {
      if (this.payload.images && this.payload.images.length) {
        this.payload.images.forEach((image, index) => {
          this.payload[`image${index + 1}`] = [image];
        });
      }

      structure.properties = structure.properties.map(property => {
        const value = this.payload[property.code];
        // tslint:disable-next-line:prefer-conditional-expression
        if (property.type === 'array' || property.type === 'image' && value) {
          property.value = typeof value === 'string' ? JSON.parse(value) : value;
        } else {
          property.value = value;
        }

        return property;
      });
    }

    this.structure = [structure];
  }

  formSubmitted(dataForm: any): void {
    const room = dataForm.room;
    room['images'] = [];
    if (dataForm.room.image1 && dataForm.room.image1.length) { room['images'].push(dataForm.room.image1[0]); }
    if (dataForm.room.image2 && dataForm.room.image2.length) { room['images'].push(dataForm.room.image2[0]); }
    if (dataForm.room.image3 && dataForm.room.image3.length) { room['images'].push(dataForm.room.image3[0]); }
    if (dataForm.room.image4 && dataForm.room.image4.length) { room['images'].push(dataForm.room.image4[0]); }

    if (isNullOrUndefined(this.payload)) {
      this.roomService.saveRoom(room)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<ILivingRoomResponse>) => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    } else {
      this.roomService.updateRoom(this.payload.id, room)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<ILivingRoomResponse>) => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    }
  }

  save(): void {
    this.dynamicForm.saveForm();
  }
}
