import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaCardModule } from '@ca-core/ui/lib/components';
import { DynamicFormModule } from '../dynamic-form';
import { HeadingModule } from '../heading/heading.module';
import { RoomFormComponent } from './room-form.component';

@NgModule({
  imports: [
    CommonModule,
    DynamicFormModule,
    CaCardModule,
    HeadingModule,
    ModalModule,
    CaButtonModule
  ],
  declarations: [RoomFormComponent],
  entryComponents: [RoomFormComponent],
  exports: [RoomFormComponent]
})
export class RoomFormModule { }
