import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { GeneralLang } from '@ca-admin/settings/lang';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';

@Component({
  selector: 'caadmin-card-form',
  templateUrl: './card-form.component.html'
})
export class CardFormComponent implements OnDestroy {

  @Input() title: string;
  @Input() buttonAdd: string;
  @Input() textSave = GeneralLang.buttons.saveChange;

  @Output() save: EventEmitter<boolean>;
  @Output() cancel: EventEmitter<boolean>;
  @Output() add: EventEmitter<boolean>;

  private _hideSave = false;
  @Input()
  get hideSave(): boolean {
    return this._hideSave;
  }
  set hideSave(value: boolean) {
    this._hideSave = coerceBooleanProp(value);
  }

  private _hideCancel = false;
  @Input()
  get hideCancel(): boolean {
    return this._hideCancel;
  }
  set hideCancel(value: boolean) {
    this._hideCancel = coerceBooleanProp(value);
  }

  GENERAL_LANG = GeneralLang;

  constructor() {
    this.save = new EventEmitter<boolean>();
    this.cancel = new EventEmitter<boolean>();
    this.add = new EventEmitter<boolean>();
  }

  ngOnDestroy(): void {
    if (this.add) {
      this.add.unsubscribe();
    }
    if (this.save) {
      this.save.unsubscribe();
    }
    if (this.cancel) {
      this.cancel.unsubscribe();
    }
  }

  onAdd(event: boolean): void {
    if (this.add) {
      this.add.emit(event);
    }
  }

  onSave(): void {
    if (this.save) {
      this.save.emit(true);
    }
  }

  onCancel(): void {
    if (this.cancel) {
      this.cancel.emit(true);
    }
  }

}
