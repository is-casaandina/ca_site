import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaButtonModule } from '@ca-core/ui/lib/components';
import { SubheadingActionsModule } from '../subheading-actions/subheading-actions.module';
import { CardFormComponent } from './card-form.component';

@NgModule({
  imports: [
    CommonModule,
    CaButtonModule,
    SubheadingActionsModule
  ],
  declarations: [CardFormComponent],
  exports: [CardFormComponent]
})
export class CardFormModule { }
