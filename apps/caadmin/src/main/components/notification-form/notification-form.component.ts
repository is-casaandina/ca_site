import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PageNotificationService } from '@ca-admin/services/page-notification.service';
import { ACTION_TYPES, LANGUAGE_TYPES, LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import {
  ILanguagePageNotificationRequest,
  IPageNotificationRequest,
  IPageNotificationResponse
} from '@ca-admin/statemanagement/models/notification.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import { takeUntil } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'caadmin-notification-form',
  templateUrl: './notification-form.component.html'
})
export class NotificationFormComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {
  payload: IPageNotificationResponse;

  GENERAL_LANG = GeneralLang;

  titlePage = 'Agregar nueva Alerta';
  language: string;

  frmNotification: FormGroup;
  name: AbstractControl;
  pageId: AbstractControl;
  beginDate: AbstractControl;
  endDate: AbstractControl;

  languages: Array<ILanguagePageNotificationRequest> = [];
  title: FormControl;
  detail: FormControl;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private pageNotificationService: PageNotificationService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    const notification = this.payload || {} as IPageNotificationResponse;
    this.frmNotification = this.fb.group({
      name: [notification.name, Validators.required],
      pageId: [notification.pageId, Validators.required],
      beginDate: [notification.beginDate && DateUtil.setTimezoneDate(notification.beginDate), Validators.required],
      endDate: [notification.endDate && DateUtil.setTimezoneDate(notification.endDate), Validators.required]
    });

    this.name = this.frmNotification.get('name');
    this.pageId = this.frmNotification.get('pageId');
    this.beginDate = this.frmNotification.get('beginDate');
    this.endDate = this.frmNotification.get('endDate');

    this.title = this.fb.control(null);
    this.detail = this.fb.control(null);

    this.languages = [];
    LANGUAGE_TYPES.forEach(languageType => {
      const language = (notification.languages || []).find(l => l.language === languageType.code);
      this.languages.push({
        language: languageType.code,
        title: language && language.title || '',
        detail: language && language.detail || ''
      });
    });
    this.getLanguageData();
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmNotification);

    return this.frmNotification.valid;
  }

  private paramsSave(): IPageNotificationRequest {
    const request = this.frmNotification.value as IPageNotificationRequest;
    request['languages'] = this.languages;

    return request;
  }

  save(): void {
    this.setLanguageData();
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSave();
      if (isNullOrUndefined(this.payload)) {
        this.pageNotificationService.saveAlert(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      } else {
        this.pageNotificationService.updateAlert(this.payload.id, params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      }
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  changeLanguage(language: string): void {
    this.setLanguageData();
    this.language = language;
    this.getLanguageData();
  }

  private setLanguageData(): void {
    const data = this.languages.find(item => item.language === (this.language || LANGUAGE_TYPES_CODE.es));

    data.title = this.title.value;
    data.detail = this.detail.value;
  }

  private getLanguageData(): void {
    const data = this.languages.find(item => item.language === (this.language || LANGUAGE_TYPES_CODE.es));

    this.title.setValue(data.title || null);
    this.detail.setValue(data.detail || null);
  }

  close(): void {
    this.activeModal.dismiss();
  }

  preSave(): void {
    this.setLanguageData();
    const missing = this.languages.find(item => !item.detail || !item.title);
    if (!missing) {
      this.save();
    } else {
      this.notificationService.addWarning('El nombre y url deben ser completos en todos los lenguages.');
    }
  }
}
