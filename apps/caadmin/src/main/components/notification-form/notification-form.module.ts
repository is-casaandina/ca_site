import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaDatepickerModule, CaInputModule } from '@ca-core/ui/lib/components';
import { AutocompleteModule } from '../inputs/form-autocomplete/autocomplete.module';
import { TabsLanguageModule } from '../tabs-language/tabs-language.module';
import { NotificationFormComponent } from './notification-form.component';

@NgModule({
  imports: [
    CommonModule,
    CaInputModule,
    ModalModule,
    ReactiveFormsModule,
    CaButtonModule,
    TabsLanguageModule,
    CaInputModule,
    AutocompleteModule,
    CaDatepickerModule
  ],
  declarations: [NotificationFormComponent],
  exports: [NotificationFormComponent],
  entryComponents: [NotificationFormComponent]
})
export class NotificationFormModule { }
