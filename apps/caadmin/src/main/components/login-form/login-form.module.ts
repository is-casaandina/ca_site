import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CaAlertErrorModule, CaButtonModule, CaInputModule, CaSwitchModule } from '@ca-core/ui/lib/components';
import { LoginFormComponent } from './login-form.component';

@NgModule({
  declarations: [LoginFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    CaSwitchModule,
    CaButtonModule,
    CaAlertErrorModule,
    RouterModule
  ],
  exports: [LoginFormComponent]
})
export class LoginFormModule { }
