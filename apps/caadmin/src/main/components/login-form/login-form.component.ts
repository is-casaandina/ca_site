import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@ca-admin/services';
import { IMAGES } from '@ca-admin/settings/constants/images.constant';
import { LoginLang } from '@ca-admin/settings/lang/login.lang';
import { IAuthRequest } from '@ca-admin/statemanagement/models/auth.interface';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent extends UnsubscribeOnDestroy implements OnInit {

  frmLogin: FormGroup;
  username: AbstractControl;
  password: AbstractControl;
  rememberUser: AbstractControl;
  invalidUser: boolean;

  IMAGES = IMAGES;
  LOGIN_LANG = LoginLang;

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmLogin = this._formBuilder.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      rememberUser: [false]
    });
    this.username = this.frmLogin.get('username');
    this.password = this.frmLogin.get('password');
    this.rememberUser = this.frmLogin.get('rememberUser');
    console.log("mostrando mensaje");
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmLogin, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmLogin);

    return this.frmLogin.valid;
  }

  private _paramsLogin(): IAuthRequest {
    return {
      username: this.username.value,
      password: this.password.value,
      rememberMe: this.rememberUser.value
    } as IAuthRequest;
  }
  login(): void {
    const validateForm = this._validateForm();
    this.invalidUser = false;
    if (validateForm) {
      const auth = this._paramsLogin();

      this._authService.login(auth, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this._router.navigate(['/home']);
        }, () => {
          this.invalidUser = true;
        });
    }
  }

}
