import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'caadmin-collapse-action-buttons',
  templateUrl: './collapse-action-buttons.component.html'
})
export class CollapseActionButtonsComponent implements OnInit {

  @Input() disabled: boolean;
  @Input() confirmDelete: string;

  @Output() actionButton: EventEmitter<string>;

  GENERAL_LANG = GeneralLang;

  isCollapsed: boolean;
  deleteSwal: SweetAlertOptions;

  constructor(
    private _swalConfirmService: SwalConfirmService
  ) {
    this.isCollapsed = true;
    this.actionButton = new EventEmitter<string>();
  }

  ngOnInit(): void {
    this.deleteSwal = this._swalConfirmService.warning(this.confirmDelete);
  }

  toggle(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  edit(): void {
    this.toggle();
    this.actionButton.next(ACTION_TYPES.edit);
  }

  delete(event: string): void {
    this.actionButton.next(ACTION_TYPES.delete);
  }

  cancel(): void {
    this.toggle();
    this.actionButton.next(ACTION_TYPES.cancel);
  }

  save(): void {
    this.actionButton.next(ACTION_TYPES.save);
  }

}
