import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaButtonModule, CaCardModule } from '@ca-core/ui/lib/components';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CollapseActionButtonsComponent } from './collapse-action-buttons.component';

@NgModule({
  declarations: [CollapseActionButtonsComponent],
  imports: [
    CommonModule,
    CaCardModule,
    CaButtonModule,
    SweetAlert2Module,
    CollapseModule
  ],
  exports: [CollapseActionButtonsComponent]
})
export class CollapseActionButtonsModule { }
