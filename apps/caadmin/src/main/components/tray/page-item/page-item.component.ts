import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from '@ca-admin/services';
import { DomainService } from '@ca-admin/services/domain.service';
import { CATEGORY, PAGE_CODE_STATES } from '@ca-admin/settings/constants/general.constant';
import { MENU_CODE } from '@ca-admin/settings/constants/menu.constant';
import { IPageItemResponse, IPageResponse } from '@ca-admin/statemanagement/models/page.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { GeneralResponse } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';
import { CreateExternalPageComponent } from '../../modals/create-external-page/create-external-page.component';

@Component({
  selector: 'caadmin-page-item',
  templateUrl: './page-item.component.html',
  providers: [DatePipe]
})
export class PageItemComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('domainAlert') private domainSwal: SwalComponent;

  @Input() item: IPageItemResponse;
  @Output() edited: EventEmitter<IPageResponse> = new EventEmitter<IPageResponse>();
  @Output() deleted: EventEmitter<any> = new EventEmitter<any>();

  external: boolean;
  deleteSwal: SweetAlertOptions;
  unpublishSwal: SweetAlertOptions;
  setFreeSwal: SweetAlertOptions;

  language: string;

  setFreePagePermission: boolean;

  constructor(
    private router: Router,
    private pageService: PageService,
    private modalService: ModalService,
    private notificationService: NotificationService,
    private swalConfirmService: SwalConfirmService,
    private domainsService: DomainService,
    private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.external = CATEGORY.LINK === this.item.categoryId;
    this.deleteSwal = this.swalConfirmService.warning(
      'Una vez eliminado no podrá ingresar ni recuperar a la página.<br>',
      `¿Seguro de eliminar la página <strong>${this.item.name}</strong>?`
    );
    this.unpublishSwal = this.swalConfirmService.warning(
      'Una vez cambiada a borrardor, no podrá ingresar desde el sitio web.<br>',
      `¿Seguro de cambiar la página <strong>${this.item.name}</strong> a borrador?`
    );
    this.setFreeSwal = this.swalConfirmService.warning(
      'Una vez liberada, El usuario actual en edición. Penderá todos sus cambios.<br>',
      `¿Seguro de liberar la página <strong>${this.item.name}</strong>?`
    );

    this.setFreePagePermission = !!this.activatedRoute.snapshot.data.userPermissions.permissionList.find(
      permission => permission.permissionCode === MENU_CODE.setFreePage
    );
  }

  edit(): void {
    if (!this.external) {
      this.pageService
        .editPage(this.item.id, true)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(res => {
          this.pageService.setPageEditor(res);
          this.pageService.setPageStorage(null);

          const queryParams = { state: res.state };
          this.router.navigate(['editor/edit/publication', this.item.id], { queryParams });
        });
    } else {
      this.editExternal();
    }
  }

  see(language: string): void {
    this.language = language;

    if (!this.item.publish) {
      const queryParams = { state: this.item.state };
      this.router.navigate(['editor/see/publication', this.item.id], { queryParams });
    } else if (!this.external) {
      this.domainsService
        .getDomainsSelect()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(domains => {
          const options = {};
          domains.forEach(domain => (options[domain.text] = domain.text));

          this.domainSwal.options = {
            inputValue: domains && domains.length && (domains[0] as any),
            inputOptions: options as any
          };

          this.domainSwal.show();
        });
    } else {
      this.goToDomain(language);
    }
  }

  editExternal(): void {
    this.pageService
      .getPageView(this.item.id, PAGE_CODE_STATES.published, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(page => {
        this.pageService.setPageEditor(null);
        this.pageService.setPageStorage(page);

        const modal = this.modalService.open(CreateExternalPageComponent, {
          title: 'Editar página externa',
          classBody: 'px-4 pb-3'
        });
        modal.result
          .then((res: GeneralResponse<IPageResponse>) => {
            this.edited.next(res.data);
            this.notificationService.addSuccess(res.message);
            this.pageService.setPageEditor(null);
            this.pageService.setPageStorage(null);
          })
          .catch(() => {});
      });
  }

  delete(): void {
    this.pageService
      .deletePage(this.item.id)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.notificationService.addSuccess(res.message);
        this.deleted.next();
      });
  }

  unpublish(): void {
    this.pageService
      .unPublishPage(this.item.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        res.data.name = this.item.name;
        this.edited.next(res.data);
        this.notificationService.addSuccess(res.message);
      });
  }

  publish(): void {
    this.pageService
      .publishPage(this.item.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        res => {
          res.data.name = this.item.name;
          res.data.publish = true;
          this.edited.next(res.data);
          this.notificationService.addSuccess(res.message);
        },
        () => {}
      );
  }

  setFree(): void {
    this.pageService
      .setFreePage(this.item.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.item.fullname = res.data.fullname;
        this.item.inEdition = false;

        this.notificationService.addSuccess(res.message);
      });
  }
  goToDomain(domain?: string): void {
    this.pageService.viewExternalPage(this.item.id, this.language, domain);
  }
}
