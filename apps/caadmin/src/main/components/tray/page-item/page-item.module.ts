import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CaAdminPipesModule } from 'apps/caadmin/src/providers/pipes/pipes.module';
import { CreateExternalPageModule } from '../../modals/create-external-page/create-external-page.module';
import { PageItemComponent } from './page-item.component';

@NgModule({
  imports: [
    CommonModule,
    CaCardModule,
    CaAdminPipesModule,
    CreateExternalPageModule,
    ModalModule,
    SweetAlert2Module
  ],
  declarations: [PageItemComponent],
  exports: [PageItemComponent]
})
export class PageItemModule { }
