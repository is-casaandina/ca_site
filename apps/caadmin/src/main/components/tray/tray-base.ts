import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService, GeneralService, PageService, UserService } from '@ca-admin/services';
import { CountryService } from '@ca-admin/services/country.service';
import { DestinationService } from '@ca-admin/services/destination.service';
import { PromotionService } from '@ca-admin/services/promotion.service';
import { CATEGORY, LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { MENU, MENU_CODE } from '@ca-admin/settings/constants/menu.constant';
import { TRAY_FILTER_DEFINITION, TRAY_FILTER_PROPERTY } from '@ca-admin/settings/constants/tray.constant';
import { ICategoryResponse } from '@ca-admin/statemanagement/models/category.interface';
import { ICountryResponse } from '@ca-admin/statemanagement/models/country.interface';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { IPagePaginationRequest, IPagePaginationResponse } from '@ca-admin/statemanagement/models/page.interface';
import { IUserResponse } from '@ca-admin/statemanagement/models/user.interface';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { CaPaginator } from '@ca-core/ui/lib/components';
import { Observable, zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export class TrayBaseComponent extends UnsubscribeOnDestroy {

  title: string;
  form: FormGroup;

  cTemplates: AbstractControl;
  cUsers: AbstractControl;
  cStates: AbstractControl;
  cDate: AbstractControl;
  cName: AbstractControl;
  cCountry: AbstractControl;
  cDestination: AbstractControl;
  cGenericType: AbstractControl;

  tmpTextSearch: string;

  pagination: CaPaginator;

  params: any;

  templates: Array<ICategoryResponse>;
  users: Array<IUserResponse>;
  states: Array<IPageStateResponse>;
  genericTypes: Array<IPageStateResponse>;
  destinations: Array<IPageStateResponse>;
  countries: Array<ICountryResponse>;
  data: IPagePaginationResponse;

  menuCode: string;

  definition: any = {};

  TRAY_FILTER_PROPERTY = TRAY_FILTER_PROPERTY;
  MENU_CODE = MENU_CODE;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected formBuilder: FormBuilder,
    protected router: Router,
    protected generalService: GeneralService,
    protected userService: UserService,
    protected categoryService: CategoryService,
    protected spinnerService: SpinnerService,
    protected pageService: PageService,
    protected promotionService: PromotionService,
    protected countryService: CountryService,
    protected destinationService: DestinationService
  ) {
    super();
    this.pagination = new CaPaginator();
    this.createForm();
    this.setTitle();
    this.setDefaultFilter();
    this.initData();
  }

  setTitle(): void {
    this.menuCode = this.activatedRoute.snapshot.data.menuCode;
    this.title = MENU.find(menu => menu.code === this.menuCode).name;
    const definitions = TRAY_FILTER_DEFINITION.filter(definition => definition.menuCode === this.menuCode);
    this.definition = definitions.length ? definitions[0] : {};
  }

  private createForm(): void {
    this.form = this.formBuilder.group(
      {
        cTemplates: [],
        cUsers: [],
        cStates: [],
        cDate: {},
        cName: '',
        cCountry: [],
        cDestination: [],
        cGenericType: []
      }
    );
    this.cTemplates = this.form.get('cTemplates');
    this.cUsers = this.form.get('cUsers');
    this.cStates = this.form.get('cStates');
    this.cDate = this.form.get('cDate');
    this.cName = this.form.get('cName');
    this.cCountry = this.form.get('cCountry');
    this.cDestination = this.form.get('cDestination');
    this.cGenericType = this.form.get('cGenericType');

    this.getDestination();
    this.cCountry.valueChanges
      .subscribe(() => {
        this.getDestination();
      });
  }

  private initData(): void {
    this.spinnerService.showSpinner();
    zip(
      this.generalService.pageStates(),
      this.userService.usersList(),
      this.categoryService.categoryList(),
      this.getPages(),
      this.promotionService.types(LANGUAGE_TYPES_CODE.es),
      this.countryService.countriesActive()
    )
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.states = res[0] || [];
        this.users = res[1] || [];
        this.templates = res[2] || [];

        this.data = res[3];
        this.genericTypes = res[4];
        this.countries = res[5];

        this.spinnerService.hideSpinner();
      }, err => this.spinnerService.hideSpinner());
  }

  private getDestination(): void {
    const countries = this.cCountry.value || [];
    const ids = countries.map(country => country.value);
    this.destinationService.byCountries([CATEGORY.DESTINATIONS_DETAIL], ids)
      .subscribe(res => {
        this.destinations = res || [];
      });
  }

  updateUrlParams(params: any): void {
    const queryParams = {};
    Object.keys(params)
      .forEach(key => {
        const validCategory = !(this.definition.categories && key === 'categories');

        if (key !== 'menuCode' && validCategory) {
          const value = params[key];
          const isArray = Array.isArray(value);
          if ((isArray && value.length) || (!isArray && value)) {
            queryParams[key] = value;
          }
        }
      });
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams,
        queryParamsHandling: '',
        replaceUrl: true
      });
  }

  getPages(): Observable<IPagePaginationResponse> {
    const cDate = this.cDate.value || {};
    const states = (this.cStates.value || []).map(data => data.code);
    const categories = (this.cTemplates.value || []).map(data => data.id);
    const userList = (this.cUsers.value || []).map(data => data.username);
    const genericType = (this.cGenericType.value || []).map(data => data.value);
    const countries = (this.cCountry.value || []).map(data => data.value);
    const destinations = (this.cDestination.value || []).map(data => data.value);
    const params: IPagePaginationRequest = {
      states,
      categories,
      userList,
      genericType,
      countries,
      destinations,
      startDate: cDate.from || '',
      endDate: cDate.to || '',
      size: this.pagination.pageSize,
      page: this.pagination.page,
      name: this.cName.value || '',
      menuCode: this.menuCode
    };

    this.updateUrlParams(params);

    return this.pageService.pagesList(params);
  }

  private setDefaultFilter(): void {
    const params = this.activatedRoute.snapshot.queryParams;
    this.pagination.pageSize = Number(params.size || this.pagination.pageSize);
    this.pagination.page = Number(params.page || this.pagination.page);

    const valueTemplate = [].concat(this.definition.categories || params.categories)
      .filter(category => category)
      .map(category => ({ id: category }));

    const valueUser = [].concat(params.userList)
      .filter(username => username)
      .map(username => ({ username }));

    const valueStatess = [].concat(params.states)
      .filter(state => state)
      .map(state => ({ code: Number(state) }));

    const genericType = [].concat(params.genericType)
      .filter(value => value)
      .map(value => ({ value }));

    const countries = [].concat(params.countries)
      .filter(value => value)
      .map(value => ({ value }));

    const destinations = [].concat(params.destinations)
      .filter(value => value)
      .map(value => ({ value }));

    const valueDate = {
      from: params.startDate,
      to: params.endDate
    };
    this.cTemplates.setValue(valueTemplate);
    this.cUsers.setValue(valueUser);
    this.cStates.setValue(valueStatess);
    this.cDate.setValue(valueDate, {});
    this.cGenericType.setValue(genericType);
    this.cCountry.setValue(countries);
    this.cDestination.setValue(destinations);

    this.tmpTextSearch = params.name || '';
    this.cName.setValue(this.tmpTextSearch);
  }

  showProperty(property: TRAY_FILTER_PROPERTY): boolean {
    return !!(this.definition.properties || []).find(prop => prop === property);
  }
}
