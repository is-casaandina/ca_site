import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaInputModule } from '@ca-core/ui/lib/components';
import { CaIconPickerModule } from '@ca-core/ui/lib/components/icon-picker';
import { AssistanceFormComponent } from './assistance-form.component';

@NgModule({
  imports: [
    CommonModule,
    CaInputModule,
    ModalModule,
    ReactiveFormsModule,
    CaButtonModule,
    CaIconPickerModule
  ],
  declarations: [AssistanceFormComponent],
  entryComponents: [AssistanceFormComponent],
  exports: [AssistanceFormComponent]
})
export class AssistanceFormModule { }
