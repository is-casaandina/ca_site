import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AssistanceService } from '@ca-admin/services/assistance.service';
import { IconService } from '@ca-admin/services/icon.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IAssistanceRequest, IAssistanceResponse } from '@ca-admin/statemanagement/models/assistance.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { IIcon } from '@ca-core/ui/lib/components/icon-picker/lib';
import { takeUntil } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'caadmin-assistance-form',
  templateUrl: './assistance-form.component.html'
})
export class AssistanceFormComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {
  payload: { typeAssistance: number, assistance: IAssistanceResponse };

  GENERAL_LANG = GeneralLang;

  title = 'Agregar nuevo Servicio de hotel';
  icons: Array<IIcon>;

  frmAssistance: FormGroup;
  iconClass: AbstractControl;
  spanishTitle: AbstractControl;
  englishTitle: AbstractControl;
  portugueseTitle: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private assistanceService: AssistanceService,
    private iconService: IconService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.iconService.getIcons()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(icons => {
        this.icons = icons;
      });
  }

  private createForm(): void {
    const assistance = (this.payload && this.payload.assistance) || {} as IAssistanceResponse;
    this.frmAssistance = this.fb.group({
      iconClass: [assistance.iconClass, Validators.required],
      spanishTitle: [assistance.spanishTitle, Validators.required],
      englishTitle: [assistance.englishTitle, Validators.required],
      portugueseTitle: [assistance.portugueseTitle, Validators.required],
      type: [this.payload.typeAssistance]
    });

    this.iconClass = this.frmAssistance.get('iconClass');
    this.spanishTitle = this.frmAssistance.get('spanishTitle');
    this.englishTitle = this.frmAssistance.get('englishTitle');
    this.portugueseTitle = this.frmAssistance.get('portugueseTitle');
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmAssistance);

    return this.frmAssistance.valid;
  }

  private paramsSaveAssistance(): IAssistanceRequest {
    return this.frmAssistance.value as IAssistanceRequest;
  }

  save(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveAssistance();
      if (isNullOrUndefined(this.payload.assistance)) {
        this.assistanceService.saveAssistance(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      } else {
        this.assistanceService.updateAssistance(this.payload.assistance.id, params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      }
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
