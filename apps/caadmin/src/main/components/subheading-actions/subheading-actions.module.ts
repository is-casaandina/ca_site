import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SubheadingActionsComponent } from './subheading-actions.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SubheadingActionsComponent],
  exports: [SubheadingActionsComponent]
})
export class SubheadingActionsModule { }
