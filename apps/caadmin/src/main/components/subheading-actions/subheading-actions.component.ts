import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'caadmin-subheading-actions',
  templateUrl: './subheading-actions.component.html'
})
export class SubheadingActionsComponent {

  @Input() title: string;
  @Input() buttonAdd: string;
  @Output() add: EventEmitter<boolean>;

  constructor() {
    this.add = new EventEmitter<boolean>();
  }

  onAdd(): void {
    if (this.add) {
      this.add.emit(true);
    }
  }

}
