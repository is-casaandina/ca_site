import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DescriptorService } from '@ca-admin/services/descriptor.service';
import {
  ACTION_TYPES,
  CATEGORIES_DESCRIPTOR,
  CATEGORIES_DESCRIPTOR_HOTEL,
  TYPOGRAFY_LIST
} from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IDescriptorRequest, IDescriptorResponse } from '@ca-admin/statemanagement/models/descriptor.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'caadmin-descriptor-form',
  templateUrl: './descriptor-form.component.html'
})
export class DescriptorFormComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {
  payload: { category: string, descriptor: IDescriptorResponse };

  GENERAL_LANG = GeneralLang;
  categoriesDescriptor = CATEGORIES_DESCRIPTOR;
  typographies = TYPOGRAFY_LIST;

  titlePage = 'Agregar nueva Etiqueta';

  frmDescriptor: FormGroup;
  name: AbstractControl;
  url: AbstractControl;
  urlBanner: AbstractControl;
  typographyStyle: AbstractControl;
  color: AbstractControl;
  title: AbstractControl;
  category: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private descriptorService: DescriptorService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.titlePage = `${!isNullOrUndefined(this.payload.descriptor) ? 'Editar ' : 'Agregar nueva '}`;
    this.titlePage += `Etiqueta de ${this.getCategoryName()}`;
  }

  getCategoryName(): string {
    return this.categoriesDescriptor.filter(categoria => categoria.code === this.payload.category)[0].name;
  }

  isHotelCategory(): boolean {
    return this.payload.category === CATEGORIES_DESCRIPTOR_HOTEL;
  }

  private createForm(): void {
    const assistance = (this.payload && this.payload.descriptor) || {} as IDescriptorResponse;
    this.frmDescriptor = this.fb.group({
      name: [assistance.name, Validators.required],
      url: [assistance.url, Validators.required],
      urlBanner: [assistance.urlBanner],
      typographyStyle: [assistance.typographyStyle],
      color: [assistance.color, Validators.required],
      title: [assistance.title, Validators.required],
      category: [this.payload.category]
    });

    this.name = this.frmDescriptor.get('name');
    this.url = this.frmDescriptor.get('url');
    this.urlBanner = this.frmDescriptor.get('urlBanner');
    this.typographyStyle = this.frmDescriptor.get('typographyStyle');
    this.color = this.frmDescriptor.get('color');
    this.title = this.frmDescriptor.get('title');
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmDescriptor);

    return this.frmDescriptor.valid;
  }

  private paramsSaveAssistance(): IDescriptorRequest {
    return this.frmDescriptor.value as IDescriptorRequest;
  }

  save(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveAssistance();
      if (isNullOrUndefined(this.payload.descriptor)) {
        this.descriptorService.saveDescriptor(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      } else {
        this.descriptorService.updateDescriptor(this.payload.descriptor.id, params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      }
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
