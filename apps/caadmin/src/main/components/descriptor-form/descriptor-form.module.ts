import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaInputModule } from '@ca-core/ui/lib/components';
import { GallerySelectModule } from '../gallery-select/gallery-select.module';
import { DescriptorFormComponent } from './descriptor-form.component';

@NgModule({
  imports: [
    CommonModule,
    CaInputModule,
    ModalModule,
    ReactiveFormsModule,
    CaButtonModule,
    GallerySelectModule
  ],
  declarations: [DescriptorFormComponent],
  entryComponents: [DescriptorFormComponent],
  exports: [DescriptorFormComponent]
})
export class DescriptorFormModule { }
