import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TabsLanguageComponent } from './tabs-language.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TabsLanguageComponent],
  exports: [TabsLanguageComponent]
})
export class TabsLanguageModule { }
