import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LANGUAGE_TYPES, LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';

@Component({
  selector: 'caadmin-tabs-language',
  templateUrl: './tabs-language.component.html'
})
export class TabsLanguageComponent implements OnInit {

  @Output() selected: EventEmitter<string> = new EventEmitter<string>();

  @Input() bgc = 'g-bgc-gray7';
  @Input() classBody: string;

  LANGUAGE_TYPES = LANGUAGE_TYPES;
  language: string;

  constructor() { }

  ngOnInit(): void {
    this.language = LANGUAGE_TYPES_CODE.es;
    this.selected.next(this.language);
  }

  changeLanguage(language: string): void {
    this.language = language;
    this.selected.next(this.language);
  }

}
