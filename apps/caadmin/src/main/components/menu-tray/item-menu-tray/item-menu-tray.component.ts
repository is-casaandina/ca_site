import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AddItemMenuComponent } from '@ca-admin/components/modals/add-item-menu/add-item-menu.component';
import { PageService } from '@ca-admin/services';
import { DomainService } from '@ca-admin/services/domain.service';
import { MenuSiteService } from '@ca-admin/services/menu-site.service';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { GeneralResponse } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { IMenuSite, MENU_SITE_ACTION, MENU_SITE_TYPE } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'caadmin-item-menu-tray',
  templateUrl: './item-menu-tray.component.html',
  styleUrls: ['./item-menu-tray.component.scss']
})
export class ItemMenuTrayComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild('domainAlert') private domainSwal: SwalComponent;
  @Output() collapsed: EventEmitter<any> = new EventEmitter<any>();
  @Output() updated: EventEmitter<IMenuSite> = new EventEmitter<IMenuSite>();
  @Output() add: EventEmitter<IMenuSite> = new EventEmitter<IMenuSite>();
  @Output() remove: EventEmitter<IMenuSite> = new EventEmitter<IMenuSite>();

  @Output() down: EventEmitter<any> = new EventEmitter<any>();
  @Output() up: EventEmitter<any> = new EventEmitter<any>();

  @Input() showAdd: boolean;
  @Input() item: IMenuSite;
  @Input() parent: IMenuSite;
  @Input() brothers: Array<IMenuSite>;

  @Input() showUp: boolean;
  @Input() showDown: boolean;

  MENU_SITE_TYPE = MENU_SITE_TYPE;
  showView: boolean;
  deleteSwal: SweetAlertOptions;

  constructor(
    private domainsService: DomainService,
    private menuService: MenuSiteService,
    private notificationService: NotificationService,
    private swalConfirmService: SwalConfirmService,
    private pageService: PageService,
    private modalService: ModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this.fnShowView();
    this.deleteSwal = this.swalConfirmService
      .warning(
        'Una vez eliminado no podrá ingresar a la opción ni sus dependencias.<br>',
        `¿Seguro de eliminar <strong>${this.item.name}</strong>?`
      );
  }

  emitCollapse(): void {
    this.collapsed.next();
  }

  see(): void {
    if (!this.item.external) {
      this.domainsService.getDomainsSelect()
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(domains => {
          const options = {};
          domains.forEach(domain => options[domain.text] = domain.text);

          this.domainSwal.options = {
            inputValue: domains && domains.length && domains[0] as any,
            inputOptions: options as any
          };

          this.domainSwal.show();
        });
    } else {
      this.goToDomain();
    }
  }

  goToDomain(domain: string = ''): void {
    domain = domain.endsWith('/') ? domain : `${domain}/`;
    if (this.item.pagePath) {
      window.open(`${domain}${this.item.pagePath}`);
    } else {
      this.pageService.viewExternalPage(this.item.pageId, this.item.languageCode, domain);
    }
  }

  addChild(): void {
    const actions = [{ value: MENU_SITE_ACTION.REDIRECT, text: 'Reedirección' }];

    if (this.item.type === MENU_SITE_TYPE.HEADER) {
      actions.push({ value: MENU_SITE_ACTION.PROMOTION_ALL, text: 'Promoción todos' });
    }

    const modal = this.modalService.open(AddItemMenuComponent, { title: 'Agregar Submenu' });

    modal.setPayload({
      actions,
      currentParent: this.item,
      type: this.item.type,
      language: this.item.languageCode,
      order: (this.item.children || []).length
    });
    modal.result
      .then((res: GeneralResponse<IMenuSite>) => {
        res.data.sectionFooter = !!res.data.sectionFooter;
        this.notificationService.addSuccess(res.message);
        this.item.children.push(res.data);
        this.add.next(res.data);
        if (this.item.collapse) {
          this.emitCollapse();
        }
        this.fnShowView();
      })
      .catch(() => { });
  }

  update(): void {
    const actions = [];

    if (this.item.action !== MENU_SITE_ACTION.BOOKING && !(!this.parent && this.item.type === MENU_SITE_TYPE.FOOTER)) {
      actions.push({ value: MENU_SITE_ACTION.REDIRECT, text: 'Reedirección' });

      if (this.item.type === MENU_SITE_TYPE.HEADER) {
        actions.push({ value: MENU_SITE_ACTION.PROMOTION_ALL, text: 'Promoción todos' });
      }

      if (!this.item.children || (this.item.children && !this.item.children.length)) {
        actions.push({ value: MENU_SITE_ACTION.BOOKING, text: 'Reserva' });
      }
    }

    const modal = this.modalService.open(AddItemMenuComponent, { title: 'Editar' });
    modal.setPayload({
      actions,
      currentParent: this.parent,
      current: this.item,
      children: this.item.children,
      type: this.item.type,
      language: this.item.languageCode
    });
    modal.result
      .then((res: GeneralResponse<IMenuSite>) => {
        this.notificationService.addSuccess(res.message);
        res.data.sectionFooter = !!res.data.sectionFooter;

        this.item.name = res.data.name;
        this.item.pageId = res.data.pageId;
        this.item.pageId = res.data.pageId;
        this.item.pagePath = res.data.pagePath;
        this.item.sectionFooter = res.data.sectionFooter;
        this.item.action = res.data.action;
        this.item.external = res.data.external;

        this.updated.next(res.data);
        this.fnShowView();
      })
      .catch(() => { });
  }

  delete(): void {
    this.menuService.deleteMenu(this.item.id)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.notificationService.addSuccess(res.message);
        this.remove.next();
      });
  }

  upFn(): void {
    if (this.showUp) {
      this.up.next();
    }
  }

  downFn(): void {
    if (this.showDown) {
      this.down.next();
    }
  }

  private fnShowView(): void {
    this.showView = this.item.action === MENU_SITE_ACTION.REDIRECT && (!!this.item.pagePath || !!this.item.pageId);
  }
}
