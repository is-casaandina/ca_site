import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AddItemMenuModule } from '@ca-admin/components/modals/add-item-menu/add-item-menu.module';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ItemMenuTrayComponent } from './item-menu-tray.component';

@NgModule({
  imports: [
    CommonModule,
    SweetAlert2Module,
    AddItemMenuModule
  ],
  exports: [ItemMenuTrayComponent],
  declarations: [ItemMenuTrayComponent]
})
export class ItemMenuTrayModule { }
