import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CollapseActionButtonsModule } from '@ca-admin/components/collapse-action-buttons/collapse-action-buttons.module';
import { CaInputModule, CaLinkModule, CaRadioButtonModule } from '@ca-core/ui/lib/components';
import { CollapseUserComponent } from './collapse-user.component';

@NgModule({
  declarations: [CollapseUserComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CollapseActionButtonsModule,
    CaRadioButtonModule,
    CaLinkModule,
    CaInputModule
  ],
  exports: [CollapseUserComponent]
})
export class CollapseUserModule { }
