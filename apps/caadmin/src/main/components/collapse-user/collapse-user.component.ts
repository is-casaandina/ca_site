import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CollapseActionButtonsComponent } from '@ca-admin/components/collapse-action-buttons/collapse-action-buttons.component';
import { RoleService, UserService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { UserLang } from '@ca-admin/settings/lang/user.lang';
import { IRoleResponse } from '@ca-admin/statemanagement/models/role.interface';
import { IDeleteUserRequest, ISaveUserRequest, IUserView } from '@ca-admin/statemanagement/models/user.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'caadmin-collapse-user',
  templateUrl: './collapse-user.component.html',
  styleUrls: ['./collapse-user.component.scss']
})
export class CollapseUserComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild(CollapseActionButtonsComponent) collapseActionButtons: CollapseActionButtonsComponent;

  @Input() user: IUserView;
  @Input() disabled: boolean;

  GENERAL_LANG = GeneralLang;
  USER_LANG = UserLang;

  frmUser: FormGroup;
  confirmDelete: string;
  rolesList: Array<IRoleResponse>;
  radioRoles: AbstractControl;
  fullName: AbstractControl;

  constructor(
    private _formBuilder: FormBuilder,
    private _roleService: RoleService,
    private _userService: UserService,
    private _notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.confirmDelete = `${this.GENERAL_LANG.modalConfirm.titleDelete} al usuario ${this.user.username}?`;
    this.createForm();
  }

  private createForm(): void {
    this.frmUser = this._formBuilder.group({
      radioRoles: [null, Validators.required],
      fullName: [null, Validators.required]
    });
    this.radioRoles = this.frmUser.get('radioRoles');
    this.fullName = this.frmUser.get('fullName');
  }

  edit(): void {
    this.fullName.setValue(this.user.fullname);
    this._roleService.rolesList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        tap(preResponse => {
          const role = preResponse.find((value: IRoleResponse) => {
            return value.roleCode === this.user.role.roleCode;
          });
          this.radioRoles.setValue(role);
        })
      )
      .subscribe((response: Array<IRoleResponse>) => {
        this.rolesList = response;
        this._userService.setUsersList(ACTION_TYPES.edit, this.user);
      });
  }

  private paramsDeleteUser(): IDeleteUserRequest {
    return {
      userId: this.user.id
    } as IDeleteUserRequest;
  }

  delete(): void {
    const params = this.paramsDeleteUser();
    this._userService.deleteUser(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._userService.setUsersList(ACTION_TYPES.delete, this.user);
      });
  }

  cancel(): void {
    this._userService.setUsersList(ACTION_TYPES.cancel, this.user);
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmUser);

    return this.frmUser.valid;
  }

  private paramsSaveUser(): ISaveUserRequest {
    return {
      username: this.user.username,
      fullname: this.fullName.value,
      roleCode: this.radioRoles.value.roleCode,
      id: this.user.id
    } as ISaveUserRequest;
  }

  save(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveUser();
      this._userService.updateUser(this.user.id, params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.collapseActionButtons.toggle();
          this.user.role = this.radioRoles.value;
          this.user.fullname = this.fullName.value;
          this._userService.setUsersList(ACTION_TYPES.cancel, this.user);
        });
    } else {
      this._notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  actionButton(event: string): void {
    switch (event) {
      case ACTION_TYPES.edit:
        this.edit();
        break;
      case ACTION_TYPES.delete:
        this.delete();
        break;
      case ACTION_TYPES.cancel:
        this.cancel();
        break;
      case ACTION_TYPES.save:
        this.save();
        break;
      default:
    }
  }
}
