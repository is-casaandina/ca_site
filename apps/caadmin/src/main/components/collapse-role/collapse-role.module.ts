import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CollapseActionButtonsModule } from '@ca-admin/components/collapse-action-buttons/collapse-action-buttons.module';
import { CaCheckboxModule, CaInputModule } from '@ca-core/ui/lib/components';
import { CollapseRoleComponent } from './collapse-role.component';

@NgModule({
  declarations: [CollapseRoleComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CollapseActionButtonsModule,
    CaInputModule,
    CaCheckboxModule
  ],
  exports: [CollapseRoleComponent]
})
export class CollapseRoleModule { }
