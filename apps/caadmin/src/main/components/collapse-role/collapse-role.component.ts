import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CollapseActionButtonsComponent } from '@ca-admin/components/collapse-action-buttons/collapse-action-buttons.component';
import { PermissionService, RoleService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { RoleLang } from '@ca-admin/settings/lang/role.lang';
import { IPermissionResponse } from '@ca-admin/statemanagement/models/permission.interface';
import { IDeleteRoleRequest, IRolePermissionsDetailView, IUpdateRoleRequest } from '@ca-admin/statemanagement/models/role.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-collapse-role',
  templateUrl: './collapse-role.component.html'
})
export class CollapseRoleComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild(CollapseActionButtonsComponent) collapseActionButtons: CollapseActionButtonsComponent;

  @Input() role: IRolePermissionsDetailView;
  @Input() disabled: boolean;

  GENERAL_LANG = GeneralLang;
  ROLE_LANG = RoleLang;

  frmRole: FormGroup;
  confirmDelete: string;
  roleText: AbstractControl;
  permissionsCheck: FormArray;
  permissionsList: Array<IPermissionResponse>;

  constructor(
    private _formBuilder: FormBuilder,
    private _permissionService: PermissionService,
    private _roleService: RoleService,
    private _notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.confirmDelete = `${this.GENERAL_LANG.modalConfirm.titleDelete} el rol ${this.role.roleName}?`;
    this._createForm();
  }

  private _createForm(): void {
    this.frmRole = this._formBuilder.group({
      roleText: [this.role.roleName, Validators.required],
      permissionsCheck: this._formBuilder.array([])
    });
    this.roleText = this.frmRole.get('roleText');
    this.permissionsCheck = this.frmRole.get('permissionsCheck') as FormArray;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmRole, controlName);
  }

  edit(): void {
    this._permissionService.permissionsList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IPermissionResponse>) => {
        this.permissionsList = response;
        this._createPermissionsCheckControl();
        this._roleService.setRolesList(ACTION_TYPES.edit, this.role);
      });
  }

  private _createPermissionsCheckControl(): void {
    this.permissionsList.forEach((fv, fk) => {
      const checked = this.role.permissionCodeList.find((sv, sk) => {
        return fv.permissionCode === sv.permissionCode;
      });
      this.permissionsCheck.setControl(fk, this._formBuilder.control(!!checked));
    });
  }

  private _paramsDeleteRole(): IDeleteRoleRequest {
    return {
      roleCode: this.role.roleCode
    } as IDeleteRoleRequest;
  }

  delete(): void {
    const params = this._paramsDeleteRole();
    this._roleService.deleteRole(params, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(() => {
        this._roleService.setRolesList(ACTION_TYPES.delete, this.role);
      });
  }

  cancel(): void {
    this._roleService.setRolesList(ACTION_TYPES.cancel, this.role);
  }

  private _getPermissionsList(): Array<IPermissionResponse> {
    const permissionsList = [];
    this.permissionsCheck.controls.forEach((fv, fk) => {
      if (fv.value) { permissionsList.push(this.permissionsList[fk]); }
    });

    return permissionsList;
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmRole);

    return this.frmRole.valid;
  }

  private _paramsUpdateRole(): IUpdateRoleRequest {
    const getPermissionsCode = () => {
      return this._getPermissionsList()
        .map((fv, fk) => {
          return fv.permissionCode;
        });
    };

    return {
      roleName: this.roleText.value,
      permissionCodeList: getPermissionsCode()
    } as IUpdateRoleRequest;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsUpdateRole();
      this._roleService.updateRole(this.role.roleCode, params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.collapseActionButtons.toggle();
          this._setRole();
        });
    } else {
      this._notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  private _setRole(): void {
    this.role.roleName = this.roleText.value;
    this.role.permissionCodeList = this._getPermissionsList();
    this._roleService.setRolesList(ACTION_TYPES.cancel, this.role);
  }

  actionButton(event: string): void {
    switch (event) {
      case ACTION_TYPES.edit:
        this.edit();
        break;
      case ACTION_TYPES.delete:
        this.delete();
        break;
      case ACTION_TYPES.cancel:
        this.cancel();
        break;
      case ACTION_TYPES.save:
        this.save();
        break;

      default:
    }
  }

}
