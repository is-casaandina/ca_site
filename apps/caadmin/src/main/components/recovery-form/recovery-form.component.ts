import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@ca-admin/services';
import { IMAGES } from '@ca-admin/settings/constants/images.constant';
import { LoginLang } from '@ca-admin/settings/lang/login.lang';
import { IRecoveryRequest } from '@ca-admin/statemanagement/models/recovery.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-recovery-form',
  templateUrl: './recovery-form.component.html',
  styleUrls: ['./recovery-form.component.scss']
})
export class RecoveryFormComponent extends UnsubscribeOnDestroy implements OnInit {
  frmRecovery: FormGroup;
  email: AbstractControl;
  invalidUser: boolean;

  IMAGES = IMAGES;
  LOGIN_LANG = LoginLang;

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _notificationService: NotificationService,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmRecovery = this._formBuilder.group({
      email: [null, [Validators.required, Validators.email]]
    });
    this.email = this.frmRecovery.get('email');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmRecovery, controlName);
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmRecovery);

    return this.frmRecovery.valid;
  }

  private _paramsRecovery(): IRecoveryRequest {
    return {
      email: this.email.value
    } as IRecoveryRequest;
  }

  sendMail(): void {
    const validateForm = this._validateForm();
    this.invalidUser = false;
    if (validateForm) {
      const recovery = this._paramsRecovery();

      this._authService
        .sendMail(recovery, true)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            this._router.navigate(['/login']);
            this._notificationService.addSuccess(res.message);
          },
          () => {
            this.invalidUser = true;
          }
        );
    }
  }
}
