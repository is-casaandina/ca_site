import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CaAlertErrorModule, CaButtonModule, CaInputModule, CaSwitchModule } from '@ca-core/ui/lib/components';
import { RecoveryFormComponent } from './recovery-form.component';

@NgModule({
  declarations: [RecoveryFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    CaSwitchModule,
    CaButtonModule,
    CaAlertErrorModule
  ],
  exports: [RecoveryFormComponent]
})
export class RecoveryFormModule { }
