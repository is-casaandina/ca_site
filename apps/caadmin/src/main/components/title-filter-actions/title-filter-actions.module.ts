import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CaAutocompleteModule, CaButtonModule, CaCardModule, CaRadioButtonModule } from '@ca-core/ui/lib/components';
import { TitleFilterActionsComponent } from './title-filter-actions.component';

@NgModule({
  declarations: [TitleFilterActionsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaAutocompleteModule,
    CaCardModule,
    CaRadioButtonModule,
    CaButtonModule
  ],
  exports: [TitleFilterActionsComponent],
  entryComponents: [TitleFilterActionsComponent]
})
export class TitleFilterActionsModule { }
