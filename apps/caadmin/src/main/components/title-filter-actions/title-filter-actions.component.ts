import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GeneralLang } from '@ca-admin/settings/lang';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';

@Component({
  selector: 'caadmin-title-filter-actions',
  templateUrl: './title-filter-actions.component.html'
})
export class TitleFilterActionsComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  @Input() title: string;
  @Input() showButtons: boolean;
  @Output() apply: EventEmitter<Boolean>;
  @Output() clean: EventEmitter<Boolean>;

  constructor(
  ) {
    super();
    this.apply = new EventEmitter<Boolean>();
    this.clean = new EventEmitter<Boolean>();
  }

  ngOnInit(): void {
  }

  onClickApply(): void {
      this.apply.emit(true);
  }

  onClickClean(): void {
    this.clean.emit(true);
  }

}
