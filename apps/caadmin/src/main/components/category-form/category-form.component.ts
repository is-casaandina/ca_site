import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ICategoryRequest } from '@ca-admin/statemanagement/models/category.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-category-form',
  templateUrl: './category-form.component.html'
})
export class CategoryFormComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  frmCategory: FormGroup;
  name: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private categoryService: CategoryService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.frmCategory = this.fb.group({
      name: [null, Validators.required],
      system: [false],
      viewPage: [true]
    });

    this.name = this.frmCategory.get('name');
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmCategory);

    return this.frmCategory.valid;
  }

  private paramsSaveUser(): ICategoryRequest {
    return this.frmCategory.value as ICategoryRequest;
  }

  save(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveUser();
      this.categoryService.saveCategory(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
