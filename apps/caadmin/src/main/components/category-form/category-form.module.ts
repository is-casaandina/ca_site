import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaInputModule } from '@ca-core/ui/lib/components';
import { CategoryFormComponent } from './category-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    ModalModule,
    CaButtonModule
  ],
  declarations: [CategoryFormComponent],
  entryComponents: [CategoryFormComponent],
  exports: [CategoryFormComponent]
})
export class CategoryFormModule { }
