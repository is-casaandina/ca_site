import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormModule } from '@ca-admin/components/dynamic-form';
import { TabsLanguageModule } from '@ca-admin/components/tabs-language/tabs-language.module';
import { CaInputModule } from '@ca-core/ui/lib/components';
import { EditorContactComponent } from './editor-contact.component';

@NgModule({
  imports: [
    CommonModule,
    TabsLanguageModule,
    CaInputModule,
    ReactiveFormsModule,
    DynamicFormModule
  ],
  declarations: [EditorContactComponent],
  exports: [EditorContactComponent],
  entryComponents: [EditorContactComponent]
})
export class EditorContactModule { }
