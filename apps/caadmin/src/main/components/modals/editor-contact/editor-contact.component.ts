import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { DynamicFormComponent } from '@ca-admin/components/dynamic-form';
import { PageService } from '@ca-admin/services';
import { LANGUAGE_TYPES, LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { STRUCTURE_COMPONENT_EDIT_CONTACT } from '@ca-admin/settings/constants/structures.contant';
import { IComponent } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { IContactDetailPage, IContactLanguageDetail, IPageResponse } from '@ca-admin/statemanagement/models/page.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { AngularUtil } from '@ca-core/shared/helpers/util';

@Component({
  selector: 'caadmin-editor-contact',
  templateUrl: './editor-contact.component.html'
})
export class EditorContactComponent implements OnInit {

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  title1: FormControl;
  title2: FormControl;

  languages: Array<IContactLanguageDetail> = [];

  language: string;

  page: IPageResponse;

  structure: Array<IComponent> = null;

  constructor(
    private fb: FormBuilder,
    private pageService: PageService,
    private activeModal: ActiveModal,
    private spinnerService: SpinnerService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.page = this.pageService.getPageEditor();
    if (!this.page.contact) {
      this.title1 = this.fb.control(null);
      this.title2 = this.fb.control(null);
      LANGUAGE_TYPES.forEach(language => {
        this.languages.push({ language: language.code });
      });
    } else {
      this.title1 = this.fb.control(this.page.contact.title1 || '');
      this.title2 = this.fb.control(this.page.contact.title2 || '');
      this.languages = this.page.contact.languages;
    }
    this.getLanguageData();
  }

  changeLanguage(language: string): void {
    this.setLanguageData();
    this.language = language;
    this.getLanguageData();
  }

  private setLanguageData(): void {
    const data = this.languages.find(item => item.language === (this.language || LANGUAGE_TYPES_CODE.es));

    if (this.dynamicForm) {
      data.details = (this.dynamicForm.getFormValue() as any).contact.details;
    }
  }

  private getLanguageData(): void {
    const data = this.languages.find(item => item.language === (this.language || LANGUAGE_TYPES_CODE.es));
    this.setDataForm(data);
  }

  close(): void {
    this.activeModal.dismiss();
  }

  save(): void {
    this.setLanguageData();
    const pageEditor = this.pageService.getPageEditor();
    const contact: IContactDetailPage = {
      title1: this.title1.value,
      title2: this.title2.value,
      languages: this.languages
    };

    pageEditor.contact = contact;

    this.pageService.setPageEditor(pageEditor);
    this.pageService.reloadPublicationEdit()
      .next();
    this.activeModal.close();
  }

  private setDataForm(data: IContactLanguageDetail): void {
    this.structure = null;
    this.spinnerService.showSpinner();
    const structure = AngularUtil.clone(STRUCTURE_COMPONENT_EDIT_CONTACT) as IComponent;
    structure.properties = structure.properties.map(property => {
      const value = data[property.code];
      property.value = value;

      return property;
    });
    setTimeout(() => this.structure = [structure], 100);
    setTimeout(() => this.spinnerService.hideSpinner(), 500);
  }
}
