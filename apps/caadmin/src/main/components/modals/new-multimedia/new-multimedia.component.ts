import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FileService } from '@ca-admin/services';
import { FILE_TYPE, IMultimediaResponse } from '@ca-admin/statemanagement/models/file.interface';
import { MultimediaBase } from '@ca-admin/views/multimedia/multimedia-base';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';

@Component({
  selector: 'caadmin-new-multimedia',
  templateUrl: './new-multimedia.component.html'
})
export class NewMultimediaComponent extends MultimediaBase implements OnInit {

  FILE_TYPE = FILE_TYPE;

  payload: any;

  constructor(
    protected notificationService: NotificationService,
    protected fileService: FileService,
    protected fb: FormBuilder,
    protected activeModal: ActiveModal,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(notificationService, fileService, fb, document);
  }

  ngOnInit(): void {
    if (this.payload.file) {
      this.getPreview(this.payload.file, this.payload.property);
      this.createForm();
    } else {
      this.createForm(this.payload.currentImage);
      this.tmpFile = this.payload.currentImage;
    }
  }

  selected(file: IMultimediaResponse, message: string): void {
    this.activeModal.close(message);
  }

  upload(): void {
    if (this.payload.currentImage) {
      this.update();
    } else {
      this.uploadFile(this.payload.file, this.payload.property);
    }
  }

  back(): void {
    this.activeModal.dismiss();
  }

  update(): void {
    this.payload.currentImage.properties = this.form.value;

    this.fileService.updateImage(this.payload.currentImage, true)
      .subscribe(res => {
        this.activeModal.close(res.message);
      });
  }

  getSizeImg(event: any): void {
    this.tmpFile.width = event.target.naturalWidth;
    this.tmpFile.height = event.target.naturalHeight;
  }
}
