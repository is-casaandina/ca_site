import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { CaInputModule } from '@ca-core/ui/lib/components';
import { VideoModule } from '@ca/library';
import { NewMultimediaComponent } from './new-multimedia.component';

@NgModule({
  imports: [
    CommonModule,
    VideoModule,
    PipesModule,
    CaInputModule,
    ReactiveFormsModule
  ],
  declarations: [NewMultimediaComponent],
  exports: [NewMultimediaComponent],
  entryComponents: [NewMultimediaComponent]
})
export class NewMultimediaModule { }
