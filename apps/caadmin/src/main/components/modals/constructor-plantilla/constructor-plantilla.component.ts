import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConstructorTemplate } from '@ca-admin/services/constructor_template.service'
import { CATEGORY, LANGUAGE_TYPES, LANGUAGE_TYPES_CODE, PAGE_CODE_STATES, TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { PAGE_TYPES_CODE } from '@ca-admin/settings/constants/page.constant';
import { IPageByLanguage, IPageResponse } from '@ca-admin/statemanagement/models/page.interface';
import { ConstrutorPlantilla } from '@ca-admin/statemanagement/models/constructor_plantilla.interface';

import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { catchError, map, switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-constructor-plantilla',
  templateUrl: './constructor-plantilla.component.html'
})
export class ConstructorPlantillaComponent extends UnsubscribeOnDestroy implements OnInit {

  languages: Array<IPageByLanguage> = [];

  title: FormControl;
  path: FormControl;
  code: FormControl
  messageRegister: string
  language: string;
  texto_titulo: string;
  texto_code: string;

  page: IPageResponse;
  pageConstructor: ConstrutorPlantilla;
  differentials
  components: Array<Object> = [

    { code: 'slider-banner', name: 'Componente slider banner' },
    { code: 'search', name: 'Componente de búsqueda' },
    { code: 'information-brand', name: 'Componente de información' },
    { code: 'hotel-summary', name: 'Componente Hotel Resumen' },
    { code: 'gallery', name: 'Componente de Gallery' },
    { code: 'rooms', name: 'Componente Rooms' },
    { code: 'paragraph-full', name: 'Componente HTML y CSS' },
    { code: 'hotel-rest-promotion', name: 'Componente Hotel/Restaurantes promoción' },


    /*{ code: 'differentials', name: 'Componente de differentials' },
    { code: 'slider-promotions', name: 'Componente slider promocional' },
    { code: 'information-brand', name: 'Componente de información' },
    { code: 'slider-product', name: 'Componente slider de producto' },*/

  ]

  dataSource: any
  dataSources: any
  subject: BehaviorSubject<any>
  detalleComponent: Array<any>
  addComponent: FormGroup

  selectedComponent: any;
  selectedComponentDefault: any;
  concatHTML: string;
  required: string
  constructor(
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private ConstructorTemplate: ConstructorTemplate,
    private activeModal: ActiveModal,
    private spinnerService: SpinnerService
  ) {
    super();
    this.title = this.fb.control(null);
    this.code = this.fb.control(null);
    this.pageConstructor = new ConstrutorPlantilla('', '', '', '', '', '', [])
    this.messageRegister = "";
    this.required = ""
    //this.texto = ""

  }

  ngOnInit(): void {
    this.selectedComponent = this.components[0]
    this.detalleComponent = [];
    this.subject = new BehaviorSubject(this.detalleComponent);
    this.dataSource = this.subject.asObservable();
    this.dataSources = this.dataSource.source.value
    this.validateForm()
  }


  close(): void {
    this.activeModal.dismiss();
  }

  validateForm() {


  }

  getComponent() {

    this.selectedComponentDefault = this.selectedComponent

    this.detalleComponent.push(this.selectedComponent)
    this.subject.next(this.detalleComponent)

    console.log(this.detalleComponent.length)
  }

  deleteComponent(i: number) {

    this.detalleComponent.splice(i, 1);
    
  }



  savePlantilla() {
    let slider_banner = "<section class='g-section'> <!-- Slider Principal --> <ca-slider-banner keyc banner-size='LG' is-animation='true' sliders='[ { \"type\": \"formImage\", \"background-image\": [ { \"name\": \"55555.jpg\", \"properties\": { \"title\": \"\", \"alt\": \"\", \"description\": \"\" }, \"small\": { \"name\": \"480X270_55555.jpg\", \"width\": 480, \"height\": 270, \"imageUri\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/480X270_55555.jpg\", \"size\": 131248 }, \"medium\": { \"name\": \"960X540_55555.jpg\", \"width\": 960, \"height\": 540, \"imageUri\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/960X540_55555.jpg\", \"size\": 396601 }, \"large\": { \"name\": \"1920X1080_55555.jpg\", \"width\": 1920, \"height\": 1080, \"imageUri\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/1920X1080_55555.jpg\", \"size\": 162344 } } ], \"is-frame\": true, \"align-image\": \"center\", \"align\": \"center\", \"subtitle\": \"MÁS QUE UN VIAJE, UNA EXPERIENCIA\", \"title\": \"MEMORABLE\", \"color-title\": \"Dorado\", \"is-button\": false, \"type-descriptor\": \"\", \"descriptor\": \"\", \"text-button\": \"\" }, { \"type\": \"formVideo\", \"background-video\": { \"mimeType\": \"video/mp4\", \"name\": \"cancion_para_prueba.mp4\", \"properties\": { \"title\": \"esto es un titulo\", \"alt\": \"alturaaa\", \"description\": \"descripcion\" }, \"url\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/video/cancion_para_prueba.mp4\", \"size\": 4142126, \"small\": { \"name\": \"160X88_cancion_para_prueba_frame80.png\", \"width\": 160, \"height\": 88, \"imageUri\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/160X88_cancion_para_prueba_frame80.png\", \"size\": 39107 }, \"medium\": { \"name\": \"320X176_cancion_para_prueba_frame80.png\", \"width\": 320, \"height\": 176, \"imageUri\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/320X176_cancion_para_prueba_frame80.png\", \"size\": 46681 }, \"large\": { \"name\": \"640X352_cancion_para_prueba_frame80.png\", \"width\": 640, \"height\": 352, \"imageUri\": \"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/640X352_cancion_para_prueba_frame80.png\", \"size\": 414226 }, \"createDate\": \"2019-05-28T14:28:11.219\" }, \"is-frame\": true, \"align-image\": \"center\", \"align\": \"center\", \"subtitle\": \"MÁS QUE UN VIAJE, UNA EXPERIENCIA\", \"title\": \"MEMORABLE\", \"color-title\": \"Dorado\", \"is-button\": false, \"type-descriptor\": \"\", \"descriptor\": \"\", \"text-button\": \"\" } ]'></ca-slider-banner> </section>"
    let search = "<section class='g-section'> <div class='container'> <!-- Formulario reservas --> <ca-search keyc></ca-search> </div> </section>"
    let information_brand = "<section master class='g-section py-4 py-md-6 g-bgc-gray7'> <!-- Por qué Casa Andina --> <ca-information-brand keyc title='¿Por qué Casa Andina?' description='Somos una cadena hotelera peruana fundada en febrero de 2003, enfocada en proporcionar a nuestros huéspedes una experiencia de viaje. Integramos las particularidades de cada destino donde nos encontramos desde arquitectura, decorazión, gastronomía, actividades y música, ofreciendo así a nuestros visitantes una experiencia de viaje única.' text-link='SABER MÁS' badge1='https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/icons/100X100_TC_2018_100x100.png' badge2='https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/icons/100X100_TRIPADVISOR_TRAVELLERS_CHOICE_2019_100x100.png' badge3='https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/icons/100X100_tripadvisor2_100x100.png' badge4='https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/icons/100X100_TC_2018_100x100.png' background-image='[{\"id\":\"5d1e7e3972bf310a1ccdb71d\",\"name\":\"Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"260X172_Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"width\":260,\"height\":172,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/260X172_Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"size\":115718},\"medium\":{\"name\":\"520X345_Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"width\":520,\"height\":345,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/520X345_Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"size\":430885},\"large\":{\"name\":\"1040X690_Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"width\":1040,\"height\":690,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/1040X690_Casa_andina_-_Hotel_en_Cusco_Valle_Sagrado_-Peru_-Cusco_Valle_Sagrado_-_balcón.JPG\",\"size\":281172},\"createDate\":\"2019-07-04T17:31:19.245\",\"updateDate\":null}]' detail-image='[{\"id\":\"5d1e81ed72bf310a1ccdb71f\",\"name\":\"casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"mimeType\":\"image/png\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"141X65_casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"width\":141,\"height\":65,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/141X65_casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"size\":18186},\"medium\":{\"name\":\"283X130_casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"width\":283,\"height\":130,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/283X130_casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"size\":60482},\"large\":{\"name\":\"567X260_casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"width\":567,\"height\":260,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/567X260_casa-andina-peru-hoteles-en-arequipa-select-arequipa-2-2000-x-644-trapecio.png\",\"size\":176904},\"createDate\":\"2019-07-04T17:47:09.021\",\"updateDate\":null}]' > </ca-information-brand> </section>"
    let hotel_summary = "<section class='g-section pt-xl-10 py-md-6 py-4 g-section--texture'><div class='container'> <ca-hotel-summary keyc datac description='Ubicado en el barrio de San Blas apartado de la zona agitada de Cusco. Como una tranquila casa de campo en plena ciudad, Casa Andina Standard Cusco San Blas alberga 41 cómodas y acogedoras habitaciones 100% no fumadoras levantadas en torno a un antiguo patio de piedra. Desde aquí podrás obtener las mejores vistas de los alrededores de Cusco. Disfruta de los extras gratuitos que ofrecemos como: desayuno buffet, servicios en la habitación, conexión WiFi. Contamos con personal altamente capacitado que podrá recomendar los mejores lugares en donde podrás probar la comida y bebida tradicional local.' description-living-room='Somos una cadena hotelera peruana fundada en febrero de 2003, enfocada en proporcionar a nuestros huéspedes una experiencia de viaje. Integramos las particularidades de cada destino donde nos encontramos, desde arquitectura, decoración, gastronomía, actividades y música.' is-ubication='true' map-url='' background-image-map='[{\"id\":\"5d1cf7d246b6a710e07dfafd\",\"name\":\"mapa.jpg\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"245X155_mapa.jpg\",\"width\":245,\"height\":155,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediadev.casa-andina.com/images/245X155_mapa.jpg\",\"size\":61116},\"medium\":{\"name\":\"491X311_mapa.jpg\",\"width\":491,\"height\":311,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediadev.casa-andina.com/images/491X311_mapa.jpg\",\"size\":226579},\"large\":{\"name\":\"983X622_mapa.jpg\",\"width\":983,\"height\":622,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediadev.casa-andina.com/images/983X622_mapa.jpg\",\"size\":104937},\"createDate\":\"2019-07-03T13:45:37.376\"}]' is-traslate='true' title-traslate='Horarios de traslado' description-traslate='<p>El servicio de traslado deberá ser coordinado 48 horas antes de su llegada y está sujeto a los horarios establecidos a continuación. Además, deberá ser coordinado enviando un correo a recep-castarequipa@casa-andina.com indicando código de reserva, número de vuelo y hora de llegada.</p>' tables='[ { \"title\":\"Lunes a Viernes\", \"calendar\":{\"head\":[\"Línea Aérea\"],\"body\":[[\"LATAM\"]]} } ]' restrictions='<ul><li>Los traslados Hotel - Aeropuerto tendrán un costo adicional.</li></ul>' is-image-total='true' image-total-url='' image-main='[{\"id\":\"5d1cfc5b46b6a710e07dfaff\",\"name\":\"group-16.jpg\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"111X167_group-16.jpg\",\"width\":111,\"height\":167,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediadev.casa-andina.com/images/111X167_group-16.jpg\",\"size\":48468},\"medium\":{\"name\":\"222X335_group-16.jpg\",\"width\":222,\"height\":335,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediadev.casa-andina.com/images/222X335_group-16.jpg\",\"size\":174823},\"large\":{\"name\":\"445X670_group-16.jpg\",\"width\":445,\"height\":670,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediadev.casa-andina.com/images/445X670_group-16.jpg\",\"size\":111322},\"createDate\":\"2019-07-03T14:04:58.664\"}]'> </ca-hotel-summary></div> </section>"
    let gallery = "<section class='g-section g-section--padding-60 pt-4 pb-md-6 g-bgc-gray7'> <ca-gallery keyc title='Disfruta Arequipa en nuestra casa' sliders='[{\"items\":[{\"type\":null},{\"type\":null},{\"type\":null}]}]'></ca-gallery> </section> "
    let rooms = "<section master class='g-section g-section--padding-60 g-bgc-gray7'><div class='container'> <ca-rooms keyc datac title='Las habitaciones de la casa' rooms='[{\"title\":\"MATRIMONIAL SUPERIOR\",\"description\":\"<p>Somos una cadena peruana hotelera peruana fundada en febrero de 2003, enfocada en proporcionar a nuestros huéspedes una experiencia de viaje.</p>\", \"description-special\":\"<p>1 Cama King | 25 m2 | Vista a la calle</p>\"},{\"title\":\"SUPERIOR DOBLE\",\"description\":\"<p>Somos una cadena peruana hotelera peruana fundada en febrero de 2003, enfocada en proporcionar a nuestros huéspedes una experiencia de viaje.</p>\",\"description-special\":\"<p>1 Cama King | 25 m2 | Vista a la calle</p>\"}]'> </ca-rooms></div> </section>"
    let hotel_rest_promotion = "<section class='g-section g-section--padding-60 g-bgc-gray7'> <div class='container'> <!-- Oportunidades para Aventurarse --> <ca-hotel-rest-promotion keyc datac title='Oportunidades para aventurarte' items='[ { \"type\":\"Hoteles\",\"country\":\"5cf19ebd08308e3ea4a8725f\",\"destination\":null,\"descriptor\":\"5cf69c4c53f6c7394c0012af\",\"hotel\":null, \"url-reservation\":null,\"price\":120,\"discount\":30,\"days\":5,\"nights\":4, \"is-restriction\":true, \"image-restriction\":[{\"id\":\"5d2e3e3e46e0fb0001667cf9\",\"name\":\"Promocion_Slider.jpg\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"360X115_Promocion_Slider.jpg\",\"width\":360,\"height\":115,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/360X115_Promocion_Slider.jpg\",\"size\":58841},\"medium\":{\"name\":\"720X230_Promocion_Slider.jpg\",\"width\":720,\"height\":230,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/720X230_Promocion_Slider.jpg\",\"size\":200736},\"large\":{\"name\":\"1440X460_Promocion_Slider.jpg\",\"width\":1440,\"height\":460,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/1440X460_Promocion_Slider.jpg\",\"size\":131985},\"createDate\":\"2019-07-16T21:14:37.572\",\"updateDate\":null}], \"body-restriction\":\"<h2><strong>Almuerzo buffete marino</strong></h2><p>Todos los domingos disfruta en familia estación de ceviches en vivo, tiraditos, casas, arroz con mariscos, timbal, sudados, variados fondos y estación de postres.</p><p><br></p><h3><strong>INCLUYE: BARRA LIBRE DE CHILCANOS</strong></h3><h3><strong>ESTACIÓN DE HELADOS HAANGEN DAZ</strong></h3><p><br></p><p><strong>Domingo de 1:00 p.m. a 4:00 p.m.</strong></p><p><strong>S/. 65.00 soles por persona</strong></p>\", \"footer-restriction\":\"<p>Por favor reservar con 24 horas de anticipación, imagen referencial. Precio por 2 personas. Promoción vigente hasta el 30/06/2019. No válida para feriados o días festivos.</p><p>Capacidad limitada.Reservas de más de 15 personas requieren un prepago del 50%. No acumulable con otras promociones y/o descuentos. Válido solo en el horario mencionado. Precio por adulto:S/. 54. Niños de 0 a 7 no pagan, de 8 a 12 años pagan S/. 28 cada uno y a partir de 13 años pagan como adulto. Precio sujeto a cambios sin previo aviso.</p>\", \"background-image\":[{\"id\":\"5d0a9dcd46b6a72eb88ec32f\",\"name\":\"55555.jpg\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"480X270_55555.jpg\",\"width\":480,\"height\":270,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/480X270_55555.jpg\",\"size\":375130},\"medium\":{\"name\":\"960X540_55555.jpg\",\"width\":960,\"height\":540,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/960X540_55555.jpg\",\"size\":1429515},\"large\":{\"name\":\"1920X1080_55555.jpg\",\"width\":1920,\"height\":1080,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/1920X1080_55555.jpg\",\"size\":739603},\"createDate\":\"2019-06-19T15:40:41.93\"}], \"description\":\"<p>Descripción personalizada</p>\" }, { \"type\":\"Restaurantes\",\"country\":\"5cf19ebd08308e3ea4a8725f\",\"destination\":null,\"descriptor\":\"5cfe74f18cc8128fc85cd58d\",\"restaurant\":null, \"url-reservation\":null,\"price\":120,\"discount\":30, \"is-restriction\":true, \"image-restriction\":[{\"id\":\"5d2e3e3e46e0fb0001667cf9\",\"name\":\"Promocion_Slider.jpg\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"360X115_Promocion_Slider.jpg\",\"width\":360,\"height\":115,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/360X115_Promocion_Slider.jpg\",\"size\":58841},\"medium\":{\"name\":\"720X230_Promocion_Slider.jpg\",\"width\":720,\"height\":230,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/720X230_Promocion_Slider.jpg\",\"size\":200736},\"large\":{\"name\":\"1440X460_Promocion_Slider.jpg\",\"width\":1440,\"height\":460,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/1440X460_Promocion_Slider.jpg\",\"size\":131985},\"createDate\":\"2019-07-16T21:14:37.572\",\"updateDate\":null}], \"body-restriction\":\"<h2><strong>Almuerzo buffete marino</strong></h2><p>Todos los domingos disfruta en familia estación de ceviches en vivo, tiraditos, casas, arroz con mariscos, timbal, sudados, variados fondos y estación de postres.</p><p><br></p><h3><strong>INCLUYE: BARRA LIBRE DE CHILCANOS</strong></h3><h3><strong>ESTACIÓN DE HELADOS HAANGEN DAZ</strong></h3><p><br></p><p><strong>Domingo de 1:00 p.m. a 4:00 p.m.</strong></p><p><strong>S/. 65.00 soles por persona</strong></p>\", \"footer-restriction\":\"<p>Por favor reservar con 24 horas de anticipación, imagen referencial. Precio por 2 personas. Promoción vigente hasta el 30/06/2019. No válida para feriados o días festivos.</p><p>Capacidad limitada.Reservas de más de 15 personas requieren un prepago del 50%. No acumulable con otras promociones y/o descuentos. Válido solo en el horario mencionado. Precio por adulto:S/. 54. Niños de 0 a 7 no pagan, de 8 a 12 años pagan S/. 28 cada uno y a partir de 13 años pagan como adulto. Precio sujeto a cambios sin previo aviso.</p>\", \"background-image\":[{\"id\":\"5d0a9dcd46b6a72eb88ec32f\",\"name\":\"55555.jpg\",\"mimeType\":\"image/jpeg\",\"properties\":{\"title\":\"\",\"alt\":\"\",\"description\":\"\"},\"small\":{\"name\":\"480X270_55555.jpg\",\"width\":480,\"height\":270,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/480X270_55555.jpg\",\"size\":375130},\"medium\":{\"name\":\"960X540_55555.jpg\",\"width\":960,\"height\":540,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/960X540_55555.jpg\",\"size\":1429515},\"large\":{\"name\":\"1920X1080_55555.jpg\",\"width\":1920,\"height\":1080,\"imageUri\":\"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/1920X1080_55555.jpg\",\"size\":739603},\"createDate\":\"2019-06-19T15:40:41.93\"}], \"description\":\"<p>Descripción personalizada.</p>\" } ]' > </ca-hotel-rest-promotion> </div> </section>"
    let paragraph_full = "<section master class='g-section py-4 py-md-6'><div class='container'> <ca-paragraph-full keyc description='<h1>Titulo ejemplo</h1><h2>Titulo 2 ejemplo</h2><p>Ingresar información personalizada. Puede definir muchos parrafos de texto.</p>'> </ca-paragraph-full></div> </section>"

    let differentials = '</section> <section class="g-section"> <div class="container"> <!-- Formulario reservas --> <ca-search keyc></ca-search> </div> </section> <section class="g-section g-section--texture py-3 py-sm-4 py-lg-7"> <div class="container"> <!-- Diferenciales --> <ca-differentials keyc items="[{"icon":"la la-bed", "subtitle":"29", "title":"Hoteles"}, {"icon":"la la-compass", "subtitle":"18", "title":"Destinos"}, {"icon":"la la-cutlery", "subtitle":"27", "title":"Restaurantes"}, {"icon":"la la-hand-peace-o", "subtitle":"Hospitalidad", "title":"Peruana"}, {"icon":"la la-map-marker", "subtitle":"Excelente", "title":"Ubicación"}]"></ca-differentials> </div> </section>'
    let benefits = '<section class="g-section py-4 g-bgc-gray7"> <div class="container"> <!-- Beneficios --> <ca-benefits keyc></ca-benefits> </div> </section>'
    let slider_promotions = '<section class="g-section g-section--padding-90 g-bgc-gray1" style="background-color: #232323!important"><ca-slider-promotions keyc title="Oportunidades para aventurarte" sliders="[ { "type": null } ]" text-link="MÁS PROMOCIONES" url-link=""></ca-slider-promotions> </section>'
    let slider_product = '<section class="g-section g-section--padding-60" id="background-color-descriptor"> <ca-slider-product keyc title="Productos" sliders="[ { "title":"Desayuno buffete", "description":"Disfruta de una gran variedad de jugos, frutas, quesos, jamones, panes, estación de calientes y más.", "background-image":[{"id":"5d1d29e446b6a752cc436b76","name":"Products.jpg","mimeType":"image/jpeg","properties":{"title":"","alt":"","description":""},"small":{"name":"230X87_Products.jpg","width":230,"height":87,"imageUri":"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/230X87_Products.jpg","size":59722},"medium":{"name":"460X175_Products.jpg","width":460,"height":175,"imageUri":"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/460X175_Products.jpg","size":211394},"large":{"name":"920X350_Products.jpg","width":920,"height":350,"imageUri":"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/920X350_Products.jpg","size":90268},"createDate":"2019-07-03T17:19:14.174","updateDate":null}] }, { "title":"Desayuno buffete", "description":"Disfruta de una gran variedad de jugos, frutas, quesos, jamones, panes, estación de calientes y más.", "background-image":[{"id":"5d1d29e446b6a752cc436b76","name":"Products.jpg","mimeType":"image/jpeg","properties":{"title":"","alt":"","description":""},"small":{"name":"230X87_Products.jpg","width":230,"height":87,"imageUri":"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/230X87_Products.jpg","size":59722},"medium":{"name":"460X175_Products.jpg","width":460,"height":175,"imageUri":"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/460X175_Products.jpg","size":211394},"large":{"name":"920X350_Products.jpg","width":920,"height":350,"imageUri":"https://s3.us-east-1.amazonaws.com/multimediaqa.casa-andina.com/system/images/920X350_Products.jpg","size":90268},"createDate":"2019-07-03T17:19:14.174","updateDate":null}] } ]"></ca-slider-product> </section> '

    var main1: string = '<main>'
    var main2: string = '</main>'
    var html: string = '';
    var html2: string = '';
    var referenceArray: Array<string> = []
   

    for (let componente of this.detalleComponent) {

      if (componente.code === 'slider-banner') {
        html2 = html2.concat('', slider_banner)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'search') {
        html2 = html2.concat('', search)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'information-brand') {
        html2 = html2.concat('', information_brand)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'hotel-summary') {
        html2 = html2.concat('', hotel_summary)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'gallery') {
        html2 = html2.concat('', gallery)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'rooms') {
        html2 = html2.concat('', rooms)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'paragraph-full') {
        html2 = html2.concat('', paragraph_full)
        referenceArray.push(componente.code)
      }

      if (componente.code === 'hotel-rest-promotion') {
        html2 = html2.concat('', hotel_rest_promotion)
        referenceArray.push(componente.code)
      }

      /* --- ESTO PARA DESPUES     
      
            if (componente.code === 'differentials') {
              html2 = html2.concat('', differentials)
              referenceArray.push(componente.code)
            }
      
            if (componente.code === 'slider-promotions') {
              html2 = html2.concat('', slider_promotions)
              referenceArray.push(componente.code)
            }
      
            if (componente.code === 'information-brand') {
              html2 = html2.concat('', information_brand)
              referenceArray.push(componente.code)
            }
      
            
      
            if (componente.code === 'slider-product') {
              html2 = html2.concat('', slider_product)
              referenceArray.push(componente.code)
            } */



    }

    html2 = main1.concat(html2)
    html2 = html2.concat(main2)


    this.pageConstructor = new ConstrutorPlantilla(
      this.title.value.trim(),
      this.title.value.trim(),
      this.title.value.trim(),
      'f950e1ff-a05c-4c2c-becc-d082499e2590',
      'https://s3.amazonaws.com/multimediadev.casa-andina.com/images/templates/temp-home.jpg',
      html2,
      referenceArray)

    if(this.detalleComponent.length <= 120 ){

    this.ConstructorTemplate.saveConstructor(this.pageConstructor, true)
      .pipe(
        catchError((error) => {
          this.messageRegister = error.originalError.error.message
          return EMPTY;
        })
      ).subscribe((response) => {
        this.messageRegister = response.message;

        this.detalleComponent = [];
        this.subject = new BehaviorSubject(this.detalleComponent);
        this.dataSource = this.subject.asObservable();
        this.dataSources = this.dataSource.source.value

        this.title = this.fb.control(null);
        this.code = this.fb.control(null);
        this.pageConstructor = new ConstrutorPlantilla('', '', '', '', '', '', [])

      }
      );
    }else{
      this.messageRegister = 'No debe sobrepasar los 120 componentes'
    }

  }
  validartitulo(e) {
    if (e.value.trim() == "") {

      this.texto_titulo = 'invalid'
    } else {
      this.texto_titulo = ''
    }

  }
  validarcode(e) {
    if (e.value.trim() == "") {

      this.texto_code = 'invalid'
    } else {
      this.texto_code = ''
    }

  }




}
