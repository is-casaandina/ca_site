import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsLanguageModule } from '@ca-admin/components/tabs-language/tabs-language.module';
import { CaInputModule } from '@ca-core/ui/lib/components';
import { ConstructorPlantillaComponent } from './constructor-plantilla.component';

@NgModule({
  imports: [
    CommonModule,
    TabsLanguageModule,
    CaInputModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [ConstructorPlantillaComponent],
  exports: [ConstructorPlantillaComponent],
  entryComponents: [ConstructorPlantillaComponent]
})
export class ConstructorPlantillaModule { }
