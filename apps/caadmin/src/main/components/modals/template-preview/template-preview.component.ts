import { Component, OnInit } from '@angular/core';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ITemplateResponse } from '@ca-admin/statemanagement/models/template.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';

@Component({
  selector: 'caadmin-template-preview',
  templateUrl: './template-preview.component.html'
})
export class TemplatePreviewComponent implements OnInit, IPayloadModalComponent {

  GENERAL_LANG = GeneralLang;
  ACTION_TYPES = ACTION_TYPES;

  payload: ITemplateResponse;

  constructor(
    private activeModal: ActiveModal
  ) { }

  ngOnInit(): void { }

  close(): void {
    this.activeModal.dismiss();
  }

  select(): void {
    this.activeModal.close();
  }

}
