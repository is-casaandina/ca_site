import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaButtonModule } from '@ca-core/ui/lib/components';
import { TemplatePreviewComponent } from './template-preview.component';

@NgModule({
  declarations: [TemplatePreviewComponent],
  imports: [
    CommonModule,
    CaButtonModule
  ],
  exports: [TemplatePreviewComponent],
  entryComponents: [TemplatePreviewComponent]
})
export class TemplatePreviewModule { }
