import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AutocompleteModule } from '@ca-admin/components/inputs/form-autocomplete/autocomplete.module';
import { CaAutocompleteModule, CaCheckboxModule, CaInputModule } from '@ca-core/ui/lib/components';
import { AddItemMenuComponent } from './add-item-menu.component';

@NgModule({
  imports: [
    CommonModule,
    CaAutocompleteModule,
    AutocompleteModule,
    ReactiveFormsModule,
    CaCheckboxModule,
    CaInputModule
  ],
  declarations: [AddItemMenuComponent],
  entryComponents: [AddItemMenuComponent],
  exports: [AddItemMenuComponent]
})
export class AddItemMenuModule { }
