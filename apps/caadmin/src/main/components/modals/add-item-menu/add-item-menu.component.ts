import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MenuSiteService } from '@ca-admin/services/menu-site.service';
import { LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { IMenuSite, MENU_SITE_ACTION, MENU_SITE_TYPE } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-add-item-menu',
  templateUrl: './add-item-menu.component.html'
})
export class AddItemMenuComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  form: FormGroup;

  payload: {
    list: Array<IMenuSite>,
    children: Array<IMenuSite>,
    current: IMenuSite,
    currentParent: IMenuSite,
    actions: Array<{ text: string, value: string }>,
    type: MENU_SITE_TYPE,
    language: LANGUAGE_TYPES_CODE,
    order: number
  };

  MENU_SITE_TYPE = MENU_SITE_TYPE;
  MENU_SITE_ACTION = MENU_SITE_ACTION;

  constructor(
    private fb: FormBuilder,
    private activeModel: ActiveModal,
    private notificationService: NotificationService,
    private menuService: MenuSiteService
  ) {
    super();
  }

  ngOnInit(): void {
    const parent = (this.payload.current && this.payload.current.parentId) || (this.payload.currentParent && this.payload.currentParent.id);
    this.form = this.fb.group({
      parent: parent || null,
      page: this.payload.current && this.payload.current.pageId || null,
      footer: this.payload.current && this.payload.current.sectionFooter || null,
      name: this.payload.current && this.payload.current.name || null,
      action: this.payload.current && this.payload.current.action || null,
      phone: this.payload.current && this.payload.current.phone || null,
      email: this.payload.current && this.payload.current.email || null
    });
  }

  close(): void {
    this.activeModel.dismiss();
  }

  save(): void {
    const value = this.form.value;
    let error = '';
    if (!value.name) { error = 'Debe completar el campo nombre.'; }
    else if (value.action === MENU_SITE_ACTION.REDIRECT && !value.page) { error = 'Debe completar el campo página.'; }

    const menu: IMenuSite = {
      parentId: value.parent || null,
      action: value.action,
      languageCode: this.payload.language,
      name: value.name,
      type: this.payload.type,
      pageId: value.page,
      sectionFooter: value.footer,
      phone: value.phone,
      email: value.email,
      order: (this.payload.current && this.payload.current.order) > -1 ? this.payload.current.order : this.payload.order
    };

    if (this.payload.current) {
      menu.id = this.payload.current.id;
    }

    if (error) {
      this.notificationService.addWarning(error);
    } else {
      const service$ = this.payload.current ? this.menuService.updateMenu(menu) : this.menuService.addMenu(menu);
      service$
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(res => {
          this.activeModel.close(res);
        });
    }
  }

}
