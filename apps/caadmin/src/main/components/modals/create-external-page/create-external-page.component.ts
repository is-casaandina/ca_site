import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { PageService } from '@ca-admin/services';
import { CATEGORY, LANGUAGE_TYPES, LANGUAGE_TYPES_CODE, PAGE_CODE_STATES, TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { PAGE_TYPES_CODE } from '@ca-admin/settings/constants/page.constant';
import { IPageByLanguage, IPageResponse } from '@ca-admin/statemanagement/models/page.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-create-external-page',
  templateUrl: './create-external-page.component.html'
})
export class CreateExternalPageComponent extends UnsubscribeOnDestroy implements OnInit {

  languages: Array<IPageByLanguage> = [];

  title: FormControl;
  path: FormControl;

  language: string;

  page: IPageResponse;

  constructor(
    private notificationService: NotificationService,
    private fb: FormBuilder,
    private pageService: PageService,
    private activeModal: ActiveModal,
    private spinnerService: SpinnerService
  ) {
    super();
    this.createForm();
  }

  ngOnInit(): void {
  }

  private createForm(): void {
    this.title = this.fb.control(null);
    this.path = this.fb.control(null);

    this.page = this.pageService.getPageStorage();

    if (!this.page) {
      LANGUAGE_TYPES.forEach(language => {
        this.languages.push({ language: language.code });
      });
    } else {
      this.languages = this.page.languages;
      this.getLanguageData();
    }
  }

  changeLanguage(language: string): void {
    this.setLanguageData();
    this.language = language;
    this.getLanguageData();
  }

  private setLanguageData(): void {
    const data = this.languages.find(item => item.language === (this.language || LANGUAGE_TYPES_CODE.es));

    data.title = this.title.value;
    data.path = this.path.value;
  }

  private getLanguageData(): void {
    const data = this.languages.find(item => item.language === (this.language || LANGUAGE_TYPES_CODE.es));

    this.title.setValue(data.title || null);
    this.path.setValue(data.path || null);
  }

  close(): void {
    this.activeModal.dismiss();
  }

  preSave(): void {
    this.setLanguageData();
    const missing = this.languages.find(item => !item.path || !item.title);
    if (!missing) {
      this.save();
    } else {
      this.notificationService.addWarning('El nombre y url deben ser completos en todos los lenguages.');
    }
  }

  private save(): void {
    if (!this.page) {
      this.page = {
        pageType: PAGE_TYPES_CODE.publication,
        type: TYPES_CODE.dynamic,
        state: PAGE_CODE_STATES.published,
        categoryId: CATEGORY.LINK,
        languages: this.languages
      } as any;
    } else {
      this.page.languages = this.languages;
    }

    this.spinnerService.showSpinner();
    this.pageService.savePage(this.page)
      .pipe(
        switchMap(res => this.pageService.publishPage(res.data.id)),
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe(response => {
        this.spinnerService.hideSpinner();
        this.activeModal.close(response);
      }, err => {
        this.spinnerService.hideSpinner();
      });
  }
}
