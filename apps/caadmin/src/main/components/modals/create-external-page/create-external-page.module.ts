import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TabsLanguageModule } from '@ca-admin/components/tabs-language/tabs-language.module';
import { CaInputModule } from '@ca-core/ui/lib/components';
import { CreateExternalPageComponent } from './create-external-page.component';

@NgModule({
  imports: [
    CommonModule,
    TabsLanguageModule,
    CaInputModule,
    ReactiveFormsModule
  ],
  declarations: [CreateExternalPageComponent],
  exports: [CreateExternalPageComponent],
  entryComponents: [CreateExternalPageComponent]
})
export class CreateExternalPageModule { }
