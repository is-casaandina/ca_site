import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NumberDirectiveModule } from '@ca-core/shared/helpers/directives/number.directive';
import { CaCheckboxModule, CaInputModule } from '@ca-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { AddDomainComponent } from './add-domain.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    CaCheckboxModule,
    NumberDirectiveModule,
    SweetAlert2Module
  ],
  declarations: [AddDomainComponent],
  exports: [AddDomainComponent],
  entryComponents: [AddDomainComponent]
})
export class AddDomainModule { }
