import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageService } from '@ca-admin/services';
import { CurrencyService } from '@ca-admin/services/currency.service';
import { DomainService } from '@ca-admin/services/domain.service';
import { LANGUAGE_TYPES, REGEX } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ICurrencyResponse } from '@ca-admin/statemanagement/models/currency.interface';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { IDomain } from '@ca-core/shared/statemanagement/models/domain.interface';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'caadmin-add-domain',
  templateUrl: './add-domain.component.html'
})
export class AddDomainComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {

  payload: IDomain;
  form: FormGroup;

  deleteSwal: SweetAlertOptions;
  promotionsAll: Array<IPageStateResponse>;
  currencies: Array<ICurrencyResponse>;
  languages = LANGUAGE_TYPES;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private domainService: DomainService,
    private notificationService: NotificationService,
    private swalConfirmService: SwalConfirmService,
    private pageService: PageService,
    private currencyService: CurrencyService,
    private spinnerService: SpinnerService
  ) {
    super();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      path: [this.getDomain(), [Validators.required, Validators.pattern(new RegExp(REGEX.DOMAIN))]],
      principal: this.payload && this.payload.isCurrent || false,
      tax: [this.payload && this.payload.tax || null, Validators.required],
      promotionAllId: [this.payload && this.payload.promotionAllId || null, Validators.required],
      language: [this.payload && this.payload.language || null, Validators.required],
      currency: [this.payload && this.payload.currency || null, Validators.required],
      hideLanguage: [this.payload && this.payload.hideLanguage || false]
    });

    this.getData();
    if (this.payload.path) {
      this.deleteSwal = this.swalConfirmService.warning(
        'Una vez eliminado las páginas enlazadas al dominio, serán inaccesibles.<br>',
        `¿Seguro de eliminar el dominio <strong>${this.payload.path}</strong>?`
      );
    }
  }

  save(): void {
    ValidatorUtil.validateForm(this.form);
    if (this.form.valid) {
      const body = {
        path: this.formatDomain(),
        tax: Number(this.form.get('tax').value),
        promotionAllId: this.form.get('promotionAllId').value,
        language: this.form.get('language').value,
        currency: this.form.get('currency').value,
        hideLanguage: coerceBooleanProp(this.form.get('hideLanguage').value),
        state: 1
      } as IDomain;
      const sub$ = this.payload ? this.domainService.updateDomain(this.payload.id, body, true) : this.domainService.saveDomain(body, true);

      sub$
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(res => {
          res.data.isCurrent = coerceBooleanProp(this.form.get('principal').value);
          this.activeModal.close(res);
        });
    } else {
      this.notificationService.addWarning(GeneralLang.notifications.completeFields);
    }
  }

  delete(): void {
    this.domainService.deleteDomain(this.payload.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.activeModal.dismiss(res);
      });
  }

  close(): void {
    this.activeModal.dismiss();
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.form, controlName);
  }

  private getData(): void {
    this.spinnerService.showSpinner();
    zip(
      this.pageService.getPromotionAllSelect(),
      this.currencyService.getCurrencies()
    )
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.promotionsAll = res[0];
        this.currencies = res[1];

        this.spinnerService.hideSpinner();
      });
  }

  private formatDomain(): string {
    let path: string = this.form.get('path').value;
    path = path.endsWith('/') ? path : `${path}/`;

    return path;
  }

  private getDomain(): string {
    const path: string = this.payload && this.payload.path || '';

    return path && path.endsWith('/') ? path.substring(0, path.length - 1) : path;
  }

}
