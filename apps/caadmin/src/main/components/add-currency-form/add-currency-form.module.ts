import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CaButtonModule, CaInputModule } from '@ca-core/ui/lib/components';
import { AddCurrencyFormComponent } from './add-currency-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    CaButtonModule
  ],
  declarations: [AddCurrencyFormComponent],
  exports: [AddCurrencyFormComponent],
  entryComponents: [AddCurrencyFormComponent]
})
export class AddCurrencyFormModule { }
