import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencyService } from '@ca-admin/services/currency.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ICurrencyRequest } from '@ca-admin/statemanagement/models/currency.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-add-currency-form',
  templateUrl: './add-currency-form.component.html'
})
export class AddCurrencyFormComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  frmCurrency: FormGroup;
  codeIso: AbstractControl;
  name: AbstractControl;
  symbol: AbstractControl;
  value: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private currencyService: CurrencyService,
    private activeModal: ActiveModal,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
  }

  createForm(): void {
    this.frmCurrency = this.fb.group({
      codeIso: [null, Validators.required],
      name: [null, Validators.required],
      symbol: [null, Validators.required],
      value: [0, Validators.required],
      isPrincipal: [0]
    });

    this.codeIso = this.frmCurrency.get('codeIso');
    this.name = this.frmCurrency.get('name');
    this.symbol = this.frmCurrency.get('symbol');
    this.value = this.frmCurrency.get('value');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmCurrency, controlName);
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmCurrency);

    return this.frmCurrency.valid;
  }

  private paramsSaveUser(): ICurrencyRequest {
    return this.frmCurrency.value as ICurrencyRequest;
  }

  save(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveUser();
      this.currencyService.saveCurrency(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
