import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaCardModule, CaCheckboxModule, CaInputModule } from '@ca-core/ui/lib/components';
import { CreateRoleFormComponent } from './create-role-form.component';

@NgModule({
  declarations: [CreateRoleFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ModalModule,
    CaInputModule,
    CaCardModule,
    CaCheckboxModule,
    CaButtonModule
  ],
  exports: [CreateRoleFormComponent],
  entryComponents: [CreateRoleFormComponent]
})
export class CreateRoleFormModule { }
