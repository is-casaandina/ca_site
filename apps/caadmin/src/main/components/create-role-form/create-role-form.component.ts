import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PermissionService, RoleService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { RoleLang } from '@ca-admin/settings/lang/role.lang';
import { IPermissionResponse } from '@ca-admin/statemanagement/models/permission.interface';
import { ISaveRoleRequest } from '@ca-admin/statemanagement/models/role.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-create-role-form',
  templateUrl: './create-role-form.component.html',
  styleUrls: ['./create-role-form.component.scss']
})
export class CreateRoleFormComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;
  ROLE_LANG = RoleLang;

  frmCreateRole: FormGroup;
  role: AbstractControl;
  permissionsCheck: FormArray;
  permissionsList: Array<IPermissionResponse>;

  constructor(
    private formBuilder: FormBuilder,
    private permissionService: PermissionService,
    private roleService: RoleService,
    private notificationService: NotificationService,
    private activeModal: ActiveModal
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
    this._permissionsList();
  }

  private _createForm(): void {
    this.frmCreateRole = this.formBuilder.group({
      role: [null, Validators.required],
      permissionsCheck: this.formBuilder.array([], ValidatorUtil.minChecksRequired())
    });
    this.role = this.frmCreateRole.get('role');
    this.permissionsCheck = this.frmCreateRole.get('permissionsCheck') as FormArray;
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmCreateRole, controlName);
  }

  private _permissionsList(): void {
    this.permissionService.permissionsList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IPermissionResponse>) => {
        this.permissionsList = response;
        this._createPermissionsCheckControl();
      });
  }

  private _createPermissionsCheckControl(): void {
    this.permissionsList.forEach((fv, fk) => {
      this.permissionsCheck.setControl(fk, this.formBuilder.control(false));
    });
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmCreateRole);

    return this.frmCreateRole.valid;
  }

  private _paramsSaveRole(): ISaveRoleRequest {
    const getPermissionsCode = () => {
      const permissionsCode = [];
      this.permissionsCheck.controls.forEach((fv, fk) => {
        if (fv.value) { permissionsCode.push(this.permissionsList[fk].permissionCode); }
      });

      return permissionsCode;
    };

    return {
      roleName: this.role.value,
      permissionCodeList: getPermissionsCode()
    } as ISaveRoleRequest;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsSaveRole();
      this.roleService.saveRole(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
