import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CaAlertErrorModule, CaButtonModule, CaInputModule, CaSwitchModule } from '@ca-core/ui/lib/components';
import { ResetPasswordFormComponent } from './reset-password-form.component';

@NgModule({
  declarations: [ResetPasswordFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    CaSwitchModule,
    CaButtonModule,
    CaAlertErrorModule
  ],
  exports: [ResetPasswordFormComponent]
})
export class ResetPasswordFormModule { }
