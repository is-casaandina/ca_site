import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@ca-admin/services';
import { IMAGES } from '@ca-admin/settings/constants/images.constant';
import { LoginLang } from '@ca-admin/settings/lang/login.lang';
import { IResetPasswordRequest } from '@ca-admin/statemanagement/models/reset-password.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-reset-password-form',
  templateUrl: './reset-password-form.component.html',
  styleUrls: ['./reset-password-form.component.scss']
})
export class ResetPasswordFormComponent extends UnsubscribeOnDestroy implements OnInit {
  frmReset: FormGroup;
  password: AbstractControl;
  cPassword: AbstractControl;
  invalidPasswords: boolean;
  invalidData: boolean;

  IMAGES = IMAGES;
  LOGIN_LANG = LoginLang;
  @Input() token;

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService,
    private _notificationService: NotificationService,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmReset = this._formBuilder.group({
      password: [null, Validators.required],
      cPassword: [null, Validators.required]
    });
    this.password = this.frmReset.get('password');
    this.cPassword = this.frmReset.get('cPassword');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmReset, controlName);
  }

  private _validateForm(): boolean {
    if (this.password.value !== this.cPassword.value) {
      this.invalidPasswords = true;

      return false;
    }
    ValidatorUtil.validateForm(this.frmReset);

    return this.frmReset.valid;
  }

  private _paramsReset(): IResetPasswordRequest {
    return {
      password: this.password.value,
      token: this.token
    } as IResetPasswordRequest;
  }

  reset(): void {
    const validateForm = this._validateForm();
    this.invalidData = false;
    if (validateForm) {
      const reset = this._paramsReset();

      this._authService
        .resetPassword(reset, true)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(
          res => {
            this._router.navigate(['/login']);
            this._notificationService.addSuccess(res.message);
          },
          () => {
            this.invalidData = true;
          }
        );
    }
  }
}
