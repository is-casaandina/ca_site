import { Component, Input, OnInit } from '@angular/core';
import { AuthService, ConfigurationService } from '@ca-admin/services';
import { IMAGES } from '@ca-admin/settings/constants/images.constant';
import { MENU_CODE, MENU_HEAD } from '@ca-admin/settings/constants/menu.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IMenu } from '@ca-admin/statemanagement/models/general.interface';

@Component({
  selector: 'caadmin-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  @Input() customClass: string;

  IMAGES = IMAGES;
  GENERAL_LANG = GeneralLang;
  menu: Array<IMenu> = [];

  fullname: string;
  roleName: string;
  isCollapsed: boolean;

  constructor(
    private configurationService: ConfigurationService,
    private authService: AuthService
  ) {
    this.isCollapsed = true;
  }

  ngOnInit(): void {
    const userPermissions = this.configurationService.getUserPermissions();
    this.fullname = userPermissions.fullname;
    this.roleName = userPermissions.roleName;
    this.menu = MENU_HEAD;
    const exits = userPermissions.permissionList.findIndex(permission => permission.permissionCode === MENU_CODE.configurations) !== -1;
    if (!exits) {
      this.menu = this.menu.filter(m => m.code !== MENU_CODE.configurations);
    }
  }

  logout(): void {
    this.authService.logout();
  }

  toggle(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  autoClose(): void {
    this.isCollapsed = true;
  }

}
