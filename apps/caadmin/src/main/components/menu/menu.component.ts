import { Component, OnInit } from '@angular/core';
import { MenuService } from '@ca-admin/services';
import { IMenu } from '@ca-admin/statemanagement/models/general.interface';

@Component({
  selector: 'caadmin-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {

  menu: Array<IMenu>;

  constructor(
    private menuService: MenuService
  ) { }

  ngOnInit(): void {
    const menu = this.menuService.generateMenu();
    this.menu = menu.filter(m => !m.header);
  }

}
