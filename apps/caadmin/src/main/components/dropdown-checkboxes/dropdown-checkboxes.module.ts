import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CaCheckboxModule } from '@ca-core/ui/lib/components';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { DropdownCheckboxesComponent } from './dropdown-checkboxes.component';

@NgModule({
  declarations: [DropdownCheckboxesComponent],
  imports: [
    CommonModule,
    CaCheckboxModule,
    CollapseModule,
    FormsModule
  ],
  exports: [DropdownCheckboxesComponent]
})
export class DropdownCheckboxesModule { }
