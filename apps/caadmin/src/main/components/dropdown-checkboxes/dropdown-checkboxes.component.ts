import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AngularUtil } from '@ca-core/shared/helpers/util';

@Component({
  selector: 'caadmin-dropdown-checkboxes',
  templateUrl: './dropdown-checkboxes.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownCheckboxesComponent),
      multi: true
    }
  ]
})
export class DropdownCheckboxesComponent implements OnInit, ControlValueAccessor {

  private _oldModel: Array<any>;
  protected _model: Array<any>;
  @Input()
  get model(): Array<any> {
    return this._model;
  }
  set model(value: Array<any>) {
    this._model = value;
  }

  protected _icon: string;
  @Input()
  get icon(): string {
    return this._icon;
  }
  set icon(value: string) {
    this._icon = value || '';
  }

  protected _dataList: Array<any>;
  @Input()
  get dataList(): Array<any> {
    return this._dataList;
  }
  set dataList(value: Array<any>) {
    this._dataList = value || [];
    /** Necessary for the first charge */
    if (this._dataList.length && this._oldModel && AngularUtil.isArray(this._oldModel)) {
      this._setModel(this._oldModel);
    }
  }
  @Input() placeholder: string;
  @Input() valueField: string;
  @Input() textField: string;

  isCollapsed: boolean;

  constructor() {
    this.isCollapsed = true;
  }

  ngOnInit(): void { }

  private _setModel(values: Array<any>): void {
    values.forEach(value => {
      const item = this.dataList.find(fv => fv[this.valueField] === value[this.valueField]);
      if (item) {
        item.checked = true;
        this.selectedItem(item);
      }
    });

  }

  toggle(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  selectedItem(item: any): void {
    this._model = this._model || [];
    if (item.checked) {
      const tmp = this._model.find(elem => elem[this.valueField] === item[this.valueField]);
      if (!tmp) {
        this._model.push(item);
      }
    } else {
      const index = this._model.indexOf(item);
      this._model.splice(index, 1);
    }
    this.propagateChange(this._model);
  }

  autoClose(): void {
    this.isCollapsed = true;
  }

  private _clearList(): void {
    this._model = [];
    this._oldModel = [];
    this.dataList.forEach(fv => {
      fv.checked = false;
    });
    this.propagateChange(this._model);
  }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value && AngularUtil.isArray(value)) {
      this._oldModel = value;
      this._setModel(value);
    } else {
      if (this.dataList) { this._clearList(); }
    }
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

}
