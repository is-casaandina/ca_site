import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CaAlertErrorModule, CaButtonModule, CaInputModule, CaSwitchModule } from '@ca-core/ui/lib/components';
import { PasswordChangeFormComponent } from './password-change-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    CaSwitchModule,
    CaButtonModule,
    CaAlertErrorModule
  ],
  declarations: [PasswordChangeFormComponent],
  exports: [PasswordChangeFormComponent]
})
export class PasswordChangeFormModule { }
