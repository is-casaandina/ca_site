import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@ca-admin/services';
import { IMAGES } from '@ca-admin/settings/constants/images.constant';
import { LoginLang } from '@ca-admin/settings/lang';
import { IPasswordChangeRequest } from '@ca-admin/statemanagement/models/user.interface';
import { StorageService, UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-password-change-form',
  templateUrl: './password-change-form.component.html'
})
export class PasswordChangeFormComponent extends UnsubscribeOnDestroy implements OnInit {

  frmPasswordChange: FormGroup;
  currentPassword: AbstractControl;
  password: AbstractControl;
  confirmPassword: AbstractControl;
  invalidPasswords: boolean;
  messageError: string;
  invalidData: boolean;

  IMAGES = IMAGES;
  LOGIN_LANG = LoginLang;

  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private userService: UserService,
    private router: Router,
    private location: Location
  ) {
    super();
  }

  ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.frmPasswordChange = this.formBuilder.group({
      currentPassword: [null, Validators.required],
      password: [null, Validators.required],
      confirmPassword: [null, Validators.required]
    });
    this.currentPassword = this.frmPasswordChange.get('currentPassword');
    this.password = this.frmPasswordChange.get('password');
    this.confirmPassword = this.frmPasswordChange.get('confirmPassword');
  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmPasswordChange, controlName);
  }

  private validateForm(): boolean {
    if (this.password.value !== this.confirmPassword.value) {
      this.messageError = 'La contraseña de confirmación es incorrecta.';
      this.invalidPasswords = true;

      return false;
    }
    ValidatorUtil.validateForm(this.frmPasswordChange);

    return this.frmPasswordChange.valid;
  }

  cancel(): void {
    this.location.back();
  }

  change(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      this.invalidPasswords = false;
      const params = this.frmPasswordChange.value as IPasswordChangeRequest;

      this.userService.updatePasswordChange(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.storageService.clear();
          this.router.navigate(['/login']);
        }, (error: any) => {
          this.messageError = error.message;
          this.invalidPasswords = true;
        });
    }
  }

}
