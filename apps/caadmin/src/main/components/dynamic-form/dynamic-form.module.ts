import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { CaAccordionModule, CaCheckboxModule, CaTabsModule } from '@ca-core/ui/lib/components';
import { CaButtonModule } from '@ca-core/ui/lib/components/button';
import { CaIconPickerModule } from '@ca-core/ui/lib/components/icon-picker';
import { CaInputModule } from '@ca-core/ui/lib/components/input';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { QuillModule } from 'ngx-quill';
import { GallerySelectModule } from '../gallery-select/gallery-select.module';
import { FormAlignComponent } from './components/form-align/form-align.component';
import { FormAutocompleteComponent } from './components/form-autocomplete/form-autocomplete.component';
import { FormCheckboxComponent } from './components/form-checkbox/form-checkbox.component';
import { FormFileComponent } from './components/form-file/form-file.component';
import { FormImageComponent } from './components/form-image/form-image.component';
import { FormInputComponent } from './components/form-input/form-input.component';
import { FormRadioComponent } from './components/form-radio/form-radio.component';
import { FormRichtextComponent } from './components/form-richtext/form-richtext.component';
import { FormSelectIconComponent } from './components/form-select-icon/form-select-icon.component';
import { FormSelectComponent } from './components/form-select/form-select.component';
import { FormStaticComponent } from './components/form-static/form-static.component';
import { FormTableComponent } from './components/form-table/form-table.component';
import { FormTextareaComponent } from './components/form-textarea/form-textarea.component';
import { FormUrlComponent } from './components/form-url/form-url.component';
import { FormVideoComponent } from './components/form-video/form-video.component';
import { DynamicFormComponent } from './containers/dynamic-form/dynamic-form.component';
import { DynamicFieldDirective } from './directives/field.directive';
import { DynamicUiDirective } from './directives/ui.directive';
import { ActiveSlidePipe } from './pipes/active-slide.pipe';
import { CustomFormatPipe } from './pipes/custom-format.pipe';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
    CaInputModule,
    CaButtonModule,
    CaAccordionModule,
    CaCheckboxModule,
    CaTabsModule,
    GallerySelectModule,
    PipesModule,
    CaIconPickerModule,
    NgbModule,
    SweetAlert2Module
  ],
  declarations: [
    DynamicFieldDirective,
    DynamicUiDirective,
    DynamicFormComponent,

    FormAlignComponent,
    FormCheckboxComponent,
    FormFileComponent,
    FormImageComponent,
    FormInputComponent,
    FormRadioComponent,
    FormRichtextComponent,
    FormSelectComponent,
    FormSelectIconComponent,
    FormStaticComponent,
    FormTextareaComponent,
    FormUrlComponent,
    FormVideoComponent,
    FormAutocompleteComponent,
    FormTableComponent,

    ActiveSlidePipe,
    CustomFormatPipe
  ],
  entryComponents: [
    FormAlignComponent,
    FormCheckboxComponent,
    FormFileComponent,
    FormImageComponent,
    FormInputComponent,
    FormRadioComponent,
    FormRichtextComponent,
    FormSelectComponent,
    FormSelectIconComponent,
    FormStaticComponent,
    FormTextareaComponent,
    FormUrlComponent,
    FormVideoComponent,
    FormAutocompleteComponent,
    FormTableComponent
  ],
  exports: [DynamicFormComponent]
})
export class DynamicFormModule { }
