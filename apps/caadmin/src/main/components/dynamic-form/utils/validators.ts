import { ValidatorFn, Validators } from '@angular/forms';
import { IProperty } from '@ca-admin/statemanagement/models/dynamic-form.interface';

export class DynamicFormUtils {

  static getValidator(prop: IProperty): Array<ValidatorFn> {
    const validators = [];

    if (prop.required) { validators.push(Validators.required); }
    if (prop.maxLength) { validators.push(Validators.maxLength(prop.maxLength)); }
    if (prop.minLength) { validators.push(Validators.minLength(prop.minLength)); }

    return validators;
  }
}
