import { Injectable } from '@angular/core';
import { LANGUAGE_TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { StorageService } from '@ca-core/shared/helpers/util';

const KEY_STORAGE = 'df_language';

@Injectable({
  providedIn: 'root'
})
export class DynamicFormService {

  constructor(
    private storageService: StorageService
  ) { }

  getLanguage(): string {
    return this.storageService.getItem(KEY_STORAGE) || LANGUAGE_TYPES_CODE.es;
  }

  setLanguage(language: string): void {
    this.storageService.setItem(KEY_STORAGE, language);
  }

}
