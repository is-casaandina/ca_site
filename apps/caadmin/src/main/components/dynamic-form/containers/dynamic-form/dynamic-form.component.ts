import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ApplicationRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TYPES_CODE } from '@ca-admin/settings/constants/general.constant';
import { DESIGN_ARRAY, IComponent, IProperty, TYPE_ARRAY } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { AngularUtil } from '@ca-core/shared/helpers/util';
import { ValidatorUtil } from '@ca-core/shared/helpers/util/validator';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { CaAccordionComponent } from '@ca-core/ui/lib/components/accordion/accordion/accordion.component';
import { SweetAlertOptions } from 'sweetalert2';
import { DynamicFormUtils } from './../../utils/validators';

@Component({
  selector: 'caadmin-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {

  @Input() components: Array<IComponent> = [];
  @Input() hideTitleComponent: boolean;

  @Output() submitted: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup;

  TYPES_CODE = TYPES_CODE;
  TYPE_ARRAY = TYPE_ARRAY;
  TYPE_DESIGN = DESIGN_ARRAY;

  deleteSwal: SweetAlertOptions;
  changeTypeSwal: SweetAlertOptions;

  defaultLabelKey = '-default-label';

  constructor(
    private fb: FormBuilder,
    private appRef: ApplicationRef,
    private swalConfirmService: SwalConfirmService
  ) { }

  ngOnInit(): void {
    this.createForm(this.components);
    this.deleteSwal = this.swalConfirmService.warning(`¿Seguro de eliminar el elemento?`);
    this.changeTypeSwal = this.swalConfirmService.warning(`¿Seguro de cambiar de tipo?`);
  }

  createForm(components: Array<IComponent>): void {
    // const cmpts = components.filter(component => component.type !== TYPES_CODE.static);
    this.form = this.getControl(components);
  }

  private getControl(properties: Array<IProperty | IComponent>): FormGroup {
    const comp = this.fb.group({});
    properties
      .filter(prop => prop.code)
      .forEach(prop => {
        if (prop.type === 'array') {
          this.getControlArray(prop, comp);
        } else {
          this.getControlDefault(prop, comp);
        }
      });

    return comp;
  }

  private getControlDefault(property: IProperty, form: FormGroup): void {
    if (property.properties) {
      form.addControl(property.code, this.getControl(property.properties));
    } else {
      const validators = DynamicFormUtils.getValidator(property);
      property.value = property.type === 'checkbox' ? coerceBooleanProp(property.value) : property.value;
      form.addControl(property.code, this.fb.control(property.value || null, validators));
    }
  }

  //#region Métodos para array
  private getControlArray(property: IProperty, form: FormGroup): void {
    const array = this.fb.array([]);

    if (Array.isArray(property.value) && property.value.length) {
      this.setDefaultDataArray(property, array);
    } else {
      this.setDefaultSizeArray(property, array);
    }

    if (property.design === DESIGN_ARRAY.tabs) {
      (array.controls as Array<FormGroup>).forEach((control, index) => {
        control.addControl('isSelected', this.fb.control(!index));
      });
    }

    (array.controls as Array<FormGroup>).forEach(control => {
      this.setControlDependenceLabel(property, control);
    });

    form.addControl(property.code, array);
  }

  private setControlDependenceLabel(property: IProperty, control: FormGroup): void {
    if (property.dependenceLabel) {
      const dependences = property.dependenceLabel.split('|');
      dependences.forEach(dependence => {
        const controlName = `${dependence}${this.defaultLabelKey}`;
        const controlLabel = this.fb.control(null);
        const exists = control.get(dependence);
        if (exists) {
          control.addControl(controlName, controlLabel);
        }
      });
    }
  }

  private setDefaultDataArray(property: IProperty, array: FormArray): void {
    property = AngularUtil.clone(property);
    property.value.forEach(value => {
      let control: FormControl | FormGroup;

      switch (property.typeArray) {
        case TYPE_ARRAY.string:
          const validators = DynamicFormUtils.getValidator(property);
          control = this.fb.control(value, validators);
          break;
        case TYPE_ARRAY.object:
          Object.keys(value)
            .forEach(key => {
              const item = property.properties.find(tmp => tmp.code === key);
              if (item) { item.value = value[key]; }
            });

          control = this.getControl(property.properties);
          break;
        case TYPE_ARRAY.custom:
          if (value.type) {
            const properties = AngularUtil.clone((property.definitions[value.type].properties));

            Object.keys(value)
              .forEach(key => {
                const item = properties.find(tmp => tmp.code === key);
                if (item) {
                  item.value = value[key];
                  (item.properties || [])
                    .filter(prop => prop.code)
                    .forEach(prop => {
                      prop.value = value[key][prop.code];
                    });
                }
              });

            control = this.getControl(properties);
            control.addControl('type', this.fb.control(value.type));
          } else {
            control = this.fb.group({ type: [null] });
          }
          break;
        default:
      }

      if (control) {
        array.push(control);
      }
    });
  }

  private setDefaultSizeArray(property: IProperty, array: FormArray): void {
    property = AngularUtil.clone(property);
    const tmp = new Array(property.size || 1);

    for (const _iterator of tmp) {
      let control: FormControl | FormGroup;

      switch (property.typeArray) {
        case TYPE_ARRAY.string:
          const validators = DynamicFormUtils.getValidator(property);
          control = this.fb.control('', validators);
          break;
        case TYPE_ARRAY.object:
          control = this.getControl(property.properties);
          break;
        case TYPE_ARRAY.custom:
          control = this.fb.group({ type: [null] });
          break;
        default:
      }

      if (control) {
        array.push(control);
      }
    }
  }

  addItem(form: FormArray, properties: Array<IProperty>, property: IProperty, containerCollapse?: CaAccordionComponent): void {
    properties = properties ? AngularUtil.clone(properties) : properties;
    property = property ? AngularUtil.clone(property) : property;

    if (this.isIimitItems(form, property)) {
      const validators = DynamicFormUtils.getValidator(property);
      let control;
      switch (property.typeArray) {
        case TYPE_ARRAY.object:
          control = this.getControl(properties);
          this.setControlDependenceLabel(property, control);
          break;
        case TYPE_ARRAY.string:
          control = this.fb.control('', validators);
          break;

        case TYPE_ARRAY.custom:
          control = this.fb.group({ type: [null] });
          this.setControlDependenceLabel(property, control);
          break;

        default:
      }

      form.push(control);

      if (DESIGN_ARRAY.tabs === property.design) {
        control.addControl('isSelected', this.fb.control(false));
        this.selectedSlide(form, control);
      } else if (DESIGN_ARRAY.nothing !== property.design && containerCollapse) {
        setTimeout(() => {
          containerCollapse.toggle(form.length - 1);
        }, 100);
      }
    }
  }

  removeItem(form: FormArray, index: number, isSelected: boolean): void {
    form.removeAt(index);
    this.appRef.tick();
    if (isSelected) {
      const newIndex = index > 1 ? index - 1 : 0;
      (form.controls[newIndex] as FormGroup).controls.isSelected.setValue(true);
    }
  }

  selectedSlide(form: FormArray, control: FormGroup): void {
    form.controls.forEach((ctrl: FormGroup) => {
      ctrl.controls.isSelected.setValue(false);
    });
    control.controls.isSelected.setValue(true);
  }

  isIimitItems(form: FormArray, property: IProperty): boolean {
    return (property && property.max && form.length < property.max) ||
      (!property || (property && !property.max));
  }

  disabledAdd(property: IProperty): boolean {
    return property.size !== undefined;
  }

  generateDynamicControl(control: FormGroup, propertyDefinition: IProperty, property: IProperty): void {
    control.controls.type.setValue(propertyDefinition.value);
    propertyDefinition.properties.forEach(proper => proper.typeSelected = propertyDefinition.value);
    const newControl = this.getControl(propertyDefinition.properties);
    Object.keys(newControl.controls)
      .forEach(key => {
        control.addControl(key, newControl.controls[key]);
      });
    this.setControlDependenceLabel(property, control);
  }

  resetCustomArray(control: FormGroup, property: IProperty): void {
    Object.keys(control.controls)
      .forEach(key => {
        control.removeControl(key);
      });
    control.addControl('isSelected', this.fb.control(true));
    control.addControl('type', this.fb.control(null));
    this.setControlDependenceLabel(property, control);
  }

  collapsedAccordion(containerAccordion: CaAccordionComponent): void {
    if (containerAccordion) {
      containerAccordion.toggle(-1);
    }
  }

  getLabel(form: FormGroup, property: IProperty): string {
    let label = '';

    if (property.dependenceLabel) {
      const controlName = Object.keys(form.controls)
        .find(key => key.endsWith(this.defaultLabelKey));
      const control = form.get(controlName);
      label = control && control.value;
    }

    return label;
  }
  //#endregion Métodos para array

  onDrop(items: Array<FormControl>, event: CdkDragDrop<Array<string>>): void {
    moveItemInArray(items, event.previousIndex, event.currentIndex);
    const tmpPrevious = { ...items[event.previousIndex] };
    const tmpCurrent = { ...items[event.currentIndex] };

    items[event.previousIndex].setValue(tmpPrevious.value);
    items[event.currentIndex].setValue(tmpCurrent.value);
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.form);

    return this.form.valid;
  }

  formatForm(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      this.components = this.getFormatComponent();
      this.submitted.next(this.components);
    }
  }

  saveForm(): void {
    const validateForm = this.validateForm();
    const formValue = (validateForm)
      ? this.getFormValue()
      : null;
    this.submitted.next(formValue);
  }

  getFormValue(): {} {
    const formValue = this.form.getRawValue();

    return this.rmSystemProperties(formValue);
  }

  getFormatComponent(): Array<IComponent> {
    return this.components.map(component => {
      // if (component.type !== TYPES_CODE.static) {
      let values = this.form.controls[component.code].value;

      const div = document.createElement('div');
      const child = document.createElement(component.code);

      div.appendChild(child);
      if (values && typeof values === 'object') {
        values = this.rmSystemProperties(values);
        component.properties
          .forEach(prop => {
            const value = values[prop.code];
            if (value) {
              const tmp = typeof value === 'string' ? value : JSON.stringify(value);
              child.setAttribute(prop.code, tmp);
            }
          });
      }
      component.template = div.innerHTML;
      // }

      return component;
    });
  }

  private rmSystemProperties(obj: any): any {
    if (Array.isArray(obj)) {
      return obj.map(ob => (this.rmSystemProperties(ob)));
    }
    else if ((typeof obj === 'object') && (obj !== null)) {
      const keys = Object.keys(obj);
      keys.forEach(key => {

        // INFO: Se compara con null, para dejar pasar los valores con 0
        if (key.endsWith(this.defaultLabelKey) || obj[key] === null) {
          // tslint:disable-next-line:no-dynamic-delete
          delete obj[key];
        } else {
          this.rmSystemProperties(obj[key]);
        }
      });
    }

    return obj;
  }

}
