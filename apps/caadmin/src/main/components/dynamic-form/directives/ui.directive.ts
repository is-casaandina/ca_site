import { DOCUMENT } from '@angular/common';
import { Directive, ElementRef, Inject, Input, OnInit } from '@angular/core';
import { IProperty } from '@ca-admin/statemanagement/models/dynamic-form.interface';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[dynamicUi]'
})
export class DynamicUiDirective implements OnInit {

  @Input() config: IProperty;

  constructor(
    private elementref: ElementRef<HTMLDivElement>,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {
    let template: string = templates[this.config.type];
    if (template) {
      template = template.replace('{{label}}', this.config.label || '');

      const div = this.document.createElement('div');
      div.innerHTML = template;
      this.elementref.nativeElement.replaceWith(div.firstChild);
    } else {
      this.elementref.nativeElement.remove();
    }
  }

}

const templates = {
  'ui-br': '<hr class="g-hr mt-1 mb-3">',
  'ui-title': '<h5 class="g-fs-18 mb-2">{{label}}</h5>',
  'ui-description': '<div class="mt-1 mb-2"><span class="d-block g-fs-14">{{label}}</span></div>'
};
