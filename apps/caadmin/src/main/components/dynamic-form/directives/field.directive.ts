import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IProperty } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { FormAlignComponent } from '../components/form-align/form-align.component';
import { FormAutocompleteComponent } from '../components/form-autocomplete/form-autocomplete.component';
import { FormCheckboxComponent } from '../components/form-checkbox/form-checkbox.component';
import { FormFileComponent } from '../components/form-file/form-file.component';
import { FormImageComponent } from '../components/form-image/form-image.component';
import { FormInputComponent } from '../components/form-input/form-input.component';
import { FormRadioComponent } from '../components/form-radio/form-radio.component';
import { FormRichtextComponent } from '../components/form-richtext/form-richtext.component';
import { FormSelectIconComponent } from '../components/form-select-icon/form-select-icon.component';
import { FormSelectComponent } from '../components/form-select/form-select.component';
import { FormTableComponent } from '../components/form-table/form-table.component';
import { FormTextareaComponent } from '../components/form-textarea/form-textarea.component';
import { FormUrlComponent } from '../components/form-url/form-url.component';
import { FormVideoComponent } from '../components/form-video/form-video.component';
import { DynamicFormComponentBase } from '../dynamic-form-base.component';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[dynamicField]'
})
export class DynamicFieldDirective implements OnInit {

  @Input() config: IProperty;
  @Input() properties: IProperty;
  @Input() group: FormGroup;

  component: ComponentRef<DynamicFormComponentBase>;

  constructor(
    private resolver: ComponentFactoryResolver,
    private container: ViewContainerRef
  ) { }

  ngOnInit(): void {
    const component = components[this.config.type];
    if (component) {
      const factory = this.resolver.resolveComponentFactory<DynamicFormComponentBase>(component);
      this.component = this.container.createComponent(factory);
      this.component.instance.config = this.config;
      this.component.instance.properties = this.properties;
      this.component.instance.group = this.group;
    }
  }
}

const components = {
  text: FormInputComponent,
  number: FormInputComponent,
  autocomplete: FormAutocompleteComponent,
  url: FormUrlComponent,
  date: FormInputComponent,
  image: FormImageComponent,
  badge: FormImageComponent,
  video: FormVideoComponent,
  richtext: FormRichtextComponent,
  select: FormSelectComponent,
  align: FormAlignComponent,
  radio: FormRadioComponent,
  checkbox: FormCheckboxComponent,
  textarea: FormTextareaComponent,
  file: FormFileComponent,
  table: FormTableComponent,
  'select-icon': FormSelectIconComponent,
  'richtext-html': FormRichtextComponent
};
