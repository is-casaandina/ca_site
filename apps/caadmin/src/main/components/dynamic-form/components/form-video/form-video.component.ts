import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { TYPE_VIDEO } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-video',
  templateUrl: './form-video.component.html'
})
export class FormVideoComponent extends DynamicFormComponentBase {

  videoForm: FormGroup;

  gallery: AbstractControl;
  url: AbstractControl;
  type: AbstractControl;

  TYPE_VIDEO = TYPE_VIDEO;
  constructor(
    private fb: FormBuilder
  ) {
    super();
    this.cretateFormVideo();
  }

  caOnInit(): void {
    const value = this.control.value;

    if (value && !value.mimeType) {
      this.type.setValue(TYPE_VIDEO.url);
      this.url.setValue(value.url);
    } else if (value && value.mimeType) {
      this.type.setValue(TYPE_VIDEO.gallery);
      this.gallery.setValue([value]);
    } else {
      this.type.setValue(TYPE_VIDEO.gallery);
    }
  }

  cretateFormVideo(): void {
    this.videoForm = this.fb.group({
      gallery: [],
      url: [],
      type: TYPE_VIDEO.gallery
    });

    this.gallery = this.videoForm.get('gallery');
    this.url = this.videoForm.get('url');
    this.type = this.videoForm.get('type');

    this.gallery.valueChanges
      .subscribe(res => this.setValue());
    this.url.valueChanges
      .subscribe(res => this.setValue());
    this.type.valueChanges
      .subscribe(res => this.setValue());
  }

  private setValue(): void {
    const type = this.type.value;
    const gallery = this.gallery.value;
    const url = { url: this.url.value || '' };

    const value = type === TYPE_VIDEO.gallery ? ((gallery && gallery.length) ? gallery[0] : null) : url;
    this.group.get(this.controlName)
      .setValue(value);
  }
}
