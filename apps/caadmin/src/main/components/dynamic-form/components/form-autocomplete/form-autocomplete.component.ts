import { Component } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { environment } from '@ca-admin/environments/environment';
import { IActionServiceResponse } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';
import { DynamicFormService } from '../../providers/dynamic-form.service';

const TYPE_AUTOCOMPLETE = {
  text: 1,
  value: 2
};

@Component({
  selector: 'caadmin-form-autocomplete',
  template: `
  <div class="row align-items-center no-gutters mb-2">
    <div class="col-auto g-w-100">
      <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
    </div>
    <div class="col">
      <div class="g-input-autocomplete">
        <div class="g-input-autocomplete-search">
          <i class="la la-search"></i>
        </div>
        <input class="g-input"
          [attr.type]="config.type"
          [attr.maxlength]="config.maxLength"
          [attr.placeholder]="config.placeholder || ''"
          [formControl]="controlAutocomplete"
          [ngbTypeahead]="search"
          [inputFormatter]="inputFormatter"
          [resultFormatter]="resultFormatter"
          (blur)="blur()"/>
        </div>
    </div>
  </div>
  `
})
export class FormAutocompleteComponent extends DynamicFormComponentBase {

  controlAutocomplete: FormControl;

  private list: Array<IActionServiceResponse> = [];
  private endpoint: string;

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private dynamicFormService: DynamicFormService
  ) {
    super();
  }

  caOnInit(): void {
    this.controlAutocomplete = this.fb.control('autocomplete');

    this.endpoint = `${environment.API_URL}${this.config.action.request}`;

    if (this.control.value) {
      this.getdata(this.control.value, TYPE_AUTOCOMPLETE.value)
        .subscribe(data => {
          this.list = data || [];
          if (this.list.length) {
            this.controlAutocomplete.setValue(this.list[0]);
          }
        });
    }
  }

  blur(): void {
    const data = this.list.find(item => item.text === (this.controlAutocomplete.value || {}).text);
    this.control.setValue(data && data.value || '');
    this.controlAutocomplete.setValue(data);
    this.focus();
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.getdata(term, TYPE_AUTOCOMPLETE.text)
          .pipe(
            map(preResult => {
              this.list = preResult;

              return this.list;
            }),
            catchError(() => {
              return of([]);
            }))
      )
    )

  private getdata(value: string, type: number): IObservableArray<IActionServiceResponse> {
    return this.apiService.get<IObservableArray<IActionServiceResponse>>(this.endpoint,
      {
        params: {
          value,
          limit: 10,
          type,
          language: this.dynamicFormService.getLanguage()
        }
      });
  }

  inputFormatter = (x: { text: string }) => x.text;
  resultFormatter = (x: { text: string }) => x.text ? x.text : x;

}
