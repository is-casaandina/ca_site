import { Component } from '@angular/core';
import { IconService } from '@ca-admin/services/icon.service';
import { IIcon } from '@ca-core/ui/lib/components/icon-picker/lib';
import { takeUntil } from 'rxjs/operators';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-select-icon',
  template: `
  <div class="row align-items-center no-gutters mb-2" [formGroup]="group">
    <div class="col-auto g-w-100">
      <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
    </div>
    <div class="col">
      <ca-icon-picker
        [icons]="icons"
        [formControlName]="controlName">
      </ca-icon-picker>
    </div>
  </div>
  `
})
export class FormSelectIconComponent extends DynamicFormComponentBase {

  icons: Array<IIcon>;

  constructor(
    private iconService: IconService
  ) {
    super();
    this.iconService.getIcons()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(icons => {
        this.icons = icons;
      });
  }

}
