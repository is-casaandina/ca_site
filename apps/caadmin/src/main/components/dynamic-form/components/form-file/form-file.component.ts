import { Component } from '@angular/core';
import { AbstractControl, FormBuilder } from '@angular/forms';
import { FILE_MIME_TYPE } from '@ca-admin/settings/constants/file.constant';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-file',
  templateUrl: './form-file.component.html'
})
export class FormFileComponent extends DynamicFormComponentBase {

  formFile: AbstractControl;
  mimeTypes = FILE_MIME_TYPE;

  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  caOnInit(): void {
    this.formFile = this.fb.group({ file: null });
    this.formFile.get('file')
      .valueChanges
      .subscribe(val => {
        this.group.get(this.controlName)
          .setValue(val && val.length ? val[0].url : '');
      });
  }

}
