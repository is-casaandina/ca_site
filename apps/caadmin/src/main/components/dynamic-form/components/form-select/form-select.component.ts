import { AfterViewInit, Component } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { environment } from '@ca-admin/environments/environment';
import { EVENT_PROPERTY, IActionComponent, IActionServiceResponse } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable, of } from 'rxjs';
import { delay, switchMapTo, takeUntil } from 'rxjs/operators';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';
import { DynamicFormService } from '../../providers/dynamic-form.service';

@Component({
  selector: 'caadmin-form-select',
  template: `
  <div class="row align-items-center no-gutters mb-2">
    <div class="col-auto g-w-100">
      <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
    </div>
    <div class="col" [formGroup]="group">
      <div class="d-flex align-items-center">
        <div class="g-select w-100">
          <i class="la la-angle-down"></i>
          <select [formControlName]="config.code" [ngClass]="{ 'g-input-error' : validateControl() }">
            <option [ngValue]="null">{{ config.placeholder }}</option>
            <option [value]="item.value" *ngFor="let item of list">
              {{ item.text }}
            </option>
          </select>
          <span></span>
        </div>
        <div
          class="ml-3 p-3 g-b-line g-bc-gray3 g-b d-flex justify-content-center align-items-center g-bgc-gray5"
          *ngIf="getIcon()">
          <i [class]="getIcon()"></i>
        </div>
      </div>
    </div>
  </div>
  `
})
export class FormSelectComponent extends DynamicFormComponentBase implements AfterViewInit {

  list: Array<IActionServiceResponse> = [];
  endpoint: string;
  language: string;

  constructor(
    private apiService: ApiService,
    private dynamicFormService: DynamicFormService
  ) {
    super();
  }

  ngAfterViewInit(): void {
    this.language = this.dynamicFormService.getLanguage();

    const preValue = this.group.controls[this.config.code].value;
    const value = this.config.value ? this.config.value : null;
    this.group.controls[this.config.code].setValue(preValue || value);
    this.initData();
  }

  private initData(): void {
    if (this.config.list) {
      of([])
        .pipe(delay(0))
        .subscribe(() => {
          this.list = this.config.list.map(item => {
            if (typeof item === 'string') {
              return { value: item, text: item };
            } else {
              return item;
            }
          });
        });
    } else {
      this.endpoint = `${environment.API_URL}${this.config.action.request}`;
      this.dependences();
    }
  }

  private dependences(): void {
    const action = this.config.action || {} as IActionComponent;
    switch (action.event) {
      case EVENT_PROPERTY.change:
        this.onChange();
        break;
      case EVENT_PROPERTY.load:
        this.onLoad();
        break;
      case EVENT_PROPERTY.changeDependence:
        this.onChangeDependence();
        break;

      default:
        break;
    }
  }

  private onChange(): void {
    const params = { params: { language: this.language } };
    params[this.config.code] = this.control.value;

    this.control.valueChanges
      .pipe(delay(0), switchMapTo(this.apiService.get<Observable<Array<IActionServiceResponse>>>(this.endpoint, { params })))
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.setData(response));
  }

  private onChangeDependence(): void {
    const params = {};
    params[this.config.code] = this.control.value;

    let hasDefaultValue: boolean;

    this.config.action.dependencies.forEach(dependence => {

      const control = this.getControlByDependence(dependence, this.group);
      hasDefaultValue = !!control.value;
      this[dependence] = control.value;

      control.valueChanges
        .subscribe(result => {
          if (this[dependence] !== result) {
            this[dependence] = result;
            this.control.setValue(null);
            this.dependencesCompleted();
          }
        });
    });

    if (hasDefaultValue) {
      this.dependencesCompleted(false);
    }
  }

  private dependencesCompleted(reset: boolean = true): void {
    let valid: boolean;
    const params: any = { language: this.language };
    this.config.action.dependencies.forEach(dependence => {
      valid = !!this[dependence];
      params[dependence] = this[dependence];
    });

    if (valid) {
      this.apiService.get<Observable<Array<IActionServiceResponse>>>(this.endpoint, { params })
        .pipe(delay(0), takeUntil(this.unsubscribeDestroy$))
        .subscribe(response => this.setData(response, reset));
    }
  }

  private onLoad(): void {
    this.apiService.get<Observable<Array<IActionServiceResponse>>>(this.endpoint, { params: { language: this.language } })
      .pipe(delay(0), takeUntil(this.unsubscribeDestroy$))
      .subscribe(response => this.setData(response, false));
  }

  private setData(list: Array<IActionServiceResponse>, reset: boolean = true): void {
    this.list = list || [];
    if (reset) {
      this.control.setValue(null);
    }
    this.setLabelValue();
  }

  private getControlByDependence(dependence: string, group: FormGroup | FormArray): AbstractControl {
    let control = group.controls[dependence];
    if (!control) {
      const parent = group.parent instanceof FormArray ? group.parent.parent : group.parent;
      control = this.getControlByDependence(dependence, parent);
    }

    return control;
  }

  getIcon(): string {
    const item = this.list.find(elem => elem.value === this.control.value);

    return (item || {} as IActionServiceResponse).icon;
  }

  setLabelValue(value?: any): void {
    if (this.controlLabel) {
      value = value || this.control.value;
      const tmp = this.list.find(item => item.value === value);
      this.controlLabel.setValue(tmp && tmp.text);
    }
  }

}
