import { Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-checkbox',
  template: `
  <div class="mb-2" [formGroup]="group">
    <ca-checkbox [formControlName]="config.code">{{ config.label }}</ca-checkbox>
  </div>
  `
})
export class FormCheckboxComponent extends DynamicFormComponentBase { }
