import { Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-align',
  template: `
  <div class="row align-items-center no-gutters mb-2">
    <div class="col-auto g-w-100" *ngIf="config.label">
      <label [for]="config.label" class="g-fw-bold g-fs-15">{{ config.label }}:</label>
    </div>
    <div class="col" [formGroup]="group">
      <div class="g-btn-group" [ngClass]="{ 'g-input-error' : validateControl() }">
        <label>
          <input type="radio" value="left" [name]="config.code" [formControlName]="config.code">
          <span class="button">
            <i class="la la-align-left"></i>
          </span>
        </label>
        <label>
          <input type="radio" value="center" [name]="config.code" [formControlName]="config.code">
          <span class="button">
            <i class="la la-align-center"></i>
          </span>
        </label>
        <label>
          <input type="radio" value="right" [name]="config.code" [formControlName]="config.code">
          <span class="button">
            <i class="la la-align-right"></i>
          </span>
        </label>
      </div>
    </div>
  </div>
  `
})
export class FormAlignComponent extends DynamicFormComponentBase { }
