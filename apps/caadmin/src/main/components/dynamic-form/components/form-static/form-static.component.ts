import { Component } from '@angular/core';

@Component({
  selector: 'caadmin-form-static',
  template: `
  <div class="g-card-form">
    <div class="p-3">
      <div class="g-bgc-white p-4 ">
        <div class="row w-100 no-gutters justify-content-center">
          <div class="col-8">
            <div class="row">
              <div class="col-3 text-right pr-0">
                <i class="la la-exclamation-circle g-fs-30"></i>
              </div>
              <div class="col-9">
                <h4 class="g-fs-20">
                  Este elemento no es editable.
                </h4>
                <span class="g-block">
                  Para cambiar la información de este elemento ir a Configuración General.
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `
})
export class FormStaticComponent { }
