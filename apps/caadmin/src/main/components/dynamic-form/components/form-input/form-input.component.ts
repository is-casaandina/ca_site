import { Component, Input } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-input',
  templateUrl: './form-input.component.html'
})
export class FormInputComponent extends DynamicFormComponentBase {
  @Input() label: string;
}
