import { Component } from '@angular/core';
import { UrlService } from '@ca-admin/services/url.service';
import { IUrlAutocompleteView } from '@ca-admin/statemanagement/models/url.interface';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';
import { DynamicFormService } from '../../providers/dynamic-form.service';

@Component({
  selector: 'caadmin-form-url',
  templateUrl: './form-url.component.html'
})
export class FormUrlComponent extends DynamicFormComponentBase {

  private urls: Array<IUrlAutocompleteView> = [];

  constructor(
    private urlService: UrlService,
    private dynamicFormService: DynamicFormService
  ) {
    super();
  }

  caOnInit(): void {
    const value = this.control.value;
    if (value) {
      this.urls.push(value);
    }
  }

  blur(): void {
    const value = this.urls.find(item => item.url === (this.control.value && this.control.value.url));
    this.control.setValue(value);
    this.focus();
  }
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.urlService.urlSearch({
          value: term, limit: 10,
          language: this.config.defaultLanguage || this.dynamicFormService.getLanguage()
        })
          .pipe(
            map(preResult => {
              this.urls = preResult;

              return this.urls;
            }),
            catchError(() => {
              return of([]);
            }))
      )
    )

  inputFormatter = (x: { url: string }) => x.url;
  resultFormatter = (x: { url: string }) => x.url ? x.url : x;

}
