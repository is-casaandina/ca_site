import { Component } from '@angular/core';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-radio',
  template: `
  <div [formGroup]="group">
    <label class="g-radio mb-2 mr-2" *ngFor="let item of config.list">
      <input type="radio" [name]="config.code" [value]="item" [formControlName]="config.code">
      {{ item }}
      <span class="checkmark"></span>
    </label>
  </div>
  `
})
export class FormRadioComponent extends DynamicFormComponentBase { }
