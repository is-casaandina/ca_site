import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, Inject, Renderer2, ViewChild } from '@angular/core';
import { Size } from '@ca-core/ui/common';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';

@Component({
  selector: 'caadmin-form-image',
  templateUrl: './form-image.component.html'
})
export class FormImageComponent extends DynamicFormComponentBase {
  @ViewChild('contentLayouts') contentLayouts: ElementRef<HTMLDivElement>;

  constructor(
    private renderer2: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) {
    super();
  }

  caOnInit(): void {
    this.addLayoutIcon();
  }

  private addLayoutIcon(): void {
    this.properties.typeSelected = this.properties.typeSelected || this.control.parent.parent.value.type;

    if (this.properties.typeSelected) {
      const layout: Array<string> = iconSlidePosition[this.properties.typeSelected];
      if (layout) {
        layout.forEach((size: Size, index: number) => {
          const div = this.document.createElement('div');
          this.renderer2.addClass(div, 'c-indicator-slider-item');
          size.split(' ')
            .forEach(siz => {
              this.renderer2.addClass(div, siz);
            });

          if (index + 1 === Number(this.properties.code)) {
            this.renderer2.addClass(div, 'active');
          }
          this.renderer2.appendChild(this.contentLayouts.nativeElement, div);

        });

        return;
      }
    }

    this.contentLayouts.nativeElement.parentElement.remove();
  }

}

export const iconSlidePosition = {
  layout1: ['lg', 'md', 'xs', 'xs'],
  layout2: ['lg', 'lg'],
  layout3: ['md', 'xs', 'xs', 'md', 'xs', 'xs'],
  layout4: ['md', 'md', 'md', 'md'],
  layout5: ['lg', 'md', 'md'],
  layout6: ['xs', 'xs', 'xs', 'xs', 'xs', 'xs', 'xs', 'xs'],
  layout7: ['xs', 'xs', 'md', 'xs', 'xs', 'md'],
  layout8: ['md', 'lg right', 'md'],
  layout9: ['xs', 'xs', 'lg right', 'xs', 'xs'],
  layout10: ['lg', 'xs', 'xs', 'xs', 'xs']
};
