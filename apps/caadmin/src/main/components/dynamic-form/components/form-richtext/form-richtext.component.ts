import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { QuillEditorComponent, QuillModules } from 'ngx-quill';
import * as prettifyHtml from 'prettify-html';
import * as Quill from 'quill';
import { fromEvent } from 'rxjs';
import { DynamicFormComponentBase } from '../../dynamic-form-base.component';
import { defaultToolbar, toolbarWithHtml } from '../../utils/toolbar';
import { FileEndpoint } from '@ca-admin/endpoints';
import { FileService } from '../../../../../providers/services/file.service';
import { ComponentService } from '@ca-admin/services/component.service';

@Component({
  selector: 'caadmin-form-richtext',
  templateUrl: './form-richtext.component.html'
})
export class FormRichtextComponent extends DynamicFormComponentBase implements AfterViewInit {
  @ViewChild(QuillEditorComponent) quill: QuillEditorComponent;
  modules: QuillModules;
  page_editor: any;
  page_editor_id:any;
  archivoCapturado:any;
  mostrando_boton_archivo= true;
  ruta_s3 =  null;
  ruta_s3_complete= null;
  archivo_permitido = true;

  constructor(private elementRef: ElementRef<HTMLElement>, private fileService:ComponentService, private fileServiceS: FileService) {
    super();
  }

  caOnInit(): void {
    this.loadToolbar();
    this.loadStyle();
    //console.log(JSON.parse(sessionStorage.getItem('PAGE_EDITOR')));
    this.page_editor = JSON.parse(sessionStorage.getItem('PAGE_EDITOR'));
    //console.log(this.page_editor.id);
    this.page_editor_id = this.page_editor.id;
    let nombre = this.page_editor_id+'.js';
    this.ruta_s3 = FileEndpoint.js_ruta;
    let n = /-/gi;
    let nombre_sin_guion = nombre.replace(n,"");
    var aleatorio =  Math.round(Math.random() * 1000);
    this.ruta_s3_complete = this.ruta_s3+'/'+nombre_sin_guion+'?'+aleatorio;
    //console.log("s3", this.ruta_s3_complete);
    this.fileServiceS.getJs(String(nombre))
    .subscribe(
      data => {
        //console.log("respuesta",data)
      },
      err => {
        //console.log("error respuesta js", err);
        if(err.status != 0){
          //console.log("archivo no existe", err);
          this.mostrando_boton_archivo = true;
        }else{
          //console.log("archivo existe", err);
          this.mostrando_boton_archivo = false;
        }
      });
  }

  ngAfterViewInit(): void {
    this.registerHtmlView();
  }

  private loadToolbar(): void {
    this.modules = { syntax: false, toolbar: this.config.type === 'richtext-html' ? toolbarWithHtml : defaultToolbar };
  }

  private loadStyle(): void {
    const SizeStyle = Quill.import('attributors/style/size');
    const FontAttributor = Quill.import('formats/font');
    FontAttributor.whitelist = ['sans serif', 'serif', 'monospace', 'roboto'];

    const AlignStyle = Quill.import('attributors/style/align');

    Quill.register(AlignStyle, true);
    Quill.register(FontAttributor, true);
    Quill.register(SizeStyle, true);
  }

  private registerHtmlView(): void {
    const txtArea = document.createElement('textarea');
    const styleArea = document.getElementById('styleArea');

    let  htmlstyle : Array<string> = []
    txtArea.placeholder = 'Ingresa html...';
    txtArea.className = 'ql-html--content';
    const htmlEditor = this.quill.quillEditor.addContainer('ql-html');
    txtArea.value = !this.control.value
      ? this.control.value
      : prettifyHtml(this.control.value);

      if((txtArea.value).includes('<style>')){
        htmlstyle = (txtArea.value).split('<style>');
        txtArea.value = (htmlstyle[0])
        styleArea.innerHTML = htmlstyle[1].split('</style>')[0]
        
      }

    htmlEditor.appendChild(txtArea);
    this.changeHtmlView(txtArea,styleArea);
  }

  private changeHtmlView(txtArea: HTMLTextAreaElement, styleArea : any = null): void {
    const dividerToolbarButton = this.elementRef.nativeElement.querySelector(
      '.ql-html'
    );
    this.quill.onContentChanged.subscribe(result => {
      if (!txtArea.classList.contains('active')) {
        txtArea.value = !result.html ? result.html : prettifyHtml(result.html);
      }
    });

    if (dividerToolbarButton) {
      fromEvent(dividerToolbarButton, 'click')
        .subscribe(() => {
          if (!txtArea.classList.contains('active')) {
            this.quill.setDisabledState(true);
          } else {
            this.quill.setDisabledState(false);
          }
          txtArea.classList.toggle('active');
        }
      );

      fromEvent([txtArea,styleArea], 'input')
        .subscribe(() =>{
          //console.log(txtArea.value)
          //console.log(styleArea.value)
        const styleOpen = '<style>'
        const styleClose = '</style>'
        const desing = txtArea.value  + '<style>' + styleArea.value + '</style>'
        this.control.setValue( desing )
        }
      );
    }
  }

  subirJs(event:any){
    this.archivoCapturado = event.target.files;
    //console.log(this.archivoCapturado[0].type);
    //console.log(this.archivoCapturado);
    this.fileService.file = undefined;
    this.fileService.name = undefined;
    this.archivo_permitido = true;
    if(this.archivoCapturado[0].type == 'text/javascript' || this.archivoCapturado[0].type == 'application/x-javascript'){
      if(this.archivoCapturado != undefined){
       this.fileService.file = this.archivoCapturado;
       this.fileService.name = this.page_editor_id+'.js';
       this.archivo_permitido = true;
     }
    }else{
      let input = <HTMLInputElement> document.getElementById('js_html');
      //console.log("entrando input", input.value);
      input.value='';
      this.archivo_permitido = false;
    }
    //console.log("1",this.fileService.file);
    //console.log("2",this.fileService.name)
  }

  presionarA(){
    window.open(this.ruta_s3_complete, '_blank');
  }
}
