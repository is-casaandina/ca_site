import { Input, OnInit } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { IProperty } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

export class DynamicFormComponentBase extends UnsubscribeOnDestroy implements OnInit {

  @Input() config: IProperty;
  @Input() properties: IProperty;
  @Input() group: FormGroup;
  @Input() code: any;

  control: AbstractControl;
  controlName: any;

  pattern: string;
  controlLabel: AbstractControl;

  ngOnInit(): void {
    this.controlName = this._getCode();
    this.control = this.group.controls[this.controlName];
    this.caOnInit();
    this.dependenceLabel();
  }

  validateControl(): boolean {
    return ValidatorUtil.validateControlByName(this.group, this.controlName);
  }

  focus(): void {
    this.control.markAsTouched({ onlySelf: true });
  }

  private _getCode(): any {
    // INFO: El code puede ser 0
    return this.code !== undefined ? this.code : this.config.code;
  }

  private dependenceLabel(): void {
    const controlName = `${this.controlName}-default-label`;
    const controlLabel = this.group.get(controlName);
    if (controlLabel) {
      this.controlLabel = controlLabel;
      this.group.addControl(controlName, this.controlLabel);

      this.setLabelValue(this.control.value);

      this.control.valueChanges
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(value => this.setLabelValue(value));
    }

  }

  setLabelValue(value: any): void {
    if (this.controlLabel) {
      this.controlLabel.setValue(value);
    }
  }

  caOnInit(): void { }

}
