import { Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Pipe({
  name: 'activeSlide',
  // tslint:disable-next-line:pipe-impure
  pure: false
})
export class ActiveSlidePipe implements PipeTransform {

  transform(controls: Array<FormGroup>, args?: any): any {
    return (controls || []).filter(control => control.controls.isSelected.value);
  }

}
