import { Pipe, PipeTransform } from '@angular/core';
import { ICustomComponent } from '@ca-admin/statemanagement/models/dynamic-form.interface';

@Pipe({
  name: 'customFormat'
})
export class CustomFormatPipe implements PipeTransform {

  transform(custom: ICustomComponent, args?: any): any {
    return custom.types.map(type => ({
      value: type,
      icon: custom.definitions[type].detail.icon,
      image: custom.definitions[type].detail.image,
      label: custom.definitions[type].detail.label,
      properties: custom.definitions[type].properties
    }));
  }

}
