import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoleService, UserService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang, UserLang } from '@ca-admin/settings/lang';
import { IRolePermissionsDetailResponse } from '@ca-admin/statemanagement/models/role.interface';
import { ISaveUserRequest, IUserView } from '@ca-admin/statemanagement/models/user.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'caadmin-add-user-form',
  templateUrl: './add-user-form.component.html'
})
export class AddUserFormComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {
  payload: IUserView;

  USER_LANG = UserLang;
  GENERAL_LANG = GeneralLang;

  frmAddUser: FormGroup;
  username: AbstractControl;
  name: AbstractControl;
  lastname: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  radioRoles: AbstractControl;
  rolesList: Array<IRolePermissionsDetailResponse>;

  edit = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private roleService: RoleService,
    private notificationService: NotificationService,
    private activeModal: ActiveModal
  ) {
    super();
  }

  ngOnInit(): void {
    this.edit = !isNullOrUndefined(this.payload);
    this.createForm();
    this.getRoles();
  }

  private createForm(): void {
    const user = (this.payload) || {} as IUserView;
    this.frmAddUser = this.formBuilder.group({
      username: [{ value: user.username, disabled: this.edit }, Validators.required],
      name: [user.name, Validators.required],
      lastname: [user.lastname, Validators.required],
      email: [user.email, Validators.required],
      password: [{ value: null, disabled: this.edit }, Validators.required],
      radioRoles: [null, Validators.required]
    });

    this.username = this.frmAddUser.get('username');
    this.name = this.frmAddUser.get('name');
    this.lastname = this.frmAddUser.get('lastname');
    this.email = this.frmAddUser.get('email');
    this.password = this.frmAddUser.get('password');
    this.radioRoles = this.frmAddUser.get('radioRoles');

  }

  validateControl(controlName: string): boolean {
    return ValidatorUtil.validateControlByName(this.frmAddUser, controlName);
  }

  private getRoles(): void {
    this.roleService.rolesListPermissionsDetail()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IRolePermissionsDetailResponse>) => {
        this.rolesList = response;
        if (!isNullOrUndefined(this.payload)) {
          const role = response.find((value: IRolePermissionsDetailResponse) => {
            return value.roleCode === this.payload.role.roleCode;
          });
          this.radioRoles.setValue(role);
        }
      });
  }

  private _validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmAddUser);

    return this.frmAddUser.valid;
  }

  private _paramsSaveUser(): ISaveUserRequest {
    return {
      username: this.username.value,
      name: this.name.value,
      lastname: this.lastname.value,
      password: this.password.value,
      email: this.email.value,
      roleCode: this.radioRoles.value.roleCode
    } as ISaveUserRequest;
  }

  save(): void {
    const validateForm = this._validateForm();
    if (validateForm) {
      const params = this._paramsSaveUser();
      if (isNullOrUndefined(this.payload)) {
        this.userService.saveUser(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      } else {
        this.userService.updateUser(this.payload.id, params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      }

    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
