import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  CaButtonModule,
  CaCardModule,
  CaInputModule,
  CaRadioButtonModule
} from '@ca-core/ui/lib/components';
import { AddUserFormComponent } from './add-user-form.component';

@NgModule({
  declarations: [AddUserFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaCardModule,
    CaRadioButtonModule,
    CaButtonModule,
    CaInputModule
  ],
  exports: [AddUserFormComponent],
  entryComponents: [AddUserFormComponent]
})
export class AddUserFormModule { }
