import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { DynamicFormComponentBase } from '@ca-admin/components/dynamic-form/dynamic-form-base.component';
import { environment } from '@ca-admin/environments/environment';
import { IActionServiceResponse } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap, takeUntil } from 'rxjs/operators';

const TYPE_AUTOCOMPLETE = {
  text: 1,
  value: 2
};

@Component({
  selector: 'caadmin-autocomplete',
  template: `
  <div class="g-input-autocomplete">
    <div class="g-input-autocomplete-search">
      <i class="la la-search"></i>
    </div>
    <input class="g-input"
      type="text"
      [attr.placeholder]="placeholder || ''"
      [formControl]="controlAutocomplete"
      [ngbTypeahead]="search"
      [inputFormatter]="inputFormatter"
      [resultFormatter]="resultFormatter"
      (blur)="blur()"/>
  </div>
  `
})
export class AutocompleteComponent extends DynamicFormComponentBase {

  @Input() placeholder: string;
  @Input() language: string;
  @Input() request: string;

  get disabled(): boolean {
    return this.control.disabled;
  }

  controlAutocomplete: FormControl;

  private list: Array<IActionServiceResponse> = [];
  private endpoint: string;

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder
  ) {
    super();
  }

  caOnInit(): void {
    this.controlAutocomplete = this.fb.control('autocomplete');

    this.endpoint = `${environment.API_URL}${this.request}`;
    this.onReset();

    if (this.control.value) {
      this.getdata(this.control.value, TYPE_AUTOCOMPLETE.value)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(data => {
          this.list = data || [];
          if (this.list.length) {
            this.controlAutocomplete.setValue(this.list[0]);
          }
        });
    }
  }

  private onReset(): void {
    this.control.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(value => {
        if (this.controlAutocomplete.value !== value) {
          this.controlAutocomplete.setValue(value);
        }
      });
  }

  blur(): void {
    const data = this.list.find(item => item.text === (this.controlAutocomplete.value || {}).text);
    this.control.setValue(data && data.value || '');
    this.controlAutocomplete.setValue(data);
    this.focus();
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term =>
        this.getdata(term, TYPE_AUTOCOMPLETE.text)
          .pipe(
            map(preResult => {
              this.list = preResult;

              return this.list;
            }),
            catchError(() => {
              return of([]);
            }))
      )
    )

  private getdata(value: string, type: number): IObservableArray<IActionServiceResponse> {
    return this.apiService.get<IObservableArray<IActionServiceResponse>>(this.endpoint,
      {
        params: {
          value,
          limit: 10,
          type,
          language: this.language
        }
      });
  }

  inputFormatter = (x: { text: string }) => x.text;
  resultFormatter = (x: { text: string }) => x.text ? x.text : x;

}
