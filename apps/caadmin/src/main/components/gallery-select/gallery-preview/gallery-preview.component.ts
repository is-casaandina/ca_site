import { Component, OnInit } from '@angular/core';
import { FILE_TYPE } from '@ca-admin/statemanagement/models/file.interface';
import { IPayloadModalComponent } from '@ca-core/shared/helpers/modal';

@Component({
  selector: 'caadmin-gallery-preview',
  templateUrl: './gallery-preview.component.html'
})
export class GalleryPreviewComponent implements OnInit, IPayloadModalComponent {

  payload: { file: string; type: FILE_TYPE, mimetype: string };
  FILE_TYPE = FILE_TYPE;

  constructor() { }

  ngOnInit(): void {
  }

}
