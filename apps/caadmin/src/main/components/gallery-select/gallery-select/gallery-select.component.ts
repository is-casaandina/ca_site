import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DIMENTION_IMAGE } from '@ca-admin/settings/constants/general.constant';
import { IMAGE_SIZE } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { FILE_RESPONSE, FILE_TYPE, IMultimediaResponse } from '@ca-admin/statemanagement/models/file.interface';
import { IModalConfig, ModalService } from '@ca-core/shared/helpers/modal';
import { ModalRef } from '@ca-core/shared/helpers/modal/models/modal-ref';
import { Size } from '@ca-core/ui/common';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { GalleryModalComponent } from '../gallery-modal/gallery-modal.component';

@Component({
  selector: 'caadmin-gallery-select',
  templateUrl: './gallery-select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GallerySelectComponent),
      multi: true
    }
  ]
})
export class GallerySelectComponent implements OnInit, ControlValueAccessor {

  protected _value: any;
  @Input()
  get value(): any {
    return this._value;
  }
  set value(value: any) {
    this._value = value;
    this.propagateChange(this._value);
  }

  protected _error = false;
  @Input()
  get error(): boolean {
    return this._error;
  }
  set error(value: boolean) {
    this._error = coerceBooleanProp(value);
  }

  @Input() maxLength: number;
  /** Valor en megabytes */
  @Input() maxSize: number;
  @Input() mimeTypes: Array<string>;
  @Input() type: FILE_TYPE;
  @Input() placeholder: string;
  @Input() buttonText: string;
  @Input() size: IMAGE_SIZE;
  @Input() responseFormat: FILE_RESPONSE = FILE_RESPONSE.object;

  private _model: Array<IMultimediaResponse> = [];
  get model(): Array<IMultimediaResponse> {
    return this._model;
  }
  set model(model: Array<IMultimediaResponse>) {
    this._model = model || [];

    if (this.responseFormat === FILE_RESPONSE.string) {
      this.value = this.model.map(mod => mod.url ? mod.url : mod.large.imageUri)
        .join('');
    } else {
      this.value = model;
    }

  }

  FILE_TYPE = FILE_TYPE;

  private modalConfig: IModalConfig;
  private modalRef: ModalRef;

  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit(): void {
    const typeText = this.type === FILE_TYPE.document ? 'documento' :
      this.type === FILE_TYPE.video ? 'video' : 'imagen';

    this.buttonText = this.buttonText || `Subir ${typeText}`;

    this.modalConfig = {
      size: DIMENTION_IMAGE.large.abbreviation as Size,
      title: `Seleccionar ${typeText}`,
      classHeader: 'text-left text-capitalize',
      classBody: 'pb-0 pr-3',
      notificationDuration: 5000
    };
  }

  openModal(): void {
    this.modalRef = this.modalService.open(GalleryModalComponent, this.modalConfig);
    this.modalRef.setPayload({
      mimeTypes: this.mimeTypes,
      maxSize: this.maxSize || 999999,
      type: this.type,
      defaultFile: this.value && this.value[0] || null
    });
    this.modalRef.result
      .then((images: Array<IMultimediaResponse>) => {
        if (images && images.length) { this.addImages(images); }
      })
      .catch(err => {

      });
  }

  private addImages(images: Array<IMultimediaResponse>): void {
    const value = this.model || [];
    let tmp = images.filter(image => !value.find(val => val === image));
    if (this.maxLength > 1) {
      tmp = value.concat(tmp);
    }
    if (this.maxLength > 0) { tmp.splice(this.maxLength); }
    this.model = tmp;
  }

  removeImage(index: number): void {
    this.model.splice(index, 1);
    if (!this.model.length) { this.setModelNull(); }
  }

  /* Takes the value  */
  writeValue(value: any): void {
    if (value !== undefined) {
      this._value = value;
      this._model = this.responseFormat === FILE_RESPONSE.string && value ?
        [{ url: value } as IMultimediaResponse] : value;
    } else {
      this.setModelNull();
    }
  }

  private setModelNull(): void {
    this.model = null;
  }

  propagateChange = (_: any) => { };

  registerOnChange(fn): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void { }

}
