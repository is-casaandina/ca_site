import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileService } from '@ca-admin/services';
import { DateUtil } from '@ca-core/shared/helpers/util/date';
import * as _ from 'underscore';

@Component({
  selector: 'caadmin-gallery-filter-date',
  templateUrl: './gallery-filter-date.component.html'
})
export class GalleryFilterDateComponent implements OnInit {

  @Output() selected: EventEmitter<string> = new EventEmitter<string>();
  @Input() current: string;
  @Input() type: string;

  list: Array<{ dates: Array<string>, year: string }> = [];

  constructor(
    private fileService: FileService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(type?: string): void {
    this.fileService.rangeDateByType(type || this.type)
      .subscribe(range => {
        const dateList = DateUtil.dateRange(range.dateMin, range.dateMax);
        const groups = _.groupBy(dateList, (date: string) => date.split('-')[0]);
        this.list = _.map(groups, (dates: Array<string>, year: string) => ({ dates, year }))
          .reverse();
      });
  }

  selectedFn(date: string): void {
    this.current = date === this.current ? null : date;
    this.selected.next(this.current);
  }

}
