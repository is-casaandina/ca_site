import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebounceInputDirectiveModule } from '@ca-core/shared/helpers/directives/debounce-input.directive';
import { IconFileDirectiveModule } from '@ca-core/shared/helpers/directives/icon-file.directive';
import { InfiniteScrollerDirectiveModule } from '@ca-core/shared/helpers/directives/infinite-scroller.directive';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { CaInputModule, CaNotifyModule } from '@ca-core/ui/lib/components';
import { VideoModule } from '@ca/library';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { GalleryFilterDateComponent } from './gallery-filter-date/gallery-filter-date.component';
import { GalleryModalComponent } from './gallery-modal/gallery-modal.component';
import { GalleryPreviewComponent } from './gallery-preview/gallery-preview.component';
import { GallerySelectComponent } from './gallery-select/gallery-select.component';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    CaNotifyModule,
    PipesModule,
    CaInputModule,
    VideoModule,
    SweetAlert2Module,
    InfiniteScrollerDirectiveModule,
    DebounceInputDirectiveModule,
    IconFileDirectiveModule
  ],
  declarations: [GalleryModalComponent, GallerySelectComponent, GalleryFilterDateComponent, GalleryPreviewComponent],
  exports: [GalleryModalComponent, GallerySelectComponent, GalleryFilterDateComponent, GalleryPreviewComponent],
  entryComponents: [GalleryModalComponent, GallerySelectComponent, GalleryFilterDateComponent, GalleryPreviewComponent]
})
export class GallerySelectModule { }
