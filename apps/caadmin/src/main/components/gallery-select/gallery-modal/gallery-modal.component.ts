import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DOCUMENT } from '@angular/platform-browser';
import { FileService } from '@ca-admin/services';
import { IMAGE_SIZE } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { FILE_TYPE, IMultimediaProperty, IMultimediaResponse } from '@ca-admin/statemanagement/models/file.interface';
import { MultimediaBase } from '@ca-admin/views/multimedia/multimedia-base';
import { ActiveModal, ModalService } from '@ca-core/shared/helpers/modal';
import { IPayloadModalComponent } from '@ca-core/shared/helpers/modal/models/modal.interface';
import { NOTIFICATION_TYPES, NotificationService } from '@ca-core/shared/helpers/notification';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';
import { GalleryPreviewComponent } from '../gallery-preview/gallery-preview.component';

@Component({
  selector: 'caadmin-gallery-modal',
  templateUrl: './gallery-modal.component.html'
})
export class GalleryModalComponent extends MultimediaBase implements OnInit, IPayloadModalComponent {

  @ViewChild('file') input: ElementRef<HTMLInputElement>;
  @ViewChild('inputSearch') inputSearch: ElementRef<HTMLInputElement>;

  NOTIFICATION_TYPES = NOTIFICATION_TYPES;
  FILE_TYPE = FILE_TYPE;

  currentSearchText: string;
  currentDate: string;
  searchInput: string;

  typeLabel: string;

  isLastPage: boolean;

  deleteSwal: SweetAlertOptions;
  typeText: string;

  iconNoSelectedFile: string;
  textSelectedFile: string;

  constructor(
    protected notificationService: NotificationService,
    protected swalConfirmService: SwalConfirmService,
    protected modalService: ModalService,
    protected activeModal: ActiveModal,
    protected fileService: FileService,
    protected fb: FormBuilder,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(notificationService, fileService, fb, document);
  }

  ngOnInit(): void {
    this.typeText = this.payload.type === FILE_TYPE.document ? 'el documento' :
      this.payload.type === FILE_TYPE.video ? 'el video' : 'la imagen';

    this.textSelectedFile = this.payload.type === FILE_TYPE.document ? 'un documento' :
      this.payload.type === FILE_TYPE.video ? 'un video' : 'una imagen';

    this.iconNoSelectedFile = this.payload.type === FILE_TYPE.document ? 'la la-file' :
      this.payload.type === FILE_TYPE.video ? 'la la-file-movie-o' : 'la la-image';

    this.payload.mimeTypes = this.payload.mimeTypes || [];
    this.deleteSwal = this.swalConfirmService.warning(`¿Seguro de eliminar ${this.typeText}?`);

    this.setDefaultFile();
    this.getFiles();
  }

  private setDefaultFile(): void {
    if (this.payload.defaultFile && typeof this.payload.defaultFile !== 'string') {
      this.selected(this.payload.defaultFile);
    }
  }

  debounceInput(text: string): void {
    this.currentSearchText = text;
    this.files = [];
    this.isLastPage = false;
    this.getFiles();
  }

  selectedDate(date: string): void {
    this.currentDate = date;
    this.openFilter = false;
    this.files = [];
    this.isLastPage = false;
    this.getFiles();
  }

  selected(file: IMultimediaResponse): void {
    this.openFilter = false;
    this.currentImage = this.currentImage === file ? null : file;
    this.createForm();
  }

  confirm(): void {
    ValidatorUtil.validateForm(this.form);
    if (this.form.valid) {
      if (this.currentImage) {
        this.updateImageProperties();
        this.currentImage.properties = this.form.getRawValue();
      }
      this.activeModal.close([this.currentImage]);
      this.currentImage = null;
    } else {
      this.notificationService.addWarning('Debe completar todos los campos');
    }
  }

  private updateImageProperties(): void {

    if (this.payload.type === FILE_TYPE.image) {
      const properties = this.currentImage.properties || {} as IMultimediaProperty;
      const newProperties = this.form.getRawValue() || {} as IMultimediaProperty;

      if (!properties.alt) { properties.alt = newProperties.alt; }
      if (!properties.title) { properties.title = newProperties.title; }
      if (!properties.description) { properties.description = newProperties.description; }

      const subscribe = this.fileService.updateImage({ properties, id: this.currentImage.id } as IMultimediaResponse)
        .subscribe(() => subscribe.unsubscribe(), () => subscribe.unsubscribe());
    }
  }

  getFiles(): void {
    if (!this.isLastPage) {
      this.showLoading = true;
      const tmp = (this.currentDate || '').split('-');
      tmp.splice(tmp.length - 1);
      const date = tmp.join('-');

      this.fileService.getFilesByType(
        this.payload.type,
        {
          value: this.currentSearchText || '',
          date,
          page: Number(Number((this.files.length / 24) + 1)
            .toFixed(0)),
          size: 24
        })
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(files => {
          files.content
            .filter(file => this.payload.type !== FILE_TYPE.document && this.payload.type !== FILE_TYPE.badge ?
              file.large && file.medium && file.small : file.url)
            .forEach(img => this.files.push(img));
          this.showLoading = false;
          this.isLastPage = files.last;
        }, () => {
          this.showLoading = false;
          this.isLastPage = false;
        });
    }
  }

  delete(): void {
    this.openFilter = false;
    this.fileService.removeFileByType(this.payload.type, this.currentImage.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        () => {
          const index = this.files.indexOf(this.currentImage);
          if (index > -1) {
            this.files.splice(index, 1);
          }

          this.currentImage = null;
        },
        () => {
          this.activeError(this.lang.errors.image.failDelete);
        });
  }

  openPreview(): void {
    if (this.payload.type !== FILE_TYPE.document) {
      const modalConfig = {
        classDialog: 'multimedia',
        size: IMAGE_SIZE.lg
      };

      const modalRef = this.modalService.open(GalleryPreviewComponent, modalConfig);
      modalRef.setPayload({
        file: this.currentImage.url || this.currentImage.large.imageUri,
        mimetype: this.currentImage.mimeType,
        type: this.payload.type
      });
      modalRef.result
        .then(() => {
        })
        .catch(() => {

        });
    } else {
      const a = this.document.createElement<'a'>('a');
      a.href = this.currentImage.url;
      a.target = '_blank';
      a.click();
    }
  }
}
