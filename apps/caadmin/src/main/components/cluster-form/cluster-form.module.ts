import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaInputModule } from '@ca-core/ui/lib/components';
import { ClusterFormComponent } from './cluster-form.component';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    CaInputModule,
    CaButtonModule,
    ReactiveFormsModule
  ],
  declarations: [ClusterFormComponent],
  exports: [ClusterFormComponent],
  entryComponents: [ClusterFormComponent]
})
export class ClusterFormModule { }
