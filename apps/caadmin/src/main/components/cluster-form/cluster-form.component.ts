import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClusterService } from '@ca-admin/services/cluster.service';
import { CountryService } from '@ca-admin/services/country.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IClusterRequest, IClusterResponse } from '@ca-admin/statemanagement/models/cluster.interface';
import { ICountryResponse } from '@ca-admin/statemanagement/models/country.interface';
import { ActiveModal, IPayloadModalComponent } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'caadmin-cluster-form',
  templateUrl: './cluster-form.component.html'
})
export class ClusterFormComponent extends UnsubscribeOnDestroy implements OnInit, IPayloadModalComponent {
  payload: IClusterResponse;

  GENERAL_LANG = GeneralLang;

  countryList: Array<ICountryResponse> = [];
  titlePage = 'Agregar nuevo Cluster';

  frmCluster: FormGroup;
  name: AbstractControl;
  countries: AbstractControl;
  @ViewChild('selectCountry') country: ElementRef;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private clusterService: ClusterService,
    private countryService: CountryService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.countryService.countries()
      .subscribe((response: Array<ICountryResponse>) => {
        this.countryList = response;
      });
    if (!isNullOrUndefined(this.payload)) {
      this.titlePage = 'Editar Cluster';
    }
  }

  private createForm(): void {
    const cluster = (this.payload) || {} as IClusterResponse;
    const countries = cluster.countries || [];
    this.frmCluster = this.fb.group({
      name: [cluster.name, Validators.required],
      countries: [countries.map(country => country.id), Validators.required]
    });

    this.name = this.frmCluster.get('name');
    this.countries = this.frmCluster.get('countries');
  }

  addCountry(): void {
    const countries = this.countries.value as Array<string> || [];
    const countySelected = this.country.nativeElement.value;
    this.countries.setValue([...countries, countySelected]);
  }

  deleteCountry(country: ICountryResponse): void {
    const countries = this.countries.value as Array<string> || [];
    const indexCountry = countries.indexOf(country.id);
    countries.splice(indexCountry, 1);
    this.countries.setValue([...countries]);
  }

  getSelectedCountries(): Array<ICountryResponse> {
    const countries = this.countries.value as Array<string> || [];

    return this.countryList.filter(country => countries.includes(country.id));
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmCluster);

    return this.frmCluster.valid;
  }

  private paramsSaveUser(): IClusterRequest {
    return {
      name: this.name.value,
      status: true,
      countries: this.getSelectedCountries()
    } as IClusterRequest;
  }

  save(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveUser();
      if (isNullOrUndefined(this.payload)) {
        this.clusterService.saveCluster(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      } else {
        this.clusterService.updateCluster(this.payload.id, params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.activeModal.close(ACTION_TYPES.save);
          });
      }

    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
