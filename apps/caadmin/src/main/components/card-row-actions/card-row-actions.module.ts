import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { CardRowActionsComponent } from './card-row-actions.component';

@NgModule({
  imports: [
    CommonModule,
    SweetAlert2Module
  ],
  declarations: [CardRowActionsComponent],
  exports: [CardRowActionsComponent]
})
export class CardRowActionsModule { }
