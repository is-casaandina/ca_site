import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { coerceBooleanProp } from '@ca-core/ui/common/helpers';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'caadmin-card-row-actions',
  templateUrl: './card-row-actions.component.html'
})
export class CardRowActionsComponent implements OnChanges {

  @Output() edit: EventEmitter<boolean>;
  @Output() delete: EventEmitter<boolean>;
  @Input() confirmDelete = '';

  private _disabledEdit = false;
  @Input()
  get disabledEdit(): boolean {
    return this._disabledEdit;
  }
  set disabledEdit(value: boolean) {
    this._disabledEdit = coerceBooleanProp(value);
  }

  private _disabledDelete = false;
  @Input()
  get disabledDelete(): boolean {
    return this._disabledDelete;
  }
  set disabledDelete(value: boolean) {
    this._disabledDelete = coerceBooleanProp(value);
  }

  deleteSwal: SweetAlertOptions;

  constructor(
    protected swalConfirmService: SwalConfirmService
  ) {
    this.edit = new EventEmitter<boolean>();
    this.delete = new EventEmitter<boolean>();
    this.formatDelete();
  }

  ngOnChanges(): void {
    this.formatDelete();
  }

  private formatDelete(): void {
    if (!this.confirmDelete) {
      this.deleteSwal = this.swalConfirmService
        .warning(
          'Una vez eliminado no podrá ingresar ni recuperar la información.<br>',
          `¿Seguro de eliminar el registro?`
        );
    } else {
      this.deleteSwal = this.swalConfirmService.warning(this.confirmDelete);
    }
  }

  onEdit(): void {
    if (this.edit) {
      this.edit.emit(true);
    }
  }

  onDelete(): void {
    if (this.delete) {
      this.delete.emit(true);
    }
  }

}
