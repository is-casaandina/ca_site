import { Component, Input, OnInit } from '@angular/core';
import { SEOService } from '@ca-admin/services/seo.service';
import { DISTINCTFUNC, RULES_SEO_VALID, SPLITFUNC } from '@ca-admin/settings/constants/rules-seo.constant';
import { SEOConfiguration } from '@ca-admin/statemanagement/models/seo.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-seo-snippet-preview',
  templateUrl: './seo-snippet-preview.component.html',
  styleUrls: ['./seo-snippet-preview.component.scss']
})
export class SeoSnippetPreviewComponent extends UnsubscribeOnDestroy implements OnInit {

  @Input() isMobile: boolean;

  seo: SEOConfiguration;

  viewData: {
    title?: string;
    description?: string;
    path?: string
  } = {};

  ELLIPSIS = '...';

  constructor(
    private seoService: SEOService
  ) {
    super();
  }

  ngOnInit(): void {
    this.seoService.configuration$
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe((seoConfiguration: SEOConfiguration) => {
      this.seo = seoConfiguration;
      this.setTitle();
      this.setPath();
      this.setDescription();
    });
  }

  setPath(): void {
    this.viewData.path = `${this.seo.urlSite}/${this.seo.slug}`.replace(/\/+/g, '/');
  }

  setDescription(): void {
    let description = (this.seo && this.seo.description || this.seo.descriptionSite) || '';
    if (description.length > 160) {
      const description_words = description.split(' ');
      // delete last word
      description_words.pop();
      description = `${description_words.join(' ')} ${this.ELLIPSIS}`;
    }

    const keywords = `${(this.seo && this.seo.focusKeyword) || ''} ${(this.seo && this.seo.secondaryKeywords) || ''}`;
    if (keywords.trim()) {
      const wordsAll = SPLITFUNC(keywords);
      const words = wordsAll.filter(DISTINCTFUNC);
      words.forEach(word => {
        const re = new RegExp(`\\b(${word})\\b`, 'gi');
        description = description.replace(re, `<strong>$1</strong>`);
      });

    }

    this.viewData.description = description;
  }

  setTitle(): void {
    let title = (this.seo && this.seo.title) || '';
    if (title.length > RULES_SEO_VALID.pageTitleMaxLength) {
      const title_words = title.split(' ');
      // delete last word
      title_words.pop();
      title = `${title_words.join(' ')} ${this.ELLIPSIS}`;
    }

    const keywords = `${(this.seo && this.seo.focusKeyword) || ''}`;
    if (keywords.trim()) {
      const wordsAll = keywords.trim()
        .replace(/\s\s+/g, ' ')
        .split(' ');
      const distinct = (value: string, index: number, self: Array<string>) => self.indexOf(value) === index;
      const words = wordsAll.filter(distinct);
      words.forEach(word => {
        const re = new RegExp(`\\b(${word})\\b`, 'gi');
        title = title.replace(re, `<strong>$1</strong>`);
      });

    }

    this.viewData.title = title;
  }

}
