import { Component, OnInit } from '@angular/core';
import { SEOService } from '@ca-admin/services/seo.service';
import { RULES_SEO_VALID } from '@ca-admin/settings/constants/rules-seo.constant';
import { SEOConfiguration } from '@ca-admin/statemanagement/models/seo.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-seo-form-meta',
  templateUrl: './seo-form-meta.component.html',
  styleUrls: ['./seo-form-meta.component.scss']
})
export class SeoFormMetaComponent extends UnsubscribeOnDestroy implements OnInit {

  seo: SEOConfiguration;
  titleContent: Array<{ content: string; variable: boolean }> = [];
  rulesValid = RULES_SEO_VALID;

  constructor(
    private seoService: SEOService
  ) {
    super();
  }

  ngOnInit(): void {
    this.seoService.configuration$
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe((seoConfiguration: SEOConfiguration) => {
      this.seo = seoConfiguration;
      this.titleContent = seoConfiguration.titleContent;
    });
  }

  changePartialTitle($event: any, index: number): void {
    this.seoService.setVariable($event.target.value, index);
  }

  changeTitleContent($event: any): void {
    this.seoService.setTitle($event.target.value);
  }

  changeSlug($event: any): void {
    this.seoService.setSlug($event.target.value);
  }

  changeDescription($event: any): void {
    this.seoService.setDescription($event.target.value);
  }

}
