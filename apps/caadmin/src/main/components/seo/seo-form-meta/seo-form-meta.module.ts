import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoInputContentModule } from '../seo-input-content/seo-input-content.module';
import { SeoFormMetaComponent } from './seo-form-meta.component';

@NgModule({
  imports: [
    CommonModule,
    SeoInputContentModule
  ],
  declarations: [SeoFormMetaComponent],
  exports: [SeoFormMetaComponent]
})
export class SeoFormMetaModule { }
