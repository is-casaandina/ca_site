import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaAccordionModule } from '@ca-core/ui/lib/components';
import { SeoBtnPlatformModule } from './seo-btn-platform/seo-btn-platform.module';
import { SeoFormAdvanceModule } from './seo-form-advance/seo-form-advance.module';
import { SeoFormMetaModule } from './seo-form-meta/seo-form-meta.module';
import { SeoFormComponent } from './seo-form/seo-form.component';
import { SeoFormModule } from './seo-form/seo-form.module';
import { SeoInputContentModule } from './seo-input-content/seo-input-content.module';
import { SeoSnippetPreviewModule } from './seo-snippet-preview/seo-snippet-preview.module';
import { SeoSocialSnippetPreviewModule } from './seo-social-snippet-preview/seo-social-snippet-preview.module';

@NgModule({
  imports: [
    CommonModule,
    CaAccordionModule,
    SeoBtnPlatformModule,
    SeoSnippetPreviewModule,
    SeoSocialSnippetPreviewModule,
    SeoInputContentModule,
    SeoFormMetaModule,
    SeoFormModule,
    SeoFormAdvanceModule
  ],
  declarations: [],
  exports: [SeoFormComponent]
})
export class SeoModule { }
