import { Component, OnInit } from '@angular/core';
import { SEOService } from '@ca-admin/services/seo.service';
import { SEOConfiguration } from '@ca-admin/statemanagement/models/seo.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-seo-social-snippet-preview',
  templateUrl: './seo-social-snippet-preview.component.html',
  styleUrls: ['./seo-social-snippet-preview.component.scss']
})
export class SeoSocialSnippetPreviewComponent extends UnsubscribeOnDestroy implements OnInit {
  currentTabSocial = 1;

  seo: SEOConfiguration;

  facebook: {
    title?: string;
    description?: string;
    image?: string;
  };

  twitter: {
    title?: string;
    description?: string;
    image?: string;
  };

  constructor(
    private seoService: SEOService
  ) {
    super();
    this.facebook = {};
    this.twitter = {};
  }

  ngOnInit(): void {
    this.seoService.configuration$
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe((seoConfiguration: SEOConfiguration) => {
      this.seo = seoConfiguration;
      this.setDatos();
    });
  }

  setDatos(): void {
    this.facebook.title = this.seo.social.ogTitle || this.seo.title;
    this.facebook.description = this.seo.social.ogDescription || this.seo.description || this.seo.descriptionSite || '';
    this.facebook.image = this.seo.social.ogImage || 'https://placehold.it/1200x630';

    this.twitter.title = this.seo.social.twitterTitle || this.seo.social.ogTitle || this.seo.title;
    this.twitter.description = this.seo.social.twitterDescription ||
      this.seo.social.ogDescription ||
      this.seo.description ||
      this.seo.descriptionSite || '';
    this.twitter.image = this.seo.social.twitterImage || this.seo.social.ogImage || 'https://placehold.it/1024x510';
  }

  changeTitle($event: any, isFacebook = true): void {
    this.seoService.setTitleSocial($event.target.value, isFacebook);
  }

  changeDescription($event: any, isFacebook = true): void {
    this.seoService.setDescriptionSocial($event.target.value, isFacebook);
  }

  changeImage($event: any, isFacebook = true): void {
    this.seoService.setImageSocial($event, isFacebook);
  }

}
