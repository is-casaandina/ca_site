import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CaInputModule } from '@ca-core/ui/lib/components';
import { GallerySelectModule } from '../../gallery-select/gallery-select.module';
import { SeoSocialSnippetPreviewComponent } from './seo-social-snippet-preview.component';

@NgModule({
  imports: [
    CommonModule,
    CaInputModule,
    GallerySelectModule,
    FormsModule
  ],
  declarations: [SeoSocialSnippetPreviewComponent],
  exports: [SeoSocialSnippetPreviewComponent]
})
export class SeoSocialSnippetPreviewModule { }
