import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoBtnPlatformComponent } from './seo-btn-platform.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SeoBtnPlatformComponent],
  exports: [SeoBtnPlatformComponent]
})
export class SeoBtnPlatformModule { }
