import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaAccordionModule, CaInputModule } from '@ca-core/ui/lib/components';
import { GallerySelectModule } from '../../gallery-select/gallery-select.module';
import { SeoBtnPlatformModule } from '../seo-btn-platform/seo-btn-platform.module';
import { SeoFormAdvanceModule } from '../seo-form-advance/seo-form-advance.module';
import { SeoFormMetaModule } from '../seo-form-meta/seo-form-meta.module';
import { SeoSnippetPreviewModule } from '../seo-snippet-preview/seo-snippet-preview.module';
import { SeoSocialSnippetPreviewModule } from '../seo-social-snippet-preview/seo-social-snippet-preview.module';
import { SeoValidMetadataModule } from '../seo-valid-metadata/seo-valid-metadata.module';
import { SeoFormComponent } from './seo-form.component';

@NgModule({
  imports: [
    CommonModule,
    CaAccordionModule,
    CaInputModule,
    GallerySelectModule,
    SeoBtnPlatformModule,
    SeoSnippetPreviewModule,
    SeoSocialSnippetPreviewModule,
    SeoFormMetaModule,
    SeoValidMetadataModule,
    SeoFormAdvanceModule
  ],
  declarations: [SeoFormComponent],
  exports: [SeoFormComponent]
})
export class SeoFormModule { }
