import { Component, OnInit } from '@angular/core';
import { GeneralService } from '@ca-admin/services';
import { SEOService } from '@ca-admin/services/seo.service';
// import { IConfigurationResponse } from '@ca-admin/statemanagement/models/configuration.interface';

@Component({
  selector: 'caadmin-seo-form',
  templateUrl: './seo-form.component.html',
  styleUrls: ['./seo-form.component.scss']
})
export class SeoFormComponent implements OnInit {

  isMobile = true;

  constructor(
    private seoService: SEOService,
    private generalService: GeneralService
  ) { }

  currentTabSeo = 1;

  ngOnInit(): void {
    // const general = this.generalService.getConfiguration(false)
    //   .subscribe((response: IConfigurationResponse) => {
    //     this.seoService.setInfoSite(response.metadata.title, response.domain, response.metadata.description);
    //     general.unsubscribe();
    //   });
  }

}
