import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SeoFormAdvanceComponent } from './seo-form-advance.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SeoFormAdvanceComponent],
  exports: [SeoFormAdvanceComponent]
})
export class SeoFormAdvanceModule { }
