import { Component, OnInit } from '@angular/core';
import { SEOService } from '@ca-admin/services/seo.service';
import { SEOConfiguration } from '@ca-admin/statemanagement/models/seo.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-seo-form-advance',
  templateUrl: './seo-form-advance.component.html',
  styleUrls: ['./seo-form-advance.component.scss']
})
export class SeoFormAdvanceComponent extends UnsubscribeOnDestroy implements OnInit {
  seo: SEOConfiguration;

  constructor(
    private seoService: SEOService
  ) {
    super();
  }

  ngOnInit(): void {
    this.seoService.configuration$
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe((seoConfiguration: SEOConfiguration) => {
      this.seo = seoConfiguration;
    });
  }

  changeCanonical($event: any): void {
    this.seoService.setCanonical($event.target.value);
  }

}
