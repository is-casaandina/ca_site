import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaButtonModule, CaInputModule } from '@ca-core/ui/lib/components';
import { RobotsFormComponent } from './robots-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaInputModule,
    ModalModule,
    CaButtonModule
  ],
  declarations: [RobotsFormComponent],
  entryComponents: [RobotsFormComponent],
  exports: [RobotsFormComponent]
})
export class RobotsFormModule { }
