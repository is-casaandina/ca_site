import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { GeneralService } from '@ca-admin/services';
import {
  ACTION_TYPES,
  TYPE_ROBOT_ALLOW,
  TYPE_ROBOT_DISALLOW,
  TYPE_ROBOT_SITEMAP,
  TYPES_VALUES_ROBOTS,
  USER_AGENTS
} from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IRobotsRequest } from '@ca-admin/statemanagement/models/configuration.interface';
import { ActiveModal } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-robots-form',
  templateUrl: './robots-form.component.html',
  styleUrls: ['./robots-form.component.scss']
})
export class RobotsFormComponent extends UnsubscribeOnDestroy implements OnInit {
  GENERAL_LANG = GeneralLang;
  listUserAgent = USER_AGENTS;
  listTypes = TYPES_VALUES_ROBOTS;

  frmRobots: FormGroup;
  userAgent: AbstractControl;
  type: AbstractControl;
  value: AbstractControl;

  constructor(
    private fb: FormBuilder,
    private activeModal: ActiveModal,
    private generalService: GeneralService,
    private notificationService: NotificationService
    ) {
      super();
    }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.frmRobots = this.fb.group({
      userAgent: [''],
      type: [TYPE_ROBOT_ALLOW],
      value: [null]
    });

    this.userAgent = this.frmRobots.get('userAgent');
    this.type = this.frmRobots.get('type');
    this.value = this.frmRobots.get('value');
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmRobots);

    return this.frmRobots.valid;
  }

  private paramsSaveUser(): IRobotsRequest {
    return this.frmRobots.value as IRobotsRequest;
  }

  getPlaceholder(): string {
    let message = '';
    switch (this.type.value) {
      case TYPE_ROBOT_SITEMAP:
        message = 'Ingrese el nombre del mapa del sitio (sitemap.xml)';
        break;
      case TYPE_ROBOT_ALLOW:
        message = 'Ingrese la URL permitida';
        break;
      case TYPE_ROBOT_DISALLOW:
        message = 'Ingrese la URL no permitida';
        break;
      default:
        break;
    }

    return message;
  }

  save(): void {
    const validateForm = this.type.value === TYPE_ROBOT_SITEMAP ? (!!this.value.value) : (!!this.value.value && !!this.userAgent.value);
    if (validateForm) {
      const params = this.paramsSaveUser();
      this.generalService.addRobots(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.activeModal.close(ACTION_TYPES.save);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }
}
