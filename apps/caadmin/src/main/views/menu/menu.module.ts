import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemMenuTrayModule } from '@ca-admin/components/menu-tray/item-menu-tray/item-menu-tray.module';
import { AddItemMenuModule } from '@ca-admin/components/modals/add-item-menu/add-item-menu.module';
import { TabsLanguageModule } from '@ca-admin/components/tabs-language/tabs-language.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { OrderByPipeModule } from '@ca-core/shared/helpers/pipes/orderby.pipe';
import { CaAccordionModule, CaTabsModule } from '@ca-core/ui/lib/components';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';

@NgModule({
  imports: [
    CommonModule,
    MenuRoutingModule,
    TabsLanguageModule,
    CaAccordionModule,
    ItemMenuTrayModule,
    CollapseModule,
    CaTabsModule,
    AddItemMenuModule,
    ModalModule,
    OrderByPipeModule,
    DragDropModule
  ],
  declarations: [MenuComponent]
})
export class MenuModule { }
