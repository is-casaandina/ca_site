import { Component, OnInit } from '@angular/core';
import { AddItemMenuComponent } from '@ca-admin/components/modals/add-item-menu/add-item-menu.component';
import { MenuSiteService } from '@ca-admin/services/menu-site.service';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { GeneralResponse } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { AngularUtil, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { MenuUtil } from '@ca-core/shared/helpers/util/menu';
import { IMenuSite, MENU_SITE_ACTION, MENU_SITE_TYPE } from '@ca-core/shared/statemanagement/models/menu-site.interface';
import { zip } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent extends UnsubscribeOnDestroy implements OnInit {

  private menus: Array<IMenuSite>;

  header: Array<IMenuSite> = [];
  footer: Array<IMenuSite> = [];

  language: string;

  MENU_SITE_TYPE = MENU_SITE_TYPE;

  constructor(
    private menuService: MenuSiteService,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
  }

  getMenus(language: string): void {
    this.language = language;
    this.menuService.getMenus(language)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.menus = res || [];
        this.header = MenuUtil.menuTree(this.menus.filter(menu => menu.type === MENU_SITE_TYPE.HEADER));
        this.footer = MenuUtil.menuTree(this.menus.filter(menu => menu.type === MENU_SITE_TYPE.FOOTER));
      });
  }

  collapse(person: IMenuSite, brothers: Array<IMenuSite>): void {
    (brothers || [])
      .filter(brother => brother.id !== person.id)
      .forEach(brother => brother.collapse = true);

    person.collapse = !person.collapse;
  }

  addItem(type: MENU_SITE_TYPE): void {
    const list = type === MENU_SITE_TYPE.HEADER ? this.header : this.footer;
    const actions = [];

    if (type === MENU_SITE_TYPE.HEADER) {
      actions.push({ value: MENU_SITE_ACTION.REDIRECT, text: 'Reedirección' });
      actions.push({ value: MENU_SITE_ACTION.PROMOTION_ALL, text: 'Promoción todos' });
      actions.push({ value: MENU_SITE_ACTION.BOOKING, text: 'Reserva' });
    }

    const modal = this.modalService.open(AddItemMenuComponent, { title: 'Agregar Menu' });
    modal.setPayload({
      list,
      actions,
      type,
      language: this.language,
      order: list.length
    });
    modal.result
      .then((res: GeneralResponse<IMenuSite>) => {
        this.notificationService.addSuccess(res.message);
        this.menus.push(res.data);

        this.header = MenuUtil.menuTree(this.menus.filter(menu => menu.type === MENU_SITE_TYPE.HEADER));
        this.footer = MenuUtil.menuTree(this.menus.filter(menu => menu.type === MENU_SITE_TYPE.FOOTER));
      })
      .catch(() => { });
  }

  updated(menu: IMenuSite): void {
    const index = this.menus.findIndex(item => item.id === menu.id);
    this.menus[index] = menu;
  }

  add(menu: IMenuSite): void {
    this.menus.push(menu);
  }

  remove(menu: IMenuSite, parent: IMenuSite, index: number): void {
    const origin = this.menus.findIndex(item => item.id === menu.id);
    this.menus.splice(origin, 1);
    if (parent && parent.children) {
      parent.children.splice(index, 1);
    } else {
      const list = menu.type === MENU_SITE_TYPE.HEADER ? this.header : this.footer;

      list.splice(index, 1);
    }
  }

  validChild(type: MENU_SITE_TYPE, person: IMenuSite, index: number): boolean {
    person.order = person.order > -1 ? person.order : index;
    const tmp = this.menus.find(men => men.id === person.id);
    if (tmp) {
      tmp.order = person.order;
    }

    const valid = person.action !== MENU_SITE_ACTION.BOOKING && !person.sectionFooter;

    const list = type === MENU_SITE_TYPE.HEADER ? this.header : this.footer;
    const position = type === MENU_SITE_TYPE.HEADER ? 3 : 2;
    const res = this.positionChild(list, person, 0);

    return valid && res && res.length && res[0].position < position;
  }

  private positionChild(list: Array<IMenuSite>, person: IMenuSite, position?: number): Array<IMenuSite> {
    return list.map(element => {
      if (element.id === person.id) {
        element.position = position + 1;

        return element;
      } else if (element.children != null) {
        let result = null;
        for (let i = 0; result == null && i < element.children.length; i++) {
          const tmp = this.positionChild(element.children, person, position + 1);

          result = tmp && tmp.length ? tmp[0] : null;
        }

        return result;
      }

      return null;
    })
      .filter(element => element);
  }

  upMenu(item: IMenuSite, brother: Array<IMenuSite>, index: number): void {
    const newItem = AngularUtil.clone<IMenuSite>(brother[index - 1]);
    const currentItem = AngularUtil.clone<IMenuSite>(item);

    currentItem.order--;
    newItem.order++;

    brother[index - 1] = currentItem;
    brother[index] = newItem;

    zip(
      this.menuService.updateMenu(currentItem, false),
      this.menuService.updateMenu(newItem, false)
    )
      .subscribe(() => { });
  }

  downMenu(item: IMenuSite, brother: Array<IMenuSite>, index: number): void {
    const newItem = AngularUtil.clone<IMenuSite>(brother[index + 1]);
    const currentItem = AngularUtil.clone<IMenuSite>(item);

    currentItem.order++;
    newItem.order--;

    brother[index + 1] = currentItem;
    brother[index] = newItem;

    zip(
      this.menuService.updateMenu(currentItem, false),
      this.menuService.updateMenu(newItem, false)
    )
      .subscribe(() => { });
  }

}
