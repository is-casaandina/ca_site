import { Component, OnInit } from '@angular/core';
import { AddUserFormComponent } from '@ca-admin/components/add-user-form/add-user-form.component';
import { UserService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { UserLang } from '@ca-admin/settings/lang/user.lang';
import {
  IUserPaginationRequest,
  IUserPaginationResponse,
  IUserPaginationView,
  IUserView
} from '@ca-admin/statemanagement/models/user.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-users-list',
  templateUrl: './users-list.component.html'
})
export class UsersListComponent extends UnsubscribeOnDestroy implements OnInit {

  usersList: IUserPaginationView;

  GENERAL_LANG = GeneralLang;
  USER_LANG = UserLang;

  constructor(
    private _userService: UserService,
    private _modalService: ModalService,
    private _notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this._usersPagination(true);
    this._updateUsersList();
  }

  goAddUser(): void {
    this._modalService.open(AddUserFormComponent)
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.acept) {
          this._notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this._usersPagination();
        }
      });
  }

  private _paramsUsersPagination(): IUserPaginationRequest {
    return {
      size: 10,
      page: 1
    } as IUserPaginationRequest;
  }

  private _usersPagination(showSpin: boolean = false): void {
    const params = this._paramsUsersPagination();
    this._userService.usersPagination(params, showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IUserPaginationResponse) => {
        const usersList: IUserPaginationView = response;
        usersList.content = response.content.map((user: IUserView) => {
          user.disabled = false;

          return user;
        });
        this.usersList = usersList;
      });
  }

  private _updateUsersList(): void {
    this._userService.updateUsersList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IUserView) => {
        if (response && this.usersList) {
          this.usersList.content = this.usersList.content.filter((value: IUserView) => {
            if (response.actionType === ACTION_TYPES.edit) {
              value.disabled = (value.username !== response.username);
            } else if (response.actionType === ACTION_TYPES.cancel) {
              value.disabled = false;
            } else {
              return value.username !== response.username;
            }

            return value;
          });
        }
      });
  }

}
