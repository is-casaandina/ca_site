import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AddUserFormModule } from '@ca-admin/components/add-user-form/add-user-form.module';
import { CollapseUserModule } from '@ca-admin/components/collapse-user/collapse-user.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule, CaLinkModule } from '@ca-core/ui/lib/components';
import { UsersListRoutingModule } from './users-list-routing.module';
import { UsersListComponent } from './users-list.component';

@NgModule({
  declarations: [UsersListComponent],
  imports: [
    CommonModule,
    UsersListRoutingModule,
    CaCardModule,
    CaLinkModule,
    CollapseUserModule,
    ModalModule,
    AddUserFormModule
  ]
})
export class UsersListModule { }
