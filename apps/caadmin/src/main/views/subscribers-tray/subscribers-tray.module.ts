import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { DropdownCheckboxesModule } from '@ca-admin/components/dropdown-checkboxes/dropdown-checkboxes.module';
import { TitleFilterActionsModule } from '@ca-admin/components/title-filter-actions/title-filter-actions.module';
import { CaDatepickerModule, CaPaginatorModule } from '@ca-core/ui/lib/components';
import { SubscribersTrayRoutingModule } from './subscribers-tray-routing.module';
import { SubscribersTrayComponent } from './subscribers-tray.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SubscribersTrayRoutingModule,
    TitleFilterActionsModule,
    DropdownCheckboxesModule,
    CaDatepickerModule,
    CaPaginatorModule,
    CardRowActionsModule
  ],
  declarations: [SubscribersTrayComponent]
})
export class SubscribersTrayModule { }
