import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubscribersTrayComponent } from './subscribers-tray.component';

const routes: Routes = [
  {
    path: '',
    component: SubscribersTrayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubscribersTrayRoutingModule { }
