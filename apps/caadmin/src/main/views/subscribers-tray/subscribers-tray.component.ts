import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from '@ca-admin/services';
import { SubscriberService } from '@ca-admin/services/subscriber.service';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IGeneralResponse, ITypeSubscriberResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ISubscriberPaginationResponse, ISubscriberRequest } from '@ca-admin/statemanagement/models/subcriber.interface';
import { IUserResponse } from '@ca-admin/statemanagement/models/user.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { FileUtil } from '@ca-core/shared/helpers/util/file';
import { CaPaginator } from '@ca-core/ui/lib/components';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-subscribers-tray',
  templateUrl: './subscribers-tray.component.html'
})
export class SubscribersTrayComponent extends UnsubscribeOnDestroy implements OnInit {

  form: FormGroup;
  cTypeSubscriber: AbstractControl;
  cDate: AbstractControl;

  users: Array<IUserResponse>;
  typeSubcriber: Array<ITypeSubscriberResponse>;

  pagination: CaPaginator;
  data: ISubscriberPaginationResponse;

  GENERAL_LANG = GeneralLang;
  fileUtil = new FileUtil();

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected formBuilder: FormBuilder,
    protected router: Router,
    protected generalService: GeneralService,
    protected subscriberService: SubscriberService,
    protected notificationService: NotificationService
  ) {
    super();
    this.pagination = new CaPaginator();
    this.createForm();
    this.setDefaultFilter();
    this.initData();
  }

  ngOnInit(): void {
  }

  private createForm(): void {
    this.form = this.formBuilder.group(
      {
        cTypeSubscriber: [],
        cDate: {}
      }
    );
    this.cTypeSubscriber = this.form.get('cTypeSubscriber');
    this.cDate = this.form.get('cDate');
  }

  onApply(): void {
    this.pagination.page = 1;
    this.search();
  }

  onClean(): void {
    this.form.reset();
    this.onApply();
  }

  search(): void {
    this.getSubscriber()
      .subscribe(res => {
        this.data = res;
      });
  }

  private initData(): void {
    this.generalService.getTypeSubscriptor()
    .pipe(takeUntil(this.unsubscribeDestroy$))
    .subscribe((response: Array<ITypeSubscriberResponse>) => {
      this.typeSubcriber = response;
    });
    this.search();
  }

  private setDefaultFilter(): void {
    const params = this.activatedRoute.snapshot.queryParams;
    this.pagination.pageSize = Number(params.size || this.pagination.pageSize);
    this.pagination.page = Number(params.page || this.pagination.page);

    const valueSubscriber = [].concat(params.subscriber)
      .filter(state => state)
      .map(state => ({ code: Number(state) }));

    const valueDate = {
      from: params.startDate,
      to: params.endDate
    };
    this.cTypeSubscriber.setValue(valueSubscriber);
    this.cDate.setValue(valueDate, {});
  }

  getSubscriber(): Observable<ISubscriberPaginationResponse> {
    const cDate = this.cDate.value || {};
    const typeSubscriber = (this.cTypeSubscriber.value || []).map(data => data.code);

    const params: ISubscriberRequest = {
      typeSubscriber,
      startDate: cDate.from || '',
      endDate: cDate.to || '',
      size: this.pagination.pageSize,
      page: this.pagination.page
    } as ISubscriberRequest;

    this.updateUrlParams(params);

    return this.subscriberService.subscribers(params, true);
  }

  updateUrlParams(queryParams: any): void {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams,
        queryParamsHandling: '',
        replaceUrl: true
      });
  }

  messageDelete(subscriber: any): string {
    return `Desea eliminar al suscritor ${subscriber.name}`;
  }

  deleteSubscriber(subscriber: any): void {
    this.subscriberService.delete(subscriber.id, false)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IGeneralResponse<any>) => {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.search();
      });

  }

  download(): void {
    const cDate = this.cDate.value || {};
    const typeSubscriber = (this.cTypeSubscriber.value || []).map(data => data.code);

    const params: ISubscriberRequest = {
      typeSubscriber,
      startDate: cDate.from || '',
      endDate: cDate.to || ''
    } as ISubscriberRequest;

    this.subscriberService.download(params, true)
    .subscribe((response: any) => {
      this.fileUtil.download(response.data, 'application/vnd.ms-excel', 'suscriptores', '.xls');
    });
  }

}
