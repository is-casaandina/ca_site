import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewEditorComponent } from './view-editor.component';

const routes: Routes = [
  {
    path: '',
    component: ViewEditorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewEditorRoutingModule { }
