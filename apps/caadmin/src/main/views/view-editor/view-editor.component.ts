import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { DynamicFormComponent } from '@ca-admin/components/dynamic-form';
import { IComponent } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { StorageService } from '@ca-core/shared/helpers/util';

@Component({
  selector: 'caadmin-view-editor',
  templateUrl: './view-editor.component.html',
  styleUrls: ['./view-editor.component.scss']
})
export class ViewEditorComponent {

  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  private keyStorage = 'viewEditor';

  components: Array<IComponent>;
  formResponse: string;
  htmlResponse: string;

  control: FormControl;

  constructor(
    private fb: FormBuilder,
    private storageService: StorageService
  ) {
    this.getData();
  }

  getData(): void {
    const compStorage = this.storageService.getItemObject<any>(this.keyStorage);
    this.control = this.fb.control(compStorage ? this.formatComponent(compStorage) : '');

    if (compStorage) { this.setComponent(); }

    this.control.valueChanges
      .subscribe(() => {
        try {
          this.setComponent();
        } catch (error) {
          this.components = null;
        }
      });
  }

  private setComponent(): void {
    const components = JSON.parse(this.control.value);
    this.components = Array.isArray(components) ? components : [components];

    this.storageService.setItem(this.keyStorage, this.components);
  }

  private formatComponent(component: string): string {
    return JSON.stringify(component, undefined, 2);
  }

  formSubmitted(formResponse: Array<IComponent>): void {
    this.formResponse = JSON.stringify(formResponse, undefined, 2);
  }

  save(): void {
    this.htmlResponse = this.dynamicForm.getFormatComponent()[0].template
      .replace(/&quot;/g, '"');

    this.formResponse = JSON.stringify(this.dynamicForm.getFormValue(), undefined, 2);
  }

}
