import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CreateExternalPageComponent } from '@ca-admin/components/modals/create-external-page/create-external-page.component';
import { ConstructorPlantillaComponent } from '@ca-admin/components/modals/constructor-plantilla/constructor-plantilla.component';
import { TrayBaseComponent } from '@ca-admin/components/tray/tray-base';
import { CategoryService, ConfigurationService, GeneralService, PageService, UserService } from '@ca-admin/services';
import { CountryService } from '@ca-admin/services/country.service';
import { DestinationService } from '@ca-admin/services/destination.service';
import { DomainService } from '@ca-admin/services/domain.service';
import { PromotionService } from '@ca-admin/services/promotion.service';
import { IPageItemResponse, IPageResponse } from '@ca-admin/statemanagement/models/page.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { GeneralResponse } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-page-tray',
  templateUrl: './page-tray.component.html'
})
export class PageTrayComponent extends TrayBaseComponent implements OnInit {
  isCollapsed: boolean;

  constructor(
    protected generalService: GeneralService,
    protected userService: UserService,
    protected categoryService: CategoryService,
    protected pageService: PageService,
    protected spinnerService: SpinnerService,
    protected formBuilder: FormBuilder,
    protected modalService: ModalService,
    protected notificationService: NotificationService,
    protected configurationService: ConfigurationService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected domainService: DomainService,
    protected promotionService: PromotionService,
    protected countryService: CountryService,
    protected destinationService: DestinationService
  ) {
    super(
      activatedRoute,
      formBuilder,
      router,
      generalService,
      userService,
      categoryService,
      spinnerService,
      pageService,
      promotionService,
      countryService,
      destinationService
    );
    this.isCollapsed = true;
  }

  ngOnInit(): void {
    this.getDomains();
    const route = this.router.config.find(r => r.path === 'editor');
    route.data = { menuCode: null };
  }

  search(): void {
    this.spinnerService.showSpinner();
    this.getPages().subscribe(
      res => {
        this.spinnerService.hideSpinner();
        this.data = res;
      },
      () => this.spinnerService.hideSpinner()
    );
  }

  onApply(): void {
    this.pagination.page = 1;
    this.search();
  }

  searchByName(): void {
    this.cName.setValue((this.tmpTextSearch || '').trim());
    this.onApply();
  }

  onClean(): void {
    this.form.reset();
    this.tmpTextSearch = '';
    const valueTemplate = []
      .concat(this.definition.categories)
      .filter(category => category)
      .map(category => ({ id: category }));
    this.cTemplates.setValue(valueTemplate);
    this.onApply();
  }

  toggle(): void {
    this.isCollapsed = !this.isCollapsed;
  }

  autoClose(): void {
    this.isCollapsed = true;
  }

  external(): void {
    this.pageService.setPageStorage(null);
    this.pageService.setPageEditor(null);
    this.modalService
      .open(CreateExternalPageComponent, {
        title: 'Crear página',
        classBody: 'px-4 pb-3'
      })
      .result.then((res: GeneralResponse<IPageResponse>) => {
        this.notificationService.addSuccess(res.message);
        const item = this.formatAddExternal(res.data);

        this.data.content.unshift(item);
        if (this.data.content.length >= this.pagination.pageSize) {
          this.data.content.splice(this.data.content.length - 1);
        }
        this.data.totalElements++;
      })
      .catch(() => {});
  }

  editedItem(page: IPageResponse, index: number): void {
    const item = this.formatAddExternal(page);
    this.data.content.splice(index, 1);
    this.data.content.unshift(item);
  }

  private formatAddExternal(page: any): IPageItemResponse {
    return {
      id: page.id,
      name: page.languages[0].title,
      state: page.state,
      type: page.type,
      lastUpdate: page.modifiedDate,
      languages: page.languages.map(data => data.language),
      fullname: this.configurationService.getUserPermissions().fullname,
      categoryId: page.categoryId,
      publish: page.publish,
      categoryName: page.categoryName
    };
  }

  private getDomains(): void {
    this.domainService
      .getDomainsSelect()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {});
  }

  createTemplate(): void {
    const route = this.router.config.find(r => r.path === 'editor');
    route.data = { menuCode: this.menuCode };
    this.router.navigateByUrl('/editor/create/publication');
  }

  openForm(){
    this.pageService.setPageStorage(null);
    this.pageService.setPageEditor(null);
    this.modalService
      .open(ConstructorPlantillaComponent, {
        title: 'Constructor de plantilla',
        classBody: 'px-4 pb-3'
      })
      .result.then((res: GeneralResponse<IPageResponse>) => {
        this.notificationService.addSuccess(res.message);
        const item = this.formatAddExternal(res.data);

        this.data.content.unshift(item);
        if (this.data.content.length >= this.pagination.pageSize) {
          this.data.content.splice(this.data.content.length - 1);
        }
        this.data.totalElements++;
      })
      .catch(() => {});
  }
}
