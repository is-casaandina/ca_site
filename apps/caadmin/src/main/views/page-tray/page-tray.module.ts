import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownCheckboxesModule } from '@ca-admin/components/dropdown-checkboxes/dropdown-checkboxes.module';
import { CreateExternalPageModule } from '@ca-admin/components/modals/create-external-page/create-external-page.module';
import { TitleFilterActionsModule } from '@ca-admin/components/title-filter-actions/title-filter-actions.module';
import { PageItemModule } from '@ca-admin/components/tray/page-item/page-item.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaDatepickerModule, CaInputModule, CaPaginatorModule, CaSpinnerModule } from '@ca-core/ui/lib/components';
import { CollapseModule } from '@ca-core/ui/lib/directives';
import { PageTrayRoutingModule } from './page-tray-routing.module';
import { PageTrayComponent } from './page-tray.component';
import { ConstructorPlantillaModule } from '@ca-admin/components/modals/constructor-plantilla/constructor-plantilla.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PageTrayRoutingModule,
    TitleFilterActionsModule,
    DropdownCheckboxesModule,
    CaSpinnerModule,
    CaPaginatorModule,
    CaInputModule,
    CaDatepickerModule,
    CollapseModule,
    ModalModule,
    PageItemModule,
    CreateExternalPageModule,
    ConstructorPlantillaModule
  ],
  declarations: [PageTrayComponent]
})
export class PageTrayModule { }
