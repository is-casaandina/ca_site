import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoginFormModule } from '@ca-admin/components/login-form/login-form.module';
import { ResetPasswordFormModule } from '@ca-admin/components/reset-password-form/reset-password-form.module';
import { ResetPasswordRoutingModule } from './reset-password-routing.module';
import { ResetPasswordComponent } from './reset-password.component';

@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [
    CommonModule,
    ResetPasswordRoutingModule,
    ResetPasswordFormModule,
    LoginFormModule
  ]
})
export class ResetPasswordModule { }
