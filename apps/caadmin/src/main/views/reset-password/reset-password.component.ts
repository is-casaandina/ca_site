import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'caadmin-recovery',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {
  token: string;

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => this.token = params.token);
  }

  ngOnInit(): void {
  }

}
