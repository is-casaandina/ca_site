import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PromotionsTrayComponent } from './promotions-tray.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PromotionsTrayComponent]
})
export class PromotionsTrayModule { }
