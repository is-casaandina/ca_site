import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AddUserFormModule } from '@ca-admin/components/add-user-form/add-user-form.module';
import { CreateRoleFormModule } from '@ca-admin/components/create-role-form/create-role-form.module';
import { CaButtonModule } from '@ca-core/ui/lib/components';
import { CaTabsModule } from '@ca-core/ui/lib/components/tabs';
import { UsersRolesRoutingModule } from './users-roles-routing.module';
import { UsersRolesComponent } from './users-roles.component';

@NgModule({
  declarations: [UsersRolesComponent],
  imports: [
    CommonModule,
    UsersRolesRoutingModule,
    CaTabsModule,
    AddUserFormModule,
    CreateRoleFormModule,
    CaButtonModule
  ]
})
export class UsersRolesModule { }
