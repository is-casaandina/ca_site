import { Component, OnInit } from '@angular/core';
import { RoleService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { RoleLang } from '@ca-admin/settings/lang/role.lang';
import {
  IRolePaginationRequest,
  IRolePaginationResponse,
  IRolePaginationView,
  IRolePermissionsDetailView
} from '@ca-admin/statemanagement/models/role.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-roles-list',
  templateUrl: './roles-list.component.html'
})
export class RolesListComponent extends UnsubscribeOnDestroy implements OnInit {

  rolesPaginated: IRolePaginationResponse;

  GENERAL_LANG = GeneralLang;
  ROLE_LANG = RoleLang;
  pageZise = 10;
  pageCurrent = 1;

  constructor(
    private roleService: RoleService
  ) {
    super();
  }

  ngOnInit(): void {
    this.search();
    this.updateRolesList();
  }

  private paramsRolesPagination(): IRolePaginationRequest {
    return {
      size: this.pageZise,
      page: this.pageCurrent
    } as IRolePaginationRequest;
  }

  private updateRolesList(): void {
    this.roleService.updateRolesList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IRolePermissionsDetailView) => {
        if (response && this.rolesPaginated) {
          if (response.actionType === ACTION_TYPES.search) {
            this.search();
          } else {
            this.rolesPaginated.content = this.rolesPaginated.content.filter((value: IRolePermissionsDetailView) => {
              if (response.actionType === ACTION_TYPES.edit) {
                value.disabled = (response.roleCode !== value.roleCode);
              } else if (response.actionType === ACTION_TYPES.cancel) {
                value.disabled = false;
              } else {
                return response.roleCode !== value.roleCode;
              }

              return value;
            });
          }
        }
      });
  }

  search(showSpin: boolean = true): void {
    const params = this.paramsRolesPagination();
    this.roleService.rolesPagination(params, showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IRolePaginationResponse) => {
        const rolesPaginated: IRolePaginationView = response;
        rolesPaginated.content = rolesPaginated.content.map((role: IRolePermissionsDetailView) => {
          role.disabled = false;

          return role;
        });
        this.rolesPaginated = rolesPaginated;
      });
  }

  trackByItems(index: number, item: IRolePermissionsDetailView): any {
    if (!item) { return null; }

    return item.roleCode;
  }

}
