import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CollapseRoleModule } from '@ca-admin/components/collapse-role/collapse-role.module';
import { CreateRoleFormModule } from '@ca-admin/components/create-role-form/create-role-form.module';
import { CaCardModule, CaLinkModule, CaPaginatorModule } from '@ca-core/ui/lib/components';
import { RolesListRoutingModule } from './roles-list-routing.module';
import { RolesListComponent } from './roles-list.component';

@NgModule({
  declarations: [RolesListComponent],
  imports: [
    CommonModule,
    RolesListRoutingModule,
    CaCardModule,
    CaLinkModule,
    CollapseRoleModule,
    CaPaginatorModule,
    CreateRoleFormModule,
    NgbModule
  ]
})
export class RolesListModule { }
