import { Component } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { AddUserFormComponent } from '@ca-admin/components/add-user-form/add-user-form.component';
import { CreateRoleFormComponent } from '@ca-admin/components/create-role-form/create-role-form.component';
import { RoleService, UserService } from '@ca-admin/services';
import { ACTION_TYPES, TAB_USERS_ROLES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang, UserLang } from '@ca-admin/settings/lang';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';

@Component({
  selector: 'caadmin-users-roles',
  templateUrl: './users-roles.component.html'
})
export class UsersRolesComponent {

  tabItemsList = TAB_USERS_ROLES;
  GENERAL_LANG = GeneralLang;
  USER_LANG = UserLang;
  isRole = false;

  constructor(
    private modalService: ModalService,
    private notificationService: NotificationService,
    private roleService: RoleService,
    private userService: UserService,
    private router: Router
  ) {
    this.router
    .events
    .subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        this.isRole = event.url.indexOf('roles-list') > -1;
      }
    });
  }

  goCreateRole(): void {
    this.modalService.open(CreateRoleFormComponent)
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.roleService.setActions(ACTION_TYPES.search);
        }
      });
  }

  goCreateUser(): void {
    this.modalService.open(AddUserFormComponent, { title: this.USER_LANG.titles.addUser, classHeader: 'text-left' })
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.userService.setActions(ACTION_TYPES.search);
        }
      }, () => { });
  }

}
