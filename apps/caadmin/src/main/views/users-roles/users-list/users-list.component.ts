import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { AddUserFormComponent } from '@ca-admin/components/add-user-form/add-user-form.component';
import { RoleService, UserService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { UserLang } from '@ca-admin/settings/lang/user.lang';
import { IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { IRoleResponse } from '@ca-admin/statemanagement/models/role.interface';
import {
  IUserPaginationRequest,
  IUserPaginationResponse,
  IUserPaginationView,
  IUserView
} from '@ca-admin/statemanagement/models/user.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-users-list',
  templateUrl: './users-list.component.html'
})
export class UsersListComponent extends UnsubscribeOnDestroy implements OnInit {

  usersPaginated: IUserPaginationView;
  roles: Array<IRoleResponse>;
  frmSearch: FormGroup;
  rolDropdown: AbstractControl;

  GENERAL_LANG = GeneralLang;
  USER_LANG = UserLang;
  pageZise = 10;
  pageCurrent = 1;

  constructor(
    private userService: UserService,
    private roleService: RoleService,
    private formBuilder: FormBuilder,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.search(true);
    this.roleService.rolesList()
      .subscribe((response: Array<IRoleResponse>) => {
        this.roles = response;
      });
    this.userService.updateUsersList()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IUserView) => {
        if (response && response.actionType === ACTION_TYPES.search) {
          this.search();
        }
      });
  }

  messageDelete(user: IUserView): string {
    return `${this.GENERAL_LANG.modalConfirm.titleDelete} al usuario ${user.username}?`;
  }

  private createForm(): void {
    this.frmSearch = this.formBuilder.group(
      {
        rolDropdown: [[]]
      }
    );
    this.rolDropdown = this.frmSearch.get('rolDropdown');
  }

  private paramsUsersPagination(): IUserPaginationRequest {
    const getList = (list: Array<any>, field: string) => {
      return list.map(fv => {
        return fv[field];
      }) || [];
    };

    return {
      rolesCode: getList(this.rolDropdown.value, 'roleCode') || [],
      size: this.pageZise,
      page: this.pageCurrent
    } as IUserPaginationRequest;
  }

  search(showSpin: boolean = false): void {
    const params = this.paramsUsersPagination();
    this.userService.usersPagination(params, showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IUserPaginationResponse) => {
        const usersList: IUserPaginationView = response;
        usersList.content = response.content.map((user: IUserView) => {
          user.disabled = false;

          return user;
        });
        this.usersPaginated = usersList;
      });
  }

  editUser(user: IUserView): void {
    const refModal = this.modalService.open(AddUserFormComponent, { title: this.USER_LANG.titles.editUser, classHeader: 'text-left' });
    refModal.setPayload(user);
    refModal
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.search();
        }
      }, () => { });
  }

  deleteUser(user: IUserView): void {
    this.userService.deleteUser({ userId: user.id }, false)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IMessageResponse) => {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.search();
      });
  }

  onApply(event: Boolean): void {
    this.search();
  }

  onClean(event: Boolean): void {
    this.frmSearch.reset();
    this.search();
  }

}
