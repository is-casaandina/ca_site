import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { AddUserFormModule } from '@ca-admin/components/add-user-form/add-user-form.module';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { CollapseUserModule } from '@ca-admin/components/collapse-user/collapse-user.module';
import { DropdownCheckboxesModule } from '@ca-admin/components/dropdown-checkboxes/dropdown-checkboxes.module';
import { TitleFilterActionsModule } from '@ca-admin/components/title-filter-actions/title-filter-actions.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule, CaLinkModule, CaPaginatorModule } from '@ca-core/ui/lib/components';
import { UsersListRoutingModule } from './users-list-routing.module';
import { UsersListComponent } from './users-list.component';

@NgModule({
  declarations: [UsersListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AddUserFormModule,
    CaCardModule,
    CaLinkModule,
    CollapseUserModule,
    ModalModule,
    CaPaginatorModule,
    TitleFilterActionsModule,
    UsersListRoutingModule,
    DropdownCheckboxesModule,
    CardRowActionsModule
  ]
})
export class UsersListModule { }
