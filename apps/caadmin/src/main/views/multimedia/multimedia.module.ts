import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { GallerySelectModule } from '@ca-admin/components/gallery-select/gallery-select.module';
import { NewMultimediaModule } from '@ca-admin/components/modals/new-multimedia/new-multimedia.module';
import { TitleFilterActionsModule } from '@ca-admin/components/title-filter-actions/title-filter-actions.module';
import { IconFileDirectiveModule } from '@ca-core/shared/helpers/directives/icon-file.directive';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaPaginatorModule, CaTabsModule } from '@ca-core/ui/lib/components';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { MultimediaRoutingModule } from './multimedia-routing.module';
import { MultimediaComponent } from './multimedia.component';

@NgModule({
  imports: [
    CommonModule,
    MultimediaRoutingModule,
    CaTabsModule,
    IconFileDirectiveModule,
    TitleFilterActionsModule,
    ReactiveFormsModule,
    CaPaginatorModule,
    GallerySelectModule,
    ModalModule,
    SweetAlert2Module,
    NewMultimediaModule
  ],
  declarations: [MultimediaComponent]
})
export class MultimediaModule { }
