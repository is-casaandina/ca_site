import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { GalleryFilterDateComponent } from '@ca-admin/components/gallery-select/gallery-filter-date/gallery-filter-date.component';
import { GalleryPreviewComponent } from '@ca-admin/components/gallery-select/gallery-preview/gallery-preview.component';
import { NewMultimediaComponent } from '@ca-admin/components/modals/new-multimedia/new-multimedia.component';
import { FileService } from '@ca-admin/services';
import { MULTIMEDIA_PROPERTIES, MULTIMEDIA_TABS } from '@ca-admin/settings/constants/multimedia.constant';
import { IMAGE_SIZE } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { FILE_TYPE, IMultimediaResponse } from '@ca-admin/statemanagement/models/file.interface';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { SwalConfirmService } from '@ca-core/shared/helpers/swal';
import { CaPaginator } from '@ca-core/ui/lib/components';
import { takeUntil } from 'rxjs/operators';
import { SweetAlertOptions } from 'sweetalert2';
import { MultimediaBase } from './multimedia-base';

@Component({
  selector: 'caadmin-multimedia',
  templateUrl: './multimedia.component.html'
})
export class MultimediaComponent extends MultimediaBase implements OnInit {

  @ViewChild(GalleryFilterDateComponent) filterDate: GalleryFilterDateComponent;

  currentDate: string;
  pagination: CaPaginator = new CaPaginator();
  totalElements: number;
  totalPages: number;

  type: string;
  openFilter: boolean;

  FILE_TYPE = FILE_TYPE;

  files: Array<IMultimediaResponse> = [];

  tabItemsList: Array<any> = MULTIMEDIA_TABS;
  deleteSwal: SweetAlertOptions;

  constructor(
    private router: Router,
    private spinnerService: SpinnerService,
    private activedRoute: ActivatedRoute,
    private modalService: ModalService,
    protected notificationService: NotificationService,
    protected fileService: FileService,
    private swalConfirmService: SwalConfirmService,
    @Inject(DOCUMENT) protected document: Document
  ) {
    super(notificationService, fileService, null, document);
    this.type = this.activedRoute.snapshot.params.type || this.activedRoute.snapshot.routeConfig.path;
  }

  ngOnInit(): void {
    this.router
      .events
      .subscribe((event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.tmpFile = null;
          this.formatData();
        }
      });
    this.formatData();
  }

  private formatData(): void {
    const type = this.activedRoute.snapshot.params.type || this.activedRoute.snapshot.routeConfig.path;
    this.type = FILE_TYPE[type];

    this.payload = MULTIMEDIA_PROPERTIES[type];
    this.deleteSwal = this.swalConfirmService.warning(`¿Seguro de eliminar la multimedia?`);

    this.files = [];
    this.currentDate = null;
    this.totalPages = 0;
    this.pagination = new CaPaginator();
    this.pagination.pageSize = 20;
    if (this.type) {
      this.getFiles();
      this.filterDate.getData(this.type);
    } else {
      this.router.navigate(['multimedia/image']);
    }
  }

  onApply(): void {
    this.pagination.page = 1;
    this.getFiles();
  }

  onClean(): void {
    this.currentDate = null;
  }

  selectedDate(date: string): void {
    this.currentDate = date;
  }

  getFiles(): void {
    this.files = [];
    const tmp = (this.currentDate || '').split('-');
    tmp.splice(tmp.length - 1);
    const date = tmp.join('-');

    this.spinnerService.showSpinner();
    this.fileService.getFilesByType(
      this.type as FILE_TYPE,
      {
        value: '',
        date,
        size: this.pagination.pageSize,
        page: this.pagination.page
      })
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(files => {
        this.totalElements = files.totalElements;
        this.totalPages = files.totalPages;

        files.content
          .filter(file => this.type !== FILE_TYPE.document && this.type !== FILE_TYPE.badge ?
            file.large && file.medium && file.small : file.url)
          .forEach(img => this.files.push(img));
        this.spinnerService.hideSpinner();
      }, err => this.spinnerService.hideSpinner());
  }

  selected(file: IMultimediaResponse): void {
  }

  uploadFile(file: File): void {
    if (this.payload.type === FILE_TYPE.image) {
      const modal = this.modalService.open(NewMultimediaComponent, {
        classDialog: 'multimedia',
        size: MODAL_BREAKPOINT.lg,
        hideHeader: true
      });

      modal.setPayload({
        property: this.payload,
        file,
        title: 'Agregar multimedia'
      });
      modal.result
        .then((message: string) => {
          this.notificationService.addSuccess(message);
          this.onApply();
        })
        .catch(() => { });
    } else {
      super.uploadFile(file, this.payload);
    }
  }

  delete(file: IMultimediaResponse): void {
    this.openFilter = false;
    this.fileService.removeFileByType(this.payload.type, file.id, true)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        () => {
          const index = this.files.indexOf(file);
          if (index > -1) {
            this.files.splice(index, 1);
          }
          this.spinnerService.hideSpinner();
          this.getFiles();
        });
  }

  openPreview(file: IMultimediaResponse): void {
    const modalConfig: any = {
      classDialog: 'multimedia',
      size: IMAGE_SIZE.lg
    };

    if (this.payload.type === FILE_TYPE.image) {
      const modalRef = this.modalService.open(NewMultimediaComponent, modalConfig);
      modalConfig.hideHeader = true;
      modalRef.setPayload({
        property: this.payload,
        currentImage: file,
        title: 'Editar multimedia'
      });
      modalRef.result
        .then((message: string) => {
          this.notificationService.addSuccess(message);
          this.getFiles();
        })
        .catch(() => { });
    } else if (this.payload.type !== FILE_TYPE.document) {
      const modalRef = this.modalService.open(GalleryPreviewComponent, modalConfig);
      modalRef.setPayload({
        file: file.url || file.large.imageUri,
        mimetype: file.mimeType,
        type: this.payload.type
      });
      modalRef.result
        .then(() => {
        })
        .catch(() => {

        });
    } else {
      const a = this.document.createElement<'a'>('a');
      a.href = file.url;
      a.target = '_blank';
      a.click();
    }
  }

}
