import { Component, OnInit } from '@angular/core';
import { ConfigurationService, MenuService } from '@ca-admin/services';
import { MENU_CODE } from '@ca-admin/settings/constants/menu.constant';
import { IMenu } from '@ca-admin/statemanagement/models/general.interface';

@Component({
  selector: 'caadmin-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  fullname: string;
  menu: Array<IMenu>;

  constructor(
    private configurationService: ConfigurationService,
    private menuService: MenuService
  ) { }

  ngOnInit(): void {
    const userPermissions = this.configurationService.getUserPermissions();
    this.fullname = userPermissions.fullname;

    const menu = this.menuService.generateMenu();
    this.menu = menu.filter(m => !m.header && m.code !== MENU_CODE.home);
  }

}
