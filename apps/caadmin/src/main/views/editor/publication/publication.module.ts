import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { EditorCloneModule } from '@ca-admin/components/editor/editor-clone/editor-clone.module';
import { EditorFormHeaderModule } from '@ca-admin/components/editor/editor-form-header/editor-form-header.module';
import { EditorHeaderModule } from '@ca-admin/components/editor/editor-header/editor-header.module';
import { NewPublicationOptionsModule } from '@ca-admin/components/editor/new-publication-options/new-publication-options.module';
import { SeoModule } from '@ca-admin/components/seo/seo.module';
import { PipesModule } from '@ca-core/shared/helpers/pipes';
import { CaInputModule, CaLinkModule } from '@ca-core/ui/lib/components';
import { PublicationRoutingModule } from './publication-routing.module';
import { PublicationComponent } from './publication.component';

@NgModule({
  declarations: [PublicationComponent],
  imports: [
    CommonModule,
    PublicationRoutingModule,
    ReactiveFormsModule,
    EditorHeaderModule,
    PipesModule,
    CaInputModule,
    CaLinkModule,
    NewPublicationOptionsModule,
    EditorFormHeaderModule,
    SeoModule,
    EditorCloneModule
  ]
})
export class PublicationModule { }
