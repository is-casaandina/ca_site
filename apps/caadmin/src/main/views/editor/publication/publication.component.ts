import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicFormService } from '@ca-admin/components/dynamic-form/providers/dynamic-form.service';
import { EditorFormHeaderComponent } from '@ca-admin/components/editor/editor-form-header/editor-form-header.component';
import { environment } from '@ca-admin/environments/environment';
import { GeneralService, PageService } from '@ca-admin/services';
import { ComponentService } from '@ca-admin/services/component.service';
import { SEOService } from '@ca-admin/services/seo.service';
import { CA, CATEGORY, LANGUAGE_TYPES, LANGUAGE_TYPES_CODE, PAGE_CODE_STATES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IConfigurationResponse } from '@ca-admin/statemanagement/models/configuration.interface';
import { IContent, IPageByLanguage, IPageResponse } from '@ca-admin/statemanagement/models/page.interface';
import { ComponentCanDeactivate } from '@ca-core/shared/helpers/can-deactive/can-deactive-base.component';
import { ICaObservable } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { AngularUtil, StorageService } from '@ca-core/shared/helpers/util';
import { addScriptTag, formatPageInfo } from '@ca-core/shared/helpers/util/web-components';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { IPageInfo } from 'core/typings';
import { of, zip } from 'rxjs';
import { map, switchMapTo, takeUntil } from 'rxjs/operators';
import * as _ from 'underscore';

@Component({
  selector: 'caadmin-publication',
  templateUrl: './publication.component.html'
})
export class PublicationComponent extends ComponentCanDeactivate implements OnInit {

  @ViewChild(EditorFormHeaderComponent) formHeader: EditorFormHeaderComponent;

  GENERAL_LANG = GeneralLang;
  LANGUAGE_TYPES = LANGUAGE_TYPES;

  pageId: string;
  pageState: number;
  pageCategory: CATEGORY;

  pageItemIndexEditor: number;
  language: string;
  template: string;
  refComponents: Array<string>;
  /** Necessary for only seePublicaction */
  seePublication: boolean;

  frmEditor: FormGroup;
  mTitlePage: AbstractControl;
  mPagePath: AbstractControl;
  canonicalPartial: string;

  // Datos generales
  config: IConfigurationResponse;

  constructor(
    private activatedRoute: ActivatedRoute,
    private pageService: PageService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private generalService: GeneralService,
    private componentService: ComponentService,
    private spinnerService: SpinnerService,
    private storageService: StorageService,
    private dynamicFormService: DynamicFormService,
    private seoService: SEOService,
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {
    super();
  }

  ngOnInit(): void {
    const snapshot = this.activatedRoute.snapshot;
    this.pageId = snapshot.params.idPage;
    this.pageState = parseInt(snapshot.queryParams.state, 10);
    this.seePublication = snapshot.data && snapshot.data.seePublication;

    this.createForm();
    this.pageById();
    this.load();
    this.reloadPublicationEdit();
  }

  private createForm(): void {
    /**
     * Crear formulario para titulo y url
     */
    this.frmEditor = this.formBuilder.group({
      mTitlePage: null,
      mPagePath: null
    });
    this.mTitlePage = this.frmEditor.get('mTitlePage');
    this.mPagePath = this.frmEditor.get('mPagePath');
    this.canonicalPartial = CA.urlSite;
  }

  private reloadPublicationEdit(): void {
    /**
     * Recargamos el editor cuando se modifica un valor en el formulario general.
     */
    this.pageService.reloadPublicationEdit()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.template = null;
        setTimeout(() => {
          this.setViewByLanguage(this.language);
        }, 0);
      });
  }

  changeLanguage(newLanguage: string): void {
    /**
     * se activa cuando cambiamos de Tab de idioma
     *
     * 1.- Almacenamos el lenguaje actual
     * 2.- Borramos el template actual del editor
     * 3.- Actualizamos el template del editor con el actual correspondiente al idioma
     * 4.- Almacenamos toda la página en el storage - 3 idiomas
     */
    this.language = newLanguage;
    this.template = null;
    setTimeout(() => {
      this.setViewByLanguage(newLanguage);
      this.setPageEditor();
    }, 0);
  }

  private pageById(): void {
    this.spinnerService.showSpinner();
    zip(
      /** Obtenemos lista de componentes para armar el formulario de componentes */
      this.componentService.list(),
      /** Obtenemos la página de la BD */
      this.pageService.pageById(this.pageId, this.pageState),
      /** Obtenemos la configuración del sitio */
      this.generalService.getConfiguration(false)
    )
      .subscribe(response => {
        this.componentService.setComponentsEditor(response[0]);

        const page = this.pageService.getPageStorage();
        if (!page || !Object.keys(page).length) {
          this.pageService.setPageStorage(response[1]);
        }
        this.pageService.setPageEditor(response[1]);
        this.pageCategory = this.pageService.getPageStorage().categoryId;

        this.config = response[2];
        this.canonicalPartial = (this.config && this.config.domain) || CA.urlSite;
        /* se agrega funcion para obtener las configuraciones del storage*/
        this.generalService.setConfigurationStorage(response[2]);
        setTimeout(() => {
          this.setViewByLanguage(this.language || LANGUAGE_TYPES_CODE.es);
          this.onTitleAndSeoChange();
          this.spinnerService.hideSpinner();
        }, 0);
      }, () => this.spinnerService.hideSpinner());
  }

  private setViewByLanguage(newLanguage: string): void {
    /** Asignamos el nuevo lenguaje, para su uso en toda la clase */
    this.language = newLanguage;
    /** Obtenemos la configuración de la página actual */
    const pageEditor = this.pageService.getPageEditor();
    /** Obtenemos la posición del lenguaje en la página */
    this.pageItemIndexEditor = this.pageItemIndex(pageEditor, newLanguage);
    /** Obtenemos la página por lenguaje actual */
    const pageItem = pageEditor.languages[this.pageItemIndexEditor];

    /** Damos formato a la página actual para el uso de los web components */
    const pageInfo: IPageInfo = formatPageInfo(pageEditor, newLanguage, false, pageItem.data, pageItem.title);
    /** Guardamos en storage la información de la página para su tratamiento */
    this.storageService.setItem(STORAGE_KEY.pageInfo, pageInfo);

    /** Asignamos el template html de la actual página por lenguaje */
    this.template = pageItem.content.html;
    /** Asignamos las refernecias de los webcompoents de la actual página por lenguaje */
    this.refComponents = pageItem.content.refComponents;

    /** Almacenamos el lenguaje en storage para uso del formulario de edición de los web components */
    this.dynamicFormService.setLanguage(newLanguage);

    /** Asignamos los valores de la página a los formarios por lenguaje */
    this.setTitleAndSeoData(pageItem);

    /** Cargamos los bundles de los web components a usar */
    addScriptTag(pageItem.content.refComponents, environment.BUCKET_WC);
  }

  private setTitleAndSeoData(page: IPageByLanguage): void {
    /** Asignamos valor del título de la página por lenguaje */
    this.mTitlePage.setValue(page.title || '');
    /** Asignamos valor del path de la página por lenguaje */
    this.mPagePath.setValue(page.pagePath || '');
    /** Asignamos valor del SEO de la página por lenguaje */
    this.setSeoData(page);
  }

  private setSeoData(page: IPageByLanguage): void {
    /* SEO
    * pagePath => casa-andina-sama
    * pageItem.canonical => www.casa-andina.com/es/casa-andina-sama
    * pageItem.metadata.title => Casa Andina Sama
    * pageItem.content.html
    * contentHtml
    */
    const contentHtml = this.document.getElementById('#contentHtml');
    this.seoService.setInfo({
      language: this.language,
      pageTitle: page.title,
      path: page.pagePath,
      sitename: (this.config && this.config.sitename) || '',
      urlSite: `${this.canonicalPartial}${page.parentPath}`,
      contentHtml,
      meta: page.metadata
    });
  }

  private onTitleAndSeoChange(): void {
    /**
     * Guardamos el título en el temporal del editor
     */
    this.mTitlePage.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        const page = this.setPageEditor();
        this.setSeoData(page);
      });
    /**
     * Guardamos el path de la página en el temporal del editor
     */
    this.mPagePath.valueChanges
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => {
        this.seoService.setSlug(this.mPagePath.value || '');
        this.setPageEditor();
      });
    /**
     * Guardamos la configuración del SEO de la página en el temporal del editor
     */
    this.seoService.configuration$
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(conf => {
        /** Actualizamos el path */
        this.mPagePath.setValue(conf.slug, { emitEvent: false });
        /** Página actual al storage */
        this.setPageEditor();
      });
  }

  setPageEditor(content?: IContent): IPageByLanguage {
    /* Ingresa cuando se modifica:
     *  1- Un web components (contiene content)
     *  2- El título (no contiene content)
     *  3- El path de la página (no contiene content)
    */
    const pageEditor = this.pageService.getPageEditor();
    const pageItem = pageEditor.languages[this.pageItemIndexEditor];

    /**
     * Estructura de URL:
     * 1- parentPath => es
     * 2- pagePath => casa-andina-sama
     * 3- path => es/casa-andina-sama
     * 4- canonical => www.casa-andina.com/es/casa-andina-sama
     */
    pageItem.parentPath = this.language;
    pageItem.pagePath = this.mPagePath.value || '';
    pageItem.path = `${pageItem.parentPath}/${pageItem.pagePath}`;
    pageItem.canonical = `${this.canonicalPartial}${pageItem.path}`;
    pageItem.title = this.mTitlePage.value;

    if (content) {
      /** Almacenamos el nuevo html */
      pageItem.content.html = content.html;
      /** Almacenamos las referencias a los bundles de los web components */
      pageItem.content.refComponents = content.refComponents;
      /** Almacenamos la información llenada en los web components (con la propiedad *data*), para un procesamiento en Backend */
      pageItem.data = content.data;
    }

    /** Almacenamos metadata de SEO */
    pageItem.metadata = this.seoService.getMetaData();

    /** Reemplazamos en la configuración general la página del lenguaje actual */
    pageEditor.languages.splice(this.pageItemIndexEditor, 1, pageItem);

    /** Guardamos en storage */
    this.pageService.setPageEditor(pageEditor);

    /** Retornamos la página por lenguaje para su tratado */
    return pageItem;
  }

  private pageItemIndex(page: IPageResponse, language: string): number {
    return page.languages
      .map(fv => fv.language)
      .indexOf(language);
  }

  changeDataHeader(data: any): void {
    /**
     * Ingresa cuando se modifica el formulario general de la página
     */
    const pageEditor = this.pageService.getPageEditor();
    Object.keys(data)
      .forEach(key => pageEditor[key] = data[key]);
    this.pageService.setPageEditor(pageEditor);
  }

  cancel(): void {
    /** Función para cancelar cambios realizados en las páginas por lenguaje */

    /** Obtenemos el estado original de la configuración general de la página */
    const pageStorage = this.pageService.getPageStorage();
    /** La guardamos en el storage de la página en edición */
    this.pageService.setPageEditor(pageStorage);
    /** Reiniciamos el formulario general de la página */
    this.formHeader.setFormStructure();
    /** Reiniciamos todo el contenido por lenguaje */
    this.pageById();
  }

  preSave(): void {
    /**
     * Validamos si el formulario general de la página es completo
     */
    if (!this.formHeader || (this.formHeader && this.formHeader.isValid())) {
      this.save()
        .subscribe(response => { });
    }
  }

  private save(update?: boolean): ICaObservable<IPageResponse> {
    /** Función para guardar los cambios */
    const pageEditor = this.pageService.getPageEditor();

    return this.pageService.savePage(pageEditor, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$),
        map(response => {
          if (!update) {
            /** Actualizamos la configuración original a la actual */
            this.pageService.setPageStorage(response.data);
            /** Actualizamos la configuración en edición a la actual */
            this.pageService.setPageEditor(response.data);
            this.notificationService.addSuccess(response.message);
          }

          return response;
        })
      );
  }

  publish(update?: boolean): void {
    /** Evaluamos si no hay cambios sin guardar */
    if (this.canDeactivate() || update) {
      this.pageService.publishPage(this.pageId, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(response => {
          /** Actualizamos la configuración original a la actual */
          this.pageService.setPageStorage(response.data);
          /** Actualizamos la configuración en edición a la actual */
          this.pageService.setPageEditor(response.data);

          this.notificationService.addSuccess(update ? GeneralLang.notifications.updateSucces : response.message);

          this.router.navigate(
            [],
            {
              relativeTo: this.activatedRoute,
              queryParams: { state: PAGE_CODE_STATES.published },
              queryParamsHandling: '',
              replaceUrl: true
            });
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.thereAreChangesWithoutSaving);
    }
  }

  update(): void {
    const page = this.pageService.getPageStorage();
    if (!this.formHeader || (this.formHeader && this.formHeader.isValid())) {
      of([])
        .pipe(
          takeUntil(this.unsubscribeDestroy$),
          switchMapTo(page.state === PAGE_CODE_STATES.published ? this.pageService.editPage(this.pageId) : of([])),
          switchMapTo(this.save(true))
        )
        .subscribe(response => this.publish(true));
    }
  }

  unload(): void {
    /** Informamos al backend que el usuario a salido de la página */
    this.pageService.pageHeaderEdit({ inEdition: false, id: this.pageId })
      .subscribe(() => { });
  }

  load(): void {
    /** Informamos al backend que el usuario a ingresado al modo ediciíon de la página */
    this.pageService.pageHeaderEdit({ inEdition: true, id: this.pageId })
      .subscribe(() => { });
  }

  canDeactivate(): boolean {
    /** Método que se ejecuta al intentar salir del editor o intentar publicar */
    if (this.seePublication || !environment.production) {
      /** Ingresa cuando estamos en modo vista o si es en desarrollo */
      return true;
    }
    /** Obtenemos la configuración general de la página en ediciíón */
    const pageEditor = this.pageService.getPageEditor();
    /** Clonamos la configuración general de la página original */
    const page = AngularUtil.clone<IPageResponse>(this.pageService.getPageStorage());

    /** eliminamos propiedades que no se mantienen en ambos formatos */
    delete pageEditor.categoryName;
    delete page.categoryName;

    /** Evaluamos y retornamos si son igual, para su tratado */
    return _.isEqual(pageEditor, page);
  }

  clone(from: LANGUAGE_TYPES_CODE, to: LANGUAGE_TYPES_CODE): void {
    const page = AngularUtil.clone<IPageResponse>(this.pageService.getPageEditor());
    const pageFrom = page.languages.find(item => item.language === from);
    const newTo = AngularUtil.clone<IPageByLanguage>(pageFrom);
    const pageTo = page.languages.findIndex(item => item.language === to);

    newTo.language = to;
    newTo.parentPath = to;
    newTo.path = (pageFrom.path || '').replace(`${from}/`, `${to}/`);
    newTo.canonical = (pageFrom.canonical || '').replace(`/${from}/`, `/${to}/`);

    newTo.metadata = pageFrom.metadata = pageFrom.metadata || {};

    if (pageFrom.canonical === pageFrom.metadata.canonicalLink) {
      newTo.metadata.canonicalLink = (pageFrom.metadata.canonicalLink || '').replace(`/${from}/`, `/${to}/`);
    }
    newTo.metadata.ogUrl = (newTo.metadata.ogUrl || '').replace(`/${from}/`, `/${to}/`);

    page.languages[pageTo] = newTo;
    this.pageService.setPageEditor(page);

    this.changeLanguage(to);
  }

}
