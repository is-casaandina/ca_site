import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from '@ca-core/shared/helpers/can-deactive/can-deactive.guard';
import { PublicationComponent } from './publication.component';

const routes: Routes = [
  {
    path: '',
    component: PublicationComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationRoutingModule { }
