import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ItemTemplateModule } from '@ca-admin/components/editor/item-template/item-template.module';
import { CaButtonModule } from '@ca-core/ui/lib/components';
import { CreatePublicationRoutingModule } from './create-publication-routing.module';
import { CreatePublicationComponent } from './create-publication.component';

@NgModule({
  declarations: [CreatePublicationComponent],
  imports: [
    CommonModule,
    CreatePublicationRoutingModule,
    CaButtonModule,
    ItemTemplateModule
  ]
})
export class CreatePublicationModule { }
