import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateService } from '@ca-admin/services';
import { TRAY_FILTER_DEFINITION } from '@ca-admin/settings/constants/tray.constant';
import { GeneralLang, PageLang } from '@ca-admin/settings/lang';
import { ITemplateResponse } from '@ca-admin/statemanagement/models/template.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-create-publication',
  templateUrl: './create-publication.component.html'
})
export class CreatePublicationComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;
  PAGE_LANG = PageLang;

  templateList: Array<ITemplateResponse>;

  constructor(
    protected activatedRoute: ActivatedRoute,
    private templateService: TemplateService,
    private location: Location
  ) {
    super();
    const menuCode = this.activatedRoute.snapshot.data.menuCode;
    const definition = TRAY_FILTER_DEFINITION.find(def => def.menuCode === menuCode);
    if (definition) {
      const categories = definition.categories || [];
      this.getTemplates(categories);
    }
  }

  ngOnInit(): void {
  }

  private getTemplates(categories: Array<string>): void {
    this.templateService.list(categories, true)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<ITemplateResponse>) => {
        this.templateList = response;
      });
  }

  goBack(): void {
    this.location.back();
  }

}
