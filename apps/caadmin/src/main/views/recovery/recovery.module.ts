import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RecoveryFormModule } from '@ca-admin/components/recovery-form/recovery-form.module';
import { RecoveryRoutingModule } from './recovery-routing.module';
import { RecoveryComponent } from './recovery.component';

@NgModule({
  declarations: [RecoveryComponent],
  imports: [
    CommonModule,
    RecoveryRoutingModule,
    RecoveryFormModule
  ]
})
export class RecoveryModule { }
