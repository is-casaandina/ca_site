import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddDomainComponent } from '@ca-admin/components/modals/add-domain/add-domain.component';
import { GeneralService, PageService } from '@ca-admin/services';
import { DomainService } from '@ca-admin/services/domain.service';
import { ErrorService } from '@ca-admin/services/error.service';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ConfigurationLang } from '@ca-admin/settings/lang/configuration.lang';
import { IConfigurationResponse } from '@ca-admin/statemanagement/models/configuration.interface';
import { IError } from '@ca-admin/statemanagement/models/error.interface';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { GeneralResponse } from '@ca-core/shared/helpers/models/general.model';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { SpinnerService } from '@ca-core/shared/helpers/spinner';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { IDomain } from '@ca-core/shared/statemanagement/models/domain.interface';
import { concat, forkJoin } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent extends UnsubscribeOnDestroy implements OnInit {

  CONFIGURATION_LANG = ConfigurationLang;
  formError: FormGroup;
  pageError: Array<IPageStateResponse> = [];

  private errors: Array<IError> = [];

  domains: Array<IDomain> = [];
  configuration: IConfigurationResponse;

  formHome: FormGroup;
  pageHome: Array<IPageStateResponse> = [];

  constructor(
    private fb: FormBuilder,
    private errorService: ErrorService,
    private spinnerService: SpinnerService,
    private notificationService: NotificationService,
    private domainsService: DomainService,
    private generalService: GeneralService,
    private modalService: ModalService,
    private pageService: PageService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getData();
  }

  private getData(): void {
    this.spinnerService.showSpinner();
    forkJoin(
      this.errorService.getErrors(),
      this.errorService.getErrorSelect(),
      this.domainsService.getDomains(),
      this.generalService.getConfiguration(),
      this.pageService.getHomeSelect()
    )
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.errors = res && res[0] || [];
        this.pageError = res && res[1] || [];
        this.domains = res && res[2] || [];
        this.configuration = res && res[3] || {} as IConfigurationResponse;
        this.pageHome = res && res[4] || [];

        this.generalService.setConfigurationStorage(this.configuration);
        this.createFormError(this.errors);
        this.createFormHome();
        this.spinnerService.hideSpinner();

      }, () => this.spinnerService.hideSpinner());
  }

  private createFormError(errors: Array<IError>): void {
    const error403 = errors.find(error => error.errorCode === 403);
    const error404 = errors.find(error => error.errorCode === 404);
    const error500 = errors.find(error => error.errorCode === 500);
    const error502 = errors.find(error => error.errorCode === 502);
    const error503 = errors.find(error => error.errorCode === 503);

    this.formError = this.fb.group({
      403: [error403 && error403.pageId || null, Validators.required],
      404: [error404 && error404.pageId || null, Validators.required],
      500: [error500 && error500.pageId || null, Validators.required],
      502: [error502 && error502.pageId || null, Validators.required],
      503: [error503 && error503.pageId || null, Validators.required]
    });
  }

  saveErrors(): void {
    ValidatorUtil.validateForm(this.formError);
    if (this.formError.valid) {
      const value = this.formError.getRawValue();
      Object.keys(value)
        .forEach(key => {
          const item = this.errors.find(error => error.errorCode === Number(key));
          if (item) {
            item.pageId = value[key];
          } else {
            this.errors.push({ errorCode: Number(key), pageId: value[key] });
          }
        });

      const erros$ = this.errors.map(error => {
        return error.id ? this.errorService.updateError(error.id, error) : this.errorService.saveErrors(error);
      });

      this.spinnerService.showSpinner();
      concat(...erros$)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .pipe(switchMap(res => this.errorService.getErrors()))
        .subscribe(res => {
          this.errors = res || [];
          this.spinnerService.hideSpinner();
          this.notificationService.addSuccess('Página de error actualizada correctamente');
        }, () => this.spinnerService.hideSpinner());

    } else {
      this.notificationService.addWarning(GeneralLang.notifications.completeFields);
    }
  }

  cancelErrors(): void {
    this.formError.reset();
    this.errors
      .forEach(error => this.formError.get(`${error.errorCode}`)
        .setValue(error.pageId));
  }

  validateControl(controlName: number): boolean {
    return ValidatorUtil.validateControlByName(this.formError, controlName);
  }

  openDomain(domain?: IDomain): void {
    const config = {
      title: !domain ? 'Agregar dominio' : 'Editar dominio'
    };

    if (domain) {
      domain.isCurrent = domain.path === this.configuration.domain;
    }

    const modal = this.modalService.open(AddDomainComponent, config);
    modal.setPayload(domain);
    modal.result
      .then((res: GeneralResponse<IDomain>) => {
        if (domain && domain.id) {
          domain.path = res.data.path;
          domain.tax = res.data.tax;
          domain.promotionAllId = res.data.promotionAllId;
          domain.language = res.data.language;
          domain.currency = res.data.currency;
          domain.hideLanguage = res.data.hideLanguage;
        } else {
          this.domains.push(res.data);
        }

        if (res.data.isCurrent) {
          this.configuration.domain = res.data.path;
          this.generalService.setConfigurationStorage(this.configuration);
          this.updateDomainConfiguration();
        }

        this.notificationService.addSuccess(res.message);
      })
      .catch((res: GeneralResponse<IDomain>) => {
        if (res && res.message) {
          const index = this.domains.findIndex(dom => dom.id === domain.id);
          this.domains.splice(index, 1);
          this.notificationService.addSuccess(res.message);
        }
      });
  }

  private updateDomainConfiguration(): void {
    this.generalService.saveDomain(this.configuration.domain)
      .subscribe(res => { });
  }

  private createFormHome(): void {
    this.formHome = this.fb.group({
      home: [this.configuration && this.configuration.homeId || null, Validators.required]
    });
  }

  validateControlHome(): boolean {
    return ValidatorUtil.validateControlByName(this.formHome, 'home');
  }

  saveHome(): void {
    ValidatorUtil.validateForm(this.formError);
    if (this.formHome.valid) {
      this.generalService.saveHome(this.formHome.get('home').value)
        .pipe(takeUntil(this.unsubscribeDestroy$))
        .subscribe(res => {
          this.configuration.homeId = res.data.homeId;
          this.notificationService.addSuccess(res.message);
        });
    } else {
      this.notificationService.addWarning(GeneralLang.notifications.completeFields);
    }
  }

  cancelHome(): void {
    this.formHome.reset();
    this.formHome.get('home')
      .setValue(this.configuration && this.configuration.homeId || null);
  }
}
