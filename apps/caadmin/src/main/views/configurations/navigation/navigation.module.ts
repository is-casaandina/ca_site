import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CardFormModule } from '@ca-admin/components/card-form/card-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { AddDomainModule } from '@ca-admin/components/modals/add-domain/add-domain.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { NavigationRoutingModule } from './navigation-routing.module';
import { NavigationComponent } from './navigation.component';

@NgModule({
  imports: [
    CommonModule,
    NavigationRoutingModule,
    ReactiveFormsModule,
    CaCardModule,
    HeadingModule,
    CardFormModule,
    ModalModule,
    AddDomainModule
  ],
  declarations: [NavigationComponent]
})
export class NavigationModule { }
