import { HttpEventType } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '@ca-admin/environments/environment';
import { GeneralService, PageService } from '@ca-admin/services';
import { IconService } from '@ca-admin/services/icon.service';
import { CATEGORY } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ConfigurationLang } from '@ca-admin/settings/lang/configuration.lang';
import {
  IConfigurationResponse,
  IRoiback,
  IRoibackPrice,
  IScript,
  ISiteIdentityRequest,
  ISocialNetworking
} from '@ca-admin/statemanagement/models/configuration.interface';
import { IGeneralResponse, IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-general',
  templateUrl: './general.component.html'
})
export class GeneralComponent extends UnsubscribeOnDestroy implements OnInit {

  @ViewChild('file') input: ElementRef<HTMLInputElement>;

  CONFIGURATION_LANG = ConfigurationLang;
  GENERAL_LANG = GeneralLang;
  icons: FormArray;

  configuration: IConfigurationResponse;

  // Formulario General
  frmGeneral: FormGroup;
  sitename: AbstractControl;
  description: AbstractControl;
  keywords: AbstractControl;
  iconSite: AbstractControl;
  contact: FormGroup;

  // Formulario Script
  frmScript: FormGroup;
  tracing: AbstractControl;
  header: AbstractControl;
  body: AbstractControl;
  footer: AbstractControl;

  // Formulario de Reservaciones Roiback
  frmReservationRoiback: FormGroup;
  reservationEsp: AbstractControl;
  reservationEng: AbstractControl;
  reservationPor: AbstractControl;

  // Formulario de Preciode  Roiback
  frmRoibackPrice: FormGroup;
  urlPricePen: AbstractControl;
  urlPriceUsd: AbstractControl;
  urlPriceBrl: AbstractControl;

  // Formulario de Iconos de medio de pago
  frmIcons: FormGroup;

  // Formulario de Redes Sociales
  frmNetworking: FormGroup;
  urlFacebook: AbstractControl;
  urlTwitter: AbstractControl;
  urlInstagram: AbstractControl;
  urlYoutube: AbstractControl;
  urlFlickr: AbstractControl;
  urlWhatsapp: AbstractControl;

  linkBaseIcon = environment.BUCKET_ICON_JSON;
  progressUploadZip: number;

  contactPages: Array<IPageStateResponse>;

  constructor(
    private formBuilder: FormBuilder,
    protected generalService: GeneralService,
    protected notificationService: NotificationService,
    protected iconService: IconService,
    protected pageService: PageService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createFormIdentity();
    this.createFormReservationRoiback();
    this.createFormNetworking();
    this.createFormRoibackPrice();
    this.createFormIcons();
    this.createFormScript();
    this.getConfiguration();
    this.getContactPages();
  }

  getConfiguration(): void {
    this.generalService.getConfiguration()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IConfigurationResponse) => {
        this.configuration = response;
        this.setDataForms();
      });
  }

  private getContactPages(): void {
    this.pageService.getPageSelectByCategoryId(CATEGORY.CONTACT)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(pages => {
        this.contactPages = pages || [];
      });
  }

  setDataForms(): void {
    this.setDataIdentitfy();
    // Datos de Reservaciones Roiback
    if (this.configuration.roiback) {
      this.setDataRoiback();
    }
    // Datos de Roiback Price
    if (this.configuration.roibackPrice) {
      this.setDataRoibackPrice();
    }
    // Datos de Redes Sociales
    if (this.configuration.socialNetworking) {
      this.setDataNetworking();
    }
    // Datos de Iconos
    if (this.configuration.iconsPay) {
      this.setDataIcons();
    }
    // Datos de Script
    if (this.configuration.script) {
      this.setDataScript();
    }
  }

  // Metodos Identidad del Sitio
  setDataIdentitfy(): void {
    this.sitename.setValue(this.configuration.sitename);
    this.iconSite.setValue(this.configuration.icon);
    this.description.setValue(this.configuration.metadata.description);
    this.keywords.setValue(this.configuration.metadata.keywords);
    if (this.configuration.contact) {
      this.contact.setValue(this.configuration.contact);
    }
  }

  private createFormIdentity(): void {
    const contactFormGroup = this.formBuilder.group({
      mailsEs: [null, Validators.required],
      mailsEn: [null, Validators.required],
      mailsPt: [null, Validators.required],
      phoneEs: [null, Validators.required],
      phoneEn: [null, Validators.required],
      phonePt: [null, Validators.required],
      formPage: [null, Validators.required]
    });

    this.frmGeneral = this.formBuilder.group({
      iconSite: [null, Validators.required],
      sitename: [null, Validators.required],
      description: [null, Validators.required],
      keywords: [null],
      contact: contactFormGroup
    });

    this.iconSite = this.frmGeneral.get('iconSite');
    this.sitename = this.frmGeneral.get('sitename');
    this.description = this.frmGeneral.get('description');
    this.keywords = this.frmGeneral.get('keywords');
    this.contact = this.frmGeneral.get('contact') as FormGroup;
  }

  saveIdentity(): void {
    const validateForm = this.validateForm(this.frmGeneral);
    if (validateForm) {
      const params = this.frmGeneral.value as ISiteIdentityRequest;
      this.generalService.saveSiteIdentity(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<IConfigurationResponse>) => {
          this.configuration.metadata = response.data.metadata;
          this.configuration.sitename = response.data.sitename;
          this.configuration.icon = response.data.icon;

          response.data.contact = response.data.contact || {};
          this.configuration.contact = this.configuration.contact || {};
          this.configuration.contact.mailsEs = response.data.contact.mailsEs;
          this.configuration.contact.mailsEn = response.data.contact.mailsEn;
          this.configuration.contact.mailsPt = response.data.contact.mailsPt;

          this.configuration.contact.phoneEs = response.data.contact.phoneEs;
          this.configuration.contact.phoneEn = response.data.contact.phoneEn;
          this.configuration.contact.phonePt = response.data.contact.phoneEn;

          this.configuration.contact.formPage = response.data.contact.formPage;

          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  cancelIdentity(): void {
    this.setDataIdentitfy();
  }

  // Metodos Script
  setDataScript(): void {
    this.tracing.setValue(this.configuration.script.tracing || '');
    this.header.setValue(this.configuration.script.header || '');
    this.body.setValue(this.configuration.script.body || '');
    this.footer.setValue(this.configuration.script.footer || '');
  }

  private createFormScript(): void {
    this.frmScript = this.formBuilder.group({
      tracing: [null],
      header: [null],
      body: [null],
      footer: [null]
    });
    this.tracing = this.frmScript.get('tracing');
    this.header = this.frmScript.get('header');
    this.body = this.frmScript.get('body');
    this.footer = this.frmScript.get('footer');
  }

  saveScript(): void {
    const validateForm = this.validateForm(this.frmScript);
    if (validateForm) {
      const params = this.frmScript.value as IScript;
      this.generalService.saveScript(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<IConfigurationResponse>) => {
          this.configuration.script = response.data.script;
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  cancelScript(): void {
    this.setDataScript();
  }

  // Metodos de roiback
  setDataRoiback(): void {
    this.reservationEng.setValue(this.configuration.roiback.urlReservationEng);
    this.reservationEsp.setValue(this.configuration.roiback.urlReservationEsp);
    this.reservationPor.setValue(this.configuration.roiback.urlReservationPor);
  }

  private createFormReservationRoiback(): void {
    this.frmReservationRoiback = this.formBuilder.group({
      reservationEsp: [null, Validators.required],
      reservationEng: [null, Validators.required],
      reservationPor: [null, Validators.required]
    });
    this.reservationEsp = this.frmReservationRoiback.get('reservationEsp');
    this.reservationEng = this.frmReservationRoiback.get('reservationEng');
    this.reservationPor = this.frmReservationRoiback.get('reservationPor');
  }

  saveReservationRoiback($event: boolean): void {
    const validateForm = this.validateForm(this.frmReservationRoiback);
    if (validateForm) {
      const params = {
        urlReservationEng: this.reservationEng.value,
        urlReservationEsp: this.reservationEsp.value,
        urlReservationPor: this.reservationPor.value
      } as IRoiback;
      this.generalService.saveRoiback(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<IConfigurationResponse>) => {
          this.configuration.roiback = response.data.roiback;
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  cancelReservationRoiback($event: boolean): void {
    this.setDataRoiback();
  }

  // Metodos de roiback
  setDataRoibackPrice(): void {
    this.urlPricePen.setValue(this.configuration.roibackPrice.urlPricePen);
    this.urlPriceUsd.setValue(this.configuration.roibackPrice.urlPriceUsd);
    this.urlPriceBrl.setValue(this.configuration.roibackPrice.urlPriceBrl);
  }

  private createFormRoibackPrice(): void {
    this.frmRoibackPrice = this.formBuilder.group({
      urlPricePen: [null, Validators.required],
      urlPriceUsd: [null, Validators.required],
      urlPriceBrl: [null, Validators.required]
    });
    this.urlPricePen = this.frmRoibackPrice.get('urlPricePen');
    this.urlPriceUsd = this.frmRoibackPrice.get('urlPriceUsd');
    this.urlPriceBrl = this.frmRoibackPrice.get('urlPriceBrl');
  }

  saveRoibackPrice($event: boolean): void {
    const validateForm = this.validateForm(this.frmRoibackPrice);
    if (validateForm) {
      const params = this.frmRoibackPrice.value as IRoibackPrice;
      this.generalService.saveRoibackPrice(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<IConfigurationResponse>) => {
          this.configuration.roibackPrice = response.data.roibackPrice;
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  cancelRoibackPrice($event: boolean): void {
    this.setDataRoibackPrice();
  }

  // Metodos de Icons
  setDataIcons(): void {
    if (this.configuration.iconsPay) {
      this.configuration.iconsPay.forEach((icon: string, index: number) => {
        const frm = this.frmIcons.get('icons') as FormArray;
        frm.controls[index].setValue(icon);
      });
    }
  }

  private createFormIcons(): void {
    this.frmIcons = this.formBuilder.group({
      icons: this.formBuilder.array([
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control(''),
        this.formBuilder.control('')
      ])
    });
    this.icons = this.frmIcons.get('icons') as FormArray;
  }

  saveIcons($event: boolean): void {
    const validateForm = this.validateForm(this.frmReservationRoiback);
    if (validateForm) {
      const params = this.frmIcons.get('icons').value as Array<string>;
      this.generalService.saveIconsPay(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<IConfigurationResponse>) => {
          this.configuration.iconsPay = response.data.iconsPay;
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  cancelIcons($event: boolean): void {
    this.setDataIcons();
  }

  // Metodos Redes Sociales
  setDataNetworking(): void {
    this.urlFacebook.setValue(this.configuration.socialNetworking.urlFacebook);
    this.urlTwitter.setValue(this.configuration.socialNetworking.urlTwitter);
    this.urlInstagram.setValue(this.configuration.socialNetworking.urlInstagram);
    this.urlYoutube.setValue(this.configuration.socialNetworking.urlYoutube);
    this.urlFlickr.setValue(this.configuration.socialNetworking.urlFlickr);
    this.urlWhatsapp.setValue(this.configuration.socialNetworking.urlWhatsapp);
  }

  private createFormNetworking(): void {
    this.frmNetworking = this.formBuilder.group({
      urlFacebook: [''],
      urlTwitter: [''],
      urlInstagram: [''],
      urlYoutube: [''],
      urlFlickr: [''],
      urlWhatsapp: ['']
    });
    this.urlFacebook = this.frmNetworking.get('urlFacebook');
    this.urlTwitter = this.frmNetworking.get('urlTwitter');
    this.urlInstagram = this.frmNetworking.get('urlInstagram');
    this.urlYoutube = this.frmNetworking.get('urlYoutube');
    this.urlFlickr = this.frmNetworking.get('urlFlickr');
    this.urlWhatsapp = this.frmNetworking.get('urlWhatsapp');
  }

  saveNetworking($event: boolean): void {
    const validateForm = this.validateForm(this.frmNetworking);
    if (validateForm) {
      const params = this.frmNetworking.value as ISocialNetworking;
      this.generalService.saveSocialNetworking(params)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe((response: IGeneralResponse<IConfigurationResponse>) => {
          this.configuration.socialNetworking = response.data.socialNetworking;
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  cancelNetworking($event: boolean): void {
    this.setDataNetworking();
  }

  private validateForm(formGroup: FormGroup): boolean {
    ValidatorUtil.validateForm(formGroup);

    return formGroup.valid;
  }

  addFile(): void {
    const input = this.input.nativeElement;
    input.type = '';
    input.value = null;
    // end reset
    input.type = 'file';
    input.multiple = false;
    input.accept = 'application/zip,application/x-zip,application/x-zip-compressed';

    const onchange = () => {
      const files = Array.from(input.files);

      this.uploadFile(files[0]);

      input.onchange = null;
    };

    input.onchange = onchange;
    input.click();
  }

  uploadFile(iconZip: File): void {
    this.iconService.uploadDocuments(iconZip)
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(
        event => {
          if (event.type === HttpEventType.Response) {
            this.progressUploadZip = 0;
            this.notificationService.addSuccess(event.body.message);
          }

          if (event.type === HttpEventType.UploadProgress) {
            this.progressUploadZip = Math.round(event.loaded / event.total * 100);
          }
        },
        err => {
          this.progressUploadZip = 0;
          this.notificationService.addWarning(
            err.originalError && err.originalError.error.message ||
            GeneralLang.notifications.errorIconFile
          );
        });
  }

}
