import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CardFormModule } from '@ca-admin/components/card-form/card-form.module';
import { GallerySelectModule } from '@ca-admin/components/gallery-select/gallery-select.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { CaCardModule, CaInputModule } from '@ca-core/ui/lib/components';
import { GeneralRoutingModule } from './general-routing.module';
import { GeneralComponent } from './general.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    GeneralRoutingModule,
    CaCardModule,
    CaInputModule,
    HeadingModule,
    CardFormModule,
    GallerySelectModule
  ],
  declarations: [GeneralComponent]
})
export class GeneralModule { }
