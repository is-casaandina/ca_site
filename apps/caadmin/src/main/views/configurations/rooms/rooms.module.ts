import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { ClusterFormModule } from '@ca-admin/components/cluster-form/cluster-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { AutocompleteModule } from '@ca-admin/components/inputs/form-autocomplete/autocomplete.module';
import { RoomFormModule } from '@ca-admin/components/room-form/room-form.module';
import { SubheadingActionsModule } from '@ca-admin/components/subheading-actions/subheading-actions.module';
import { CaCardModule, CaPaginatorModule } from '@ca-core/ui/lib/components';
import { RoomsRoutingModule } from './rooms-routing.module';
import { RoomsComponent } from './rooms.component';

@NgModule({
  imports: [
    CommonModule,
    RoomsRoutingModule,
    FormsModule,
    RoomFormModule,
    HeadingModule,
    CaCardModule,
    SubheadingActionsModule,
    CardRowActionsModule,
    CaPaginatorModule,
    ClusterFormModule,
    AutocompleteModule,
    ReactiveFormsModule
  ],
  declarations: [RoomsComponent]
})
export class RoomsModule { }
