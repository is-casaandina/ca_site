import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RoomFormComponent } from '@ca-admin/components/room-form/room-form.component';
import { GeneralService } from '@ca-admin/services';
import { RoomService } from '@ca-admin/services/room.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IMessageResponse, IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { IPagedLivingRoomResponse, IRoomPageableRequest } from '@ca-admin/statemanagement/models/room.interface';
import { MODAL_BREAKPOINT, ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-rooms',
  templateUrl: './rooms.component.html'
})
export class RoomsComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;
  roomPaginated: IPagedLivingRoomResponse;
  pageZise = 10;
  pageCurrent = 1;
  form: FormGroup;
  categories: Array<IPageStateResponse>;

  category: string;
  hotelId: string;

  constructor(
    private roomService: RoomService,
    private notificationService: NotificationService,
    private modalService: ModalService,
    private generalService: GeneralService,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this.form = this.fb.group({ search: null, category: null });
    this.getCategories();
    this.listRoom();
  }

  private getCategories(): void {
    this.generalService.livingRoomCategories()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(res => {
        this.categories = res || [];
      });
  }

  private paramsPagination(): IRoomPageableRequest {
    return {
      size: this.pageZise,
      page: this.pageCurrent,
      hotelId: this.hotelId || '',
      category: this.category || ''
    } as IRoomPageableRequest;
  }

  listRoom(): void {
    this.roomService.roomsPaginated(this.paramsPagination())
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IPagedLivingRoomResponse) => {
        this.roomPaginated = response;
      });
  }

  addRoom(): void {
    this.modalService.open(RoomFormComponent, { size: MODAL_BREAKPOINT.lg })
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.listRoom();
        }
      })
      .catch(() => { });
  }

  editRoom(room: any): void {
    const refModal = this.modalService.open(RoomFormComponent, { size: MODAL_BREAKPOINT.lg });
    refModal.setPayload(room);
    refModal
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.listRoom();
        }
      })
      .catch(() => { });
  }

  deleteRoom(cluster: any): void {
    this.roomService.deleteRoom(cluster.id)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IMessageResponse) => {
        this.notificationService.addSuccess(response.message || this.GENERAL_LANG.notifications.success);
        this.listRoom();
      });
  }

  search(): void {
    this.category = this.form.get('category').value;
    this.hotelId = this.form.get('search').value;
    this.pageCurrent = 1;
    this.listRoom();
  }

  reset(): void {
    this.form.reset();
    this.category = null;
    this.hotelId = null;
    this.listRoom();
  }

}
