import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardFormModule } from '@ca-admin/components/card-form/card-form.module';
import { DynamicFormModule } from '@ca-admin/components/dynamic-form';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { BenefitsRoutingModule } from './benefits-routing.module';
import { BenefitsComponent } from './benefits.component';

@NgModule({
  imports: [
    CommonModule,
    BenefitsRoutingModule,
    DynamicFormModule,
    CaCardModule,
    HeadingModule,
    CardFormModule
  ],
  declarations: [BenefitsComponent]
})
export class BenefitsModule { }
