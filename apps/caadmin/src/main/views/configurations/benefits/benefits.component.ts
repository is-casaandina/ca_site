import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicFormComponent } from '@ca-admin/components/dynamic-form';
import { BenefitsService } from '@ca-admin/services/benefits.service';
import { STRUCTURE_COMPONENT_BENEFITS } from '@ca-admin/settings/constants/structures.contant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IBenefitsResponse } from '@ca-admin/statemanagement/models/benefits.interface';
import { IComponent } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { AngularUtil, UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-benefits',
  templateUrl: './benefits.component.html'
})
export class BenefitsComponent extends UnsubscribeOnDestroy implements OnInit {
  @ViewChild(DynamicFormComponent) dynamicForm: DynamicFormComponent;

  GENERAL_LANG = GeneralLang;
  structure: Array<IComponent> = null;
  benefits: IBenefitsResponse;

  constructor(
    private notificationService: NotificationService,
    private benefitsService: BenefitsService
  ) {
    super();
  }

  ngOnInit(): void {
    this.benefitsService.getBenefits()
      .subscribe((response: IBenefitsResponse) => {
        this.benefits = response;
        this.setDataForm();
      });
  }

  setDataForm(): void {
    const structure = AngularUtil.clone(STRUCTURE_COMPONENT_BENEFITS) as IComponent;
    structure.properties = structure.properties.map(property => {
      const value = this.benefits[property.code];
      // tslint:disable-next-line:prefer-conditional-expression
      if (property.type === 'array' || property.type === 'image' && value) {
        property.value = typeof value === 'string' ? JSON.parse(value) : value;
      } else {
        property.value = value;
      }

      return property;
    });

    this.structure = [structure];
  }

  formSubmitted(dataForm: any): void {
    this.benefitsService.saveBenefits(dataForm.benefits)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IGeneralResponse<IBenefitsResponse>) => {
        this.benefits = response.data;
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      });
  }

  save(event: any): void {
    this.dynamicForm.saveForm();
  }

  cancel(event: any): void {
    this.setDataForm();
  }

}
