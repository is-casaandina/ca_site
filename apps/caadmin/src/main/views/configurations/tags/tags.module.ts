import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { DescriptorFormModule } from '@ca-admin/components/descriptor-form/descriptor-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { SubheadingActionsModule } from '@ca-admin/components/subheading-actions/subheading-actions.module';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { TagsRoutingModule } from './tags-routing.module';
import { TagsComponent } from './tags.component';

@NgModule({
  imports: [
    CommonModule,
    TagsRoutingModule,
    CaCardModule,
    HeadingModule,
    SubheadingActionsModule,
    CardRowActionsModule,
    DescriptorFormModule
  ],
  declarations: [TagsComponent]
})
export class TagsModule { }
