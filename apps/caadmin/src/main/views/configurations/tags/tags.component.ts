import { Component, OnInit } from '@angular/core';
import { DescriptorFormComponent } from '@ca-admin/components/descriptor-form/descriptor-form.component';
import { DescriptorService } from '@ca-admin/services/descriptor.service';
import {
  ACTION_TYPES,
  CATEGORIES_DESCRIPTOR,
  CATEGORIES_DESCRIPTOR_HOTEL,
  TYPOGRAFY_LIST
} from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IDescriptorResponse } from '@ca-admin/statemanagement/models/descriptor.interface';
import { IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-tags',
  templateUrl: './tags.component.html'
})
export class TagsComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;
  categoriesDescriptor = CATEGORIES_DESCRIPTOR;
  typographies = TYPOGRAFY_LIST;
  descriptors: Array<IDescriptorResponse>;

  constructor(
    private descriptorService: DescriptorService,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
    this.descriptors = [];
  }

  ngOnInit(): void {
    this.getDescriptors();
  }

  getDescriptors(showSpin = true): void {
    this.descriptorService.descriptros(showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IDescriptorResponse>) => {
        this.descriptors = response;
      });
  }

  isHotelCategory(category: string): boolean {
    return category === CATEGORIES_DESCRIPTOR_HOTEL;
  }

  getTypographyStyle(typographyStyle: string): string {
    const typography = this.typographies.filter((type: any) => type.value === typographyStyle);

    return typography && typography.length && typography[0].name || '';
  }

  addDescriptor(category: string): void {
    const refModal = this.modalService.open(DescriptorFormComponent);
    refModal.setPayload({ category });
    refModal
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.acept || response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.getDescriptors();
        }
      });
  }

  editDescriptor(category: string, descriptor: IDescriptorResponse): void {
    const refModal = this.modalService.open(DescriptorFormComponent);
    refModal.setPayload({ category, descriptor });
    refModal
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.acept || response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.getDescriptors();
        }
      });
  }

  deleteDescriptor(descriptor: IDescriptorResponse): void {
    this.descriptorService.deleteDescriptor(descriptor.id)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IMessageResponse) => {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.getDescriptors();
      });
  }

  getDescriptorByCategory(category: string): Array<IDescriptorResponse> {
    return this.descriptors.filter(descriptor => descriptor.category === category);
  }

}
