import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CaTabsModule } from '@ca-core/ui/lib/components';
import { ConfigurationsRoutingModule } from './configurations-routing.module';
import { ConfigurationsComponent } from './configurations.component';

@NgModule({
  imports: [
    CommonModule,
    ConfigurationsRoutingModule,
    CaTabsModule
  ],
  declarations: [ConfigurationsComponent]
})
export class ConfigurationsModule { }
