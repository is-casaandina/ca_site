import { Component, OnInit } from '@angular/core';
import { TAB_CONFIGURATIONS } from '@ca-admin/settings/constants/general.constant';

@Component({
  selector: 'caadmin-configurations',
  templateUrl: './configurations.component.html'
})
export class ConfigurationsComponent implements OnInit {

  tabItemsList = TAB_CONFIGURATIONS;

  constructor() { }

  ngOnInit(): void {
  }

}
