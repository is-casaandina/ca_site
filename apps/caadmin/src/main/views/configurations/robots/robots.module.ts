import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { RobotsFormModule } from '@ca-admin/components/robots-form/robots-form.module';
import { SubheadingActionsModule } from '@ca-admin/components/subheading-actions/subheading-actions.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { RobotsRoutingModule } from './robots-routing.module';
import { RobotsComponent } from './robots.component';

@NgModule({
  imports: [
    CommonModule,
    RobotsRoutingModule,
    CaCardModule,
    HeadingModule,
    ModalModule,
    SubheadingActionsModule,
    RobotsFormModule
  ],
  declarations: [RobotsComponent]
})
export class RobotsModule { }
