import { Component, OnInit } from '@angular/core';
import { RobotsFormComponent } from '@ca-admin/components/robots-form/robots-form.component';
import { GeneralService } from '@ca-admin/services';
import { ACTION_TYPES, USER_AGENTS } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IRobotsResponse } from '@ca-admin/statemanagement/models/configuration.interface';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-robots',
  templateUrl: './robots.component.html'
})
export class RobotsComponent extends UnsubscribeOnDestroy implements OnInit {
  GENERAL_LANG = GeneralLang;
  listUserAgent = USER_AGENTS;

  robots: Array<IRobotsResponse> = [];

  constructor(
    private generalServie: GeneralService,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getRobots();
  }

  getRobots(): void {
    this.generalServie.getRobots()
      .subscribe((response: Array<IRobotsResponse>) => {
        this.robots = response;
      });
  }

  addRobots(): void {
    this.modalService.open(RobotsFormComponent, { title: 'Agregar Robots' })
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.getRobots();
        }
      })
      .catch(() => { });
  }

  deleteCategory(robots: IRobotsResponse): void {
    this.generalServie.deleteRobots(robots.id)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IGeneralResponse<Array<IRobotsResponse>>) => {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.getRobots();
      });
  }

  existItems(userAgent: string): number {
    return this.robots.filter(r => r.userAgent === userAgent).length;
  }

  existSitemap(): number {
    return this.robots.filter(r => !r.userAgent).length;
  }

  getRotobos(userAgent: string): Array<IRobotsResponse> {
    return this.robots.filter((r: IRobotsResponse) => r.userAgent === userAgent)
      .sort((a: IRobotsResponse, b: IRobotsResponse) => a.type > b.type ? 1 : -1);
  }

  getSitemaps(): Array<IRobotsResponse> {
    return this.robots.filter((r: IRobotsResponse) => !r.userAgent)
      .sort((a: IRobotsResponse, b: IRobotsResponse) => a.type > b.type ? 1 : -1);
  }

  generateRobots(): void {
    this.generalServie.generateRobots()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IGeneralResponse<any>) => {
        this.notificationService.addSuccess(response.message);
      });
  }

  generateSiteMap(): void {
    this.generalServie.generateSitemap()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: IGeneralResponse<any>) => {
        this.notificationService.addSuccess(response.message);
      });
  }

}
