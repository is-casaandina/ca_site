import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { ClusterFormModule } from '@ca-admin/components/cluster-form/cluster-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { SubheadingActionsModule } from '@ca-admin/components/subheading-actions/subheading-actions.module';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { ClustersRoutingModule } from './clusters-routing.module';
import { ClustersComponent } from './clusters.component';

@NgModule({
  imports: [
    CommonModule,
    HeadingModule,
    CaCardModule,
    SubheadingActionsModule,
    ClustersRoutingModule,
    CardRowActionsModule,
    ClusterFormModule
  ],
  declarations: [ClustersComponent]
})
export class ClustersModule { }
