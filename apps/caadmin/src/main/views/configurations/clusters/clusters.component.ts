import { Component, OnInit } from '@angular/core';
import { ClusterFormComponent } from '@ca-admin/components/cluster-form/cluster-form.component';
import { ClusterService } from '@ca-admin/services/cluster.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IClusterResponse } from '@ca-admin/statemanagement/models/cluster.interface';
import { IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-clusters',
  templateUrl: './clusters.component.html'
})
export class ClustersComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  clusters: Array<IClusterResponse>;

  constructor(
    private clusterService: ClusterService,
    private notificationService: NotificationService,
    private modalService: ModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getClusteres();
  }

  getClusteres(): void {
    this.clusterService.getClusters()
    .subscribe((response: Array<IClusterResponse>) => {
      this.clusters = response;
    });
  }

  addCluster(cluster: IClusterResponse): void {
    this.modalService.open(ClusterFormComponent)
    .result
    .then((response: any) => {
      if (response === ACTION_TYPES.save) {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.getClusteres();
      }
    });
  }

  editCluster(cluster: IClusterResponse): void {
    const refModal = this.modalService.open(ClusterFormComponent);
    refModal.setPayload(cluster);
    refModal
    .result
    .then((response: any) => {
      if (response === ACTION_TYPES.save) {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.getClusteres();
      }
    });
  }

  deleteCluster(cluster: IClusterResponse): void {
    this.clusterService.deleteCluster(cluster.id)
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe((response: IMessageResponse) => {
      this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      this.getClusteres();
    });
  }

}
