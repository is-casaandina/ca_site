import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigurationsComponent } from './configurations.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigurationsComponent,
    children: [
      {
        path: '',
        redirectTo: 'general'
      },
      {
        path: 'general',
        loadChildren: './general/general.module#GeneralModule'
      },
      {
        path: 'robots',
        loadChildren: './robots/robots.module#RobotsModule'
      },
      {
        path: 'categories',
        loadChildren: './categories/categories.module#CategoriesModule'
      },
      {
        path: 'coins',
        loadChildren: './coins/coins.module#CoinsModule'
      },
      {
        path: 'clusters',
        loadChildren: './clusters/clusters.module#ClustersModule'
      },
      {
        path: 'services',
        loadChildren: './services/services.module#ServicesModule'
      },
      {
        path: 'tags',
        loadChildren: './tags/tags.module#TagsModule'
      },
      {
        path: 'benefits',
        loadChildren: './benefits/benefits.module#BenefitsModule'
      },
      {
        path: 'notifications',
        loadChildren: './notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'rooms',
        loadChildren: './rooms/rooms.module#RoomsModule'
      },
      {
        path: 'navigation',
        loadChildren: './navigation/navigation.module#NavigationModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }
