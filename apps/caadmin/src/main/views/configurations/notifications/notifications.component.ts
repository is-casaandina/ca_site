import { Component, OnInit } from '@angular/core';
import { NotificationFormComponent } from '@ca-admin/components/notification-form/notification-form.component';
import { PageNotificationService } from '@ca-admin/services/page-notification.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { IPagedPageNotificationResponse, IPageNotificationPageableRequest } from '@ca-admin/statemanagement/models/notification.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-notifications',
  templateUrl: './notifications.component.html'
})
export class NotificationsComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  alertsPaginated: IPagedPageNotificationResponse;
  pageZise = 10;
  pageCurrent = 1;

  constructor(
    private pageNotificationService: PageNotificationService,
    private notificationService: NotificationService,
    private modalService: ModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this.search();
  }

  private paramsPagination(): IPageNotificationPageableRequest {
    return {
      size: this.pageZise,
      page: this.pageCurrent
    } as IPageNotificationPageableRequest;
  }

  search(showSping = true): void {
    this.pageNotificationService.getAlerts(this.paramsPagination(), showSping)
    .subscribe((response: IPagedPageNotificationResponse) => {
      this.alertsPaginated = response;
    });
  }

  addAlert(): void {
    this.modalService.open(NotificationFormComponent, { title: 'Agregar nueva Alerta' })
    .result
    .then((response: any) => {
      if (response === ACTION_TYPES.save) {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.search();
      }
    });
  }

  editAlert(alert: any): void {
    const refModal = this.modalService.open(NotificationFormComponent, { title: 'Editar Alerta' });
    refModal.setPayload(alert);
    refModal
    .result
    .then((response: any) => {
      if (response === ACTION_TYPES.save) {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.search();
      }
    });
  }

  deleteAlert(alert: any): void {
    this.pageNotificationService.deleteAlert(alert.id)
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe((response: IMessageResponse) => {
      this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      this.search(false);
    });
  }

}
