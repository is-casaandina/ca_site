import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { ClusterFormModule } from '@ca-admin/components/cluster-form/cluster-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { NotificationFormModule } from '@ca-admin/components/notification-form/notification-form.module';
import { SubheadingActionsModule } from '@ca-admin/components/subheading-actions/subheading-actions.module';
import { CaCardModule, CaDatepickerModule, CaPaginatorModule } from '@ca-core/ui/lib/components';
import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationsComponent } from './notifications.component';

@NgModule({
  imports: [
    CommonModule,
    NotificationsRoutingModule,
    HeadingModule,
    CaCardModule,
    SubheadingActionsModule,
    CardRowActionsModule,
    CaPaginatorModule,
    ClusterFormModule,
    NotificationFormModule,
    CaDatepickerModule
  ],
  declarations: [NotificationsComponent]
})
export class NotificationsModule { }
