import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddCurrencyFormComponent } from '@ca-admin/components/add-currency-form/add-currency-form.component';
import { CurrencyService } from '@ca-admin/services/currency.service';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ICurrencyRequest, ICurrencyResponse } from '@ca-admin/statemanagement/models/currency.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-coins',
  templateUrl: './coins.component.html'
})
export class CoinsComponent extends UnsubscribeOnDestroy implements OnInit {
  GENERAL_LANG = GeneralLang;
  currencies: Array<ICurrencyResponse>;
  frmCurrencies: FormGroup;

  constructor(
    private currencyService: CurrencyService,
    private fb: FormBuilder,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createFormCurrencies();
    this.getCurrencies();
  }

  getCurrencies(): void {
    this.currencyService.getCurrencies()
    .subscribe((response: Array<ICurrencyResponse>) => {
      this.currencies = response.sort((a, b) => b.isPrincipal < a.isPrincipal ? -1 : 1);
      this.currencies.forEach(currency => {
        this.frmCurrencies.addControl(currency.codeIso, this.fb.control(currency.value, [Validators.required]));
      });
    });
  }

  createFormCurrencies(): void {
    this.frmCurrencies = this.fb.group({});
  }

  addCurrency(event: any): void {
    this.modalService.open(AddCurrencyFormComponent)
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.getCurrencies();
        }
      });
  }

  save(event: any): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveCurrencies();
      this.currencyService.saveCurrencies(params, true)
        .pipe(
          takeUntil(this.unsubscribeDestroy$)
        )
        .subscribe(() => {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        });
    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

  private paramsSaveCurrencies(): Array<ICurrencyRequest> {
    const currencies = this.frmCurrencies.value;

    return this.currencies.map((currency: ICurrencyResponse) => {
      return {
        codeIso: currency.codeIso,
        name: currency.name,
        isPrincipal: currency.isPrincipal,
        symbol: currency.symbol,
        value: Number.parseFloat(currencies[currency.codeIso])
      } as ICurrencyRequest;
    });
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmCurrencies);

    return this.frmCurrencies.valid;
  }

  cancel(event: any): void {
    this.currencies.forEach(currency => {
      this.frmCurrencies.get(currency.codeIso)
      .setValue(currency.value);
    });
  }

}
