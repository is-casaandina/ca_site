import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AddCurrencyFormModule } from '@ca-admin/components/add-currency-form/add-currency-form.module';
import { CardFormModule } from '@ca-admin/components/card-form/card-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule, CaInputModule } from '@ca-core/ui/lib/components';
import { CoinsRoutingModule } from './coins-routing.module';
import { CoinsComponent } from './coins.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CaCardModule,
    CaInputModule,
    HeadingModule,
    CardFormModule,
    CoinsRoutingModule,
    AddCurrencyFormModule,
    ModalModule
  ],
  declarations: [CoinsComponent]
})
export class CoinsModule { }
