import { Component, OnInit } from '@angular/core';
import { AssistanceFormComponent } from '@ca-admin/components/assistance-form/assistance-form.component';
import { AssistanceService } from '@ca-admin/services/assistance.service';
import { ACTION_TYPES, ASSISTANCE_TYPE } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import {
  IAssistancePaginatedResponse,
  IAssistanceResponse,
  IAssistanceSearchRequest
} from '@ca-admin/statemanagement/models/assistance.interface';
import { IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-services',
  templateUrl: './services.component.html'
})
export class ServicesComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  currentType: number;
  types: Array<{ name: string, code: number }>;
  assistancePaginated: IAssistancePaginatedResponse;
  pageZise = 10;
  pageCurrent = 1;
  currentSearchText: string;

  constructor(
    private assistanceService: AssistanceService,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.currentType = ASSISTANCE_TYPE.Hotel;
    this.types = [
      { name: 'Hotel', code: ASSISTANCE_TYPE.Hotel },
      { name: 'Habitaciones', code: ASSISTANCE_TYPE.Habitaciones }
    ];

    this.search();
  }

  private paramsRequestPaginated(): IAssistanceSearchRequest {
    return {
      typeAssistance: this.currentType,
      size: this.pageZise,
      page: this.pageCurrent,
      title: this.currentSearchText || ''
    } as IAssistanceSearchRequest;
  }

  debounceInput(text: string): void {
    this.currentSearchText = text;
    this.pageCurrent = 1;
    this.search();
  }

  search(showSpin: boolean = true): void {
    const params = this.paramsRequestPaginated();
    this.assistanceService.assistancePaginated(params, showSpin)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IAssistancePaginatedResponse) => {
        this.assistancePaginated = response;
      });
  }

  addService(): void {
    const refModal = this.modalService.open(AssistanceFormComponent);
    refModal.setPayload({ typeAssistance: this.currentType });
    refModal
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.acept || response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.search();
        }
      });
  }

  editService(assistance: IAssistanceResponse): void {
    const refModal = this.modalService.open(AssistanceFormComponent);
    refModal.setPayload({ typeAssistance: this.currentType, assistance });
    refModal
      .result
      .then((response: any) => {
        if (response === ACTION_TYPES.acept || response === ACTION_TYPES.save) {
          this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          this.search();
        }
      });
  }

  deleteService(assistance: IAssistanceResponse): void {
    this.assistanceService.deleteAsistance(assistance.id)
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: IMessageResponse) => {
        this.notificationService.addSuccess(response.message || this.GENERAL_LANG.notifications.success);
        this.search(false);
      });
  }

  selectType(code: number): void {
    this.currentType = code;
    this.currentSearchText = '';
    this.search();
  }

}
