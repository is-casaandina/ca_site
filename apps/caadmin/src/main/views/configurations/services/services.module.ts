import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AssistanceFormModule } from '@ca-admin/components/assistance-form/assistance-form.module';
import { CardRowActionsModule } from '@ca-admin/components/card-row-actions/card-row-actions.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { DebounceInputDirectiveModule } from '@ca-core/shared/helpers/directives/debounce-input.directive';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule, CaPaginatorModule } from '@ca-core/ui/lib/components';
import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ServicesRoutingModule,
    CaCardModule,
    HeadingModule,
    CardRowActionsModule,
    CaPaginatorModule,
    ModalModule,
    AssistanceFormModule,
    DebounceInputDirectiveModule
  ],
  declarations: [ServicesComponent]
})
export class ServicesModule { }
