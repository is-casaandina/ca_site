import { Component, OnInit } from '@angular/core';
import { CategoryFormComponent } from '@ca-admin/components/category-form/category-form.component';
import { CategoryService } from '@ca-admin/services';
import { ACTION_TYPES } from '@ca-admin/settings/constants/general.constant';
import { GeneralLang } from '@ca-admin/settings/lang';
import { ICategoryResponse } from '@ca-admin/statemanagement/models/category.interface';
import { IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-categories',
  templateUrl: './categories.component.html'
})
export class CategoriesComponent extends UnsubscribeOnDestroy implements OnInit {

  GENERAL_LANG = GeneralLang;

  categories: Array<ICategoryResponse>;

  constructor(
    private catgoryService: CategoryService,
    private modalService: ModalService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(): void {
    this.catgoryService.categoryList()
    .subscribe((response: Array<ICategoryResponse>) => {
      this.categories = response;
    });
  }

  addCategory(event: any): void {
    this.modalService.open(CategoryFormComponent)
    .result
    .then((response: any) => {
      if (response === ACTION_TYPES.save) {
        this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
        this.getCategories();
      }
    });
  }

  deleteCategory(category: ICategoryResponse): void {
    this.catgoryService.deleteCategory(category.id)
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe((response: IMessageResponse) => {
      this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
      this.getCategories();
    });
  }

}
