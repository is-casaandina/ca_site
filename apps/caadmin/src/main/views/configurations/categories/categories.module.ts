import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CategoryFormModule } from '@ca-admin/components/category-form/category-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { SubheadingActionsModule } from '@ca-admin/components/subheading-actions/subheading-actions.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { CaCardModule } from '@ca-core/ui/lib/components';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';

@NgModule({
  imports: [
    CommonModule,
    CaCardModule,
    HeadingModule,
    ModalModule,
    SubheadingActionsModule,
    CategoriesRoutingModule,
    CategoryFormModule
  ],
  declarations: [CategoriesComponent]
})
export class CategoriesModule { }
