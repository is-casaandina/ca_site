import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PasswordChangeFormModule } from '@ca-admin/components/password-change-form/password-change-form.module';
import { PasswordChangeRoutingModule } from './password-change-routing.module';
import { PasswordChangeComponent } from './password-change.component';

@NgModule({
  imports: [
    CommonModule,
    PasswordChangeRoutingModule,
    PasswordChangeFormModule
  ],
  declarations: [PasswordChangeComponent],
  exports: [PasswordChangeComponent]
})
export class PasswordChangeModule { }
