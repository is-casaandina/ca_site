import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CardFormModule } from '@ca-admin/components/card-form/card-form.module';
import { HeadingModule } from '@ca-admin/components/heading/heading.module';
import { CaCardModule, CaInputModule } from '@ca-core/ui/lib/components';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    CaCardModule,
    HeadingModule,
    CardFormModule,
    ReactiveFormsModule,
    CaInputModule
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
