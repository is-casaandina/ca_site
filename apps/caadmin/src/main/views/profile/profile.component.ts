import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoleService, UserService } from '@ca-admin/services';
import { GeneralLang, UserLang } from '@ca-admin/settings/lang';
import { IPermissionResponse } from '@ca-admin/statemanagement/models/permission.interface';
import { IRolePermissionsDetailResponse } from '@ca-admin/statemanagement/models/role.interface';
import { IProfileRequest, IProfileResponse } from '@ca-admin/statemanagement/models/user.interface';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { UnsubscribeOnDestroy, ValidatorUtil } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent extends UnsubscribeOnDestroy implements OnInit {
  USER_LANG = UserLang;
  GENERAL_LANG = GeneralLang;

  profile: IProfileResponse;
  permissionCodeList: Array<IPermissionResponse>;

  frmProfile: FormGroup;
  name: AbstractControl;
  fullname: AbstractControl;
  lastname: AbstractControl;
  email: AbstractControl;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private roleService: RoleService,
    private notificationService: NotificationService
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.setProfile();
  }

  private createForm(): void {
    this.frmProfile = this.formBuilder.group({
      name: [{ value: null, disabled: true }, Validators.required],
      fullname: [null, Validators.required],
      lastname: [null, Validators.required],
      email: [null, Validators.required]
    });

    this.name = this.frmProfile.get('name');
    this.fullname = this.frmProfile.get('fullname');
    this.lastname = this.frmProfile.get('lastname');
    this.email = this.frmProfile.get('email');

  }

  private setProfile(): void {
    this.userService.getProfile()
    .pipe(
      takeUntil(this.unsubscribeDestroy$)
    )
    .subscribe((profile: IProfileResponse) => {
      this.profile = profile;
      this.name.setValue(profile.name);
      this.fullname.setValue(profile.fullname);
      this.lastname.setValue(profile.lastname);
      this.email.setValue(profile.email);

      this.getRoles();
    });
  }

  private getRoles(): void {
    this.roleService.rolesListPermissionsDetail()
      .pipe(
        takeUntil(this.unsubscribeDestroy$)
      )
      .subscribe((response: Array<IRolePermissionsDetailResponse>) => {
        const role = response.find((value: IRolePermissionsDetailResponse) => {
          return value.roleCode === this.profile.role.roleCode;
        });
        this.permissionCodeList = role.permissionCodeList;
      });
  }

  private validateForm(): boolean {
    ValidatorUtil.validateForm(this.frmProfile);

    return this.frmProfile.valid;
  }

  private paramsSaveUser(): IProfileRequest {
    return {
      fullname: this.fullname.value,
      lastname: this.lastname.value,
      email: this.email.value
    } as IProfileRequest;
  }

  saveProfile(): void {
    const validateForm = this.validateForm();
    if (validateForm) {
      const params = this.paramsSaveUser();
      this.userService.updateProfile(params, true)
          .pipe(
            takeUntil(this.unsubscribeDestroy$)
          )
          .subscribe(() => {
            this.notificationService.addSuccess(this.GENERAL_LANG.notifications.success);
          });

    } else {
      this.notificationService.addWarning(this.GENERAL_LANG.notifications.completeFields);
    }
  }

}
