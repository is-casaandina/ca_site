import { OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationService } from '@ca-admin/services/configuration.service';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { INotify } from '@ca-core/shared/helpers/notification/notification.interface';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

export abstract class LayoutBase extends UnsubscribeOnDestroy implements OnInit {
  static lastNotification = 0;

  notification: INotify;
  activeNotify: boolean;
  activeModal: boolean;

  timeNotiication = 2000;
  constructor(
    protected notificationService: NotificationService,
    protected route: ActivatedRoute,
    protected configurationService: ConfigurationService,
    protected modalService: ModalService
  ) {
    super();
  }

  ngOnInit(): void {
    this.setConfiguration();
    this.getActiveModal();
    this.getNotifies();
  }

  private setConfiguration(): void {
    const userPermissions = this.route.snapshot.data.userPermissions;
    if (userPermissions) {
      const userProfile = this.configurationService.getUserProfile();
      userProfile.username = userPermissions.fullname;
      this.configurationService.setUserProfile(userProfile);
      this.configurationService.setUserPermissions(userPermissions);
    }
  }

  private getActiveModal(): void {
    this.activeModal = this.modalService.isActiveModal();
  }

  private getNotifies(): void {
    this.notificationService
      .getNotifies()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe((response: Array<INotify>) => {
        const isActiveModal = this.modalService.isActiveModal();
        if (response.length > LayoutBase.lastNotification && !isActiveModal) {
          LayoutBase.lastNotification = response.length;
          this.notification = response[response.length - 1];
          this.activeNotify = true;
          setTimeout(() => {
            this.activeNotify = false;
          }, this.timeNotiication);
        }
      });
  }
}
