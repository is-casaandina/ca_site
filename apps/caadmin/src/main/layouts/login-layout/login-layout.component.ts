import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationService } from '@ca-admin/services';
import { CA } from '@ca-admin/settings/constants/general.constant';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { LayoutBase } from '../layout-base';

@Component({
  selector: 'caadmin-login-layout',
  templateUrl: './login-layout.component.html',
  styles: []
})
export class LoginLayoutComponent extends LayoutBase implements OnInit {
  CA = CA;

  constructor(
    protected notificationService: NotificationService,
    protected route: ActivatedRoute,
    protected configurationService: ConfigurationService,
    protected modalService: ModalService
  ) {
    super(notificationService, route, configurationService, modalService);
  }

  ngOnInit(): void {
    this.timeNotiication = 4000;
    super.ngOnInit();
  }
}
