import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { CaNotifyModule } from '@ca-core/ui/lib/components';
import { LoginLayoutComponent } from './login-layout.component';

@NgModule({
  declarations: [LoginLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    NotificationModule,
    CaNotifyModule
  ]
})
export class LoginLayoutModule { }
