import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationService } from '@ca-admin/services';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { INotify } from '@ca-core/shared/helpers/notification/notification.interface';
import { LayoutBase } from '../layout-base';

@Component({
  selector: 'caadmin-editor-layout',
  templateUrl: './editor-layout.component.html'
})
export class EditorLayoutComponent extends LayoutBase implements OnInit {
  notification: INotify;
  timeNotiication = 5000;

  constructor(
    protected notificationService: NotificationService,
    protected route: ActivatedRoute,
    protected configurationService: ConfigurationService,
    protected modalService: ModalService
  ) {
    super(notificationService, route, configurationService, modalService);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }
}
