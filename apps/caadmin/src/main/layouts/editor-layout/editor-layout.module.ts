import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeaderModule } from '@ca-admin/components/layouts/header/header.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { CaNotifyModule } from '@ca-core/ui/lib/components';
import { EditorLayoutComponent } from './editor-layout.component';

@NgModule({
  declarations: [EditorLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    HeaderModule,
    NotificationModule,
    CaNotifyModule,
    ModalModule
  ]
})
export class EditorLayoutModule { }
