import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigurationService } from '@ca-admin/services/configuration.service';
import { STORAGE_KEY } from '@ca-admin/settings/constants/general.constant';
import { IMAGES } from '@ca-admin/settings/constants/images.constant';
import { ModalService } from '@ca-core/shared/helpers/modal';
import { NotificationService } from '@ca-core/shared/helpers/notification';
import { INotify } from '@ca-core/shared/helpers/notification/notification.interface';
import { LocalStorageService } from '@ca-core/shared/helpers/util/storage-manager';
import { LayoutBase } from '../layout-base';

@Component({
  selector: 'caadmin-main-layout',
  templateUrl: './main-layout.component.html',
  styles: []
})
export class MainLayoutComponent extends LayoutBase implements OnInit {

  IMAGES = IMAGES;

  notification: INotify;
  notifyActive: boolean;
  isModal: boolean;

  constructor(
    protected notificationService: NotificationService,
    protected storageService: LocalStorageService,
    protected configurationService: ConfigurationService,
    protected modalService: ModalService,
    protected router: ActivatedRoute
  ) {
    super(notificationService, router, configurationService, modalService);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  logOut(): void {
    this.storageService.removeItem(STORAGE_KEY.userToken);
  }
}
