import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeaderModule } from '@ca-admin/components/layouts/header/header.module';
import { MenuModule } from '@ca-admin/components/menu/menu.module';
import { ModalModule } from '@ca-core/shared/helpers/modal';
import { NotificationModule } from '@ca-core/shared/helpers/notification';
import { CaNotifyModule } from '@ca-core/ui/lib/components';
import { MainLayoutComponent } from './main-layout.component';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    NotificationModule,
    CaNotifyModule,
    MenuModule,
    ModalModule,
    HeaderModule
  ]
})
export class MainLayoutModule { }
