export const ConfigurationLang = {
  labels: {
    titleGeneral: 'Configuraciones Generales',
    titleNavigation: 'Navegación',
    titleInfoSite: 'Identidad del Sitio',
    titleScripts: 'Scripts',
    titleReservationRoiback: 'Reservas Roiback',
    reservationEsp: 'Reservas Español',
    reservationEng: 'Reservas Ingles',
    reservationPor: 'Reservas Portugués',
    titlePriceRoiback: 'Precio Hotel Roiback',
    soles: 'Soles',
    dollars: 'Dolares',
    real: 'Reales',
    titleRevinante: 'Servicios Revinante',
    rating: 'Rating',
    assessment: 'Valoración',
    titleIconsPay: 'Iconos de Métodos de Pago',
    iconSite: 'Ícono de sitio',
    descriptionIconSite: 'Esos íconos deberán ser cuadrados y de al menos <b>16x16</b> píxeles',
    titleNetworking: 'Redes Sociales',
    facebook: 'Facebook',
    twitter: 'Twitter',
    instagram: 'Instagram',
    youtube: 'Youtube',
    flicker: 'Flicker'
  },
  buttons: {
  },
  modalConfirm: {
  },
  notifications: {
    success: 'Se realizó correctamente',
    notSaveCorrectly: 'No se guardo correctamente',
    completeFields: 'Completa los campos'
  }
};
