export const PageLang = {
  labels: {
    searchPages: 'Buscar páginas'
  },
  descriptions: {
    staticPageCanNotEdit: 'Esta página es estática, no se puede editar.'
  }
};
