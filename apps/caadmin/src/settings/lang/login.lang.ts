export const LoginLang = {
  labels: {
    user: 'Usuario',
    password: 'Contraseña',
    cPassword: 'Confirmar contraseña',
    rememberUser: 'Recordar usuario',
    email: 'Correo electrónico',
    forgotPassword: '¿Olvidaste la contraseña?'
  },
  buttons: {
    enter: 'entrar',
    send: 'enviar'
  },
  messages: {
    invalidUser: 'La contraseña y/o el usuario no coinciden.',
    invalidUser2: 'El usuario no existe.'
  }
};
