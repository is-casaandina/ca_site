export const GeneralLang = {
  labels: {
    search: 'Buscar',
    searchPage: 'Buscar páginas',
    searchComponent: 'Buscar componente',
    searchUser: 'Buscar usuario',
    users: 'Usuarios',
    user: 'Usuario',
    states: 'Estados',
    notEditable: 'No editable',
    lastUpdate: 'Última actualización',
    state: 'Estado',
    language: 'Idioma',
    role: 'Rol',
    roles: 'Roles',
    apply: 'Aplicar',
    clean: 'Limpiar'
  },
  buttons: {
    save: 'Guardar',
    back: 'Volver',
    saveChange: 'Guardar Cambios',
    cancel: 'Cancelar',
    search: 'Buscar',
    select: 'Seleccionar',
    preview: 'Vista previa'
  },
  modalConfirm: {
    titleDelete: '¿Seguro que deseas eliminar',
    titleDeleteCustom: '¿Seguro que deseas eliminar {{ customField }}?',
    titleCancelChange: '¿Esta seguro que desea perder todos los cambios realizados?',
    titlePublishChange: '¿Esta seguro que desea publicar la página guardada?',
    textPublishChange: 'Asegúrese de haber guardado los cambios, antes de publicar.'
  },
  notifications: {
    success: 'Se realizó correctamente',
    notSaveCorrectly: 'No se guardo correctamente',
    completeFields: 'Completa los campos',
    existIncorrectFields: 'Existen campos incorrectos',
    thereAreChangesWithoutSaving: 'Existen cambios sin guardar',
    updateSucces: 'Página actualizada correctamente',
    errorIconFile: 'Error al subir zip de iconos'
  },
  links: {
    signOff: 'Cerrar sesión',
    seeAll: 'Ver todos',
    profile: 'Perfil',
    configuration: 'Configuración general',
    passwordChange: 'Cambiar contraseña'
  }
};
