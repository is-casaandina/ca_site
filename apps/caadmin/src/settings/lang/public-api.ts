export { PageLang } from './page.lang';
export { GeneralLang } from './general.lang';
export { UserLang } from './user.lang';
export { LoginLang } from './login.lang';
