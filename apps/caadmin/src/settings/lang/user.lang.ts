export const UserLang = {
  titles: {
    addUser: 'Añadir Usuario',
    editUser: 'Editar Usuario',
    assignRole: 'Ahora asignale un rol'
  },
  descriptions: {
    addUserCms: 'Añade los usuarios que desees para que participen del gestor.'
  },
  labels: {
    role: 'Rol',
    user: 'Usuario',
    fullName: 'Nombres',
    lastname: 'Apellidos',
    email: 'Correo electrónico',
    password: 'Contraseña'
  },
  buttons: {
    addUser: 'Agregar usuario'
  },
  links: {
    addUser: 'Agregar usuario',
    createRole: 'Crear rol'
  }
};
