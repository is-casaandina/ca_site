import { CATEGORY } from './general.constant';
import { MENU_CODE } from './menu.constant';

export enum TRAY_FILTER_PROPERTY {
  CATEGORIES = 'categories',
  USER_LIST = 'userList',
  STATES = 'states',
  RANGE_DATE = 'rangeDate',
  COUNTRIES = 'countries',
  DESTINATIONS = 'destinations',
  GENERIC_TYPE = 'genericType'
}

export const TRAY_FILTER_DEFINITION = [
  {
    menuCode: MENU_CODE.pages,
    properties: [
      TRAY_FILTER_PROPERTY.CATEGORIES,
      TRAY_FILTER_PROPERTY.USER_LIST,
      TRAY_FILTER_PROPERTY.STATES,
      TRAY_FILTER_PROPERTY.RANGE_DATE
    ]
  },
  {
    menuCode: MENU_CODE.pagesError,
    categories: [CATEGORY.PAGE_ERROR],
    properties: []
  },
  {
    menuCode: MENU_CODE.promotions,
    categories: [CATEGORY.PROMOTIONS_DETAIL, CATEGORY.PROMOTIONS],
    properties: [
      TRAY_FILTER_PROPERTY.GENERIC_TYPE,
      TRAY_FILTER_PROPERTY.STATES,
      TRAY_FILTER_PROPERTY.RANGE_DATE
    ]
  },
  {
    menuCode: MENU_CODE.hotels,
    categories: [CATEGORY.HOTELS_DETAIL, CATEGORY.HOTELS_DESCRIPTOR],
    properties: [
      TRAY_FILTER_PROPERTY.COUNTRIES,
      TRAY_FILTER_PROPERTY.DESTINATIONS,
      TRAY_FILTER_PROPERTY.STATES,
      TRAY_FILTER_PROPERTY.RANGE_DATE
    ]
  },
  {
    menuCode: MENU_CODE.destinations,
    categories: [CATEGORY.DESTINATIONS_DETAIL, CATEGORY.DESTINATIONS],
    properties: [
      TRAY_FILTER_PROPERTY.USER_LIST,
      TRAY_FILTER_PROPERTY.STATES,
      TRAY_FILTER_PROPERTY.RANGE_DATE,
      TRAY_FILTER_PROPERTY.COUNTRIES
    ]
  },
  {
    menuCode: MENU_CODE.restaurants,
    categories: [CATEGORY.RESTAURANTS_DETAIL, CATEGORY.RESTAURANTS_DESCRIPTOR, CATEGORY.RESTAURANTS],
    properties: [
      TRAY_FILTER_PROPERTY.USER_LIST,
      TRAY_FILTER_PROPERTY.STATES,
      TRAY_FILTER_PROPERTY.RANGE_DATE
    ]
  },
  {
    menuCode: MENU_CODE.events,
    categories: [CATEGORY.EVENTS_DETAIL, CATEGORY.EVENTS],
    properties: [
      TRAY_FILTER_PROPERTY.USER_LIST,
      TRAY_FILTER_PROPERTY.STATES,
      TRAY_FILTER_PROPERTY.RANGE_DATE
    ]
  }
];
