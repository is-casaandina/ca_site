import { IMenu } from '@ca-admin/statemanagement/models/general.interface';

export const MENU_CODE = {
  home: 'P001',
  menu: 'P002',
  pages: 'P003',
  usersRoles: 'P004',
  pagesError: 'P005',
  subscribers: 'P006',
  promotions: 'P007',
  hotels: 'P008',
  destinations: 'P009',
  restaurants: 'P010',
  events: 'P012',
  multimedia: 'P013',
  configurations: 'P014',
  setFreePage: 'P015'
};

export const MENU: Array<IMenu> = [
  {
    code: MENU_CODE.home,
    name: 'Home',
    route: '/home',
    icon: 'la-home'
  },
  {
    code: MENU_CODE.menu,
    name: 'Menú',
    route: '/menu-tray',
    icon: 'la-list'
  },
  {
    code: MENU_CODE.pages,
    name: 'Páginas',
    route: '/page-tray',
    icon: 'la-file-text'
  },
  {
    code: MENU_CODE.usersRoles,
    name: 'Administrar usuarios y roles',
    route: '/users-roles',
    icon: 'la-laptop'
  },
  {
    code: MENU_CODE.pagesError,
    name: 'Páginas Error',
    route: '/page-error-tray',
    icon: 'la-file-code-o'
  },
  {
    code: MENU_CODE.subscribers,
    name: 'Suscriptores',
    route: '/subscribers-tray',
    icon: 'la-users'
  },
  {
    code: MENU_CODE.promotions,
    name: 'Promociones',
    route: '/promotions-tray',
    icon: 'la-tags'
  },
  {
    code: MENU_CODE.hotels,
    name: 'Hoteles',
    route: '/hotels-tray',
    icon: 'la-building'
  },
  {
    code: MENU_CODE.destinations,
    name: 'Destinos',
    route: '/destinations-tray',
    icon: 'la-map-marker'
  },
  {
    code: MENU_CODE.restaurants,
    name: 'Restaurantes',
    route: '/restaurants-tray',
    icon: 'la-cutlery'
  },
  {
    code: MENU_CODE.events,
    name: 'Eventos',
    route: '/events-tray',
    icon: 'la-glass'
  },
  {
    code: MENU_CODE.multimedia,
    name: 'Multimedia',
    route: '/multimedia',
    icon: 'la-image'
  }
];

export const MENU_HEAD: Array<IMenu> = [
  {
    code: 'S001',
    name: 'Perfil',
    route: '/profile',
    icon: 'la-user',
    header: true
  },
  {
    code: MENU_CODE.configurations,
    name: 'Configuración general',
    route: '/configurations',
    icon: 'la-cog',
    header: true
  },
  {
    code: 'S003',
    name: 'Cambiar contraseña',
    route: '/passwordchange',
    icon: 'la-sign-out',
    header: true
  }
];
