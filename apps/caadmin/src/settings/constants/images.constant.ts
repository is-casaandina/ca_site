import { ASSETS_PATH } from './general.constant';

export const IMAGES = {
  logoCa: {
    src: `${ASSETS_PATH.images}ca-brand.svg`,
    alt: 'Logo Casa Andina'
  },
  logoCaBlack: {
    src: `${ASSETS_PATH.images}ca-brand-black.png`,
    alt: 'Logo Casa Andina'
  },
  logoCaWhite: {
    src: `${ASSETS_PATH.images}ca-brand-white.png`,
    alt: 'Logo Casa Andina'
  }
};
