export const ASSETS_PATH = {
  images: 'assets/images/',
  videos: 'assets/videos/'
};

export const DATE_FORMAT = {
  ddMMyyyy: 'dd-MM-yyyy'
};

export const CONTENT_TYPE = {
  auth: 'application/x-www-form-urlencoded',
  json: 'application/json'
};

export const STORAGE_KEY = {
  userToken: 'USER_TOKEN',
  userProfile: 'USER_PROFILE',
  userPermissions: 'USER_PERMISSIONS',
  menu: 'MENU',
  page: 'PAGE',
  pageEditor: 'PAGE_EDITOR',
  configurations: 'CONFIGURATIONS'
};

export const TAB_USERS_ROLES = [
  {
    name: 'Usuarios',
    route: './users-list'
  },
  {
    name: 'Roles',
    route: './roles-list'
  }
];

export const CA = {
  supportPhone: '01 234 5467',
  supportEmail: 'soporte@ca.com.pe',
  urlSite: 'https://www.ca.com.pe/'
};

export const ACTION_TYPES = {
  edit: 'edit',
  cancel: 'cancel',
  delete: 'delete',
  save: 'save',
  collapsed: 'collapsed',
  expanded: 'expanded',
  acept: 'acept',
  search: 'search'
};

export const COLOR_STATES = {
  success: 'success',
  warning: 'warning',
  pause: 'pause',
  danger: 'danger',
  disabled: 'disabled'
};

export const PAGE_CODE_STATES = {
  published: 1,
  inEdition: 2
};

export const PAGE_STATES_DESCRIPTION = {
  PUBLISHED: 'Publicada',
  IN_EDITION: 'En edición',
  DRAFT: 'Borrador'
};

export const PAGE_STATES = [
  {
    code: PAGE_CODE_STATES.published,
    description: PAGE_STATES_DESCRIPTION.PUBLISHED,
    color: COLOR_STATES.success
  },
  {
    code: PAGE_CODE_STATES.inEdition,
    description: PAGE_STATES_DESCRIPTION.IN_EDITION,
    color: COLOR_STATES.warning
  }
];

export const TYPES_CODE = {
  all: 0,
  static: 1,
  dynamic: 2,
  common: 3
};

export const PAGE_TYPES = [
  {
    code: TYPES_CODE.static,
    description: 'Estática'
  },
  {
    code: TYPES_CODE.dynamic,
    description: 'Dinámica'
  },
  {
    code: TYPES_CODE.common,
    description: 'Común'
  }
];

export const BUTTON_ACTIONS_CODE = {
  see: 1,
  edit: 2,
  publish: 3,
  delete: 4,
  revise: 5
};

export enum LANGUAGE_TYPES_CODE {
  es = 'es',
  en = 'en',
  pt = 'pt'
}

export const LANGUAGE_TYPES = [
  {
    code: LANGUAGE_TYPES_CODE.es,
    description: 'Español'
  },
  {
    code: LANGUAGE_TYPES_CODE.en,
    description: 'Inglés'
  },
  {
    code: LANGUAGE_TYPES_CODE.pt,
    description: 'Portugués'
  }
];

export const VIEW_TYPES = {
  user: 'U',
  page: 'P',
  component: 'C'
};

export const DIMENTION_IMAGE = {
  small: {
    complete: 'small',
    abbreviation: 'sm'
  },
  medium: {
    complete: 'medium',
    abbreviation: 'md'
  },
  large: {
    complete: 'large',
    abbreviation: 'lg'
  }
};

export const TAB_CONFIGURATIONS = [
  {
    name: 'General',
    route: './general'
  },
  {
    name: 'Robots',
    route: './robots'
  },
  {
    name: 'Categorías',
    route: './categories'
  },
  {
    name: 'Monedas',
    route: './coins'
  },
  {
    name: 'Clusters',
    route: './clusters'
  },
  {
    name: 'Servicios',
    route: './services'
  },
  {
    name: 'Etiquetas',
    route: './tags'
  },
  {
    name: 'Beneficios Online',
    route: './benefits'
  },
  {
    name: 'Notificaciones',
    route: './notifications'
  },
  {
    name: 'Salas',
    route: './rooms'
  },
  {
    name: 'Navegación',
    route: './navigation'
  }
];

export const ASSISTANCE_TYPE = {
  Hotel: 1,
  Habitaciones: 2
};

export const CATEGORIES_DESCRIPTOR_HOTEL = 'dad19020-cf17-4435-9cbe-38ab9d34bf85';
export const CATEGORIES_DESCRIPTOR = [
  {
    name: 'Hoteles',
    code: 'dad19020-cf17-4435-9cbe-38ab9d34bf85'
  },
  {
    name: 'Restaurantes',
    code: '0f9e341c-11dc-48db-9efe-88ae7202c64b'
  }
];

export const TYPOGRAFY_LIST = [
  {
    name: 'La Plaza',
    value: 'g-main-banner-title--plaza'
  },
  {
    name: 'Sama',
    value: 'g-main-banner-title--sama'
  },
  {
    name: 'Alma',
    value: 'g-main-banner-title--alma'
  }
];

export enum CATEGORY {
  HOME = 'a5b68eb1-977f-4ab4-b269-8c679f631016',
  DESTINATIONS = '79c8ce33-a4a6-4fb9-a5c9-f21abdfd5292',
  DESTINATIONS_DETAIL = '52ceb00a-e56d-492e-8fbd-058b7d1d750f',
  HOTELS_DESCRIPTOR = '5ccd4af8-a323-4cb1-9475-3b9d74d4a325',
  HOTELS_DETAIL = 'dad19020-cf17-4435-9cbe-38ab9d34bf85',
  RESTAURANTS = '98c073a0-783f-4c1b-8ac0-09db98298127',
  RESTAURANTS_DESCRIPTOR = 'e47f499c-fa59-42ec-b637-b4a410086fe0',
  RESTAURANTS_DETAIL = '0f9e341c-11dc-48db-9efe-88ae7202c64b',
  PROMOTIONS = '2e6c4b19-51ad-455b-8bb6-8bec6c07a1ef',
  PROMOTIONS_DETAIL = '4e8d5b70-e4c4-4974-9a6d-1397060227c2',
  EVENTS = '7928cb6f-8661-440f-82f9-d3675c8d7596',
  EVENTS_DETAIL = '7c4fe9b0-e256-44a6-b8ce-e594ec6e72f2',
  ABOUT_US = 'ab0614a2-24df-44c5-94b9-c2caf61282fb',
  CONTACT = '1aa5327c-5190-46d4-8a48-ed3a2a746dd5',
  PAGE_ERROR = '69139d58-f29d-43d8-81fb-6f02bde932c1',
  FREE = 'f950e1ff-a05c-4c2c-becc-d082499e2590',
  BELL = '8d5f1bea-6989-4a69-8fe5-d4cf5c3a6109',
  LINK = '8280867e-aa09-4d67-8254-3fb182070c53'
}

export enum CONTACT_TYPE {
  MAIL_CONTACT = 1,
  MAIL_BOOKING = 2,
  PHONE = 3,
  ADDRESS = 4,
  OTHER = 5
}

export const CONTACT_TYPE_LABELS = [
  {
    value: CONTACT_TYPE.MAIL_CONTACT,
    text: 'Correo de contacto'
  },
  {
    value: CONTACT_TYPE.MAIL_BOOKING,
    text: 'Correo de reserva'
  },
  {
    value: CONTACT_TYPE.PHONE,
    text: 'Teléfono'
  },
  {
    value: CONTACT_TYPE.ADDRESS,
    text: 'Dirección'
  },
  {
    value: CONTACT_TYPE.OTHER,
    text: 'Otro'
  }
];

export const TYPE_ROBOT_ALLOW = 'Allow';
export const TYPE_ROBOT_DISALLOW = 'Disallow';
export const TYPE_ROBOT_SITEMAP = 'Sitemap';
export const TYPES_VALUES_ROBOTS = [
  {
    value: TYPE_ROBOT_ALLOW,
    text: 'Allowed'
  },
  {
    value: TYPE_ROBOT_DISALLOW,
    text: 'Disallowed'
  },
  {
    value: TYPE_ROBOT_SITEMAP,
    text: 'Sitemap'
  }
];

export const USER_AGENTS = [
  {
    value: '*',
    text: 'All'
  },
  {
    value: 'Googlebot',
    text: 'Googlebot'
  },
  {
    value: 'Googlebot-Image',
    text: 'Googlebot-Image'
  },
  {
    value: 'MSNBot',
    text: 'MSNBot'
  },
  {
    value: 'Slurp',
    text: 'Slurp'
  },
  {
    value: 'Lycos',
    text: 'Lycos'
  },
  {
    value: 'Seekbot',
    text: 'Seekbot'
  },
  {
    value: 'Sistrix',
    text: 'Sistrix'
  },
  {
    value: 'SearchmetricsBot',
    text: 'SearchmetricsBot'
  },
  {
    value: 'BacklinkCrawler',
    text: 'BacklinkCrawler'
  },
  {
    value: 'SEOkicks-Robot',
    text: 'SEOkicks-Robot'
  },
  {
    value: 'xovi',
    text: 'xovi'
  }
];

export const LOADING_TEXT = {
  es : {
    text: 'Cargando'
  },
  en : {
    text: 'Loading'
  },
  pt : {
    text: 'Carregando'
  }
};

export const REGEX = {
  // tslint:disable-next-line:max-line-length
  DOMAIN: /^(?![^\n]*\.$)(?:https?:\/\/)(?:(?:[2][1-4]\d|25[1-5]|1\d{2}|[1-9]\d|[1-9])(?:\.(?:[2][1-4]\d|25[1-5]|1\d{2}|[1-9]\d|[0-9])){3}(?::\d{4})?|[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+){2,})$/
};
