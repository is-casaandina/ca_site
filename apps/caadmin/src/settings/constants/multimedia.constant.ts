import { FILE_TYPE } from '@ca-admin/statemanagement/models/file.interface';
import { FILE_MIME_TYPE } from './file.constant';

export const MULTIMEDIA_TABS = [
  {
    route: '../image',
    name: 'Imágenes'
  },
  {
    route: '../video',
    name: 'Videos'
  },
  {
    route: '../document',
    name: 'Documentos'
  },
  {
    route: '../icon',
    name: 'Íconos'
  }
];

export const MULTIMEDIA_PROPERTIES = {
  image: {
    type: FILE_TYPE.image,
    mimeTypes: ['image/jpeg', 'image/png'],
    maxSize: 2
  },
  video: {
    type: FILE_TYPE.video,
    mimeTypes: [
      'video/mp4',
      'video/x-msvideo',
      'video/x-ms-wmv',
      'video/mpeg',
      'video/x-matroska',
      'video/ogg'
    ],
    maxSize: 20
  },
  document: {
    type: FILE_TYPE.document,
    mimeTypes: FILE_MIME_TYPE,
    maxSize: 20
  },
  icon: {
    type: FILE_TYPE.icon,
    mimeTypes: ['image/jpeg', 'image/png'],
    maxSize: 2
  }
};
