import { IComponent } from '@ca-admin/statemanagement/models/dynamic-form.interface';
import { CONTACT_TYPE_LABELS, LANGUAGE_TYPES_CODE } from './general.constant';

export const STRUCTURE_COMPONENT_BENEFITS: IComponent = {
  name: '',
  code: 'benefits',
  type: 2,
  properties: [
    {
      code: 'textEs',
      type: 'text',
      label: 'Texto link ES',
      placeholder: 'Ir a nuestro Blog',
      maxLength: 50,
      cols: 6
    },
    {
      code: 'linkEs',
      type: 'url',
      label: 'Página ES',
      defaultLanguage: LANGUAGE_TYPES_CODE.es,
      placeholder: 'Seleccionar Página',
      cols: 6
    },
    {
      code: 'textEn',
      type: 'text',
      label: 'Texto link EN',
      placeholder: 'Ir a nuestro Blog',
      maxLength: 50,
      cols: 6
    },
    {
      code: 'linkEn',
      type: 'url',
      defaultLanguage: LANGUAGE_TYPES_CODE.en,
      label: 'Página EN',
      placeholder: 'Seleccionar Página',
      cols: 6
    },
    {
      code: 'textPr',
      type: 'text',
      label: 'Texto link PR',
      placeholder: 'Ir a nuestro Blog',
      maxLength: 50,
      cols: 6
    },
    {
      code: 'linkPr',
      type: 'url',
      label: 'Página PT',
      defaultLanguage: LANGUAGE_TYPES_CODE.pt,
      placeholder: 'Seleccionar Página',
      cols: 6
    },
    {
      code: 'detail',
      type: 'array',
      typeArray: 'object',
      labelItem: 'Beneficio',
      size: 5,
      properties: [
        {
          code: 'icon',
          type: 'select-icon',
          label: 'Ícono',
          cols: 6
        },
        {
          code: 'titleEsp',
          type: 'text',
          label: 'Título ES',
          cols: 6,
          placeholder: '10% dcto. en nuestros restaurantes',
          maxLength: 80
        },
        {
          code: 'titleEng',
          type: 'text',
          label: 'Título EN',
          cols: 6,
          placeholder: '10% dct in our restaurants',
          maxLength: 80
        },
        {
          code: 'titlePor',
          type: 'text',
          label: 'Título PR',
          cols: 6,
          placeholder: '10% dct nos nossos restaurantes',
          maxLength: 80
        },
        {
          code: 'hide',
          type: 'checkbox',
          label: 'Ocultar',
          cols: 6
        }
      ]
    }
  ]
};

export const STRUCTURE_COMPONENT_ROOM: IComponent = {
  code: 'room',
  type: 2,
  properties: [
    {
      code: 'hotelId',
      label: 'Hotel',
      type: 'autocomplete',
      maxLength: 50,
      action: {
        request: 'api/pages/hotels/rooms'
      },
      required: false
    },
    {
      code: 'name',
      type: 'text',
      label: 'Sala',
      maxLength: 50
    },
    {
      code: 'order',
      type: 'number',
      label: 'Orden',
      placeholder: 'La visualizacion es de menor a mayor.',
      cols: 6
    },
    {
      code: 'category',
      type: 'select',
      label: 'Categoría',
      placeholder: 'Seleccionar',
      action: {
        request: 'api/parameters/select/living-room-category',
        event: 'load'
      },
      cols: 6
    },
    {
      type: 'ui-br'
    },
    {
      code: 'squareMeters',
      type: 'text',
      label: 'M2',
      placeholder: 'Área (m2)',
      maxLength: 5,
      cols: 6
    },
    {
      code: 'lengthy',
      type: 'text',
      label: 'Largo',
      placeholder: 'Largo (m)',
      maxLength: 5,
      cols: 6
    },
    {
      code: 'width',
      type: 'text',
      label: 'Ancho',
      placeholder: 'Ancho (m)',
      maxLength: 5,
      cols: 6
    },
    {
      code: 'high',
      type: 'text',
      label: 'Alto',
      placeholder: 'Alto (m)',
      maxLength: 5,
      cols: 6
    },
    {
      type: 'ui-br'
    },
    {
      code: 'teather',
      type: 'number',
      label: 'Teatro / Auditorio',
      placeholder: 'Aforo',
      maxLength: 3,
      cols: 6
    },
    {
      code: 'school',
      type: 'text',
      label: 'Escuela / Aula',
      placeholder: 'Aforo',
      maxLength: 3,
      cols: 6
    },
    {
      code: 'banquet',
      type: 'text',
      label: 'Cena / Banquete',
      placeholder: 'Aforo',
      maxLength: 3,
      cols: 6
    },
    {
      code: 'directory',
      type: 'text',
      label: 'Directorio / \'U\'',
      placeholder: 'Aforo',
      maxLength: 3,
      cols: 6
    },
    {
      code: 'cocktail',
      type: 'text',
      label: 'Cocktail',
      placeholder: 'Aforo',
      maxLength: 3,
      cols: 6
    },
    {
      type: 'ui-br'
    },
    {
      code: 'sliders',
      description: 'Slider',
      type: 'array',
      typeArray: 'object',
      design: 'tabs',
      max: 4,
      labelItem: 'Slide',
      properties: [
        {
          code: 'titleEs',
          type: 'text',
          label: 'Título Español',
          placeholder: 'Ingrese título en español',
          maxLength: 50
        },
        {
          code: 'titleEn',
          type: 'text',
          label: 'Título Inglés',
          placeholder: 'Ingrese título en inglés',
          maxLength: 50
        },
        {
          code: 'titlePt',
          type: 'text',
          label: 'Título Portugués',
          placeholder: 'Ingrese título de la portugués',
          maxLength: 50
        },
        {
          code: 'images',
          label: 'Foto de sala',
          type: 'image',
          mimeTypes: [
            'image/jpeg',
            'image/png'
          ],
          height: 450,
          width: 1026,
          weight: 2097152,
          required: false
        }
      ]
    }
  ]
};

export const STRUCTURE_COMPONENT_EDIT_CONTACT = {
  type: 2,
  code: 'contact',
  name: '',
  properties: [
    {
      code: 'details',
      status: 2,
      type: 'array',
      typeArray: 'object',
      design: 'row',
      labelItem: 'Detalle',
      properties: [
        {
          code: 'icon',
          label: 'Icono',
          type: 'select-icon',
          cols: 6
        },
        {
          code: 'type',
          type: 'select',
          label: 'Tipo',
          list: CONTACT_TYPE_LABELS,
          cols: 6
        },
        {
          code: 'label',
          label: 'Título',
          type: 'text',
          maxLength: 30
        },
        {
          code: 'description',
          label: 'Descripción',
          type: 'textarea',
          maxLength: 200
        }
      ]
    }
  ]
};
