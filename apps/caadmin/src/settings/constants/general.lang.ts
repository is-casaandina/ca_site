export const RATE_TEXT = {
  es: {
    valuation: 'Valoración'
  },
  en: {
    valuation: 'Valuation'
  },
  pt: {
    valuation: 'Avaliação'
  }
};

export const RESERVE_ACTIONS = {
  es: {
    from: 'Desde',
    reserve: 'Reservar',
    reserveNow: 'Reservar Ahora'
  },
  en: {
    from: 'From',
    reserve: 'Book',
    reserveNow: 'Book now'
  },
  pt: {
    from: 'A partir',
    reserve: 'Reservar',
    reserveNow: 'Reservar Agora'
  }
};

export const CURRENCY_COPYS = {
  es: {
    usd: 'Dólar estadounidense (US$)',
    pen: 'Sol peruano (PEN)',
    brl: 'Real brasileño (BRL)'
  },
  en: {
    usd: 'US Dollar ($)',
    pen: 'Peruvian Sol (PEN)',
    brl: 'Brazilian Real (R$)'
  },
  pt: {
    usd: 'Dólar americano (US$)',
    pen: 'Sol peruano (PEN)',
    brl: 'Real brasileiro (R$)'
  }
};

export const LANGUAGES_COPYS = {
  es: {
    spanish: 'Español',
    english: 'Inglés',
    portuguese: 'Portugués'
  },
  en: {
    spanish: 'Spanish',
    english: 'English',
    portuguese: 'Portuguese'
  },
  pt: {
    spanish: 'Espanhol',
    english: 'Inglês',
    portuguese: 'Português'
  }
};

export const RESPONSE_MESSAGE = {
  es: {
    success: 'Gracias por escribirnos. Nos pondremos en contacto contigo',
    error: 'Ocurrio un error'
  },
  en: {
    success: 'Thanks for writing to us. We will get in touch with you',
    error: 'An error occurred'
  },
  pt: {
    success: 'Obrigado por escrever. Entraremos em contato com você',
    error: 'Ocorreu um erro'
  }
};

export const GENERAL_TEXT = {
  es: {
    location: 'Ubicación',
    transfers: 'Traslados',
    viewHotel: 'Ver hotel',
    ourServices: 'Nuestros servicios',
    goTo: 'Ir a',
    termConditions: 'Términos y condiciones',
    contactForm: 'Formulario de contacto',
    promotions: 'Promociones',
    destinations: 'Destinos',
    hotel: 'Hotel',
    clean: 'Limpiar',
    apply: 'Aplicar',
    viewMore: 'Ver más',
    upTo: 'Hasta',
    off: 'Dcto.',
    select: 'Seleccione'
  },
  en: {
    location: 'Location',
    transfers: 'Transfers',
    viewHotel: 'View Hotel',
    ourServices: 'Our services',
    goTo: 'Go to',
    termConditions: 'Terms and Conditions',
    contactForm: 'Contact Form',
    promotions: 'Promotions',
    destinations: 'Destinations',
    hotel: 'Hotel',
    clean: 'Clean',
    apply: 'Apply',
    viewMore: 'See more',
    upTo: 'Up to',
    off: 'Off',
    select: 'Select'
  },
  pt: {
    location: 'Localização',
    transfers: 'Transferências',
    viewHotel: 'Ver hotel',
    ourServices: 'Nossos serviços',
    goTo: 'Vá para',
    termConditions: 'Termos e Condições',
    contactForm: 'Formulário de contato',
    promotions: 'Promoções',
    destinations: 'Destinos',
    hotel: 'Hotel',
    clean: 'Limpar',
    apply: 'Aplicar',
    viewMore: 'Ver mais',
    upTo: 'Até',
    off: 'Desc.',
    select: 'Selecionar'
  }
};
