import { EditorContactComponent } from '@ca-admin/components/modals/editor-contact/editor-contact.component';
import { CATEGORY } from './general.constant';

export enum EDITOR_PAGE_PROPERTY {
  CATEGORY = 'categoryId',
  COUNTRY = 'countryId',
  COUNTRY_PROMOTION = 'countryPromotionId',
  DESTINATION = 'destinationId',
  DESCRIPTOR = 'descriptorId',
  ROIBACK = 'roiback',
  REVINATE = 'revinate',
  BEGIN_DATE = 'beginDate',
  FIRST_EXPIRATION = 'firstExpiration',
  SECOND_EXPIRATION = 'secondExpiration',
  SHORT_NAME = 'shortName',
  CLUSTER = 'clusterId',
  DOMAIN = 'domainId',
  COUNTDOWN = 'countdown',
  GENERIC_TYPE = 'genericType',
  DESTINATION_MAP = 'destinationMapId'
}

export enum EDITOR_PAGE_MODAL {
  CONTACT = 'contact'
}

export enum EDITOR_PAGE_PROPERTY_TYPE {
  SELECT = 'select',
  TEXT = 'text',
  DATETIME = 'datetime',
  CHECKBOX = 'checkbox'
}

export const EDITOR_PAGE_MODAL_DEFINITION = [
  {
    modal: EDITOR_PAGE_MODAL.CONTACT,
    title: 'Datos de contacto',
    icon: 'la-edit',
    component: EditorContactComponent
  }
];

export const EDITOR_PAGE_PROPERTY_DEFINITION = [
  {
    property: EDITOR_PAGE_PROPERTY.GENERIC_TYPE,
    label: 'Tipo',
    endpoint: 'api/generic/types/promotions',
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT,
    dependence: EDITOR_PAGE_PROPERTY.CATEGORY,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.COUNTDOWN,
    label: 'Mostrar contador',
    type: EDITOR_PAGE_PROPERTY_TYPE.CHECKBOX
  },
  {
    property: EDITOR_PAGE_PROPERTY.CLUSTER,
    label: 'Cluster',
    placeholder: 'Todos',
    endpoint: 'api/clusters/select',
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT
  },
  {
    property: EDITOR_PAGE_PROPERTY.COUNTRY,
    label: 'País',
    endpoint: 'api/countries/select',
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.COUNTRY_PROMOTION,
    label: 'País',
    endpoint: `api/clusters/{${EDITOR_PAGE_PROPERTY.CLUSTER}}/countries`,
    placeholder: 'Todos',
    dependence: EDITOR_PAGE_PROPERTY.CLUSTER,
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT
  },
  {
    property: EDITOR_PAGE_PROPERTY.DESTINATION,
    label: 'Destino',
    endpoint: 'api/pages/destinations',
    dependence: EDITOR_PAGE_PROPERTY.COUNTRY,
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.DOMAIN,
    label: 'Dominio',
    endpoint: 'api/domains/select',
    placeholder: 'Todos',
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT
  },
  {
    property: EDITOR_PAGE_PROPERTY.DESCRIPTOR,
    label: 'Descriptor',
    endpoint: `api/descriptors/categories/{${EDITOR_PAGE_PROPERTY.CATEGORY}}`,
    dependence: EDITOR_PAGE_PROPERTY.CATEGORY,
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.ROIBACK,
    label: 'Roiback',
    placeholder: 'Ej.: miraf',
    type: EDITOR_PAGE_PROPERTY_TYPE.TEXT,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.REVINATE,
    label: 'Revinate',
    placeholder: 'Ej.: 150249',
    type: EDITOR_PAGE_PROPERTY_TYPE.TEXT,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.BEGIN_DATE,
    label: 'Fecha inicio',
    placeholder: 'dd/MM/yyyy HH:mm',
    type: EDITOR_PAGE_PROPERTY_TYPE.DATETIME,
    required: true
  },
  {
    property: EDITOR_PAGE_PROPERTY.FIRST_EXPIRATION,
    label: 'Primera expiración',
    dependence: EDITOR_PAGE_PROPERTY.BEGIN_DATE,
    placeholder: 'dd/MM/yyyy HH:mm',
    type: EDITOR_PAGE_PROPERTY_TYPE.DATETIME
  },
  {
    property: EDITOR_PAGE_PROPERTY.SECOND_EXPIRATION,
    label: 'Segunda expiración',
    dependence: EDITOR_PAGE_PROPERTY.FIRST_EXPIRATION,
    placeholder: 'dd/MM/yyyy HH:mm',
    type: EDITOR_PAGE_PROPERTY_TYPE.DATETIME
  },
  {
    property: EDITOR_PAGE_PROPERTY.SHORT_NAME,
    label: 'Nombre Corto',
    type: EDITOR_PAGE_PROPERTY_TYPE.TEXT
  },
  {
    property: EDITOR_PAGE_PROPERTY.DESTINATION_MAP,
    label: 'Destino en el mapa',
    endpoint: `api/destination/maps`,
    dependence: EDITOR_PAGE_PROPERTY.COUNTRY,
    type: EDITOR_PAGE_PROPERTY_TYPE.SELECT
  }
];

export const EDITOR_FORM = [
  {
    category: CATEGORY.HOTELS_DESCRIPTOR,
    properties: [
      EDITOR_PAGE_PROPERTY.DESCRIPTOR
    ]
  },
  {
    category: CATEGORY.RESTAURANTS_DESCRIPTOR,
    properties: [
      EDITOR_PAGE_PROPERTY.DESCRIPTOR
    ]
  },
  {
    category: CATEGORY.DESTINATIONS_DETAIL,
    properties: [
      EDITOR_PAGE_PROPERTY.COUNTRY,
      EDITOR_PAGE_PROPERTY.ROIBACK,
      EDITOR_PAGE_PROPERTY.SHORT_NAME,
      EDITOR_PAGE_PROPERTY.DESTINATION_MAP
    ],
    reloaders: [
      EDITOR_PAGE_PROPERTY.ROIBACK,
      EDITOR_PAGE_PROPERTY.SHORT_NAME
    ]
  },
  {
    category: CATEGORY.HOTELS_DETAIL,
    properties: [
      EDITOR_PAGE_PROPERTY.COUNTRY,
      EDITOR_PAGE_PROPERTY.DESTINATION,
      EDITOR_PAGE_PROPERTY.DESCRIPTOR,
      EDITOR_PAGE_PROPERTY.ROIBACK,
      EDITOR_PAGE_PROPERTY.REVINATE,
      EDITOR_PAGE_PROPERTY.SHORT_NAME
    ],
    reloaders: [
      EDITOR_PAGE_PROPERTY.DESTINATION,
      EDITOR_PAGE_PROPERTY.DESCRIPTOR,
      EDITOR_PAGE_PROPERTY.ROIBACK,
      EDITOR_PAGE_PROPERTY.REVINATE
    ],
    modals: [
      EDITOR_PAGE_MODAL.CONTACT
    ]
  },
  {
    category: CATEGORY.RESTAURANTS_DETAIL,
    properties: [
      EDITOR_PAGE_PROPERTY.COUNTRY,
      EDITOR_PAGE_PROPERTY.DESTINATION,
      EDITOR_PAGE_PROPERTY.DESCRIPTOR,
      EDITOR_PAGE_PROPERTY.SHORT_NAME
    ],
    reloaders: [
      EDITOR_PAGE_PROPERTY.DESTINATION,
      EDITOR_PAGE_PROPERTY.DESCRIPTOR
    ],
    modals: [
      EDITOR_PAGE_MODAL.CONTACT
    ]
  },
  {
    category: CATEGORY.EVENTS_DETAIL,
    properties: [
      EDITOR_PAGE_PROPERTY.COUNTRY
    ]
  },
  {
    category: CATEGORY.PROMOTIONS_DETAIL,
    properties: [
      EDITOR_PAGE_PROPERTY.CLUSTER,
      EDITOR_PAGE_PROPERTY.COUNTRY_PROMOTION,
      EDITOR_PAGE_PROPERTY.GENERIC_TYPE,
      EDITOR_PAGE_PROPERTY.BEGIN_DATE,
      EDITOR_PAGE_PROPERTY.FIRST_EXPIRATION,
      EDITOR_PAGE_PROPERTY.SECOND_EXPIRATION,
      EDITOR_PAGE_PROPERTY.DOMAIN,
      EDITOR_PAGE_PROPERTY.COUNTDOWN
    ],
    reloaders: [
      EDITOR_PAGE_PROPERTY.BEGIN_DATE,
      EDITOR_PAGE_PROPERTY.FIRST_EXPIRATION,
      EDITOR_PAGE_PROPERTY.SECOND_EXPIRATION,
      EDITOR_PAGE_PROPERTY.COUNTDOWN
    ],
    changeKey: [
      {
        from: EDITOR_PAGE_PROPERTY.COUNTRY_PROMOTION,
        to: EDITOR_PAGE_PROPERTY.COUNTRY
      }
    ]
  }
];
