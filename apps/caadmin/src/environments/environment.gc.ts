export const environment = {
  production: false,
  API_URL: 'http://192.168.1.140:8093/',
  BUCKET_WC: 'https://s3.amazonaws.com/multimediadev.casa-andina.com/web-components/modules/',
  BUCKET_ICON_JSON: 'http://s3.amazonaws.com/multimediadev.casa-andina.com/icons/selection.json'
};
