export const environment = {
  production: true,
  API_URL: '//cms.casa-andina.com/',
  BUCKET_WC: 'https://s3.amazonaws.com/multimediaqa.casa-andina.com/system/web-components/modules/',
  BUCKET_JS: 'https://s3.amazonaws.com/multimediaqa.casa-andina.com/',
  BUCKET_ICON_JSON: 'http://s3.amazonaws.com/multimediaqa.casa-andina.com/icons/selection.json'

  //API_URL: 'http://ec2-34-194-5-143.compute-1.amazonaws.com/',
  //BUCKET_WC: 'https://s3.amazonaws.com/multimediadev.casa-andina.com/system/web-components/modules/',
  //BUCKET_JS: 'https://s3.amazonaws.com/multimediadev.casa-andina.com/',
  //BUCKET_ICON_JSON: 'http://s3.amazonaws.com/multimediadev.casa-andina.com/icons/selection.json'
};
