import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginLayoutModule, MainLayoutModule } from '@ca-admin/layouts';
import { EditorLayoutModule } from '@ca-admin/layouts/editor-layout/editor-layout.module';
import { RoutesModule } from '@ca-admin/routes/routes.module';
import { ApiModule } from '@ca-core/shared/helpers/api';
import { AuthenticationModule } from '@ca-core/shared/helpers/authentication';
import { I18nEsPeModule } from '@ca-core/shared/helpers/i18n-es-pe';
import { SpinnerModule } from '@ca-core/shared/helpers/spinner';
import { SWAL } from '@ca-core/shared/helpers/swal';
import { SPINNER_CONFIG } from '@ca-core/shared/settings/constants/general.constant';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ApiModule,
    LoginLayoutModule,
    MainLayoutModule,
    RoutesModule,
    SweetAlert2Module.forRoot(SWAL.defaultOptions),
    AuthenticationModule,
    SpinnerModule.forRoot(SPINNER_CONFIG),
    EditorLayoutModule,
    I18nEsPeModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
