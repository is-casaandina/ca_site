import { Component, OnInit } from '@angular/core';
import { GeneralService } from '@ca-admin/services';
import { UnsubscribeOnDestroy } from '@ca-core/shared/helpers/util';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'caadmin-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends UnsubscribeOnDestroy implements OnInit {
  title = 'caadmin';

  constructor(
    private generalService: GeneralService
  ) {
    super();
  }

  ngOnInit(): void {
    this.generalService.getConfiguration()
      .pipe(takeUntil(this.unsubscribeDestroy$))
      .subscribe(() => { });
  }
}
