import { environment } from '@ca-admin/environments/environment';

export class CountryEndpoint {
  public static countries = `${environment.API_URL}api/countries`;
  public static countriesActive = `${environment.API_URL}api/countries/select`;
}
