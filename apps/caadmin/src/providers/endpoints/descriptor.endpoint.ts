import { environment } from '@ca-admin/environments/environment';

export class DescriptorEndpoint {
  public static descriptors = `${environment.API_URL}api/descriptors`;
  public static saveDescriptor = `${environment.API_URL}api/descriptors`;
  public static updateDescriptor = `${environment.API_URL}api/descriptors/{id}`;
  public static deleteDescriptor = `${environment.API_URL}api/descriptors/{id}`;
}
