export { FileEndpoint } from './file.endpoint';
export { GeneralEndpoint } from './general.endpoint';
export { PageEndpoint } from './page.endpoint';
export { PermissionEndpoint } from './permission.endpoint';
export { RoleEndpoint } from './role.endpoint';
export { UserEndpoint } from './user.endpoint';
export { AuthEndpoint } from './auth.endpoint';
