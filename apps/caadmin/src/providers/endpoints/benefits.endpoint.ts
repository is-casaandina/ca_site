import { environment } from '@ca-admin/environments/environment';

export class BenefitsEndpoint {
  public static benefits = `${environment.API_URL}api/benefits`;
  public static saveBenefits = `${environment.API_URL}api/benefits`;
}
