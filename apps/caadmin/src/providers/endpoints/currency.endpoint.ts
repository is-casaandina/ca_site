import { environment } from '@ca-admin/environments/environment';

export class CurrencyEndpoint {
  public static currencies = `${environment.API_URL}api/currencies`;
  public static saveCurrency = `${environment.API_URL}api/currencies`;
  public static updateCurrencies = `${environment.API_URL}api/currencies`;
}
