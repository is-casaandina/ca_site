import { environment } from '@ca-admin/environments/environment';

export class DestinationEndpoint {
  public static destinationByCountry = `${environment.API_URL}api/pages/tray/destinations/countries`;
}
