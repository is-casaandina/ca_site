import { environment } from '@ca-admin/environments/environment';

export class RoomEndpoint {
  public static rooms = `${environment.API_URL}api/living-rooms`;
  public static roomsPaginated = `${environment.API_URL}api/living-rooms/paginated`;
  public static saveRoom = `${environment.API_URL}api/living-rooms/`;
  public static updateRoom = `${environment.API_URL}api/living-rooms/{id}`;
  public static deleteRoom = `${environment.API_URL}api/living-rooms/{id}`;
}
