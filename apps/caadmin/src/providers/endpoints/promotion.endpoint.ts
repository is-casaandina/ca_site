import { environment } from '@ca-admin/environments/environment';

export class PromotionEndpoint {
  public static promotionType = `${environment.API_URL}api/generic/types/promotions`;
}
