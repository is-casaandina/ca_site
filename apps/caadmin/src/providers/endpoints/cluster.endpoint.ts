import { environment } from '@ca-admin/environments/environment';

export class ClusterEndpoint {
  public static clusters = `${environment.API_URL}api/clusters`;
  public static saveCluster = `${environment.API_URL}api/clusters/`;
  public static updateCluster = `${environment.API_URL}api/clusters/{id}`;
  public static deleteCluster = `${environment.API_URL}api/clusters/{id}`;
}
