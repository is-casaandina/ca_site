import { environment } from '@ca-admin/environments/environment';

export class UrlEndpoint {
  public static urls = `${environment.API_URL}api/pages/url`;
}
