import { environment } from '@ca-admin/environments/environment';

export class PageEndpoint {
  public static siteMenu = `${environment.API_URL}admin/page/listMenuOptions`;
  public static pageList = `${environment.API_URL}api/pages/tray/list`;
  public static approvalsList = `${environment.API_URL}trays/approbations/list`;
  public static savePage = `${environment.API_URL}api/pages`;
  public static publishPage = `${environment.API_URL}api/pages/publish/{idPage}`;
  public static unPublishPage = `${environment.API_URL}api/pages/unPublish/{idPage}`;
  public static editPage = `${environment.API_URL}api/pages/{idPage}`;
  public static getPageView = `${environment.API_URL}api/pages/view/{idPage}/{state}`;
  public static deletePage = `${environment.API_URL}api/pages/{idPage}`;
  public static pageEditHeader = `${environment.API_URL}api/pages/header`;
  public static pageHomeSelect = `${environment.API_URL}api/pages/home/select`;
  public static pagePageSelectByCategoryId = `${environment.API_URL}api/pages/select/{categoryId}`;
  public static pagePromotionAllSelect = `${environment.API_URL}api/pages/promotions/all/select`;
  public static viewExternalPage = `${environment.API_URL}site/pages/view/external`;
  public static setFreePage = `${environment.API_URL}api/pages/setFree/{id}`;
  public static saveConstructorTamplate = `${environment.API_URL}api/constructortemplate/`;

}
