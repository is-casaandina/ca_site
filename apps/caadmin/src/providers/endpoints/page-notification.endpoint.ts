import { environment } from '@ca-admin/environments/environment';

export class PageNotificationEndpoint {
  public static rooms = `${environment.API_URL}api/page/notification`;
  public static saveRoom = `${environment.API_URL}api/page/notification/`;
  public static updateRoom = `${environment.API_URL}api/page/notification/{id}`;
  public static deleteRoom = `${environment.API_URL}api/page/notification/{id}`;
}
