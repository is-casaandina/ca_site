import { environment } from '@ca-admin/environments/environment';

export class ErrorEndpoint {
  public static listError = `${environment.API_URL}api/pages/error`;
  public static saveError = `${environment.API_URL}api/pages/error`;
  public static updateError = `${environment.API_URL}api/pages/error/{id}`;
  public static deleteError = `${environment.API_URL}api/pages/error/{id}`;
  public static selectError = `${environment.API_URL}api/pages/error/select`;
}
