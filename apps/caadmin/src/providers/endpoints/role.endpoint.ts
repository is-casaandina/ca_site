import { environment } from '@ca-admin/environments/environment';

export class RoleEndpoint {
  public static rolesList = `${environment.API_URL}api/roles`;
  public static rolesListPermissionsDetail = `${environment.API_URL}api/roles/detail`;
  public static rolesListPermissionsDetailPaginated = `${environment.API_URL}api/roles/detail/paginated`;
  public static saveRole = `${environment.API_URL}api/roles/`;
  public static updateRole = `${environment.API_URL}api/roles/{roleCode}`;
  public static deleteRole = `${environment.API_URL}api/roles/{roleCode}`;
}
