import { environment } from '@ca-admin/environments/environment';

export class DomainEndpoint {
  public static selectDomain = `${environment.API_URL}api/domains/select`;
  public static listDomain = `${environment.API_URL}api/domains`;
  public static saveDomain = `${environment.API_URL}api/domains`;
  public static updatetDomain = `${environment.API_URL}api/domains/{id}`;
  public static deletetDomain = `${environment.API_URL}api/domains/{id}`;
}
