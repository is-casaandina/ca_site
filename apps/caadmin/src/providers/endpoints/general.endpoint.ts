import { environment } from '@ca-admin/environments/environment';

export class GeneralEndpoint {
  public static livingRoomCategories = `${environment.API_URL}api/parameters/filters/living-room-category`;
  public static pageStates = `${environment.API_URL}api/parameters/filters/page-states`;
  public static typeSubscriptor = `${environment.API_URL}api/parameters/filters/type-subscriber`;
  public static configuration = `${environment.API_URL}api/configurations`;
  public static saveSiteIdentity = `${environment.API_URL}api/configurations/identity`;
  public static saveScript = `${environment.API_URL}api/configurations/script`;
  public static saveRoiback = `${environment.API_URL}api/configurations/roiback`;
  public static saveRoibackPrice = `${environment.API_URL}api/configurations/roiback/prices`;
  public static saveIconsPay = `${environment.API_URL}api/configurations/icons-pay`;
  public static saveSocialNetworking = `${environment.API_URL}api/configurations/social-networking`;
  public static robots = `${environment.API_URL}api/configurations/robots`;
  public static addRobots = `${environment.API_URL}api/configurations/robots`;
  public static deleteRobots = `${environment.API_URL}api/configurations/robots/{id}`;
  public static generateRobots = `${environment.API_URL}api/configurations/robots-generate`;
  public static generateSitemap = `${environment.API_URL}api/configurations/sitemap-generate`;
  public static saveDomain = `${environment.API_URL}api/configurations/domain`;
  public static saveHome = `${environment.API_URL}api/configurations/home/{id}`;
}
