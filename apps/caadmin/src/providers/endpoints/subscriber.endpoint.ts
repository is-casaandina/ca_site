import { environment } from '@ca-admin/environments/environment';

export class SubscriberEndpoint {
  public static list = `${environment.API_URL}api/subscriber/list`;
  public static delete = `${environment.API_URL}api/subscriber/{id}`;
  public static download = `${environment.API_URL}api/subscriber/downloads`;
}
