import { environment } from '@ca-admin/environments/environment';

export class AssistanceEndpoint {
  public static assistancePaginated = `${environment.API_URL}api/services/paginated/{typeAssistance}`;
  public static saveUser = `${environment.API_URL}api/services`;
  public static updateUser = `${environment.API_URL}api/services/{id}`;
  public static deleteUser = `${environment.API_URL}api/services/{id}`;
}
