import { environment } from '@ca-admin/environments/environment';

export class UserEndpoint {
  public static usersPaginate = `${environment.API_URL}api/users/paginated`;
  public static usersList = `${environment.API_URL}api/users/list`;
  public static saveUser = `${environment.API_URL}api/users`;
  public static updateUser = `${environment.API_URL}api/users/{userId}`;
  public static deleteUser = `${environment.API_URL}api/users/{userId}`;
  public static caUsersList = `${environment.API_URL}api/users/ad`;
  public static userPermissions = `${environment.API_URL}api/users/permissions`;
  public static userProfile = `${environment.API_URL}api/users/profile`;
  public static updateUserProfile = `${environment.API_URL}api/users/profile`;
  public static updatePasswordChange = `${environment.API_URL}api/users/password-change`;
}
