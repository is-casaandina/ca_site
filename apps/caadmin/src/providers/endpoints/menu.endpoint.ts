import { environment } from '@ca-admin/environments/environment';

export class MenusEndpoint {
  public static getMenu = `${environment.API_URL}api/menus`;
  public static deleteMenu = `${environment.API_URL}api/menus/{id}`;
  public static addMenu = `${environment.API_URL}api/menus`;
  public static updateMenu = `${environment.API_URL}api/menus/{id}`;
}
