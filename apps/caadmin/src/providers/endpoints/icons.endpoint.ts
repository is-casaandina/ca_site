import { environment } from '@ca-admin/environments/environment';

export class IconEndpoint {
  public static icons = `${environment.API_URL}api/icons`;
  public static iconsLoad = `${environment.API_URL}api/icons`;
}
