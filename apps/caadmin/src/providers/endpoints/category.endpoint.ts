import { environment } from '@ca-admin/environments/environment';

export class CategoryEndpoint {
  public static list = `${environment.API_URL}api/categories`;
  public static saveCategory = `${environment.API_URL}api/categories`;
  public static deleteCategory = `${environment.API_URL}api/categories/{id}`;
}
