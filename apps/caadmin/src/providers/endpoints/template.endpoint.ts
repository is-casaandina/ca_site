import { environment } from '@ca-admin/environments/environment';

export class TemplateEndpoint {
  public static list = `${environment.API_URL}api/templates`;
  public static byCode = `${environment.API_URL}api/templates/{code}`;
}
