import { environment } from '@ca-admin/environments/environment';

export class ComponentEndpoint {
  public static commonComponentsList = `${environment.API_URL}trays/component/list`;
  public static list = `${environment.API_URL}api/components`;
}
