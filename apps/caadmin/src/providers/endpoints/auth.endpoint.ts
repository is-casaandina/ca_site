import { environment } from '@ca-admin/environments/environment';

export class AuthEndpoint {
  public static login = `${environment.API_URL}api/authenticate`;
  public static recovery = `${environment.API_URL}api/users/send-mail`;
  public static reset = `${environment.API_URL}api/users/update-password-token`;
}
