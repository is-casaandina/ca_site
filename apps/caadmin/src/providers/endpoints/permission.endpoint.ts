import { environment } from '@ca-admin/environments/environment';

export class PermissionEndpoint {
  public static permissionsList = `${environment.API_URL}api/permissions`;
}
