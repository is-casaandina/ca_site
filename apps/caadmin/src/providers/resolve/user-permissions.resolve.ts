import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ConfigurationService } from '@ca-admin/services/configuration.service';
import { IUserPermissionsResponse } from '@ca-admin/statemanagement/models/auth.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionsResolve implements Resolve<any> {

  constructor(
    private configurationService: ConfigurationService
  ) { }

  resolve(): Observable<IUserPermissionsResponse> {
    return this.configurationService.userPermissions(true);
  }
}
