import { Injectable } from '@angular/core';
import { DomainEndpoint } from '@ca-admin/endpoints/domain.endpoint';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservable, IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { IDomain } from '@ca-core/shared/statemanagement/models/domain.interface';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  private domainList: Array<IPageStateResponse>;

  constructor(
    private apiService: ApiService
  ) { }

  getDomains(preloader?: boolean): IObservableArray<IDomain> {
    return this.apiService.get(DomainEndpoint.listDomain, { preloader });
  }

  updateDomain(id: string, body: IDomain, preloader?: boolean): ICaObservable<IDomain> {
    return this.apiService.put(DomainEndpoint.updatetDomain, body, { preloader, params: { id } });
  }

  deleteDomain(id: string, preloader?: boolean): ICaObservable<IDomain> {
    return this.apiService.del(DomainEndpoint.deletetDomain, { preloader, params: { id } });
  }

  saveDomain(body: IDomain, preloader?: boolean): ICaObservable<IDomain> {
    return this.apiService.post(DomainEndpoint.saveDomain, body, { preloader });
  }

  getDomainsSelect(showSpin: boolean = false): IObservableArray<IPageStateResponse> {
    if (this.domainList && this.domainList.length) { return of(this.domainList); }

    return this.apiService.get<IObservableArray<IPageStateResponse>>(DomainEndpoint.selectDomain, { preloader: showSpin })
      .pipe(
        map(res => {
          this.domainList = res;

          return res;
        }));
  }

}
