import { Injectable } from '@angular/core';
import { DestinationEndpoint } from '@ca-admin/endpoints/destination.endpoint';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DestinationService {

  constructor(
    private apiService: ApiService
  ) {
  }

  byCountries(categories: Array<string>, countries: Array<string>): Observable<Array<IPageStateResponse>> {
    return this.apiService.get(DestinationEndpoint.destinationByCountry, { params: { categories, countries } });
  }

}
