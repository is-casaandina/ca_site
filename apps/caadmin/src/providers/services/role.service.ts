import { Injectable } from '@angular/core';
import { RoleEndpoint } from '@ca-admin/endpoints';
import {
  IDeleteRoleRequest,
  IRolePaginationRequest,
  IRolePaginationResponse,
  IRolePermissionsDetailResponse,
  IRolePermissionsDetailView,
  IRoleResponse,
  ISaveRoleRequest,
  IUpdateRoleRequest
} from '@ca-admin/statemanagement/models/role.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  roleBehaviorSubject: BehaviorSubject<IRolePermissionsDetailView>;

  constructor(
    private _apiService: ApiService
  ) {
    this.roleBehaviorSubject = new BehaviorSubject<IRolePermissionsDetailView>(null);
  }

  rolesList(showSpin: boolean = false): Observable<Array<IRoleResponse>> {
    return this._apiService.get(RoleEndpoint.rolesList, { preloader: showSpin });
  }

  rolesListPermissionsDetail(showSpin: boolean = false): Observable<Array<IRolePermissionsDetailResponse>> {
    return this._apiService.get(RoleEndpoint.rolesListPermissionsDetail, { preloader: showSpin });
  }

  rolesPagination(params: IRolePaginationRequest, showSpin: boolean = false): Observable<IRolePaginationResponse> {
    return this._apiService.get(RoleEndpoint.rolesListPermissionsDetailPaginated, { preloader: showSpin, params });
  }

  updateRole(roleCode: string, body: IUpdateRoleRequest, showSpin: boolean = false): Observable<any> {
    return this._apiService.put(RoleEndpoint.updateRole, body, { preloader: showSpin, params: { roleCode } });
  }

  deleteRole(params: IDeleteRoleRequest, showSpin: boolean = false): Observable<any> {
    return this._apiService.del(RoleEndpoint.deleteRole, { preloader: showSpin, params });
  }

  setRolesList(actionType: string, role: IRolePermissionsDetailView): void {
    role.actionType = actionType;
    this.roleBehaviorSubject.next(role);
  }

  setActions(actionType: string): void {
    const role = {} as IRolePermissionsDetailView;
    role.actionType = actionType;
    this.roleBehaviorSubject.next(role);
  }

  updateRolesList(): BehaviorSubject<IRolePermissionsDetailView> {
    return this.roleBehaviorSubject;
  }

  saveRole(params: ISaveRoleRequest, showSpin: boolean = false): Observable<any> {
    return this._apiService.post(RoleEndpoint.saveRole, params, { preloader: showSpin });
  }

}
