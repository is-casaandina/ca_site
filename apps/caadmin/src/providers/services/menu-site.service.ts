import { Injectable } from '@angular/core';
import { MenusEndpoint } from '@ca-admin/endpoints/menu.endpoint';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservable, IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { IMenuSite } from '@ca-core/shared/statemanagement/models/menu-site.interface';

@Injectable({
  providedIn: 'root'
})
export class MenuSiteService {

  constructor(
    private apiService: ApiService
  ) { }

  getMenus(language: string, showSping = true): IObservableArray<IMenuSite> {
    return this.apiService.get(MenusEndpoint.getMenu, { params: { language }, preloader: showSping });
  }

  deleteMenu(id: string, showSping = true): ICaObservable<IMenuSite> {
    return this.apiService.del(MenusEndpoint.deleteMenu, { params: { id }, preloader: showSping });
  }

  addMenu(menu: IMenuSite, showSping = true): ICaObservable<IMenuSite> {
    return this.apiService.post(MenusEndpoint.addMenu, menu, { preloader: showSping });
  }

  updateMenu(menu: IMenuSite, showSping = true): ICaObservable<IMenuSite> {
    return this.apiService.put(MenusEndpoint.updateMenu, menu, { preloader: showSping, params: { id: menu.id } });
  }

}
