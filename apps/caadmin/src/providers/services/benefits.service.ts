import { Injectable } from '@angular/core';
import { BenefitsEndpoint } from '@ca-admin/endpoints/benefits.endpoint';
import { IBenefitsRequest, IBenefitsResponse } from '@ca-admin/statemanagement/models/benefits.interface';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BenefitsService {

  constructor(
    private apiService: ApiService
  ) { }

  getBenefits(showSpin = true): Observable<IBenefitsResponse> {
    return this.apiService.get(BenefitsEndpoint.benefits, { preloader: showSpin });
  }

  saveBenefits(benefits: IBenefitsRequest, showSpin = true): Observable<IGeneralResponse<IBenefitsResponse>> {
    return this.apiService.post(BenefitsEndpoint.saveBenefits, benefits, { preloader: showSpin });
  }

}
