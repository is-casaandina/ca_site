import { Injectable } from '@angular/core';
import { GeneralEndpoint } from '@ca-admin/endpoints';
import {
  IConfigurationResponse,
  IRobotsRequest,
  IRobotsResponse,
  IRoiback,
  IRoibackPrice,
  IScript,
  ISiteIdentityRequest,
  ISocialNetworking
} from '@ca-admin/statemanagement/models/configuration.interface';
import {
  IMessageResponse,
  IPageStateResponse,
  ITypeSubscriberResponse
} from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservable, IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { StorageService } from '@ca-core/shared/helpers/util';
import { STORAGE_KEY } from '@ca-core/shared/settings/constants/general.constant';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  private configurationSource: IConfigurationResponse;

  constructor(
    private apiService: ApiService,
    private storageService: StorageService
  ) { }

  livingRoomCategories(showSpin: boolean = false): IObservableArray<IPageStateResponse> {
    return this.apiService.get(GeneralEndpoint.livingRoomCategories, { preloader: showSpin });
  }

  pageStates(showSpin: boolean = false): IObservableArray<IPageStateResponse> {
    return this.apiService.get(GeneralEndpoint.pageStates, { preloader: showSpin });
  }

  getTypeSubscriptor(showSpin: boolean = false): IObservableArray<ITypeSubscriberResponse> {
    return this.apiService.get(GeneralEndpoint.typeSubscriptor, { preloader: showSpin });
  }

  getConfiguration(showSpin = true): Observable<IConfigurationResponse> {

    if (this.configurationSource) {
      return of(this.configurationSource);
    }

    return this.apiService.get<Observable<IConfigurationResponse>>(GeneralEndpoint.configuration, { preloader: showSpin })
      .pipe(map(response => {
        this.configurationSource = response || {} as IConfigurationResponse;
        this.setFavicon(response.fontIcons);

        return this.configurationSource;
      }));
  }

  saveSiteIdentity(siteIdentity: ISiteIdentityRequest, showSpin = true): ICaObservable<IConfigurationResponse> {
    if (this.configurationSource) {
      this.configurationSource.metadata.description = siteIdentity.description;
      this.configurationSource.metadata.title = siteIdentity.sitename;
    }

    return this.apiService.post(GeneralEndpoint.saveSiteIdentity, siteIdentity, { preloader: showSpin });
  }

  saveScript(script: IScript, showSpin = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.post(GeneralEndpoint.saveScript, script, { preloader: showSpin });
  }

  saveRoiback(roiback: IRoiback, showSpin = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.post(GeneralEndpoint.saveRoiback, roiback, { preloader: showSpin });
  }

  saveRoibackPrice(roibackPrice: IRoibackPrice, showSpin = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.post(GeneralEndpoint.saveRoibackPrice, roibackPrice, { preloader: showSpin });
  }

  saveIconsPay(iconsPay: Array<String>, showSpin = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.post(GeneralEndpoint.saveIconsPay, iconsPay, { preloader: showSpin });
  }

  saveSocialNetworking(socialNetworking: ISocialNetworking, showSpin = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.post(GeneralEndpoint.saveSocialNetworking, socialNetworking, { preloader: showSpin });
  }

  getRobots(showSpin = true): IObservableArray<IRobotsResponse> {
    return this.apiService.get(GeneralEndpoint.robots, { preloader: showSpin });
  }

  addRobots(robots: IRobotsRequest, showSpin = true): Observable<IMessageResponse> {
    return this.apiService.post(GeneralEndpoint.addRobots, robots, { preloader: showSpin });
  }

  deleteRobots(robotsId: string, showSpin = true): Observable<IMessageResponse> {
    return this.apiService.del(GeneralEndpoint.deleteRobots, { preloader: showSpin, params: { id: robotsId } });
  }

  generateRobots(showSpin: boolean = true): ICaObservable<any> {
    return this.apiService.post(GeneralEndpoint.generateRobots, null, { preloader: showSpin });
  }

  generateSitemap(acceptExternalPages: boolean = false, showSpin: boolean = true): ICaObservable<any> {
    return this.apiService.post(GeneralEndpoint.generateSitemap, { acceptExternalPages }, { preloader: showSpin });
  }

  setConfigurationStorage(configurations: IConfigurationResponse): void {
    this.storageService.setItem(STORAGE_KEY.configurations, configurations);
  }

  getConfigurationStorage(): IConfigurationResponse {
    return this.storageService.getItemObject(STORAGE_KEY.configurations);
  }

  saveDomain(path: string, showSpin: boolean = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.put(GeneralEndpoint.saveDomain, { path }, { preloader: showSpin });
  }

  saveHome(id: string, showSpin: boolean = true): ICaObservable<IConfigurationResponse> {
    return this.apiService.put(GeneralEndpoint.saveHome, {}, { preloader: showSpin, params: { id } });
  }

  private setFavicon(href: string): void {
    const linkElement = document.createElement('link');
    linkElement.setAttribute('rel', 'stylesheet');
    linkElement.setAttribute('type', 'text/css');
    linkElement.setAttribute('href', href);
    document.head.appendChild(linkElement);
  }

}
