import { Injectable } from '@angular/core';
import { ClusterEndpoint } from '@ca-admin/endpoints/cluster.endpoint';
import { IClusterRequest, IClusterResponse } from '@ca-admin/statemanagement/models/cluster.interface';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClusterService {

  constructor(
    private apiService: ApiService
  ) { }

  getClusters(showSpin = true): Observable<Array<IClusterResponse>> {
    return this.apiService.get(ClusterEndpoint.clusters, { preloader: showSpin });
  }

  saveCluster(cluster: IClusterRequest, showSping = true): Observable<IGeneralResponse<IClusterResponse>> {
    return this.apiService.post(ClusterEndpoint.saveCluster, cluster, { preloader: showSping });
  }

  updateCluster(clusterId: string, cluster: IClusterRequest, showSping = true): Observable<IGeneralResponse<IClusterResponse>> {
    return this.apiService.put(ClusterEndpoint.updateCluster, cluster, { params: { id: clusterId }, preloader: showSping });
  }

  deleteCluster(clusterId: string, showSping = true): Observable<any> {
    return this.apiService.del(ClusterEndpoint.deleteCluster, { params: { id: clusterId }, preloader: showSping });
  }

}
