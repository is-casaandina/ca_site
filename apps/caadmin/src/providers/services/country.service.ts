import { Injectable } from '@angular/core';
import { CountryEndpoint } from '@ca-admin/endpoints/country.endpoint';
import { ICountryResponse } from '@ca-admin/statemanagement/models/country.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(
    private apiService: ApiService
  ) {
  }

  countries(showSpin: boolean = false): Observable<Array<ICountryResponse>> {
    return this.apiService.get(CountryEndpoint.countries, { preloader: showSpin });
  }

  countriesActive(showSpin: boolean = false): Observable<Array<ICountryResponse>> {
    return this.apiService.get(CountryEndpoint.countriesActive, { preloader: showSpin });
  }

}
