import { Injectable } from '@angular/core';
import { CurrencyEndpoint } from '@ca-admin/endpoints/currency.endpoint';
import { ICurrencyRequest, ICurrencyResponse } from '@ca-admin/statemanagement/models/currency.interface';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  constructor(
    private apiService: ApiService
  ) { }

  getCurrencies(showSpin = true): Observable<Array<ICurrencyResponse>> {
    return this.apiService.get(CurrencyEndpoint.currencies, { preloader: showSpin });
  }

  saveCurrency(currency: ICurrencyRequest, showSpin = true): Observable<IGeneralResponse<ICurrencyResponse>> {
    return this.apiService.post(CurrencyEndpoint.saveCurrency, currency, { preloader: showSpin });
  }

  saveCurrencies(currencies: Array<ICurrencyRequest>, showSpin = true): Observable<IGeneralResponse<Array<ICurrencyResponse>>> {
    return this.apiService.put(CurrencyEndpoint.updateCurrencies, currencies, { preloader: showSpin });
  }

}
