import { Injectable } from '@angular/core';
import { UserEndpoint } from '@ca-admin/endpoints';
import { IGeneralResponse, IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import {
  ICaUserResponse,
  IDeleteUserRequest,
  IPasswordChangeRequest,
  IProfileRequest,
  IProfileResponse,
  ISaveUserRequest,
  IUserPaginationRequest,
  IUserPaginationResponse,
  IUserResponse,
  IUserView
} from '@ca-admin/statemanagement/models/user.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userBehaviorSubject: BehaviorSubject<IUserView>;

  constructor(
    private apiService: ApiService
  ) {
    this.userBehaviorSubject = new BehaviorSubject<IUserView>(null);
  }

  usersPagination(params: IUserPaginationRequest, showSpin: boolean = false): Observable<IUserPaginationResponse> {
    return this.apiService.get(UserEndpoint.usersPaginate, { preloader: showSpin, params });
  }

  updateUser(userId: string, body: ISaveUserRequest, showSpin: boolean = false): Observable<any> {
    return this.apiService.put(UserEndpoint.updateUser, body, { preloader: showSpin, params: { userId } });
  }

  deleteUser(params: IDeleteUserRequest, showSpin: boolean = false): Observable<any> {
    return this.apiService.del(UserEndpoint.deleteUser, { params,  preloader: showSpin });
  }

  setUsersList(actionType: string, user: IUserView): void {
    user.actionType = actionType;
    this.userBehaviorSubject.next(user);
  }

  setActions(actionType: string): void {
    const user = {} as IUserView;
    user.actionType = actionType;
    this.userBehaviorSubject.next(user);
  }

  updateUsersList(): BehaviorSubject<IUserView> {
    return this.userBehaviorSubject;
  }

  caUsersList(showSpin: boolean = false): Observable<Array<ICaUserResponse>> {
    return this.apiService.get(UserEndpoint.caUsersList, { preloader: showSpin });
  }

  saveUser(params: ISaveUserRequest, showSpin: boolean = false): Observable<any> {
    return this.apiService.post(UserEndpoint.saveUser, params, { preloader: showSpin });
  }

  usersList(showSpin: boolean = false): Observable<Array<IUserResponse>> {
    return this.apiService.get(UserEndpoint.usersList, { preloader: showSpin });
  }

  getProfile(showSpin = true): Observable<IProfileResponse> {
    return this.apiService.get(UserEndpoint.userProfile, { preloader: showSpin });
  }

  updateProfile(params: IProfileRequest, showSpin: boolean = false): Observable<IGeneralResponse<IProfileResponse>> {
    return this.apiService.put(UserEndpoint.updateUserProfile, params, { preloader: showSpin });
  }

  updatePasswordChange(params: IPasswordChangeRequest, showSpin: boolean = false): Observable<IMessageResponse> {
    return this.apiService.put(UserEndpoint.updatePasswordChange, params, { preloader: showSpin });
  }

}
