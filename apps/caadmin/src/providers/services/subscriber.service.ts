import { Injectable } from '@angular/core';
import { SubscriberEndpoint } from '@ca-admin/endpoints/subscriber.endpoint';
import { IGeneralResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ISubscriberPaginationResponse, ISubscriberRequest } from '@ca-admin/statemanagement/models/subcriber.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubscriberService {

  constructor(
    private apiService: ApiService
  ) { }

  subscribers(params: ISubscriberRequest, showSpin: boolean = false): Observable<ISubscriberPaginationResponse> {
    return this.apiService.get(SubscriberEndpoint.list, { preloader: showSpin, params });
  }

  download(params: ISubscriberRequest, showSpin: boolean = false): Observable<any> {
    return this.apiService.get(SubscriberEndpoint.download, { preloader: showSpin, params });
  }

  delete(subscriberId: string, showSpin: boolean = false): Observable<IGeneralResponse<any>> {
    return this.apiService.del(SubscriberEndpoint.delete, { params: { id: subscriberId },  preloader: showSpin });
  }

}
