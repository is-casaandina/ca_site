import { EventEmitter, Injectable } from '@angular/core';
import { PageEndpoint } from '@ca-admin/endpoints';
import {
  CATEGORY,
  LANGUAGE_TYPES,
  PAGE_CODE_STATES,
  STORAGE_KEY,
  TYPES_CODE
} from '@ca-admin/settings/constants/general.constant';
import { PAGE_TYPES_CODE } from '@ca-admin/settings/constants/page.constant';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import {
  IPageApprovalPaginationRequest,
  IPageHeadeRequest,
  IPageItemResponse,
  IPagePaginationRequest,
  IPagePaginationResponse,
  IPageResponse,
  ISavePageRequest,
  ISiteMenuResponse
} from '@ca-admin/statemanagement/models/page.interface';
import { ITemplateResponse } from '@ca-admin/statemanagement/models/template.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservable, IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { StorageService } from '@ca-core/shared/helpers/util';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  private reloadPublication: EventEmitter<any> = new EventEmitter<any>();
  private homeList: Array<IPageStateResponse>;
  private promotionAllList: Array<IPageStateResponse>;

  constructor(private apiService: ApiService, private storageService: StorageService) {}

  siteMenu(showSpin: boolean = false): Observable<Array<ISiteMenuResponse>> {
    return this.apiService.get(PageEndpoint.siteMenu, { preloader: showSpin });
  }

  approvalsList(
    params: IPageApprovalPaginationRequest,
    showSpin: boolean = false
  ): Observable<IPagePaginationResponse> {
    return this.apiService.post(PageEndpoint.approvalsList, params, { preloader: showSpin });
  }

  pagesList(params: IPagePaginationRequest, showSpin: boolean = false): Observable<IPagePaginationResponse> {
    return this.apiService.get(PageEndpoint.pageList, { preloader: showSpin, params });
  }

  editPage(idPage: string, showSpin: boolean = false): Observable<any> {
    const params = { idPage };

    return this.apiService.get(PageEndpoint.editPage, { params, preloader: showSpin });
  }

  getPageView(idPage: string, state: number, showSpin: boolean = false): Observable<any> {
    const params = { idPage, state };

    return this.apiService.get(PageEndpoint.getPageView, { params, preloader: showSpin });
  }

  pageById(idPage: string, pageState: number, showSpin: boolean = false): Observable<IPageResponse> {
    const pageEditor = this.getPageEditor();

    return pageEditor && pageEditor.id === idPage && pageEditor.state === pageState
      ? of(pageEditor)
      : this.getPageView(idPage, pageState, showSpin);
  }

  publishPage(idPage: string, showSpin: boolean = false): ICaObservable<IPageResponse> {
    const params = { idPage };

    return this.apiService.post(PageEndpoint.publishPage, {}, { preloader: showSpin, params });
  }

  unPublishPage(idPage: string, showSpin: boolean = false): ICaObservable<IPageResponse> {
    const params = { idPage };

    return this.apiService.post(PageEndpoint.unPublishPage, {}, { preloader: showSpin, params });
  }

  savePage(params: ISavePageRequest, showSpin: boolean = false): ICaObservable<IPageResponse> {
    return this.apiService.post(PageEndpoint.savePage, params, { preloader: showSpin });
  }

  createPage(template: ITemplateResponse, showSpin: boolean = false): ICaObservable<IPageResponse> {
    const params = {
      name: null,
      pageType: PAGE_TYPES_CODE.publication,
      type: TYPES_CODE.dynamic,
      state: PAGE_CODE_STATES.inEdition,
      categoryId: template.categoryId,
      inEdition: true,
      languages: []
    } as ISavePageRequest;

    LANGUAGE_TYPES.forEach(type => {
      params.languages.push({
        language: type.code,
        metadata: {},
        content: {
          html: template.html,
          refComponents: template.refComponents
        }
      });
    });

    return this.savePage(params, showSpin);
  }

  getPageStorage(): IPageResponse {
    return this.storageService.getItemObject(STORAGE_KEY.page);
  }

  setPageStorage(page: IPageResponse): void {
    this.storageService.setItem(STORAGE_KEY.page, page);
  }

  getPageEditor(): IPageResponse {
    return this.storageService.getItemObject(STORAGE_KEY.pageEditor);
  }

  setPageEditor(pageEditor: IPageResponse): void {
    this.storageService.setItem(STORAGE_KEY.pageEditor, pageEditor);
  }

  deletePage(idPage: string): ICaObservable<boolean> {
    return this.apiService.del<ICaObservable<boolean>>(PageEndpoint.deletePage, {
      params: { idPage },
      preloader: true
    });
  }

  viewExternalPage(id: string, language: string, domain: string): void {
    window.open(`${PageEndpoint.viewExternalPage}?id=${id}&language=${language}&domain=${domain}`);
  }

  pageHeaderEdit(body: IPageHeadeRequest, showSpin?: boolean): ICaObservable<any> {
    return this.apiService.put(PageEndpoint.pageEditHeader, body, { preloader: showSpin });
  }

  reloadPublicationEdit(): EventEmitter<any> {
    return this.reloadPublication;
  }

  getHomeSelect(showSpin: boolean = false): IObservableArray<IPageStateResponse> {
    if (this.homeList && this.homeList.length) {
      return of(this.homeList);
    }

    return this.apiService
      .get<IObservableArray<IPageStateResponse>>(PageEndpoint.pageHomeSelect, { preloader: showSpin })
      .pipe(
        map(res => {
          this.homeList = res;

          return res;
        })
      );
  }

  getPromotionAllSelect(showSpin: boolean = false): IObservableArray<IPageStateResponse> {
    if (this.promotionAllList && this.promotionAllList.length) {
      return of(this.promotionAllList);
    }

    return this.apiService
      .get<IObservableArray<IPageStateResponse>>(PageEndpoint.pagePromotionAllSelect, { preloader: showSpin })
      .pipe(
        map(res => {
          this.promotionAllList = res;

          return res;
        })
      );
  }

  getPageSelectByCategoryId(categoryId: CATEGORY, showSpin: boolean = false): IObservableArray<IPageStateResponse> {
    return this.apiService.get<IObservableArray<IPageStateResponse>>(PageEndpoint.pagePageSelectByCategoryId, {
      preloader: showSpin,
      params: { categoryId }
    });
  }

  setFreePage(id: string, showSpin: boolean = false): ICaObservable<IPageItemResponse> {
    return this.apiService.put<ICaObservable<IPageItemResponse>>(
      PageEndpoint.setFreePage,
      {},
      {
        preloader: showSpin,
        params: { id }
      }
    );
  }
}
