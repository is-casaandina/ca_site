import { Injectable } from '@angular/core';
import { CategoryEndpoint } from '@ca-admin/endpoints/category.endpoint';
import { ICategoryRequest, ICategoryResponse } from '@ca-admin/statemanagement/models/category.interface';
import { IGeneralResponse, IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private apiService: ApiService
  ) { }

  categoryList(showSpin: boolean = false): Observable<Array<ICategoryResponse>> {
    return this.apiService.get(CategoryEndpoint.list, { preloader: showSpin });
  }

  saveCategory(category: ICategoryRequest, showSpin = true): Observable<IGeneralResponse<ICategoryResponse>> {
    return this.apiService.post(CategoryEndpoint.saveCategory, category, { preloader: showSpin });
  }

  deleteCategory(categoryId: string, showSpin = true): Observable<IMessageResponse> {
    return this.apiService.del(CategoryEndpoint.deleteCategory, { params: { id: categoryId }, preloader: showSpin });
  }

}
