import { Injectable } from '@angular/core';
import { AssistanceEndpoint } from '@ca-admin/endpoints/assistance.endpoint';
import {
  IAssistancePaginatedResponse,
  IAssistanceRequest,
  IAssistanceResponse,
  IAssistanceSearchRequest
} from '@ca-admin/statemanagement/models/assistance.interface';
import { IGeneralResponse, IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AssistanceService {

  constructor(
    private apiService: ApiService
  ) {
  }

  assistancePaginated(params: IAssistanceSearchRequest, showSpin: boolean = false): Observable<IAssistancePaginatedResponse> {
    return this.apiService.get(AssistanceEndpoint.assistancePaginated, { preloader: showSpin, params });
  }

  saveAssistance(assistance: IAssistanceRequest, showSpin: boolean = false): Observable<IGeneralResponse<IAssistanceResponse>> {
    return this.apiService.post(AssistanceEndpoint.saveUser, assistance, { preloader: showSpin });
  }

  updateAssistance(assistanceId: string, assistance: IAssistanceRequest, showSpin: boolean = false):
    Observable<IGeneralResponse<IAssistanceResponse>> {
    return this.apiService.put(AssistanceEndpoint.updateUser, assistance, { preloader: showSpin, params: { id: assistanceId } });
  }

  deleteAsistance(assistanceId: string, showSpin: boolean = false): Observable<IMessageResponse> {
    return this.apiService.del(AssistanceEndpoint.deleteUser, { params: { id: assistanceId }, preloader: showSpin });
  }

}
