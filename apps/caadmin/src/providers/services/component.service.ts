import { Injectable } from '@angular/core';
import { ComponentEndpoint } from '@ca-admin/endpoints/component.endpoint';
import {
  IComponentPaginationRequest,
  IComponentPaginationResponse,
  IComponentResponse
} from '@ca-admin/statemanagement/models/component.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  private componentsEditor: Array<IComponentResponse>;
  file:any;
  name:any;

  constructor(
    private apiService: ApiService
  ) { }

  commonComponentsPagination(params: IComponentPaginationRequest, showSpin: boolean = false): Observable<IComponentPaginationResponse> {
    return this.apiService.post(ComponentEndpoint.commonComponentsList, params, { preloader: showSpin });
  }

  list(showSpin: boolean = false): Observable<Array<IComponentResponse>> {
    if (this.componentsEditor && this.componentsEditor.length) { return of(this.componentsEditor); }

    return this.apiService.get(ComponentEndpoint.list, { preloader: showSpin });
  }

  /** INFO: Mantener en sesión los componentes para el editor */
  setComponentsEditor(components: Array<IComponentResponse>): void {
    this.componentsEditor = components || [];
  }

  getComponentsEditor(): Array<IComponentResponse> {
    return this.componentsEditor || [];
  }

}
