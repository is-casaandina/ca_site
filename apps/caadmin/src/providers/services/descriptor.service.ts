import { Injectable } from '@angular/core';
import { DescriptorEndpoint } from '@ca-admin/endpoints/descriptor.endpoint';
import { IDescriptorRequest, IDescriptorResponse } from '@ca-admin/statemanagement/models/descriptor.interface';
import { IGeneralResponse, IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DescriptorService {

  constructor(
    private apiService: ApiService
  ) {
  }

  descriptros(showSpin: boolean = false): Observable<Array<IDescriptorResponse>> {
    return this.apiService.get(DescriptorEndpoint.descriptors, { preloader: showSpin });
  }

  saveDescriptor(descriptor: IDescriptorRequest, showSpin: boolean = false): Observable<IGeneralResponse<IDescriptorResponse>> {
    return this.apiService.post(DescriptorEndpoint.saveDescriptor, descriptor, { preloader: showSpin });
  }

  updateDescriptor(descriptorId: string, descriptor: IDescriptorRequest, showSpin: boolean = false):
    Observable<IGeneralResponse<IDescriptorResponse>> {
    return this.apiService.put(DescriptorEndpoint.updateDescriptor, descriptor, { preloader: showSpin, params: { id: descriptorId } });
  }

  deleteDescriptor(descriptorId: string, showSpin: boolean = false): Observable<IMessageResponse> {
    return this.apiService.del(DescriptorEndpoint.deleteDescriptor, { params: { id: descriptorId }, preloader: showSpin });
  }

}
