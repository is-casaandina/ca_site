import { Injectable } from '@angular/core';
import { PageNotificationEndpoint } from '@ca-admin/endpoints/page-notification.endpoint';
import { IGeneralResponse, IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import {
  IPagedPageNotificationResponse,
  IPageNotificationPageableRequest,
  IPageNotificationRequest,
  IPageNotificationResponse
} from '@ca-admin/statemanagement/models/notification.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageNotificationService {

  constructor(
    private apiService: ApiService
  ) { }

  getAlerts(params: IPageNotificationPageableRequest, showSpin = true): Observable<IPagedPageNotificationResponse> {
    return this.apiService.get(PageNotificationEndpoint.rooms, { preloader: showSpin, params });
  }

  saveAlert(pageNotification: IPageNotificationRequest, showSping = true): Observable<IGeneralResponse<IPageNotificationResponse>> {
    return this.apiService.post(PageNotificationEndpoint.saveRoom, pageNotification, { preloader: showSping });
  }

  updateAlert(pageNotificationId: number, pageNotification: IPageNotificationRequest, showSping = true):
    Observable<IGeneralResponse<IPageNotificationResponse>> {
    return this.apiService.put(PageNotificationEndpoint.updateRoom, pageNotification,
      { params: { id: pageNotificationId }, preloader: showSping });
  }

  deleteAlert(pageNotificationId: number, showSping = true): Observable<IMessageResponse> {
    return this.apiService.del(PageNotificationEndpoint.deleteRoom, { params: { id: pageNotificationId }, preloader: showSping });
  }

}
