import { Injectable } from '@angular/core';
import { STORAGE_KEY } from '@ca-admin/settings/constants/general.constant';
import { MENU, MENU_HEAD } from '@ca-admin/settings/constants/menu.constant';
import { IMenu } from '@ca-admin/statemanagement/models/general.interface';
import { LocalStorageService } from '@ca-core/shared/helpers/util/storage-manager';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menu: Array<IMenu>;

  constructor(
    private configurationService: ConfigurationService,
    private storageService: LocalStorageService
  ) { }

  generateMenu(): Array<IMenu> {
    let menu = this.getMenu();
    if (!menu || !menu.length) {
      const userPermissions = this.configurationService.getUserPermissions();
      menu = [...MENU, ...MENU_HEAD].filter((fv, fk) => {
        return userPermissions.permissionList.find((sv, sk) => {
          return fv.code === sv.permissionCode;
        });
      });
      this.setMenu(menu);
    }

    return menu;
  }

  getMenu(): Array<IMenu> {
    return this.storageService.getItemObject(STORAGE_KEY.menu);
  }

  setMenu(menu: Array<IMenu>): void {
    this.storageService.setItem(STORAGE_KEY.menu, menu);
  }

}
