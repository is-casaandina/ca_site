import { Injectable } from '@angular/core';
import { TemplateEndpoint } from '@ca-admin/endpoints/template.endpoint';
import { ITemplateResponse } from '@ca-admin/statemanagement/models/template.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IObservableArray } from '@ca-core/shared/helpers/models/general.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor(
    private apiService: ApiService
  ) { }

  list(categories: Array<string>, showSpin: boolean = false): IObservableArray<ITemplateResponse> {
    return this.apiService.get(TemplateEndpoint.list, { preloader: showSpin, params: { categories } });
  }

  byCode(code: string, showSpin: boolean = false): Observable<ITemplateResponse> {
    const params = { code };

    return this.apiService.get(TemplateEndpoint.byCode, { params, preloader: showSpin });
  }

}
