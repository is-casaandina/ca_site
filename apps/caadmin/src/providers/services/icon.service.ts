import { Injectable } from '@angular/core';
import { IconEndpoint } from '@ca-admin/endpoints/icons.endpoint';
import { ApiService } from '@ca-core/shared/helpers/api';
import { IIcon } from '@ca-core/ui/lib/components/icon-picker/lib';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IconService {

  private icons: Array<IIcon> = [];

  constructor(
    private apiService: ApiService
  ) { }

  getIcons(): Observable<Array<IIcon>> {
    if (this.icons.length) { return of(this.icons); }

    return this.apiService.get<Observable<Array<IIcon>>>(IconEndpoint.icons)
      .pipe(map(preResponse => {
        this.icons = preResponse || [];

        return preResponse;
      }));
  }

  uploadDocuments(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('type', file.type);

    return this.apiService
      .postProgress(IconEndpoint.iconsLoad, formData, { reportProgress: true });
  }

}
