import { Injectable } from '@angular/core';
import { ErrorEndpoint } from '@ca-admin/endpoints/error.endpoint';
import { IError } from '@ca-admin/statemanagement/models/error.interface';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { ICaObservable, IObservableArray } from '@ca-core/shared/helpers/models/general.model';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private apiService: ApiService
  ) { }

  getErrors(preloader?: boolean): IObservableArray<IError> {
    return this.apiService.get(ErrorEndpoint.listError, { preloader });
  }

  updateError(id: string, boyd: IError, preloader?: boolean): ICaObservable<IError> {
    return this.apiService.put(ErrorEndpoint.updateError, boyd, { preloader, params: { id } });
  }

  deleteError(id: string, preloader?: boolean): ICaObservable<IError> {
    return this.apiService.del(ErrorEndpoint.deleteError, { preloader, params: { id } });
  }

  saveErrors(body: IError, preloader?: boolean): ICaObservable<IError> {
    return this.apiService.post(ErrorEndpoint.saveError, body, { preloader });
  }

  getErrorSelect(preloader?: boolean): IObservableArray<IPageStateResponse> {
    return this.apiService.get(ErrorEndpoint.selectError, { preloader });
  }

}
