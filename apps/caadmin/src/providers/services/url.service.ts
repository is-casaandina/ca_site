import { Injectable } from '@angular/core';
import { UrlEndpoint } from '@ca-admin/endpoints/url.endpoint';
import { IUrlAutocompleteRequest, IUrlAutocompleteView } from '@ca-admin/statemanagement/models/url.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  constructor(
    private _apiService: ApiService
  ) { }

  urlSearch(params: IUrlAutocompleteRequest, showSpin: boolean = false): Observable<Array<IUrlAutocompleteView>> {
    return this._apiService.get(UrlEndpoint.urls, { preloader: showSpin, params });
  }

}
