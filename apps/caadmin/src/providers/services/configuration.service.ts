import { Injectable } from '@angular/core';
import { UserEndpoint } from '@ca-admin/endpoints';
import { STORAGE_KEY } from '@ca-admin/settings/constants/general.constant';
import { IUserPermissionsResponse, IUserToken } from '@ca-admin/statemanagement/models/auth.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { LocalStorageService } from '@ca-core/shared/helpers/util/storage-manager';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(
    private apiService: ApiService,
    private storageService: LocalStorageService
  ) { }

  userPermissions(showSpin: boolean = false): Observable<IUserPermissionsResponse> {
    const userPermissions: IUserPermissionsResponse = this.getUserPermissions();

    return (userPermissions)
              ? of(userPermissions)
              : this.apiService.get(UserEndpoint.userPermissions, { preloader: showSpin });
  }

  getUserPermissions(): IUserPermissionsResponse {
    return this.storageService.getItemObject(STORAGE_KEY.userPermissions);
  }

  setUserPermissions(userPermissions: IUserPermissionsResponse): void {
    this.storageService.setItem(STORAGE_KEY.userPermissions, userPermissions);
  }

  getUserProfile(): IUserToken {
    return this.storageService.getItemObject(STORAGE_KEY.userProfile);
  }

  setUserProfile(userProfile: IUserToken): void {
    this.storageService.setItem(STORAGE_KEY.userProfile, userProfile);
  }

}
