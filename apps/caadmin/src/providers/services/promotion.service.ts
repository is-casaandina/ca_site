import { Injectable } from '@angular/core';
import { PromotionEndpoint } from '@ca-admin/endpoints/promotion.endpoint';
import { IPageStateResponse } from '@ca-admin/statemanagement/models/general.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(
    private apiService: ApiService
  ) {
  }

  types(language: string): Observable<Array<IPageStateResponse>> {
    return this.apiService.get(PromotionEndpoint.promotionType, { params: { language } });
  }

}
