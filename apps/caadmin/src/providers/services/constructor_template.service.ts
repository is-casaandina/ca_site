import { Injectable } from '@angular/core';
import { Observable, Observer, pipe, throwError } from 'rxjs';
import { PageEndpoint } from '@ca-admin/endpoints';
import { ApiService } from '@ca-core/shared/helpers/api';

import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ConstrutorPlantilla } from '@ca-admin/statemanagement/models/constructor_plantilla.interface';
import { StorageService } from '@ca-core/shared/helpers/util';

@Injectable({
    providedIn: 'root'
  })

  export class ConstructorTemplate {

    constructor(private apiService: ApiService, private storageService: StorageService){

    }

    saveConstructor(params : ConstrutorPlantilla, showSpin: boolean = false):Observable<any>{

        return this.apiService.post(PageEndpoint.saveConstructorTamplate , params, { preloader: showSpin })
        
    }
 
    

  }