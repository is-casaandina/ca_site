import { Injectable } from '@angular/core';
import { RoomEndpoint } from '@ca-admin/endpoints/room.endpoint';
import { IGeneralResponse, IMessageResponse } from '@ca-admin/statemanagement/models/general.interface';
import {
  ILivingRoomRequest,
  ILivingRoomResponse,
  IPagedLivingRoomResponse,
  IRoomPageableRequest
} from '@ca-admin/statemanagement/models/room.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(
    private apiService: ApiService
  ) { }

  getRooms(showSpin = true): Observable<Array<ILivingRoomResponse>> {
    return this.apiService.get(RoomEndpoint.rooms, { preloader: showSpin });
  }

  roomsPaginated(params: IRoomPageableRequest, showSpin = true): Observable<IPagedLivingRoomResponse> {
    return this.apiService.get(RoomEndpoint.roomsPaginated, { preloader: showSpin, params });
  }

  saveRoom(room: ILivingRoomRequest, showSping = true): Observable<IGeneralResponse<ILivingRoomResponse>> {
    return this.apiService.post(RoomEndpoint.saveRoom, room, { preloader: showSping });
  }

  updateRoom(roomId: string, room: ILivingRoomRequest, showSping = true): Observable<IGeneralResponse<ILivingRoomResponse>> {
    return this.apiService.put(RoomEndpoint.updateRoom, room, { params: { id: roomId }, preloader: showSping });
  }

  deleteRoom(roomId: string, showSping = true): Observable<IMessageResponse> {
    return this.apiService.del(RoomEndpoint.deleteRoom, { params: { id: roomId }, preloader: showSping });
  }

}
