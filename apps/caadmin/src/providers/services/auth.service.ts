import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthEndpoint } from '@ca-admin/endpoints';
import { STORAGE_KEY } from '@ca-admin/settings/constants/general.constant';
import { IAuthRequest, IAuthResponse, IUserToken } from '@ca-admin/statemanagement/models/auth.interface';
import { IRecoveryRequest } from '@ca-admin/statemanagement/models/recovery.interface';
import { IResetPasswordRequest } from '@ca-admin/statemanagement/models/reset-password.interface';
import { ApiService } from '@ca-core/shared/helpers/api';
import { StorageService } from '@ca-core/shared/helpers/util';
import { LocalStorageService } from '@ca-core/shared/helpers/util/storage-manager';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private localStorageService: LocalStorageService,
    private configurationService: ConfigurationService,
    private router: Router
  ) { }

  login(params: IAuthRequest, showSpin: boolean = false): Observable<boolean> {
    return this.apiService.post<Observable<any>>(AuthEndpoint.login, params, { preloader: showSpin })
      .pipe(
        map((response: IAuthResponse) => {
          const userToken = {
            username: params.username,
            rememberMe: params.rememberMe,
            id_token: `${response.tokenType} ${response.accessToken}`
          } as IUserToken;
          this.localStorageService.setItem(STORAGE_KEY.userToken, userToken);
          this.configurationService.setUserProfile(userToken);

          return true;
        })
      );
  }

  logout(): void {
    this.storageService.clear();
    this.localStorageService.clear();
    this.router.navigate(['/login']);
  }

  sendMail(params: IRecoveryRequest, showSpin: boolean = false): Observable<any> {
    return this.apiService.post(AuthEndpoint.recovery, params, { preloader: showSpin });
  }

  resetPassword(params: IResetPasswordRequest, showSpin: boolean = false): Observable<any> {
    return this.apiService.post(AuthEndpoint.reset, params, { preloader: showSpin });
  }

}
