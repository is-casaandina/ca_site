import { Pipe, PipeTransform } from '@angular/core';
import { COLOR_STATES, PAGE_CODE_STATES, PAGE_STATES } from '@ca-admin/settings/constants/general.constant';

@Pipe({
  name: 'statusFormatColor'
})
export class StatusFormatColorPipe implements PipeTransform {

  transform(code: number, inEdition: boolean, publish: boolean): string {
    let color;

    // tslint:disable-next-line:prefer-conditional-expression
    if (PAGE_CODE_STATES.inEdition === code && !inEdition && !publish) {
      color = COLOR_STATES.disabled;
    } else if (inEdition) {
      color = COLOR_STATES.warning;
    } else if (!inEdition && publish) {
      color = COLOR_STATES.success;
    } else {
      color = PAGE_STATES.find(item => item.code === code).color;
    }

    return `g-badge-${color}`;
  }

}
