import { Pipe, PipeTransform } from '@angular/core';
import { LANGUAGE_TYPES } from '@ca-admin/settings/constants/general.constant';

@Pipe({
  name: 'languageFormat'
})
export class LanguageFormatPipe implements PipeTransform {

  transform(code: string, separator?: string): string {
    const language = LANGUAGE_TYPES.find(lang => lang.code === code);

    return language && language.description || '';
  }

}
