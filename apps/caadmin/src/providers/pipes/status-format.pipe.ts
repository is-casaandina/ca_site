import { Pipe, PipeTransform } from '@angular/core';
import { PAGE_CODE_STATES, PAGE_STATES, PAGE_STATES_DESCRIPTION } from '@ca-admin/settings/constants/general.constant';

@Pipe({
  name: 'statusFormat'
})
export class StatusFormatPipe implements PipeTransform {

  transform(code: number, inEdition: boolean, publish: boolean): string {
    if (PAGE_CODE_STATES.inEdition === code && !inEdition && !publish) {
      return PAGE_STATES_DESCRIPTION.DRAFT;
    } else if (inEdition) {
      return PAGE_STATES_DESCRIPTION.IN_EDITION;
    } else if (!inEdition && publish) {
      return PAGE_STATES_DESCRIPTION.PUBLISHED;
    }

    return PAGE_STATES.find(item => item.code === code).description;
  }

}
