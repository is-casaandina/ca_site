import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LanguageFormatPipe } from './language-format.pipe';
import { StatusFormatColorPipe } from './status-format-color.pipe';
import { StatusFormatPipe } from './status-format.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LanguageFormatPipe,
    StatusFormatPipe,
    StatusFormatColorPipe
  ],
  exports: [
    LanguageFormatPipe,
    StatusFormatPipe,
    StatusFormatColorPipe
  ],
  providers: [
    LanguageFormatPipe,
    StatusFormatPipe,
    StatusFormatColorPipe
  ]
})
export class CaAdminPipesModule { }
