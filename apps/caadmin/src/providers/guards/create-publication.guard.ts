import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { TRAY_FILTER_DEFINITION } from '@ca-admin/settings/constants/tray.constant';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CreatePublicationGuard implements CanActivate {

  constructor(
    private router: Router,
    private location: Location
  ) { }

  canActivate(next: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const definition = TRAY_FILTER_DEFINITION.find(def => next.data && def.menuCode === next.data.menuCode);

    if (definition && !this.router.url.startsWith('/editor/edit')) {
      return true;
    }

    this.location.back();

    return false;
  }

}
