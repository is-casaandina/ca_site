import { IGeneralPagedResponse, IGeneralSearchRequest } from './general.interface';

export interface ILivingRoomResponse {
  id: string;
  name: string;
  squareMeters: number;
  lengthy: number;
  width: number;
  high: number;
  teather: number;
  school: number;
  banquet: number;
  directory: number;
  cocktail: number;
  hotelId: string;
  hotel: string;
  destinationId: string;
  order: number;
  titleImage1: string;
  titleImage2: string;
  titleImage3: string;
  titleImage4: string;
  images: Array<any>;
}

export interface ILivingRoomRequest {
  name: string;
  squareMeters: number;
  lengthy: number;
  width: number;
  high: number;
  teather: number;
  school: number;
  banquet: number;
  directory: number;
  cocktail: number;
  hotelId: string;
  destinationId: string;
  order: number;
  titleImage1: string;
  titleImage2: string;
  titleImage3: string;
  titleImage4: string;
  images: Array<any>;
}

export interface IRoomPageableRequest extends IGeneralSearchRequest { }

export interface IPagedLivingRoomResponse extends IGeneralPagedResponse<ILivingRoomResponse> { }
