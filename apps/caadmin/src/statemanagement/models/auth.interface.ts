import { IPermissionResponse } from './permission.interface';

export interface IAuthRequest {
  username: string;
  password: string;
  rememberMe: boolean;
}

export interface IAuthResponse {
  accessToken: string;
  tokenType: string;
}

export interface IUserToken {
  username: string;
  rememberMe: boolean;
  id_token: string;
}

export interface IUserPermissionsResponse {
  username: string;
  fullname: string;
  roleCode: string;
  roleName: string;
  permissionList: Array<IPermissionResponse>;
}
