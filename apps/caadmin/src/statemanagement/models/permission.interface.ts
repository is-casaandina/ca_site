export interface IPermissionResponse {
  permissionCode: string;
  permissionName: string;
  url: string;
  status: number;
  updatedDate: string;
  createdDate: string;
}
