import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';
import { IPermissionResponse } from './permission.interface';

export interface IGeneralRole {
  roleCode: string;
  roleName: string;
  status: number;
  disabled?: boolean;
}

export interface IRolePaginationRequest extends IGeneralSearchRequest { }

export interface IRolePermissionsDetailResponse extends IGeneralRole {
  permissionCodeList: Array<IPermissionResponse>;
}

export interface IRolePermissionsDetailView extends IRolePermissionsDetailResponse {
  disabled?: boolean;
  actionType?: string;
}

export interface IRolePaginationResponse extends IGeneralSearchResponse {
  content: Array<IRolePermissionsDetailResponse>;
}

export interface IRolePaginationView extends IGeneralSearchResponse {
  content: Array<IRolePermissionsDetailView>;
}

export interface IRoleResponse extends IGeneralRole {
  permissionCodeList: Array<string>;
}

export interface IUpdateRoleRequest {
  roleName: string;
  permissionCodeList: Array<string>;
}

export interface IDeleteRoleRequest {
  roleCode: string;
}

export interface ISaveRoleRequest {
  roleName: string;
  permissionCodeList: Array<string>;
}
