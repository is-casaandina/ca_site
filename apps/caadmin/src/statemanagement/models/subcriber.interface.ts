import { IGeneralPagedResponse, IGeneralSearchRequest } from './general.interface';

export interface ISubscriberRequest extends IGeneralSearchRequest {
  typeSubscriber: Array<number>;
  startDate: string;
  endDate: string;
}

export interface ISubscriberResponse {
  id: string;
  name: string;
  countryName: string;
  phone: string;
  typeSuscriber: string;
  registerDate: Date;
}

export interface ISubscriberPaginationResponse extends IGeneralPagedResponse<ISubscriberResponse> {
}
