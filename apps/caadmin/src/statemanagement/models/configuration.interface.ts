export interface IConfigurationResponse {
  sitename: string;
  company: string;
  logo: string;
  icon: string;
  copyright: string;
  css: string;
  languages: Array<string>;
  defaultLanguage: string;
  metadata?: IMetadata;
  socialNetworking?: ISocialNetworking;
  script?: IScript;
  roiback?: IRoiback;
  roibackPrice?: IRoibackPrice;
  contact?: IIdentityContact;
  iconsPay: Array<string>;
  keywords: string;
  domain?: string;
  fontIcons?: string;
  homeId?: string;
}

export interface IMetadata {
  title: string;
  description: string;
  keywords: string;
}

export interface ISocialNetworking {
  urlFacebook: string;
  urlTwitter: string;
  urlInstagram: string;
  urlYoutube: string;
  urlFlickr: string;
  urlWhatsapp: string;
}

export interface IScript {
  tracing: string;
  header: string;
  body: string;
  footer: string;
}

export interface IRoiback {
  urlReservationEsp: string;
  urlReservationEng: string;
  urlReservationPor: string;
}

export interface IRoibackPrice {
  urlPricePen: string;
  urlPriceUsd: string;
  urlPriceBrl: string;
}

export interface ISiteIdentityRequest {
  sitename: string;
  description: string;
  iconSite: string;
  keywords: string;
  contact: IIdentityContact;
}

export interface IIdentityContact {
  phoneEs?: string;
  phoneEn?: string;
  phonePt?: string;
  mailsEs?: string;
  mailsEn?: string;
  mailsPt?: string;
  formPage?: string;
}

export interface IRobotsRequest {
  userAgent: string;
  type: string; // Allowed | Disallowed | SiteMap
  value: string;
}

export interface IRobotsResponse {
  id: string;
  userAgent: string;
  type: string; // Allowed | Disallowed | SiteMap
  value: string;
}
