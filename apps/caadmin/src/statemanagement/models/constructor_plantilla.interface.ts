export class ConstrutorPlantilla {
    constructor(
     public code: string,
     public name: string,
     public description: string,
     public categoryId: string,
     public imageUri: string,
     public html: string,
     public refComponents: Array<string>
    ){}
  }
  