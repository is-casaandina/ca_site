export interface IUrlAutocompleteView {
  url: string;
  isExternal: boolean;
}

export interface IUrlAutocompleteRequest {
  value: string;
  language: string;
  limit?: number;
}
