import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';

export interface IAssistanceSearchRequest extends IGeneralSearchRequest {
  typeAssistance: number;
}

export interface IAssistanceRequest {
  iconClass: string;
  englishTitle: string;
  spanishTitle: string;
  portugueseTitle: string;
  type: number;
}

export interface IAssistancePaginatedResponse extends IGeneralSearchResponse {
  content: Array<IAssistanceResponse>;
}

export interface IAssistanceResponse {
  id?: string;
  iconClass?: string;
  englishTitle?: string;
  spanishTitle?: string;
  portugueseTitle?: string;
}
