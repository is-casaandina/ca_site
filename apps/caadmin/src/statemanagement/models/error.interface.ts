export interface IError {
  id?: string;
  pageId?: string;
  errorCode?: number;
}
