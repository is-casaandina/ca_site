export interface ICategoryResponse {
  id: string;
  name: string;
  system: boolean;
}

export interface ICategoryRequest {
  name: string;
  system: boolean;
  viewPage: boolean;
}
