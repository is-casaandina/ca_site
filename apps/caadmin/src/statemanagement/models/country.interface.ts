export interface ICountryResponse {
  id: string;
  name: string;
  code: string;
}
