import { IComponent } from './dynamic-form.interface';
import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';

export interface IComponentPaginationRequest extends IGeneralSearchRequest {
  componentName: string;
  status: Array<number>;
  userList: Array<string>;
}

export interface IComponentItemResponse {
  code: string;
  name: string;
  username: string;
  status: number;
  type: number;
  lastUpdate: string;
  languages: Array<string>;
}

export interface IComponentPaginationResponse extends IGeneralSearchResponse {
  content: Array<IComponentItemResponse>;
}

export interface IComponentResponse extends IComponent { }
