import { CATEGORY } from '@ca-admin/settings/constants/general.constant';

export interface IComponent {
  code: string;
  properties: any;
}

export interface IGroup {
  property: string;
  value: string;
}

export interface IStyle {
  property: string;
  value: string;
  groups: Array<IGroup>;
}

export interface ISection {
  id: string;
  cols: string;
  colsPage: string;
  sectionParent: string;
  level: number;
  styles: Array<IStyle>;
  component: Array<IComponent>;
}

export interface ITemplateResponse {
  id: string;
  categoryId: CATEGORY;
  code: string;
  name: string;
  description: string;
  typeTemplate: string;
  imageUri: string;
  sections: any;
  html: string;
  refComponents: Array<string>;
}
