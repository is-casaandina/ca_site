export interface IGeneralResponse<T> {
  data: T;
  message: string;
}

export interface IGeneralSearchRequest {
  size?: number;
  page?: number;
}

export interface IGeneralSearchResponse {
  totalElements: number;
  totalPages: number;
  first: boolean;
  last: boolean;
}

export interface IGeneralPagedResponse<T> {
  totalElements: number;
  totalPages: number;
  first: boolean;
  last: boolean;
  content: Array<T>;
}

export interface ISearchFormView {
  search: string;
  users: Array<string>;
  statePages: Array<number>;
  calendar: any;
}

export interface IGeneralObjectConstant {
  code?: number;
  description?: string;
  icon?: string;
  color?: string;
  pageStates?: Array<number>;
}

export interface IPageStateResponse {
  code: number;
  value: string;
  text?: string;
}

export interface IPageStateView extends IPageStateResponse {
  checked?: boolean;
}

export interface IMenu {
  code: string;
  name: string;
  route: string;
  icon: string;
  header?: boolean;
}

export interface IMessageResponse {
  message: string;
}

export interface ITypeSubscriberResponse {
  code: number;
  value: string;
}
