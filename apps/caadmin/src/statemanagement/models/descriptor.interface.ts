export interface IDescriptorResponse {
  id: string;
  name: string;
  url: string;
  urlBanner: string;
  typographyStyle: string;
  color: string;
  title: string;
  category: string;
}

export interface IDescriptorRequest {
  name: string;
  url: string;
  urlBanner: string;
  typographyStyle: string;
  color: string;
  title: string;
  category: string;
}
