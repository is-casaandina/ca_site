import { CATEGORY } from '@ca-admin/settings/constants/general.constant';
import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';

export interface ISiteMenuResponse {
  menuCode: string;
  menuName: string;
  subMenu: Array<IPageItemResponse>;
}

export interface ISavePageRequest {
  name: string;
  pageType: number;
  type: number;
  state: number;
  languages: Array<IPageByLanguage>;
  descriptorId?: string;
}

export interface IPageResponse extends ISavePageRequest {
  id?: string;
  username: string;
  publishingDate?: string;
  categoryName?: string;
  publish?: boolean;
  contact?: IContactDetailPage;
}

export interface IContactDetailPage {
  title1: string;
  title2: string;
  languages: Array<IContactLanguageDetail>;
}

export interface IContactLanguageDetail {
  language: string;
  details?: Array<{
    icon: string;
    type: number;
    description: string;
    label: string;
  }>;
}

export interface IPageItemResponse {
  id: string;
  code?: string;
  name?: string;
  parentPath?: string;
  username?: string;
  state?: number;
  type?: number;
  lastUpdate?: string;
  createdDate?: string;
  languages?: Array<string>;
  fullname?: string;
  categoryId?: string;
  categoryName?: string;
  external?: boolean;
  publish?: boolean;
  inEdition?: boolean;
}

export interface IPagePaginationRequest extends IGeneralSearchRequest {
  states: Array<number>;
  userList: Array<string>;
  categories: Array<string>;
  genericType: Array<string>;
  countries: Array<string>;
  destinations: Array<string>;
  startDate: string;
  endDate: string;
  name: string;
  menuCode: string;
}

export interface IPageApprovalPaginationRequest extends IGeneralSearchRequest {
  namePage: string;
  userList: Array<string>;
}

export interface IPagePaginationResponse extends IGeneralSearchResponse {
  content: Array<IPageItemResponse>;
}

export interface IContent {
  html: string;
  refComponents: Array<string>;
  data?: any;
}

export interface IMetadata {
  // title?: string;
  // description?: string;
  // keywords?: string;
  // robots?: string;
  // nositelinksearchbox?: string;
  // nosnippet?: string;
  // autor?: string;
  // subject?: string;
  // language?: string;
  // revisitafter?: string;
  image?: string;
  title?: string;
  titleSnippet?: string;
  description?: string;
  ogSitename?: string;
  ogType?: string;
  ogUrl?: string;
  ogTitle?: string;
  ogDescription?: string;
  ogImage?: string;
  twitterCard?: string;
  twitterTitle?: string;
  twitterDescription?: string;
  twitterImage?: string;
  canonicalLink?: string;
  keywords?: string;
  slug?: string;
}

export interface IPageByLanguage {
  language: string;
  canonical?: string;
  path?: string;
  pagePath?: string;
  parentPath?: string;
  metadata?: IMetadata;
  social?: string;
  title?: string;
  category?: string;
  tags?: Array<string>;
  content?: IContent;
  name?: string;
  data?: any;
}
export interface ISavePageRequest {
  id?: string;
  name: string;
  categoryId: CATEGORY;
  pageType: number;
  type: number;
  state: number;
  inEdition: boolean;
  languages: Array<IPageByLanguage>;
}

export interface ICategoryByLanguage {
  category: string;
  language: string;
  path: string;
}

export interface ICategory {
  id: string;
  languages: Array<ICategoryByLanguage>;
}

export interface IPageHeadeRequest {
  id: string;
  inEdition?: boolean;
}
