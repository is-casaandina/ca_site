import { IGeneralPagedResponse, IGeneralSearchRequest } from './general.interface';

export interface IPageNotificationResponse {
  id: number;
  pageId: string;
  page: string;
  name: string;
  beginDate: Date;
  endDate: Date;
  languages: Array<ILanguagePageNotificationResponse>;
}

export interface ILanguagePageNotificationResponse {
  language: string;
  title: string;
  detail: string;
}

export interface IPageNotificationRequest {
  pageId: string;
  name: string;
  beginDate: Date;
  endDate: Date;
  languages: Array<ILanguagePageNotificationRequest>;
}

export interface ILanguagePageNotificationRequest {
  language: string;
  title: string;
  detail: string;
}

export interface IPageNotificationPageableRequest extends IGeneralSearchRequest { }

export interface IPagedPageNotificationResponse extends IGeneralPagedResponse<IPageNotificationResponse> { }
