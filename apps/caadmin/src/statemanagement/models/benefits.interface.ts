export interface IBenefitsResponse {
  id: string;
  text: string;
  link: ILinkResponse;
  detail: Array<IDetailBenefitResponse>;
}

export interface ILinkResponse {
  url: string;
  isExternal: boolean;
}

export interface IDetailBenefitResponse {
  icon: string;
  titleEsp: string;
  titleEng: string;
  titlePor: string;
}

export interface IBenefitsRequest extends IBenefitsResponse {
}
