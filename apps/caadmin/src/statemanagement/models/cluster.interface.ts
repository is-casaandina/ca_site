import { ICountryResponse } from './country.interface';

export interface IClusterResponse {
  id: string;
  name: string;
  status: string;
  countries?: Array<ICountry>;
}

export interface IClusterRequest {
  name: string;
  status: boolean;
  countries?: Array<ICountryResponse>;
}

export interface ICountry {
  id: string;
  name: string;
  code: string;
  status: boolean;
}
