import { IGeneralSearchRequest, IGeneralSearchResponse } from './general.interface';
import { IRoleResponse } from './role.interface';

export interface IGeneralUser {
  username: string;
  fullname: string;
  name: string;
  lastname: string;
  id: string;
}

export interface IUserPaginationRequest extends IGeneralSearchRequest {
  rolesCode: Array<string>;
}

export interface IUserResponse extends IGeneralUser {
  email: string;
  status: number;
  role: IRoleResponse;
}

export interface IUserView extends IUserResponse {
  disabled?: boolean;
  actionType?: string;
  checked?: boolean;
}

export interface IUserPaginationResponse extends IGeneralSearchResponse {
  content: Array<IUserResponse>;
}

export interface IUserPaginationView extends IGeneralSearchResponse {
  content: Array<IUserView>;
}

export interface ISaveUserRequest extends IGeneralUser {
  roleCode: string;
  email: string;
  password?: string;
}

export interface IDeleteUserRequest {
  userId: string;
}

export interface ICaUserResponse extends IGeneralUser {
  firstName: string;
  lastName: string;
  email: string;
  createdDate: string;
  updatedDate: string;
}

export interface IProfileResponse {
  username: string;
  fullname: string;
  name: string;
  lastname: string;
  email: string;
  role: IRoleResponse;
}

export interface IProfileRequest {
  fullname: string;
  lastname: string;
  email: string;
}

export interface IPasswordChangeRequest {
  currentPassword: string;
  password: string;
  confirmPassword: string;
}
