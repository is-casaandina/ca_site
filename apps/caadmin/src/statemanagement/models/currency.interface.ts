export interface ICurrencyResponse {
  name: string;
  symbol: string;
  value: number;
  codeIso: string;
  isPrincipal: string;
}

export interface ICurrencyRequest extends ICurrencyResponse { }
